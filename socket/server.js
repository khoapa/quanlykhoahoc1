var app = require("express")();
var server = require("http").Server(app);
var io = require("socket.io")(server);
var redis = require("redis");
require('dotenv').config();

var serverPort = process.env.SERVER_SOCKET_POST;
// CORS Issue Fix

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

server.listen(serverPort, function () {
    console.log("Server starting with port " + process.env.SERVER_SOCKET_POST);
});

io.on("connection", function (socket) {
    console.log("New client connected");
    var redisClient = redis.createClient();
    redisClient.subscribe("reading-notify");
    var chatClient = redis.createClient();
    chatClient.psubscribe(`conversation-*`);

    redisClient.on("message", function (channel, message) {
        socket.emit(channel, message);
    });

    chatClient.on('pmessage', function(pattern, channel, message) {
        socket.emit(channel, message);
    });

    var listChatClient = redis.createClient(); 

    listChatClient.psubscribe(`list-conversation-*`);

    listChatClient.on('pmessage', function(pattern, channel, message) {
        socket.emit(channel, message);
    });

    socket.on("disconnect", function () {
        redisClient.quit();
        chatClient.quit();
        listChatClient.quit();
    });

    

    redisClient.on("connect", function () {
        console.log("Redis client connected");
    });

    redisClient.on("error", function (err) {
        console.log("Something went wrong " + err);
    });

    listChatClient.on("error", function (err) {
        console.log("Something went wrong " + err);
    });
});
