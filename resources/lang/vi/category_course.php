<?php 
return [
    'create_success' => 'Thêm nhóm kỹ năng sống thành công',
    'delete_success' => 'Xóa nhóm kỹ năng sống thành công.',
    'update_success' => 'Cập nhập nhóm kỹ năng sống thành công.',
    'create_fail' => 'Thêm nhóm kỹ năng sống thất bại',
    'delete_fail' => 'Xóa nhóm kỹ năng sống thất bại.',
    'update_fail' => 'Cập nhập nhóm kỹ năng sống thất bại.',
];