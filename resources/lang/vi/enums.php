<?php

use App\Enums\U;
use App\Enums\UserTypeEnum;

return [

    UserTypeEnum::class => [
        UserTypeEnum::STUDENT => 'Học sinh',
        UserTypeEnum::TEACHER => 'Giáo viên',
        UserTypeEnum::STAFF => 'Nhân viên quản lý',
    ],

];