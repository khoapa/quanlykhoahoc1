<?php
return [
    'create_success' => 'Thêm kỹ năng sống thực hành thành công',
    'delete_success' => 'Xóa kỹ năng sống thực hành thành công.',
    'update_success' => 'Cập nhập kỹ năng sống thực hành thành công.',
    'create_fail' => 'Thêm kỹ năng sống thực hành thất bại',
    'delete_fail' => 'Xóa kỹ năng sống thực hành thất bại.',
    'update_fail' => 'Cập nhập kỹ năng sống thực hành thất bại.',
];