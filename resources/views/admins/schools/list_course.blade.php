@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh sách khóa học của trường {{$school->name}}
                    <input type="hidden" id="school_id" value="{{$school->id}}">
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                <a href="{{ route('course.create') }}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm Mới Khóa Học</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="col mb-6">
            <div class="row">
                <div class="form-group">
                    <input type="search" id="search_name" value="<?php echo request()->get('search') ?>" class="form-control" name="search" id="search_name" placeholder="Tên trường học" aria-controls="example">
                </div>
                <div class="form-group ml-2">
                    <button class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Hình ảnh</th>
                            <th>Tên</th>
                            <th>Tên danh mục</th>
                            <th>Thời gian bắt đầu</th>
                            <th>
                                <input value="1" id="selectAllCourseOfSchool" @if($statusAllCourse==1) checked @endif type="checkbox">
                                Hiển thị</th>
                            <th>Người tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($courseOfSchool as $key=>$course)
                        <tr>
                            <td>{!! Helper::indexPaginate($courseOfSchool,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$course->cover_image}}" alt="">
                            </td>
                            <td>{{$course->name}}</td>
                            <td>{{$course->category->name}}</td>
                            <td>{{date("d-m-Y", strtotime($course->start_date))}}</td>
                            <td>
                                <label class="pd-toggle-switch">
                                    @if($course->status == 1)
                                    <input type="checkbox" name='status[]' class="check" checked onclick="addStatusCourseFunction('0','{{$course->id}}')" value="{{$course->id}}">
                                    @else
                                    <input type="checkbox" name='status[]' class="check" onclick="addStatusCourseFunction('1','{{$course->id}}')" value="{{$course->id}}">
                                    @endif
                                </label>
                            </td>
                            <td>{{$course->authorUser->name}}</td>
                            <td>
                                <div class="w-100">

                                    <a class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('course.destroy',$course->id)}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <form action="{{route('question.question.index',$course->id)}}" method="GET">
                                        <button class="mr-1 pull-right mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-success" title="Xem chi tiết">
                                            <span class="pe-7s-menu" aria-hidden="true"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $courseOfSchool->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection