<div class="form-row">
    <div class="col mb-6">
        <div class="form-group {{ $errors->has('name') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="name">Tên trường (*)</label>
            <input type="text" class="form-control" maxlength="255" id="name" name="name"
                value="{{ old('name', optional(optional($school))->name) }}" placeholder="Tên trường" required>
            {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            <label for="address">Địa chỉ (*)</label>
            <input type="text" class="form-control" id="address" name="address" maxlength="255"
                value="{{ old('address', optional(optional($school))->address) }}" placeholder="Địa chỉ" required>
            {!! $errors->first('address', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            <label for="grade_ids">Khối </label>
            <?php $arrGrades = old('grade_ids');
            if($errors->any()){
                $gradesOfSchool = null;
            }   ?>
            <select id="grade_ids" class="custom-select {{ $errors->has('grade_ids') ? 'is-invalid' : '' }} grade_ids"
                name="grade_ids[]" multiple="multiple">
                @foreach($grades as $grade)
                <option @if(!is_null($arrGrades)) @if(in_array($grade->id,$arrGrades))
                    selected="selected"
                    @endif
                    @endif
                    @if(!is_null($gradesOfSchool))
                    @if(in_array($grade->id,$gradesOfSchool))
                    selected="selected"
                    @endif
                    @endif
                    value="{{$grade->id}}">{{$grade->name}}</option>
                @endforeach

            </select>
            {!! $errors->first('grade_ids', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>

    <div class="col mb-6">
        <div
            class="form-group {{ $errors->has('school_code') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="email">Mã trường (*)</label>
            <input type="text" class="form-control" id="school_code" name="school_code" maxlength="255"
                value="{{ old('school_code', optional(optional($school))->school_code) }}" placeholder="Mã trường"
                required>
            {!! $errors->first('school_code', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('email') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="email">Email (*)</label>
            <input type="email" class="form-control" id="email" name="email" maxlength="255"
                value="{{ old('email', optional(optional($school))->email) }}" placeholder="Email" required>
            {!! $errors->first('email', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('phone') ? 'was-validated-custom' : '' }}">
            <label for="phone">Số điện thoại (*)</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">+84</span>
                </div>
                <input aria-describedby="basic-addon1" class="form-control" name="phone" type="text" id="phone"
                    value="{{ old('phone', optional(optional($school))->phone) }}" placeholder="Số điện thoại" required>
                {!! $errors->first('phone', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="description">Mô tả</label>
    <div>
        <textarea type="text" class="form-control" id="description" name="description"
            placeholder="Mô tả">{{ old('description', optional(optional($school))->description) }}</textarea>
    </div>
</div>
<div class="form-group {{ $errors->has('logo') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
    <label for="logo">Logo trường học</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('logo', optional($school)->logo), 'logo') !!}
    </div>
    {!! $errors->first('logo', '<p class="invalid-feedback">:message</p>') !!}
</div>
<br>
<script type="text/javascript">
$(document).ready(function() {
    $('.grade_ids').select2({});
});
</script>