@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Danh sách User của trường {{$school->name}}</div>
                <input type="hidden" id="school_id" value="{{$school->id}}">
            </div>
            <div class="page-title-actions">
                <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                <a href="{{ route('user.create') }}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới User</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="col mb-6">
            <div class="row">
                <div class="form-group">
                    <input type="search" value="<?php echo request()->get('search') ?>" class="form-control" name="search" id="search_name" placeholder="Tên user" aria-controls="example">
                </div>
                <div class="form-group ml-2">
                    <button class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Ảnh</th>
                            <th>Họ Tên</th>
                            <th>Trường</th>
                            <th>Lớp</th>
                            <th>Loại User</th>
                            <th>
                                <input value="1" id="selectAllUserOfSchool"  @if($statusAllUser==1) checked @endif type="checkbox">
                                Trạng Thái
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        @php
                        $date=date_create($user->birthday);
                        @endphp
                        <tr role="row" class="even">
                            <td>{!! Helper::indexPaginate($users,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$user->avatar}}" alt="">
                            </td>
                            <td class="sorting_1 dtr-control">{{$user->name}}</td>
                            <td>{{!is_null($user->school) ? $user->school->name : ''}}</td>
                            <td>{{!is_null($user->classes) ? $user->classes->name : ''}}</td>
                            <td>@if($user->user_type == 1 )
                                Học sinh
                                @else
                                Giáo viên
                                @endif
                            </td>
                            <td>
                                <label class="pd-toggle-switch">
                                    @if($user->status == 1)
                                    <input type="checkbox" name="status[]" class="check" checked onclick="addStatusUserFunction('2','{{$user->id}}')" value="{{$user->id}}">
                                    @else
                                    <input type="checkbox" name="status[]" class="check" onclick="addStatusUserFunction('1','{{$user->id}}')" value="{{$user->id}}">
                                    @endif
                                </label>
                            </td>
                            <td>
                                <div class=" w-100">
                                    <a href="{{route('user.destroy',['id'=>$user->id])}}" type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{route('user.edit',['id'=>$user->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                        <span class="pe-7s-tools" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $users->links()}}
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection