@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh Sách Trường Học
                </div>
            </div>
            <div class="page-title-actions">
                <form name="form-import" action="{{ route('user.import')}}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <div class="input-group mb-3">
                        <div class="custom-file form-group {{ $errors->has('role_id') ? 'was-validated ' : '' }}">
                            <input type="file" class="custom-file-input" id="inputGroupFile02" name="import">
                            <label class="custom-file-label" for="inputGroupFile02">Chọn file excel</label>
                            {!! $errors->first('import', '<p class="invalid-feedback">:message</p>') !!}
                        </div>
                        <button class="btn-icon btn btn-success"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                            </i>Import</button>
                        <input type="hidden" name="type_import" value="1">
                    </div>
                    <p>Tải file mẫu : <a href="{{ asset('admins/assets/images/SCHOOL_LIST.xlsx') }}" download="">Tại
                            đây</a></p>
                </form>
                <div class="page-title-actions">
                    <a href="{{ route('school_create') }}" class="btn-icon btn btn-success float-right" href=""><i
                            class="mr-1 pe-7s-plus btn-icon-wrapper">
                        </i>Thêm mới trường học</a>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->first('import'))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok"></span>
        {{ $errors->first('import') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('error') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('data'))
    <?php
    $dataResponse = Session::get('data');
    $countError = $dataResponse['numberError'];
    // var_dump($dataResponse['response']);
    $countImport = $dataResponse['total'] -  $countError;
    ?>
    <div class="alert alert-success">
        <p class="glyphicon glyphicon-ok">Số lượng import : {{$countImport}}</p>
        <p class="glyphicon glyphicon-ok">Số lượng lỗi : {{$countError}}
        </p>
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="col mb-6">
            <div class="row">
                <div class="form-group">
                    <input type="search" id="search_name" value="<?php echo request()->get('search') ?>"
                        class="form-control" name="search" placeholder="Tên trường học" aria-controls="example">
                </div>
                <div class="form-group ml-2">
                    <button class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
                <div class="col mb-3">
                    <div id="export-click-school" class="mt-0 btn-icon btn btn-success export export-sata"><i
                            class="fa fa-download pr-1" aria-hidden="true"></i>Export</div>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <table class="table table-striped ">
                <thead>
                    <tr role="row">
                        <th>STT</th>
                        <th>Tên trường</th>
                        <th>Mã trường</th>
                        <th>Địa chỉ</th>
                        <th>Số điện thoại</th>
                        <th>
                            <input value="1" id="selectAllSchool" @if($statusAllSchool==1) checked @endif
                                type="checkbox">
                            Trạng thái
                        </th>
                        <th id="th_table_school"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($schools as $row)
                    <tr role="row" class="odd">
                        <td>{!! Helper::indexPaginate($schools,$loop) !!}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->school_code}}</td>
                        <td>{{$row->address}}</td>
                        <td>{{$row->phone}}</td>
                        <td>
                            <label class="pd-toggle-switch">
                                @if($row->status == 1)
                                <input type="checkbox" name='status[]' class="check" checked
                                    onclick="addStatusSchoolFunction('2','{{$row->id}}')" value="{{$row->id}}">
                                @else
                                <input type="checkbox" name='status[]' class="check"
                                    onclick="addStatusSchoolFunction('1','{{$row->id}}')" value="{{$row->id}}">
                                @endif
                            </label>
                        </td>
                        <td>
                            <div class=" w-100">
                                <div class="btn-group">
                                    <button type="button"
                                        class="mb-2 btn-icon btn-pill btn btn-outline-success dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-center">
                                        <form action="{{route('list_grade',['id'=>$row->id])}}" method="GET"
                                            class="dropdown-item">
                                            <input type="hidden" name="url" value="{{url()->full()}}">
                                            <button class="dropdown-item">Danh sách khối học</button>
                                        </form>
                                        <form action="{{route('list_class',['id'=>$row->id])}}" method="GET"
                                            class="dropdown-item">
                                            <button class="dropdown-item">Danh sách lớp học</button>
                                            <input type="hidden" name="url" value="{{url()->full()}}">
                                        </form>
                                        <form action="{{route('list_user',['id'=>$row->id])}}" method="GET"
                                            class="dropdown-item">
                                            <button class="dropdown-item">Danh sách user</button>
                                            <input type="hidden" name="url" value="{{url()->full()}}">
                                        </form>
                                        <form action="{{route('list_course',['id'=>$row->id])}}" method="GET"
                                            class="dropdown-item">
                                            <button class="dropdown-item">Danh sách khóa học</button>
                                            <input type="hidden" name="url" value="{{url()->full()}}">
                                        </form>
                                    </div>
                                </div>
                                <a type="submit"
                                    class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning "
                                    href="{{route('school_delete',['id'=>$row->id])}}" title="Xóa"
                                    onclick="return confirm('Bạn muốn xóa ?')">
                                    <span class="pe-7s-trash" aria-hidden="true"></span>
                                </a>
                                <a href="{{route('school_edit',['id'=>$row->id])}}"
                                    class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary"
                                    title="Sửa">
                                    <span class="pe-7s-tools" aria-hidden="true"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
            </table>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $schools->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection