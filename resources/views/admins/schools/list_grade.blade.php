@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh sách khối học của trường {{$school->name}}
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Ảnh khối học</th>
                            <th>Tên khối học</th>
                            <th>Mã khối học</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        @foreach($gradeOfSchool as $key=>$grade)
                        <tr role="row" class="odd">
                            <td>{{$key+1}}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$grade->thumbnail}}" alt="">
                            </td>
                            <td>{{$grade->name}}</td>
                            <td>{{$grade->grade_code}}</td>
                        </tr>
                        @endforeach               
                    </tbody>
                </table>
            </div>
        </div>
       
    </div>
</div>
@endsection