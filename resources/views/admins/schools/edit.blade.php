@extends('admin')
@section('title','Create School')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Cập Nhật Thông Tin Trường Học</h5>
            </div>
        </div>
    </div>
</div>
<div class="main-card card main-card-custom">
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card-body">
        <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('school_update',['id'=>$school->id])}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{csrf_field()}}
            @include ('admins.schools.form', [
            'school' => $school,
            'grades' => $grades,
            'gradesOfSchool'=> $gradesOfSchool,
            ])
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Cập nhật</button>
                    <a href="{{ route('school.reset_password',['id'=>$school->id]) }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-info" title="Reset password" onclick="return confirm('Bạn có muốn reset mật khẩu ?')"><i class="pe-7s-check btn-icon-wrapper"> </i>Reset mật khẩu</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection