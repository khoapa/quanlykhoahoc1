@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh sách lớp học của trường {{$school->name}}
                    <input type="hidden" id="school_id" value="{{$school->id}}">
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                <a href="{{route('class_create')}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới lớp học</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="col mb-6">
            <div class="row">
                <div class="form-group">
                    <input type="search" value="<?php echo request()->get('search') ?>" class="form-control" name="search" id="search_name" placeholder="Tên lớp học" aria-controls="example">
                </div>
                <div class="form-group ml-2">
                    <button class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div class="card mb-3">
        <div class="main-card card">
            <div class="panel-body panel-body-with-table">
                <div class="table-responsive">
                    <table class="table table-striped ">
                        <thead>
                            <tr role="row">
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tên lớp</th>
                                <th>Mã lớp</th>
                                <th>Khối</th>
                                <th>Trường</th>
                                <th>
                                    <input value="1" id="selectAllClassOfSchool" @if($statusAllClass==1) checked @endif type="checkbox">
                                    Trạng Thái
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classes as $row)
                            <tr role="row" class="odd">
                                <td>{!! Helper::indexPaginate($classes,$loop) !!}</td>
                                <td class="image-cate">
                                    <img class="rounded-circle" src="{{$row->thumbnail}}" alt="">
                                </td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->class_code}}</td>
                                <td>{{optional($row->grade)->name}}</td>
                                <td>{{optional($row->school)->name}}</td>
                                <td>
                                    <label class="pd-toggle-switch">
                                        @if($row->status == 1)
                                        <input type="checkbox" name='status[]' class="check" checked onclick="addStatusClassFunction('2','{{$row->id}}')" value="{{$row->id}}">
                                        @else
                                        <input type="checkbox" name='status[]' class="check" onclick="addStatusClassFunction('1','{{$row->id}}')" value="{{$row->id}}">
                                        @endif
                                    </label>
                                </td>
                                <td>
                                    <div class=" w-100">
                                        <a type="submit" class="pull-right mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('class_delete',['id'=>$row->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                            <span class="pe-7s-trash" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{route('class_edit',['id'=>$row->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </a>
                                        <form action="{{route('list_student',[$row->id,optional($row->school)->id])}}" method="GET" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <button type="submit" class="mr-1 pull-right mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-success" title="Danh sách học sinh">
                                                <span class="pe-7s-menu" aria-hidden="true"></span>
                                            </button>
                                            <input type="hidden" name="url" value="{{url()->full()}}">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <div class="col-12">
                    <div class="pagination pull-right">
                        {{ $classes->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection