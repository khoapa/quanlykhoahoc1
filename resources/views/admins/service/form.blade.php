<div class="form-row mb-2">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('link') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="address">Link (*)</label>
            <div>
                <input type="text" class="form-control" value="{{ old('link', optional(optional($service))->link) }}"
                    required name="link" >
                {!! $errors->first('link', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('user') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="address">User (*)</label>
            <div>
                <input type="text" class="form-control" value="{{ old('user', optional(optional($service))->user) }}"
                    required name="user">
                {!! $errors->first('user', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="form-row mb-2">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('pass') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="address">Pass (*)</label>
            <div>
                <input type="text" class="form-control" value="{{ old('pass', optional(optional($service))->pass) }}"
                    required name="pass" >
                {!! $errors->first('pass', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('key') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="address">Key (*)</label>
            <div>
                <input type="text" class="form-control" value="{{ old('key', optional(optional($service))->key) }}"
                    required name="key" >
                {!! $errors->first('key', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
</div>