@extends('admin')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Cập Nhật Web Service</h5>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {!! session('message') !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="main-card card main-card-custom">
    <div class="card-body">
        <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('service.update',['id'=>$service->id])}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form">
            {{ csrf_field() }}
            @include ('admins.service.form',[
            ])
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Cập nhập</button>
                </div>
            </div>

        </form>

    </div>
</div>
@endsection