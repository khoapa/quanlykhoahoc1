<div class="form-row ml-3 mr-3">
    <div class="col-md-6 mb-6 mt-3">
        <div class="form-group">
            <label for="role_id" class="control-label">Phân quyền mẫu</label>
            <select class="custom-select {{ $errors->has('role_id') ? 'is-invalid' : '' }}" name="role_form_id"
                id="role_form_id">
                <option value="">--Chọn phân quyền mẫu--</option>
                @foreach($rolesForm as $roleForm)
                <option @if($roleForm->id == $role_id))
                    selected = "selected"
                    @endif
                    value="{{$roleForm->id}}">{{$roleForm->description}}</option>
                @endforeach
            </select>
            {!! $errors->first('description', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <?php
    if(is_null($description)){
        $description = old('description', optional(optional($role))->description);
    } 
    ?>
    <div class="col-md-6 mb-6 mt-3">
        <div
            class="form-group @if(isset($check)) was-validated-custom  @endif {{ $errors->has('description') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="description">Tên nhóm quyền (*)</label>
            <input type="text" class="form-control" id="description" maxlength="255" name="description"
                value="{{$description}}" placeholder="Tên nhóm quyền"
                required>
            {!! $errors->first('description', '<p class="invalid-feedback">:message</p>') !!}
            @if(isset($check))
            <p style="display: block;" class="invalid-feedback">Tên nhóm quyền là trường bắt buộc.</p>
            @endif
        </div>
    </div>

</div>
<div class="form-row">
    <div class="col-lg-12 mt-3 ml-3">
        <div class="form-group project-role">
            <label>Quyền người dùng</label>
            <div>
                <input type="checkbox" value="0" id="allcheck" onClick="toggle(this)" />
                <label class="ml-2">Tất cả</label>
            </div>
        </div>
    </div>

    <?php $i = 1; ?>
    @foreach($permissions as $key=>$permission)
    <?php $i++; ?>
    <div class="col-md-3 ml-3 mt-2">
        <p class="text-role">{{$key}}</p>
        <?php
        $k = 0;
        $j = 0;
        foreach($permission as $p){ 
                foreach($roles as $role){
                    if($role->pivot->permission_id == $p->id){
                        $j+=1;
                    }
                } 
            }
            if($permission->count() == $j){
                $k = 1;
            }
        ?>

        <input type="checkbox" class="allcheck" value="<?php if($k==1){echo 1;}else{echo 0;} ?>" <?php if($k==1){echo "checked";} ?> id="all-{{$i}}" onclick="checkAll(<?php echo $i ?>)" />
        <label class="ml-2">Tất cả</label>
        @foreach($permission as $p)
        <div>
            <input multiple @foreach($roles as $role) @if($role->pivot->permission_id == $p->id)
            checked
            @endif
            @endforeach
            class="all-{{$i}} allcheck"
            id="{{$p->id}}"
            type="checkbox" name="permision[]" value="{{$p->id}}"/>

            <label class="ml-2 mt-auto description-permission">{{$p->description}}</label>

        </div>
        @endforeach
    </div>
    @endforeach
</div>