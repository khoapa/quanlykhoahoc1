@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Chi Tiết Phân Quyền ({{$role->description}})
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="main-card card">
        <form id ="haha" enctype="multipart/form-data" method="POST" novalidate=""
            action="{{route('permission.permission.update',$id)}}" accept-charset="UTF-8" id="create_exhibition_form"
            name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{csrf_field()}}
            @include ('admins.permissions.form', [
            'permissions' => $permissions,
            'roles' => $roles,
            'id' => $id,
            'role' => $role
            ])
            <div class="form-group mt-2">
                <div class="col-md-offset-2 col-md-10">
                    <a href="{{ route('permission.permission.index') }}"
                        class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i
                            class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i
                            class="pe-7s-check btn-icon-wrapper"> </i>Cập nhập</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
<script language="JavaScript">
function toggle(source) {
    if ($('#allcheck').val() == 0) {
        $('.allcheck').prop('checked', true);
        $('#allcheck').prop('checked', true);
        $('#allcheck').val(1);
    } else {
        $('.allcheck').prop('checked', false);
        $('#allcheck').prop('checked', false);
        $('#allcheck').val(0);
    }
}
</script>