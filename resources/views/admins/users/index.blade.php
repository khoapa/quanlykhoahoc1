@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Danh Sách User</div>
            </div>
            <div class="page-title-actions">
                <form name="form-import" action="{{ route('user.import')}}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <div class="input-group mb-3">
                        <div class="custom-file form-group {{ $errors->has('role_id') ? 'was-validated ' : '' }}">
                            <input type="file" class="custom-file-input" id="inputGroupFile02" name="import">
                            <label class="custom-file-label" for="inputGroupFile02">Chọn file excel</label>
                        </div>
                        <button class="btn-icon btn btn-success"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                            </i>Import</button>
                        <input type="hidden" name="type_import" value="2">
                    </div>
                    <p>Tải file mẫu : <a href="{{ asset('admins/assets/images/USER_LIST.xlsx') }}" download="">Tại đây</a></p>
                </form>
                <div class="page-title-actions">
                    <a href="{{ route('user.create') }}" class="btn-icon btn btn-success float-right" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                        </i>Thêm mới User</a>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->first('import'))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok"></span>
        {{ $errors->first('import') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif  
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('error') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('data'))
    <?php
    $dataResponse = Session::get('data');
    $countError = $dataResponse['numberError'];
    // var_dump($dataResponse['response']);
    $countImport = $dataResponse['total'] - $countError;

    ?>
    <div class="alert alert-success">
        <p class="glyphicon glyphicon-ok">Số lượng import : {{$countImport}}</p>
        <p class="glyphicon glyphicon-ok">Số lượng lỗi : {{$countError}}
        </p>
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3">
                <input type="search" id="search_name" value="<?php echo request()->get('search') ?>" class="form-control search-input" name="search" placeholder="Tên user" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
            <?php
            $user = auth()->user();
            ?>
            @hasanyrole('Admin')
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="search_school" name="school_id">
                        <option value="">--Tất cả trường--</option>
                        @foreach($schools as $school)
                        <option @if($school->id == request()->school_id)
                            selected = "selected"
                            @endif
                            value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endhasrole
            @hasanyrole('Admin|school')
            <div class="col-md-2">
                <div class="form-group">
                    <select class="custom-select" id="search_class" name="class_id">
                        <option value="">--Tất cả lớp--</option>
                        @if(!is_null($classes))
                        @foreach($classes as $class)
                        <option @if($class->id == request()->class_id)
                            selected = "selected"
                            @endif
                            value="{{$class->id}}">{{$class->name}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <select class="custom-select" id="user_type_search" name="user_type">
                        <option value="">--Loại User--</option>
                        <?php $user_type =  request()->get('user_type'); ?>
                        <option value="1" {!! Helper::setSelected($user_type, 1) !!}>Học sinh</option>
                        <option value="2" {!! Helper::setSelected($user_type, 2) !!}>Giáo viên</option>

                    </select>
                </div>
            </div>
            @endhasrole
            <input type="hidden" name="type_user" value="3" id="type">
            <span id="export-click-all-user" class="mb-1 btn-icon btn btn-success export"><i class="fa fa-download pr-1" aria-hidden="true"></i>Export</span>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Ảnh</th>
                            <th>Họ Tên</th>
                            <th>Trường</th>
                            <th>Lớp</th>
                            <th>Loại User</th>
                            @can('edit_user')
                            <th>
                                <input value="1" id="selectAll" @if($statusAllUser==1) checked @endif type="checkbox">
                                Trạng Thái
                            </th>
                            @endcan
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        @php
                        $date=date_create($user->birthday);
                        @endphp
                        <tr role="row" class="even">
                            <td>{!! Helper::indexPaginate($users,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$user->avatar}}" alt="">
                            </td>
                            <td class="sorting_1 dtr-control">{{$user->name}}</td>
                            <td>{{!is_null($user->school) ? $user->school->name : ''}}</td>
                            <td>{{!is_null($user->classes) ? $user->classes->name : ''}}</td>
                            <td>@if($user->user_type == 1 )
                                Học sinh
                                @else
                                Giáo viên
                                @endif
                            </td>
                            @can('edit_user')
                            <td>
                                <label class="pd-toggle-switch">
                                    @if($user->status == 1)
                                    <input type="checkbox" name='status[]' class="check" id='check_all' checked onclick="addStatusUserFunction('2','{{$user->id}}')" value="{{$user->id}}">
                                    @else
                                    <input type="checkbox" name='status[]' class="check" id='check_all' onclick="addStatusUserFunction('1','{{$user->id}}')" value="{{$user->id}}">
                                    @endif
                                </label>
                            </td>
                            @endcan
                            <td>
                                <div class=" w-100">
                                    <a href="{{route('user.destroy',['id'=>$user->id])}}" type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <form action="{{route('user.edit',['id'=>$user->id])}}" method="GET">
                                        <input type="hidden" name="url" value="{{url()->full()}}">
                                        <button class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $users->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection