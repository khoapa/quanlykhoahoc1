@extends('admin')
@section('content')

<div class="card mb-3">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i
                class="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>DANH SÁCH CHỨC VỤ
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <table style="width: 100%;" id="example"
                    class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid"
                    aria-describedby="example_info">
                    <thead>
                        <tr role="row">
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1">STT
                            </th>
                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1">Tên chức vụ
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($positions as $position)
                        <tr role="row" class="odd">
                            <td>{!! Helper::indexPaginate($positions,$loop) !!}</td>
                            <td>{{$position->name}}</td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
        <div>
            {{ $positions->links()}}
        </div>
        @endsection