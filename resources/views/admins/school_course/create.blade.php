@extends('admin')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Thêm Mới Khóa Học Cho Trường: {{$school->name}}</h5>
            </div>
        </div>
    </div>
    
</div>
<div class="main-card-custom mb-1">
<form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3 ">
                <input type="search" value="<?php echo request()->get('search') ?>" class="form-control search-input" id="search_name" name="search" placeholder="Tên khóa học" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</div>
<div class="main-card card main-card-custom">

        <form enctype="multipart/form-data"  method="POST" novalidate="" action="{{route('school_course.store',['id'=>$school->id])}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" >
            {{ csrf_field() }}
            @include ('admins.school_course.form', [
                'schoolCourses'=> null
            ])
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Quay lại</a>
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Thêm mới</button>
                </div>
            </div>

        </form>

        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $courses->links()}}
                </div>
            </div>
        </div>
</div>
@endsection