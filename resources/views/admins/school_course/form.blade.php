<div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">

                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Tên khóa học</th>
                            <th>Danh muc</th>
                            <th>
                                <input value="1" id="selectAllSchoolCourse" type="checkbox">
                                Chọn tất cả khóa học trên trang</th>
                            <!-- <th>Tất cả lớp</th> -->

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($courses as $key => $valueCourses)
                        <tr role="row">
                            <td>{{$key+1}}</td>
                            <td>{{$valueCourses->name}}</td>
                            <td>{{$valueCourses->category->name}}</td>
                            <td>
                                <label class="pd-toggle-switch">
                                    <input type="checkbox" class="checkSchoolCourse" name="course_id[]" value="{{$valueCourses->id}}">
                                </label>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<br>