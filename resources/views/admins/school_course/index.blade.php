@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Chương Trình Học <br>
                    <h8 class="text">Danh sách khóa học đang hiển thị trên ứng dụng</h8>
                </div>
            </div>
            @if(!is_null($school_id) && is_null($classes))
            <div class="page-title-actions">
                <a href="{{route('school_course.create',['id'=>$school_id])}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm Khóa Học Trường</a>
            </div>
            @endif
            <!-- @if(!is_null($class_id))
            <div class="page-title-actions">
                <a href="{{route('school_course.createClass',['id'=>$class_id])}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm Khóa Học Lớp</a>
            </div>
            @endif -->
        </div>
    </div>
    @if(Session::has('message') )
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('school_id'))
    <?php $school_id = session('school_id') ?>
    @endif
    <div class="col-md-12 pl-0">
        <div class="mb-3 card">
            <div class="card-body">
                <div class="tab-content">
                    <form enctype="multipart/form-data" method="get" novalidate="" action="{{route('school_course.index') }}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="col-md-1">
                                <h6 class="timeline-title">Chọn:</h6>
                            </div>
                            @hasanyrole('Admin')
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="school_id" id="school_id1" class="custom-select">
                                        <option value="">--Tất cả trường--</option>
                                        @foreach($listSchools as $school)
                                        @if($school_id == $school->id )
                                        <option value="{{$school->id}}" selected>{{$school->name}}</option>
                                        @else
                                        <option value="{{$school->id}}">{{$school->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endhasrole
                            @hasanyrole('Admin|school')
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="class_id" id="class_id" class="custom-select">
                                        <option value="">--Tất cả lớp--</option>
                                        @if(!is_null($listClasses))
                                        @foreach($listClasses as $value)
                                        @if($class_id == $value->id)
                                        <option selected value="{{$value->id}}">{{$value->name}}</option>
                                        @else
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            @endhasrole
                        </div>
                        <div class="form-row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-3">
                                @php
                                $date_begin = request()->get('date_begin');
                                if(!is_null($errorDate) && $check == 1){
                                $date_begin = null;
                                }
                                @endphp
                                <input autocomplete="off" class="form-control" name="date_begin" type="text" id="datepicker-begin" placeholder="Chọn ngày bắt đầu" value="{!! Helper::formatDateNew($date_begin) !!}" maxlength="255">
                            </div>
                            <div class="col-md-3">
                                @php
                                $date_end = request()->get('date_end');
                                if(!is_null($errorDate) && $check == 1){
                                $date_end = null;
                                }
                                @endphp
                                <input autocomplete="off" class="form-control" name="date_end" type="text" id="datepicker-end" placeholder="Chọn ngày kết thúc" value="{!! Helper::formatDateNew($date_end) !!}" maxlength="255">

                                @if(!is_null($errorDate) && $check != 1)
                                <p class="invalid-feedback check-valid">{{$errorDate}}</p>
                                @endif
                            </div>
                            <div class="col-mb-3">
                                <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Xem</button>
                                @if(isset($backUrl))
                                <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                                @endif
                            </div>
                            <div class="col mb-3">
                                <div id="export-click-school-course" class="mt-0 btn-icon btn btn-success export export-sata"><i class="fa fa-download pr-1" aria-hidden="true"></i>Export</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Tên khóa học</th>
                            <th>Danh mục</th>
                            <th>Số học sinh đang tham gia</th>
                            <th>Số học sinh đã tham gia</th>
                            <th>Tổng lượt truy cập</th>

                        </tr>
                    </thead>
                    <tbody>
                        @if(is_null($schools) && is_null($classes))
                        @foreach($listCourses as $key=>$valueCourse)
                        <tr role="row" class="even">
                            <td>{!! Helper::indexPaginate($listCourses,$loop) !!}</td>
                            <td>{{$valueCourse->name}}</td>
                            <td>{{$valueCourse->category->name}}</td>
                            <td>{{$valueCourse->getCountUserCountAll($dateBegin,$dateEnd)}}</td>
                            <td>{{$valueCourse->getCountUserJoinAll($dateBegin,$dateEnd)}}</td>
                            <td>{{$valueCourse->getCountUserVisitAll($dateBegin,$dateEnd)}}</td>
                            <td>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        @if(!is_null($schools) && is_null($classes))
                        @foreach($listCourses as $key=>$valueCourse)
                        <?php //dd($valueCourse); 
                        ?>
                        <tr role="row" class="even">
                            <td>{!! Helper::indexPaginate($listCourses,$loop) !!}</td>
                            <td>{{$valueCourse->name}}</td>
                            <td>{{$valueCourse->category->name}}</td>
                            <td>{{$valueCourse->getCountUserCount($schools->id,$dateBegin,$dateEnd)}}</td>
                            <td>{{$valueCourse->getCountUserJoin($schools->id,$dateBegin,$dateEnd)}}</td>
                            <td>{{$valueCourse->getCountUserVisit($schools->id,$dateBegin,$dateEnd)}}</td>
                            <td>
                                <div class=" w-100">
                                    <a href="{{route('school_course.destroy',['school_id'=>$schools->id,'id'=>$valueCourse->id])}}"
                                        type="submit"
                                        class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning"
                                        title="Ẩn khóa học" onclick="return confirm('Bạn muốn xóa khóa học khỏi chương trình học ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        <!--  -->
                        @if(!is_null($classes))
                        @foreach($listCourses as $key => $valueCourse)
                        <tr role="row" class="even">
                            <td>{!! Helper::indexPaginate($listCourses,$loop) !!}</td>
                            <td>{{$valueCourse->name}}</td>
                            <td>{{$valueCourse->category->name}}</td>
                            <td>{{$valueCourse->getCountUserCountClass($classes->id,$dateBegin,$dateEnd)}}</td>
                            <td>{{$valueCourse->getCountUserJoinClass($classes->id,$dateBegin,$dateEnd)}}</td>
                            <td>{{$valueCourse->getCountUserVisitClass($classes->id,$dateBegin,$dateEnd)}}</td>
                            <td>
                                <div class="w-100">
                                    <!-- <a class="pull-right mt-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning "
                                        href="{{route('school_course.destroyClass',['school_id'=>$schools->id,'id'=>$valueCourse->id])}}"
                                        title="ẩn" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a> -->
                                    <form method="GET" action="{{route('transcript.user')}}" accept-charset="UTF-8">
                                        <input type="hidden" name="class" value="{{$classes->id}}">
                                        <input type="hidden" name="date_begin" value="{{$dateBegin}}">
                                        <input type="hidden" name="date_end" value="{{$dateEnd}}">
                                        <input type="hidden" name="course" value="{{$valueCourse->id}}">
                                        <input type="hidden" name="url" value="{{url()->full()}}">
                                        <div class="col mb-3">
                                            <button class="mr-1 pull-right mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-success" title="Xem chi tiết">
                                                <span class="pe-7s-menu" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $listCourses->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection