<?php
$user = auth()->user();
$arrOrtherShow  = [];
$arrOrtherManagent = [];
$arrMap = [];
$arrSupport = [];
$arrNew =[];
$arrCourse =[];
?>
<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Dashboards</li>
        <li>
            <a class="{!! Helper::active_menu('HomeController') !!}" href="{{route('home.index')}}">
                <i class="metismenu-icon fas fa-home"></i>
                Tổng quan
            </a>
        </li>
        <li class="app-sidebar__heading">Quản lý trường học</li>
        @hasrole('Admin')
        <li>
            <a class="{!! Helper::active_menu('SchoolController') !!}" href="{{route('school_index')}}">
                <i class="metismenu-icon fa fa-school">
                </i>Trường
            </a>
        </li>
        @endhasrole
        @hasanyrole('school|Admin')
        <li>
            <a class="{!! Helper::active_menu('GradeController') !!}" href="{{route('grade_index')}}">
                <i class="metismenu-icon fas fa-graduation-cap">
                </i>Khối
            </a>
        </li>
        @endhasrole
        <li>
            <a class="{!! Helper::active_menu('ClassController') !!}" href="{{route('class_index')}}">
                <i class="metismenu-icon fas fa-warehouse">
                </i>Lớp
            </a>
        </li>
        <li class="app-sidebar__heading">Quản lý user</li>
        @hasanyrole('school|Admin')
        <li>
            <a class="{!! Helper::active_menu('UserController') !!}" href="{{route('user.index')}}">
                <i class="metismenu-icon fa fa-users">
                </i>Tất cả User
            </a>
        </li>
        @endhasrole
        <li>
            <a class="{!! Helper::active_menu('StudentController') !!}" href="{{route('student.index')}}">
                <i class="metismenu-icon fa fa-user-circle">
                </i>Học sinh
            </a>
        </li>
        @hasanyrole('school|Admin')
        <li>
            <a class="{!! Helper::active_menu('TeacherController') !!}" href="{{route('teacher.index')}}">
                <i class="metismenu-icon fa fa-user-tie">
                </i>Giáo viên
            </a>
        </li>
        @endhasrole
        <li>
            <a class="{!! Helper::active_menu('TrackingController') !!}" href="{{route('tracking.tracking.index')}}">
                <i class="metismenu-icon fas fa-map-marker-alt">
                </i>Tracking học sinh
            </a>
        </li>
        <?php
        $arrCourse = [
            "CourseCategoryController","CourseTheoryController","CourseController","StatisticalController"
            ,"SchoolCourseController","TranscriptController"
        ];
        ?>
        <li class="app-sidebar__heading" id="{!! Helper::active_menusub($arrCourse) !!}">Khóa học</li>
        @hasanyrole('school|Admin')
        <li>
            <a class="{!! Helper::active_menu('CourseCategoryController') !!}"
                href="{{route('category_course.index')}}">
                <i class="metismenu-icon fa fa-barcode">
                </i>Nhóm kỹ năng sống
            </a>
        </li>
        @endhasrole
        <li>
            <a class="{!! Helper::active_menu('CourseTheoryController') !!}" href="{{route('course_theory.index')}}">
                <i class="metismenu-icon fa fa-window-restore">
                </i>Kỹ năng sống lý thuyết
            </a>
        </li>
        <li>
            <a class="{!! Helper::active_menu('CourseController') !!}" href="{{route('course.index')}}">
                <i class="metismenu-icon fa fa-window-restore">
                </i>Kỹ năng sống thực hành
            </a>
        </li>
        <li>
            <a class="{!! Helper::active_menu('StatisticalController') !!}"
                href="{{route('statistical.statistical.index')}}">
                <i class="metismenu-icon fas fa-poll-h">
                </i>Thống kê số liệu
            </a>
        </li>
        <li>
            <a class="{!! Helper::active_menu('SchoolCourseController') !!}" href="{{route('school_course.index')}}">
                <i class="metismenu-icon fas fa-book-reader">
                </i>Chương trình học
            </a>
        </li>
        <li>
            <a class="{!! Helper::active_menu('TranscriptController') !!}" href="{{route('transcript.index')}}">
                <i class="metismenu-icon fas fa-poll-h">
                </i>Kết quả học tập
            </a>
        </li>
        <?php
        $arrChat = [
            "ChatController"
        ];
        ?>
        <li class="app-sidebar__heading" id="{!! Helper::active_menusub($arrChat) !!}">Chatting</li>
        <li>
            <a class="{!! Helper::active_menu('ChatController') !!}" href="{{route('chat.chat.index')}}">
                <i class="metismenu-icon fas fa-comments"></i>Chat
            </a>
        </li>
        @hasrole('Admin')
        <?php
        $arrNew = [
            "NewsCategoryController","NewsController"
        ];
        ?>
        <li class="app-sidebar__heading" id="{!! Helper::active_menusub($arrNew) !!}">Tin Tức</li>
        <li>
            <a class="{!! Helper::active_menu('NewsCategoryController') !!}" href="{{route('news_category_index')}}">
                <i class="metismenu-icon fa fa-folder-open "></i>Danh mục tin tức
            </a>
        </li>
        <li>
            <a class="{!! Helper::active_menu('NewsController') !!}" href="{{route('news_index')}}">
                <i class="metismenu-icon pe-7s-note2 "></i>Tin Tức
            </a>
        </li>
        <?php
        $arrSupport = [
            "SupportsController"
        ];
        ?>
        <li class="app-sidebar__heading" id="{!! Helper::active_menusub($arrSupport) !!}">Hỗ Trợ</li>
        <li>
            <a class="{!! Helper::active_menu('SupportsController') !!}" href="{{route('supports.index')}}">
                <i class="metismenu-icon fa fa-folder-open"></i>Danh mục hỗ trợ
            </a>
        </li>
        <?php
        $arrMap = [
            "MapController","MapCategoryController"
        ];
        ?>
        <li class="app-sidebar__heading" id="{!! Helper::active_menusub($arrMap) !!}">Quản Lý Map</li>
        <li>
            <a class="{!! Helper::active_menu('MapCategoryController') !!}" href="{{route('map_category_index')}}">
                <i class="metismenu-icon fa fa-folder-open "></i>Danh mục điểm map
            </a>
        </li>
        <li>
            <a class="{!! Helper::active_menu('MapController') !!}" href="{{route('map_index')}}">
                <i class="metismenu-icon fa fa-map-marker  "></i>Điểm Map
            </a>
        </li>
        <?php
        $arrOrtherManagent  = [
            "ReferController","ServiceController","PermisionController","NotificationController","PolicyController"
        ];
        $arrOrtherShow  = [
           "PermisionController","NotificationController","PolicyController"
        ];
        ?>
        <li class="app-sidebar__heading" id="{!! Helper::active_menusub($arrOrtherManagent) !!}">Quản Lý Khác</li>
        <!-- <li>
            <a href="{{route('map_index')}}">
                <i class="metismenu-icon fa fa-map"></i>Bản đồ
            </a>
        </li> -->
        <li class="">
            <a class="{!! Helper::active_menu('ReferController') !!}" href="{{route('refer.index')}}">
                <i class="metismenu-icon fa fa-folder-open"></i>Danh mục tham khảo
            </a>
        </li>
        <li>
            <a class="{!! Helper::active_menu('ServiceController') !!}" href="{{route('service.edit')}}">
                <i class="metismenu-icon fas fa-globe-americas"></i>Web service
            </a>
        </li>
        @endhasrole

        <li>
            <a href="#">
                <i class="metismenu-icon fas fa-cog"></i>
                Settings
                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul class="{!! Helper::active_show($arrOrtherShow) !!}">
                @hasrole('Admin|school')
                <li>
                    <a class="{!! Helper::active_menu('PermisionController') !!}"
                        href="{{route('permission.permission.index')}}">
                        <i class="metismenu-icon fa fa-folder-open"></i>Phân quyền
                    </a>
                </li>
                @endhasrole
                <li>
                    <a class="{!! Helper::active_menu('NotificationController') !!}"
                        href="{{route('notification.index')}}">
                        <i class="metismenu-icon fa fa-folder-open"></i>Thông báo
                    </a>
                </li>
                @hasrole('Admin')
                <li>
                    <a class="{!! Helper::active_menu('PolicyController') !!}" href="{{route('policy.index')}}">
                        <i class="metismenu-icon fa fa-folder-open"></i>Chính sách
                    </a>
                </li>
                @endhasrole
            </ul>
        </li>

    </ul>
</div>