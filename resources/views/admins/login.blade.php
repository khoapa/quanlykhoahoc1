<!doctype html>
<html lang="en">

<head>
    <base href={{asset('')}}>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ControlPanel</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <link href="{{ asset('admins/main.css') }}" rel="stylesheet">
    <link href="{{ asset('admins/assets/css/secondary.css') }}" rel="stylesheet">


    <!-- file new -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div class="d-none d-lg-block col-lg-8"
                        style="background-image: url('/admins/assets/images/image_2020_10_30T14_24_33_838Z.png');background-repeat: round;">
                    </div>
                    <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-4">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                            <div class="app-logo"></div>
                            <h4 class="mb-0">
                                <span class="d-block">Chào mừng trở lại,</span>
                                <span>Vui lòng đăng nhập để tiếp tục.</span>
                            </h4>
                            <div class="divider row"></div>
                            <div>
                                @if(Session::has('msg'))
                                    <div class="alert alert-danger" role="alert">{{Session::get('msg')}}</div>
                                @endif    
                                <form action="{{route('admins.login')}}" method="POST" class="">
                                {{ csrf_field() }}
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <label for="exampleEmail" class="">Email</label>
                                                <input name="email" id="exampleEmail" placeholder="Nhập email..."
                                                    type="email" class="form-control">
                                                    <div class="error-valid">{{ $errors->first('email') }}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-3">
                                            <label for="examplePassword" class="">Mật khẩu</label>
                                            <div class="position-relative input-group">
                                                <input aria-describedby="basic-addon4" name="password" id="examplePassword"
                                                    placeholder="Nhập mật khẩu..." type="password" class="form-control">
                                                    <div onclick="showPassword()" class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon4">
                                                    <i class="fas fa-eye"></i>
                                                    </span>
                                        </div>
                                                    <div class="error-valid">{{ $errors->first('password') }}</div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="divider row"></div>
                                    <div class="d-flex align-items-center">
                                        <div class="ml-auto">
                                            <!-- <a href="javascript:void(0);" class="btn-lg btn btn-link">Quên mật khẩu</a> -->
                                            <button class="btn btn-primary btn-lg">Đăng nhập</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
            <!-- file new -->
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
            <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
            <!--  -->
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('admins/assets/scripts/main.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admins/assets/scripts/login.js') }}"></script>
</body>

</html>