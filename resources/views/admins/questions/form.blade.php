    <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('question.question.update',$question->id) }}"
        accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form"
        class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
        {{ csrf_field() }}
        <input type="hidden" value="{{$type}}" name="type">
        <input type="hidden" value="{{$question->course_id}}" name="course_id">
        <div class="form-group {{ $errors->has('content') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <div
                class="form-group {{ $errors->has('content') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
                <label for="content">Nội dung câu hỏi:</label>
                <textarea class="form-control" name="subject" type="text" id="editor"
                    required>{{$question->subject}}</textarea>
                    @error('subject')
                        <p style="display: block;" class="invalid-feedback">{{ $message }}</p>
                    @enderror
            </div>
        </div>
        @if($type == 1)
        <div id="form-choose">
            <div class="form-row mb-2">
                <div class="col-md-6" id="form-select-multi">
                    <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                        <label for="sel1">Được chọn nhiều</label>
                        <select class="custom-select {{ $errors->has('multiselect') ? 'is-invalid' : '' }}"
                            id="multiselect-choose" name="multiselect" >
                            @php
                            $multiselect = old('multiselect',optional(optional($question))->multiselect);
                            @endphp
                            <option value="0" {!! Helper::setSelected($multiselect, 0) !!}>Không chọn nhiều đáp
                                án
                            </option>
                            <option value="1" {!! Helper::setSelected($multiselect, 1) !!}>Chọn nhiều đáp án
                            </option>
                        </select>
                        {!! $errors->first('multiselect', '<p class="invalid-feedback">:message</p>') !!}
                    </div>
                </div>
            </div>
            <input type="hidden" name="id_answer" value="{{$answers[0]['id']}}" />
            <input type="hidden" name="id_answer1" value="{{$answers[1]['id']}}" />
            <div class="form-row mb-2">
                <?php
                      $valid = 'answer.'.$answers[0]['id'];
                      $valid1 = 'answer.'.$answers[1]['id'];
                      $i=0;
                      $answer0 = 'answer['.$answers[0]['id'].']';
                      $answer1 = 'answer['.$answers[1]['id'].']';
                      $count = $answers->count();
                      for($i = 0;$i<$count;$i++){
                        $answer = null;
                        $idAnswer = 'answerTo[]';
                        $check = 'correctTo[]'; 
                        $checkCorrect = 0;
                          if(isset($answers[$i])){
                               $answer = $answers[$i]['answer'];
                               $idAnswer ='answer['.$answers[$i]['id'].']';
                               $checkCorrect = $answers[$i]['correct'];
                               $check = 'correct['.$answers[$i]['id'].']'; 
                          }
                          ?>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                        <label for="sel1">Đáp án <?php echo $i+1;?></label>
                        <input type="text" class="form-control" value="{{$answer}}" maxlength="500" name="<?php echo $idAnswer; ?>"
                            <?php if($i==0){echo "required";} ?>>
                        @if($idAnswer == $answer0 ||$idAnswer == $answer1 )
                        @error($valid)
                        <p style="display: block;" class="invalid-feedback">{{ $message }}</p>
                        @enderror
                        @endif
                        @if($idAnswer == $answer1)
                        @error($valid1)
                        <p style="display: block;" class="invalid-feedback">{{ $message }}</p>
                        @enderror
                        @endif
                        <div class="form-group mt-1 form-check-input-choose <?php if($i == 0){echo "check-first-answer";}if($i == 1){echo "form-check-input-choose-second-div";} ?>">
                            <label class="customcheck <?php if($i == 0){echo "customcheck-first";} ?>">Đáp án đúng
                                <input type="hidden" name="<?php echo $check ?>" type="checkbox" value="0">
                                <input class="form-check-input"  id="<?php if($i == 0){echo "form-check-input-choose-first";} if($i == 1){echo "form-check-input-choose-second";}?>" <?php
                                if($checkCorrect == 1){ echo "checked";}
                                ?> name="<?php echo $check ?>"  type="checkbox" value="1">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <?php    
                }  
                    ?>
            </div>
            @endif
            <div class="form-row mb-2" id="add-answer"></div>
                </div>
                <div class="col-md-offset-2">
                <a href="{{route('question.question.index',$question->course_id)}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Quay lại</a>
                    <button type="submit" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="mr-2 pe-7s-check btn-icon-wrapper"> </i>Lưu</button>
                    <a href="javascript:void(0)" onclick="addAnswerEdit()" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success" id="add-answer-choose"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                        </i>Thêm câu trả lời</a>
                </div>
    </form>
    <br>
    <!-- form -->