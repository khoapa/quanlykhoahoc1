@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Chi Tiết Khóa Học
                </div>
            </div>

        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="col-md-12 pl-0">
        <div class="mb-3 card">
            <div class="p-0 card-body">
                <div class="dropdown-menu-header mt-0 mb-0">
                    <div class="dropdown-menu-header-inner bg-heavy-rain">
                        <div class="menu-header-image opacity-1" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                        <div class="custom-course">
                            <a href="{{route('course.edit',$course->id)}}" class="badge badge-success label-detail-answer">
                                <span class="mr-1">Cập nhập khóa học</span>
                            </a>
                            <a href="{{redirect()->getUrlGenerator()->previous()}}" class=" btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                        </div>
                        <div class="menu-header-content text-dark p-2">
                            <h5 class="text-center menu-header-title">{{$course->name}}</h5>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-animated-1" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="ps">
                                    <div class="p-3">
                                        <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                            <div class="vertical-timeline-item vertical-timeline-element">
                                                <div>
                                                    <span class="vertical-timeline-element-icon bounce-in">
                                                        <i class="badge badge-dot badge-dot-xl badge-success"></i>
                                                    </span>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Danh mục khóa học</h6>
                                                        <p>{{$course->category->name}}
                                                        </p>
                                                    </div>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Ngày bắt đầu</h6>
                                                        <p>{{date("d-m-Y", strtotime($course->start_date))}}
                                                        </p>
                                                    </div>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Thời gian học</h6>
                                                        <p>@if(!is_null($course->longtime)){{$course->longtime}} Ngày @endif
                                                        </p>
                                                    </div>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Người tạo</h6>
                                                        <p>{{$course->authorUser->name}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="ps">
                                    <div class="p-3">
                                        <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                            <div class="vertical-timeline-item vertical-timeline-element">
                                                <div>
                                                    <span class="vertical-timeline-element-icon bounce-in">
                                                        <i class="badge badge-dot badge-dot-xl badge-success"></i>
                                                    </span>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Tỉ lệ điểm (trắc nghiệm/tự luận)</h6>
                                                        <p>{{$course->total_point_choice}} /
                                                            {{100-$course->total_point_choice}}
                                                        </p>
                                                    </div>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Hiển thị</h6>
                                                        <p>@if($course->new)
                                                            Mới,
                                                            @endif
                                                            @if($course->offer)
                                                            Đề xuất
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Số câu hỏi</h6>
                                                        <p>{{$course->question->count()}}
                                                        </p>
                                                    </div>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h6 class="timeline-title">Ngày tạo</h6>
                                                        <p>{{date("d-m-Y", strtotime($course->created_at))}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button p-2">

                    @if((100-$course->total_point_choice) > 0)
                    <a href="javascript:void(0)" onclick="checkFormQuestion(2)" class="badge badge-success label-detail-answer" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                        </i>Thêm câu hỏi tự luận</a>
                    @endif
                    @if($course->total_point_choice > 0)
                    <a href="javascript:void(0)" onclick="checkFormQuestion(1)" class="badge badge-success label-detail-answer" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                        </i>Thêm câu hỏi trắc nghiệm</a>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <!-- form -->
    <div class="main-card card main-custom" id="main-question">
        <input type="hidden" value="0" id="form-open">
        <div class="card-body">
            <h6 class="text-center font-weight-bold">THÊM CÂU HỎI</h6>
            <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('question.question.store') }}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
                {{ csrf_field() }}
                <?php
                $value = old('type');
                ?>
                <input type="hidden" value="{{$value}}" id="type-question" name="type">
                <input type="hidden" name="course_id" value="{{$course->id}}">
                <div class="form-group {{ $errors->has('content') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
                    <label for="subject">Nội dung câu hỏi</label>
                    <div>
                        <textarea class="form-control" name="subject" type="text" id="subject" required></textarea>
                        {!! $errors->first('subject', '<p class="invalid-feedback">:message</p>') !!}
                    </div>
                </div>
                <div id="form-choose">
                    <div class="form-row mb-2">
                        <div class="col-md-6" id="form-select-multi">
                            <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                                <label for="sel1">Được chọn nhiều</label>
                                <select class="custom-select {{ $errors->has('multiselect') ? 'is-invalid' : '' }}" id="multiselect-choose" name="multiselect">
                                    @php
                                    $multiselect = request()->multiselect;
                                    @endphp
                                    <option value="0" {!! Helper::setSelected($multiselect, 0) !!}>Không chọn nhiều đáp
                                        án
                                    </option>
                                    <option value="1" {!! Helper::setSelected($multiselect, 1) !!}>Chọn nhiều đáp án
                                    </option>
                                </select>
                                {!! $errors->first('multiselect', '<p class="invalid-feedback">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                        <p> * Với Chọn 1 đáp án đúng, bạn nhập đáp án đúng tại A và hệ thống sẽ sắp xếp ngẫu nhiên trên app.</p>
                       <p> * Với Chọn nhiều đáp án đúng, bạn nhập đáp án đúng tại A và B là mặc định hệ thống sẽ sắp xếp ngẫu nhiên trên app.</p>
                       </div>
                    </div>

                    <div class="form-row mb-2">
                        <div class="col-md-6" id="form-select-A">
                            <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                                <label for="sel1">Đáp án A</label>
                                <input type="text" class="form-control" maxlength="255" name="answer[]" required>
                                {!! $errors->first('answer.0', '<p class="invalid-feedback">:message</p>') !!}
                                <div class="form-group mt-1">
                                    <label class="customcheck  customcheck-first">Đáp án đúng
                                        <input type="hidden" name="correct[]" type="checkbox" value="0">
                                        <input id="form-check-input-choose-first"  class="form-check-input" name="correct[]" checked type="checkbox" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="form-select-B">
                            <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                                <label for="sel1">Đáp án B</label>
                                <input type="text" class="form-control" maxlength="255" name="answer[]" required>
                                {!! $errors->first('answer.1', '<p class="invalid-feedback">:message</p>') !!}
                                <div class="form-group mt-1 form-check-input-choose form-check-input-choose-second-div">
                                    <label class="customcheck">Đáp án đúng
                                        <input type="hidden" name="correct[]" type="checkbox" value="0">
                                        <input  id="form-check-input-choose-second"  class="form-check-input" name="correct[]" type="checkbox" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                {!! $errors->first('multiselect', '<p class="invalid-feedback">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-row mb-2">
                        <div class="col-md-6" id="form-select-C">
                            <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                                <label for="sel1">Đáp án C</label>
                                <input type="text" class="form-control" maxlength="255" name="answer[]">
                                <div class="form-group mt-1 form-check-input-choose">
                                    <label class="customcheck">Đáp án đúng
                                        <input type="hidden" name="correct[]" type="checkbox" value="0">
                                        <input class="form-check-input" name="correct[]" type="checkbox" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                {!! $errors->first('multiselect', '<p class="invalid-feedback">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-md-6" id="form-select-D">
                            <div class="form-group {{ $errors->has('multiselect') ? 'was-validated ' : '' }}">
                                <label for="sel1">Đáp án D</label>
                                <input type="text" class="form-control" maxlength="255" name="answer[]">
                                <div class="form-group mt-1 form-check-input-choose">
                                    <label class="customcheck">Đáp án đúng
                                        <input type="hidden" name="correct[]" type="checkbox" value="0">
                                        <input class="form-check-input" name="correct[]" type="checkbox" value="1">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                {!! $errors->first('multiselect', '<p class="invalid-feedback">:message</p>') !!}
                            </div>
                        </div>

                    </div>
                    <div class="form-row mb-2" id="add-answer"></div>
                </div>
                <div class="col-md-offset-2">
                    <button type="submit" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="mr-2 pe-7s-check btn-icon-wrapper"> </i>Lưu</button>
                    <button id="rediret-answer-question" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="mr-2 pe-7s-check btn-icon-wrapper"> </i>Hủy</button>
                    <a href="javascript:void(0)" onclick="addAnswer()" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success" id="add-answer-choose"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                        </i>Thêm câu trả lời</a>
                </div>
            </form>
        </div>
    </div>
    <br>
    <!-- form -->
    <div class="main-card card mb-5">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên</th>
                            <th>Loại</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($questions as $question)
                        <tr>
                            <td>{!! Helper::indexPaginate($questions,$loop) !!}</td>
                            <td>{{$question->subject}}</td>
                            <td>{{$question->type_question}}</td>
                            <!-- <td>
                                @if($question->type == 1)
                                <a href="{{route('answer.answer.index',$question->id)}}"
                                    class="badge badge-success label-detail-answer">
                                    <span class="mr-1">Xem câu trả lời</span>
                                </a>
                                @endif
                            </td> -->
                            <td>
                                <form method="POST" action="{{route('question.question.destroy',$question->id)}}" accept-charset="UTF-8">
                                    <input name="_method" value="DELETE" type="hidden">
                                    {{ csrf_field() }}
                                    <div class=" w-100">
                                        <button type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                            <span class="pe-7s-trash" aria-hidden="true"></span>
                                        </button>
                                        <a href="{{route('question.question.edit',$question->id)}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $questions->links()}}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection