@extends('admin')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5> Cập Nhập Câu Hỏi</h5>
            </div>
        </div>
    </div>
</div>
<div class="main-card card main-card-custom">
    <div class="card-body">
            @include ('admins.questions.form', [
            'courses' => $courses,
            'question' => $question,
            'answers' => $answers,
            'type'=> $type,
            ])

    </div>
</div>
@endsection