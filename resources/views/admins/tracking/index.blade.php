@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Tracking Học Sinh
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="main-card card">
    <div id="mapstudent" style="height: 400px;"></div>
     <input type="hidden" name="student" value="{{$arrs}}" id="studentsMap" />
    </div>
    <form class="mt-5" action="" method="GET" id="form-search">
        <div class="row">
        <div class="col-md-3">
                <input type="search" value="<?php echo request()->get('name') ?>" class="form-control search-input" name="name" placeholder="Tên học sinh" aria-controls="example">
                <button class="btn btn-info custom-search"><i class="fa fa-search"></i></button>
            </div>
            @hasanyrole('Admin')
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="school_search" name="school_id">
                        <option value="">--Tất cả trường--</option>
                        @foreach($schools as $school)
                        <option @if($school->id == request()->school_id)
                            selected = "selected"
                            @endif
                            value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endhasrole
            @hasanyrole('Admin|school')
            <div class="col-md-2">
                <div class="form-group">
                    <select class="custom-select" id="class_search" name="class_id">
                        @hasanyrole('Admin|school')    
                            <option value="">--Tất cả lớp-</option>
                            @endhasrole
                        @if(!is_null($classes))
                        @foreach($classes as $class)
                        <option @if($class->id == request()->class_id)
                            selected = "selected"
                            @endif
                            value="{{$class->id}}">{{$class->name}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            @endhasrole
        </div>
    </form>
    <div class="main-card card mt-3">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Ảnh</th>
                            <th>Tên</th>
                            <th>Trường</th>
                            <th>Lớp</th>
                            <th>Địa chỉ</th>
                            <th>Ngày giờ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($students as $student)
                    <?php 
                        $lat =  $student->latitude;
                        $long = $student->longitude ;
                        $name = $student->name;
                        $school = optional($student->school)->name;
                        $class = optional($student->classes)->name;
                        $arr = [
                            'id' => $student->id,
                            'lat' => $lat,
                            'long' => $long,
                            'name' => $name,
                            'school'=> $school,
                            'class' => $class
                        ];
                        $arrnew = json_encode($arr);
                    ?>
                        <tr role="row" class="odd click-map" onclick="moveMap(<?php echo $student->id ?>)">
                        <input type="hidden" id="{{$student->id}}" value="{{$arrnew}}"/>
                            <td>{!! Helper::indexPaginate($students,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$student->avatar}}" >
                            </td>
                            <td>{{$student->name}}</td>
                            <td>{{optional($student->school)->name}}</td>
                            <td>{{optional($student->classes)->name}}</td>
                            <td>{{optional($student->locationUser()->latest()->first())->address}}</td>
                            <td>{!! Helper::formatDateChat(optional($student->locationUser()->latest()->first())->date_tracking) !!}</td>
                            <td><a href="{{route('tracking.tracking.detail',$student->id)}}"
                                        class="pull-right badge badge-success label-detail-answer">
                                        <span class="mr-1">Xem chi tiết</span>
                                    </a></td></td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $students->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZlHcoPe33iRtvrqFr1o_ZU50pV6JRVJs&callback=initMap&libraries=places&callback=initMap" defer></script>
<script type="text/javascript" src="{{ asset('admins/assets/scripts/tracking.js') }}"></script>
