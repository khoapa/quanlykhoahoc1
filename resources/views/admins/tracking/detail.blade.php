@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Tracking Học Sinh (Chi tiết)
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{ route('tracking.tracking.index') }}" class=" btn-icon btn-pill btn btn-outline-secondary" href=""><i class="pe-7s-back btn-icon-wrapper">
                    </i>Trở lại</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="main-card card">
    <div id="mapstudentdetail" style="height: 400px;"></div>
     <input type="hidden" name="student" value="{{$arrs}}" id="studentsMap" />
    </div>
    <div class="main-card card mt-3">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Ảnh</th>
                            <th>Tên</th>
                            <th>Trường</th>
                            <th>Lớp</th>
                            <th>Địa chỉ</th>
                            <th>Ngày giờ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($locations as $location)
                        <?php 
                        $lat =  $location->latitude;
                        $long = $location->longitude ;
                        ?>
                        <tr role="row" class="odd click-map" onclick="moveMap(<?php echo $lat;echo ',';echo $long ?>)">
                            <td>{!! Helper::indexPaginate($locations,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$location->student->avatar}}" >
                            </td>
                            <td>{{$location->student->name}}</td>
                            <td>{{optional($location->student->school)->name}}</td>
                            <td>{{optional($location->student->classes)->name}}</td>
                            <td>{{$location->address}}</td>
                            <td>{!! Helper::formatDateChat($location->date_tracking) !!}</td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZlHcoPe33iRtvrqFr1o_ZU50pV6JRVJs&callback=initMap&libraries=places&callback=initMap" defer></script>
<script type="text/javascript" src="{{ asset('admins/assets/scripts/detailtracking.js') }}"></script>
