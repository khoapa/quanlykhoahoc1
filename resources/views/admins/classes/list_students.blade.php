@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    DANH SÁCH HỌC SINH CỦA LỚP {{$class->name}}
                    <input type="hidden" id="class_id" value="{{$class->id}}">
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                <a href="{{route('student.create')}}" class="mb-2 mr-2 btn-icon btn btn-success"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới học sinh</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="col mb-6">
            <div class="row">
                <div class="form-group">
                    <input type="search" value="<?php echo request()->get('search') ?>" class="form-control" name="search" id="search_name" placeholder="Tên học sinh" aria-controls="example">
                </div>
                <div class="form-group ml-2">
                    <button class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card mb-3 card">
        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped">
                        <thead>
                            <tr role="row">
                                <th>STT</th>
                                <th>Ảnh</th>
                                <th>Họ Tên</th>
                                <th>Trường</th>
                                <th>Lớp</th>
                                <th>
                                    <input value="1" id="selectAllUserOfClass" @if($statusAllStudent==1) checked @endif  type="checkbox">                                   
                                    Trạng thái
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $valueStudent)
                            @php
                            $date=date_create($valueStudent->birthday);
                            @endphp
                            <tr role="row" class="even">
                                <td>{!! Helper::indexPaginate($students,$loop) !!}</td>
                                <td class="image-cate">
                                    <img class="rounded-circle" src="{{$valueStudent->avatar}}" alt="">
                                </td>
                                <td class="sorting_1 dtr-control">{{$valueStudent->name}}</td>
                                <td>{{!is_null($valueStudent->school) ? $valueStudent->school->name : ''}}</td>
                                <td>{{!is_null($valueStudent->classes) ? $valueStudent->classes->name : ''}}</td>
                                <td>
                                    <label class="pd-toggle-switch">
                                        @if($valueStudent->status == 1)
                                        <input type="checkbox" name="status[]" class="check" checked onclick="addStatusUserFunction('2','{{$valueStudent->id}}')" value="{{$valueStudent->id}}">
                                        @else
                                        <input type="checkbox" name="status[]" class="check" onclick="addStatusUserFunction('1','{{$valueStudent->id}}')" value="{{$valueStudent->id}}">
                                        @endif
                                    </label>
                                </td>

                                <td>                                  
                                    <div class=" w-100">
                                    <a href="{{route('student.destroy',['id'=>$valueStudent->id])}}" type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <form action="{{route('student.edit',['id'=>$valueStudent->id])}}" method="GET">
                                        <input type="hidden" name="url" value="{{url()->full()}}">
                                        <button class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </button>
                                    </form>
                                </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <div class="col-12">
                    <div class="pagination pull-right">
                        {{ $students->links()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection