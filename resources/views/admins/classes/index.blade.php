@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh Sách Lớp Học
                </div>
            </div>
            <div class="page-title-actions">
                <form name="form-import" action="{{ route('class.import')}}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <div class="input-group mb-3">
                        <div class="custom-file form-group {{ $errors->has('role_id') ? 'was-validated ' : '' }}">
                            <input type="file" class="custom-file-input" id="inputGroupFile02" name="import">
                            <label class="custom-file-label" for="inputGroupFile02">Chọn file excel</label>
                            {!! $errors->first('import', '<p class="invalid-feedback">:message</p>') !!}
                        </div>
                        <button class="btn-icon btn btn-success"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                            </i>Import</button>
                        <input type="hidden" name="type_import" value="3">
                    </div>
                    <p>Tải file mẫu : <a href="{{ asset('admins/assets/images/CLASS_LIST.xlsx') }}" download="">Tại đây</a></p>
                </form>
                <div class="page-title-actions">
                    <a href="{{route('class_create')}}" class="mb-2 mr-2 btn-icon btn btn-success float-right" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                        </i>Thêm mới lớp học</a>
                </div>
            </div>
        </div>
    </div>
    @if ($errors->first('import'))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok"></span>
        {{ $errors->first('import') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('error'))
    <div class="alert alert-danger">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('error') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(Session::has('data'))
    <?php
    $dataResponse = Session::get('data');
    $countError = $dataResponse['numberError'];
    // var_dump($dataResponse['response']);
    $countImport = $dataResponse['total'] -  $countError;
    ?>
    <div class="alert alert-success">
        <p class="glyphicon glyphicon-ok">Số lượng import : {{$countImport}}</p>
        <p class="glyphicon glyphicon-ok">Số lượng lỗi : {{$countError}}
        </p>
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3">
                <input type="search" id="search_name" value="<?php echo request()->get('search') ?>" class="form-control search-input" name="search" placeholder="Tên lớp học" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
            @hasanyrole('Admin')
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select search_school" id="search_school" name="school_id">
                        <option value="">--Tất cả trường--</option>
                        @foreach($schools as $school)
                        <option @if($school->id == request()->school_id)
                            selected = "selected"
                            @endif
                            value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endhasanyrole
            @hasanyrole('Admin|school')
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="grade_search" name="grade_id">
                        <option value="">--Tất cả khối--</option>
                        <?php
                        for ($i = 0; $i < count($grades); $i++) {
                        ?>
                            <option <?php if ($grades[$i]['id'] == request()->grade_id) { ?> selected="selected" <?php } ?> value="<?php echo $grades[$i]['id'] ?>">{{$grades[$i]['name']}}</option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            @endhasanyrole
            <div class="col mb-3">
                <div id="export-click-class" class="mt-0 btn-icon btn btn-success export export-sata"><i class="fa fa-download pr-1" aria-hidden="true"></i>Export</div>
                </div>
        </div>
    </form>
    <div class="card mb-3">
        <div class="main-card card">
            <div class="panel-body panel-body-with-table">
                <div class="table-responsive">
                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Hình ảnh</th>
                                <th>Tên lớp</th>
                                <th>Mã lớp</th>
                                <th>Khối</th>
                                <th>Trường</th>
                                @can('edit_class')
                                <th>
                                    <input value="1" id="selectAllClass" @if($statusAllClass==1) checked @endif type="checkbox">
                                    Trạng Thái
                                </th>
                                @endcan
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classes as $row)
                            <tr role="row" class="odd">
                                <td>{!! Helper::indexPaginate($classes,$loop) !!}</td>
                                <td class="image-cate">
                                    <img class="rounded-circle" src="{{$row->thumbnail}}" alt="">
                                </td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->class_code}}</td>
                                <td>{{optional(optional(optional($row))->grade)->name}}</td>
                                <td>{{optional(optional(optional($row))->school)->name}}</td>
                                @can('edit_class')
                                <td>
                                    <label class="pd-toggle-switch">
                                        @if($row->status == 1)
                                        <input type="checkbox" name='status[]' class="check" checked onclick="addStatusClassFunction('2','{{$row->id}}')" value="{{$row->id}}">
                                        @else
                                        <input type="checkbox" name='status[]' class="check" onclick="addStatusClassFunction('1','{{$row->id}}')" value="{{$row->id}}">
                                        @endif
                                    </label>
                                </td>
                                @endcan
                                <td>
                                    <div>
                                        <a class="pull-right ml-1 mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('class_delete',['id'=>$row->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                            <span class="pe-7s-trash" aria-hidden="true"></span>
                                        </a>
                                        <form action="{{route('class_edit',['id'=>$row->id])}}" method="GET">
                                            <input type="hidden" name="url" value="{{url()->full()}}">
                                            <button class="pull-right mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                                <span class="pe-7s-tools" aria-hidden="true"></span>
                                            </button>
                                        </form>
                                        <form action="{{route('list_student',[$row->id,optional($row->school)->id])}}" method="GET" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <button type="submit" class="mr-1 pull-right mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-success" title="Danh sách học sinh">
                                                <span class="pe-7s-menu" aria-hidden="true"></span>
                                            </button>
                                            <input type="hidden" name="url" value="{{url()->full()}}">
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-footer">
                <div class="col-12">
                    <div class="pagination pull-right">
                        {{ $classes->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection