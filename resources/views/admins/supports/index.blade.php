@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Danh Sách Hỗ Trợ</div>
            </div>
            <div class="page-title-actions">
                <!-- <a href="{{ route('supports.create') }}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Tạo</a> -->
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card-body">
        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="main-card card">
                <div class="panel-body panel-body-with-table">
                    <div class="table-responsive">

                        <table class="table table-striped ">

                            <thead>
                                <tr role="row">
                                    <th>STT</th>
                                    <th>Tiều đề</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($supports as $key => $valueSupports)
                                <?php if($key==0) continue ?>
                                <tr role="row" class="even">
                                    <td>{!! Helper::indexPaginate($supports,$loop) !!}</td>
                                    <td>{{$valueSupports->title}}</td>
                                    <td>
                                        <div class=" w-100">
                                            <!-- <a href="{{route('supports.destroy',['id'=>$valueSupports->id])}}" type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                                <span class="pe-7s-trash" aria-hidden="true"></span>
                                            </a> -->
                                            <a href="{{route('supports.edit',['id'=>$valueSupports->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                                <span class="pe-7s-tools" aria-hidden="true"></span>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $supports->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection