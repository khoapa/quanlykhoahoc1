<div class="form-row mb-2">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('title') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="firstname">Tiêu đề (*)</label>
            <input type="text" class="form-control" name="title" maxlength="255"
                value="{{ old('title', optional(optional($supports))->title) }}" require id="title" rows="11"
                required></input>
            {!! $errors->first('title', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div>
        <label for="content">Nội dung (*)</label>
        <textarea class="form-control" name="content" type="text" id="editor"
            value="{{ old('content', optional(optional($supports))->content) }}">{{ old('content', optional(optional($supports))->content) }}</textarea>
        {!! $errors->first('content', '<p class="invalid-feedback1">:message</p>') !!}
    </div>
</div>

@section('script')

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    
    $(document).ready(function() {
       // CKEDITOR.replace('content');
        $('#editor').each(function(e){
        CKEDITOR.replace( this.id, {
            filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
         })
    });
    })
</script>
@endsection