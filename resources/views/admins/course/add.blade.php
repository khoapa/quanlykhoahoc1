@extends('admin')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Thêm Mới Kỹ Năng Sống Thực Hành</h5>
            </div>
        </div>
    </div>
</div>
<div class="main-card card main-card-custom">
    <div class="card-body">
        <form enctype="multipart/form-data" method="POST" novalidate="" 
        action="{{route('course.store') }}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{ csrf_field() }}
            @include ('admins.course.form', [
                'categoryCourses' => $categoryCourses,
                'course'=>null
                ])
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <a href="{{ redirect()->getUrlGenerator()->previous() }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Quay lại</a>
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Thêm mới</button>
                </div>
            </div>

        </form>

    </div>
</div>
@endsection