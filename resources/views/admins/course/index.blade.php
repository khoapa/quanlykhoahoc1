@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh Sách Kỹ Năng Sống Thực Hành    
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{ route('course.create') }}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i
                        class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới kỹ năng sống</a>
            </div>
        </div>
    </div>
    <?php $urlCourse = Request::fullUrl();Session()->put('urlCourse', $urlCourse);  ?>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3">
                <input type="search" value="<?php echo request()->get('search') ?>" class="form-control search-input" name="search" id="search_name" placeholder="Tên kỹ năng sống" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select class="custom-select" id="search_category" name="category_id">
                        <option value="">--Chọn nhóm kỹ năng sống--</option>
                        @foreach($categories as $category)
                        <option @if($category->id == request()->category_id)
                            selected = "selected"
                            @endif
                            value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="search_author" name="author">
                        <option value="">--Chọn người tạo--</option>
                        @foreach($authors as $value)
                        <option 
                        @if($value->author == request()->author)
                            selected = "selected"
                            @endif
                            value="{{$value->author}}">{{$value->authorUser->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Hình ảnh</th>
                            <th>Tên</th>
                            <th>Nhóm kỹ năng</th>
                            <th>Thời gian bắt đầu</th>
                            @can('edit_course')
                            <th>
                                <input value="1" id="selectAllCourse" @if($statusAllCourse==1) checked @endif type="checkbox">
                                Hiển thị
                            </th>
                            @endcan
                            <th>Người tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($courses as $course)
                        <tr>
                            <td>{!! Helper::indexPaginate($courses,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$course->cover_image}}" alt="">
                            </td>
                            <td>{{$course->name}}</td>
                            <td>{{$course->category->name}}</td>
                            <td>{{date("d-m-Y", strtotime($course->start_date))}}</td>
                            @can('edit_course')
                            <td>
                                <!-- @if($course->status == 1)
                                <label class="label-show mt-1"><span>Hiển thị</span></label>
                                @else
                                <label class="label-hide mt-1"><span>Không hiển thị</span></label>
                                @endif -->
                                <label class="pd-toggle-switch">
                                    @if($course->status == 1)
                                    <input type="checkbox" name='status[]' class="check" checked onclick="addStatusCourseFunction('0','{{$course->id}}')" value="{{$course->id}}">
                                    @else
                                    <input type="checkbox" name='status[]' class="check" onclick="addStatusCourseFunction('1','{{$course->id}}')" value="{{$course->id}}">
                                    @endif
                                </label>
                            </td>
                            @endcan
                            <td>{{$course->authorUser->name}}</td>
                            <td>
                            <?php
                             $user = auth()->user();
                            ?>
                            @if($user->hasRole('Admin') || $course->authorUser->id == $user->id)
                                <div class="w-100">
                                    <a class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning "
                                        href="{{route('course.destroy',$course->id)}}" title="Xóa"
                                        onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <form action="{{route('question.question.index',$course->id)}}" method="GET">
                                        <button class="mr-1 pull-right mb-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-success" title="Xem chi tiết">
                                            <span class="pe-7s-menu" aria-hidden="true"></span>
                                        </button>
                                    </form>
                                </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $courses->links()}}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection