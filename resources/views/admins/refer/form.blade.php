<div class="form-row mb-2">

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('title') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="firstname">Tiêu đề (*)</label>
            <div>
                <input class="form-control" name="title" type="text" id="title"
                    value="{{ old('title', optional(optional($refer))->title) }}" required>
                {!! $errors->first('title', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('link') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="address">Link (*)</label>
            <div>
                <input type="text" class="form-control" value="{{ old('link', optional(optional($refer))->link) }}"
                    required name="link" placeholder="https://www.google.com.vn/">
                {!! $errors->first('link', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="avatar" class="control-label">Hình ảnh (*)</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('icon', optional($refer)->icon), 'icon') !!}
        {!! $errors->first('icon', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>