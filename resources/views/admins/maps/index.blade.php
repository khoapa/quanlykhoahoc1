@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh Sách Điểm Map
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{route('map_create')}}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới điểm map</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3">
                <input type="search" value="<?php echo request()->get('search') ?>" class="form-control search-input" name="search" placeholder="Tiêu đề điểm map" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="search_category" name="category_id">
                        <option value="">--Chọn thể loại điểm map--</option>
                        @foreach($categories as $category)
                        <option @if($category->id == request()->category_id)
                            selected = "selected"
                            @endif
                            value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Icon</th>
                            <th>Tiêu đề</th>
                            <th>Thể loại</th>
                            <th>Phone</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($maps as $row)
                        <tr role="row" class="odd">
                            <td>{!! Helper::indexPaginate($maps,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$row->icon}}">
                            </td>
                            <td>{{$row->title}}</td>
                            <td>{{optional(optional(optional($row))->category)->name}}</td>
                            <td>{{$row->phone}}</td>
                            <td>
                                <div class=" w-100">
                                    <a type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('map_delete',['id'=>$row->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <form action="{{route('map_edit',['id'=>$row->id])}}" method="GET">
                                        <button class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </button>
                                        <input type="hidden" name="url" value="{{url()->full()}}">
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $maps->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection