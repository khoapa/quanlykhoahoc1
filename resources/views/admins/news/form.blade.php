<div class="form-row">
    <div class="col mb-6">
        <div class="form-group {{ $errors->has('title') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="title">Tiêu đề (*)</label>
            <input type="text" class="form-control" id="title" name="title" maxlength="255"
                value="{{ old('title', optional(optional($news))->title) }}" placeholder="Tiêu đề" required>
            {!! $errors->first('title', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('link') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="link">Link </label>
            <input type="text" class="form-control" name="link" maxlength="255"
                value="{{ old('link', optional(optional($news))->link)}}" placeholder="Link">
            {!! $errors->first('link', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col mb-6">
        <div class="form-group">
            <label for="news_category_id">Thể loại (*)</label>
            <select required class="custom-select {{ $errors->has('news_category_id') ? 'is-invalid' : '' }}"
                name="news_category_id">
                <option value="">--Chọn thể loại tin tức--</option>
                @foreach($categories as $category)
                <option @if($category->id == old('news_category_id', optional(optional($news))->news_category_id))
                    selected = "selected"
                    @endif
                    value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('news_category_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('content') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
    <label for="content">Nội dung (*)</label>
    <textarea class="form-control" name="content" type="text" id="editor"
        required>{{ old('content', optional(optional($news))->content)}}</textarea>
    {!! $errors->first('content', '<p class="invalid-feedback">:message</p>') !!}
</div>
<div class="form-group">
    <label for="tag_ids">Hastag </label>
    <?php $arrTags = old('tag_ids');
        $arrNew = [];
    if(!is_null($arrTags)){
        foreach($arrTags as $arr){
            if(!in_array($arr,$tagArr->toArray())){
                array_push($arrNew,$arr);
            }
        }
    }

            if($errors->any()){
                $tagsNews = null;
            }   ?>
    <select id="tag_ids" class="tag_id form-control" name="tag_ids[]" multiple="multiple">
        @foreach($tags as $tag)
        <option @if(!is_null($tagsNews)) @if(in_array($tag->id,$tagsNews))
            selected="selected"
            @endif
            @endif
            @if(!is_null($arrTags)) @foreach($arrTags as $arr) @if($tag->id==$arr)
            selected
            @endif
            @endforeach
            @endif
            value="{{$tag->id}}">{{$tag->name}}
        </option>
        @endforeach
        @empty(!$arrNew)
        @foreach($arrNew as $arr)
        <option selected value="{{$arr}}">{{$arr}}
        </option>
        @endforeach
        @endempty
    </select>
</div>
<div class="row">
    <div class="col mb-6">
        <div class="form-group {{ $errors->has('thumbnail') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="thumbnail">Hình ảnh (*)</label>
            <div class="col-md-3 block-single-upload">
                {!! Helper::uploadSingleImage( old('thumbnail', optional($news)->thumbnail), 'thumbnail') !!}
            </div>
            {!! $errors->first('thumbnail', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>

    <div class="col mb-6">
        <div class="form-group">
            <label>Hiển thị </label>
            @php
            $priority = old('priority', optional(optional($news))->priority);
            $status = old('status', optional(optional($news))->status);
            @endphp
            <label class="customcheck">Hiển thị
                <input type="hidden" name="status" value="2">
                <input class="form-check-input" {!! Helper::setChecked($status, 1) !!} name="status" type="checkbox"
                    value="1" id="defaultCheck0">
                <span class="checkmark"></span>
            </label>
            <label class="customcheck">Tin ở Slide
                <input type="hidden" name="priority" value="2">
                <input class="form-check-input" {!! Helper::setChecked($priority, 1) !!} name="priority" type="checkbox"
                    value="1" id="defaultCheck1">
                <span class="checkmark"></span>
            </label>
        </div>
    </div>
</div>
@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.tag_id').select2({
        placeholder: "Chọn hastag",
        tags: true
    });
});
$(document).ready(function() {
    $('#editor').each(function(e) {
        CKEDITOR.replace(this.id, {
            height: 350,
            filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        })
    });
})
</script>

@endsection