@extends('admin')
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh sách câu trả lời
                </div>
            </div>
            <div class="page-title-actions">
            <a href="{{ route('question.question.index',$question->course->id) }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                <a href="{{ route('answer.answer.create',$id) }}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i
                        class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Tạo câu trả lời</a>
            </div>
        </div>
    </div>
    <div class="card-body card-custom">
        <h5 class="card-title">Chi tiết câu hỏi</h5>
        <div class="form-group">
            <div class="custom-control">
                <div>
                    <p><span class="name-question card-title">Tên</span> : <span
                            class="detail-question">{{$question->subject}}</span> </p>
                    <p><span class="name-question card-title">Khóa học</span> : <span
                            class="detail-question">{{$question->course->name}}</span> </p>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Câu trả lời</th>
                            <th>Đáp án đúng</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($answers as $answer)
                        <tr>
                            <td>{!! Helper::indexPaginate($answers,$loop) !!}</td>
                            <td>{{$answer->answer}}</td>
                            <td class="center">
                                @if($answer->correct == 1)
                                <i class="fa fa-check check-answer"></i>
                                @endif
                            </td>
                            <td>
                                <form method="POST" action="{{route('answer.answer.destroy',$answer->id)}}"
                                    accept-charset="UTF-8">
                                    <input name="_method" value="DELETE" type="hidden">
                                    {{ csrf_field() }}
                                    <div class=" w-100">
                                        <button type="submit"
                                            class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning"
                                            title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                            <span class="pe-7s-trash" aria-hidden="true"></span>
                                        </button>
                                        <a href="{{route('answer.answer.edit',$answer->id)}}"
                                            class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary"
                                            title="Sửa">
                                            <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $answers->links()}}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection