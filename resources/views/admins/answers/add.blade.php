@extends('admin')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Thêm Mới Câu Trả Lời</h5>
            </div>
        </div>
    </div>
</div>
<div class="main-card card main-card-custom">
    <div class="card-body">
        <form action="{{route('answer.answer.store',$id) }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="description" class="control-label">Câu trả lời</label>
                <textarea rows="4" class="form-control" rows="6" name="answer[]" required></textarea>
            </div>
            <div class="form-group">
                <label>Đáp án đúng</label>
                <label class="customcheck">Đáp án đúng
                    <input type="hidden" name="correct[]" value="0">
                    <input class="form-check-input" name="correct[]" type="checkbox" value="1">
                    <span class="checkmark"></span>
                </label>
            </div>
            <div id="add-answer">
            </div>
            <div class="form-group">
            <a href="{{ route('answer.answer.index',$id) }}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Quay lại</a>
                <button type="submit" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i
                        class="mr-2 pe-7s-check btn-icon-wrapper"> </i>Lưu</button>
                <button class="add-answer mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i
                        class="mr-2 pe-7s-check btn-icon-wrapper"> </i>Thêm câu trả lời</button>
            </div>
        </form>
    </div>
    @endsection