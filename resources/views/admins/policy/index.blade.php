@extends('admin')
@section('content')
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Cập Nhập Thông Tin Chính sách</h5>
            </div>
        </div>
    </div>
</div>
<div class="main-card card main-card-custom">
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card-body">
        <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('policy.update',['id'=>$policy->id])}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{ csrf_field() }}
            <div class="form-row mb-2">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('title') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
                        <label for="firstname">Chính sách (Policy) (*)</label>
                        <input type="text" class="form-control" name="title" maxlength="255" require value="{{  old('title', optional(optional($policy))->title)}}" required></input>
                        {!! $errors->first('title', '<p class="invalid-feedback">:message</p>') !!}
                    </div>
                </div>
            </div>

            <div class="form-group {{ $errors->has('content') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
                <div>
                    <label for="content">Nội dung (*)</label>
                    <textarea class="form-control" name="content" type="text" id="editor">{{ old('content', optional(optional($policy))->content) }}</textarea>
                    {!! $errors->first('content', '<p class="invalid-feedback1">:message</p>') !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Cập nhập</button>
                </div>
            </div>
        </form>
    </div>
</div>

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    $(document).ready(function() {
        // CKEDITOR.replace('content');
        $('#editor').each(function(e) {
            CKEDITOR.replace(this.id, {
                filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            })
        });
    })
</script>
@endsection
@endsection