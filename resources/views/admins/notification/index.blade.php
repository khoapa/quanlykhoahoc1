@extends('admin')
@section('title','Notification')
@section('content')
<link href="{{ asset('admins/assets/css/bootree.css') }}" rel="stylesheet">
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" /> -->
<link href="{{ asset('admins/assets/css/bootree.min.css') }}" rel="stylesheet">
<div class="app-page-title app-page-title-custom">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-plus icon-gradient bg-happy-itmeo">
                </i>
            </div>
            <div>
                <h5>Thông Báo Mới</h5>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
        <div class="alert alert-success main-card-custom">
            {!! session('message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
<div class="main-card card main-card-custom">
    <div class="card-body">

        <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('push.notification.general')}}"
            accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form"
            class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{csrf_field()}}
            <input type="hidden" name="schools" id="schools" value="{{$arrSchool}}">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div id="tree"></div>

                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body" id="click">
                            <h5 class="text-center">Danh sách user</h5>
                            <div class="pull-right">
                                <input type="checkbox" id="checkStudent" value="1"><span class="ml-1">Học sinh</span>
                                <input type="checkbox" id="checkTeacher" value="1"><span class="ml-1">Giáo viên</span>
                            </div>
                            <table class="mb-0 table table-striped">
                                <thead id="custom">
                                    <tr>
                                        <th><input type="checkbox" id="check_all" value="1"></th>
                                        <th>Tên</th>
                                        <th>Mã Trường</th>
                                        <th>Mã Lớp</th>
                                        <th>Loại</th>
                                    </tr>
                                </thead>
                                <tbody id="list_user">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="mt-3 pl-2">
                    <label for="sel1">Nội dung (*)</label>
                    <textarea class="form-control text-notification" name="content" type="text" id="test"
                        required></textarea>
                </div>
                <div id="hidden"></div>
                <div class="mt-2">
                    <div class="col-md-offset-2 float-right">
                        <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success" id="btn-notifi"><i
                                class="pe-7s-check btn-icon-wrapper"> </i>Gửi</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


@endsection
@section('notification')
<script type="text/javascript" src="{{ asset('admins/assets/scripts/bootree.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('admins/assets/scripts/notification.js') }}"></script>
@endsection