@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Kết Quả Học Tập<br>
                    <h8 class="text">Bảng điểm và chấm điểm</h8>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-12 pl-0">
        <div class="mb-3 card">
            <div class="card-body">
                <div class="tab-content">
                <form enctype="multipart/form-data" method="get" novalidate="" action="{{route('transcript.user')}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-row mb-3">
                            <div class="col-md-3">
                                <h6 class="timeline-title">Chọn thông tin cần xem :</h6>
                            </div>
                            @hasanyrole('Admin')    
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="school" id="school_id1" required class="custom-select {{ $errors->has('school') ? 'is-invalid' : '' }}">
                                    @hasanyrole('Admin')    
                                    <option value="">--Tất cả trường--</option>
                                    @endhasrole
                                        @foreach($schools as $school)
                                        <option @if(isset($school_id) && $school->id == $school_id)
                                            selected = "selected"
                                            @endif
                                            value="{{$school->id}}">{{$school->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('school', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            @endhasrole
                            @hasanyrole('Admin|school')
                            <div class="col-md-3">
                                <div class="form-group">
                                    <select name="class" id="class_id" required class="custom-select {{ $errors->has('class') ? 'is-invalid' : '' }}">
                                    @hasanyrole('Admin|school')    
                                    <option value="">--Tất cả lớp-</option>
                                    @endhasrole
                                    
                                        @foreach($classes as $value)
                                        <option 
                                        @if(isset($class_id))
                                        @if($value->id == $class_id)
                                            selected="selected"
                                            @endif
                                            @endif
                                            value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('class', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            @endhasrole

                        </div>
                        <div class="form-row">
                            <div class="col mb-3">
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="category" id="category_id" required class="custom-select {{ $errors->has('category') ? 'is-invalid' : '' }}">
                                        <option value="">--Tất cả danh mục khóa học--</option>
                                        @foreach($categoryCourses as $category)
                                        <option @if(isset($category_id) && $category->id == $category_id)
                                            selected = "selected"
                                            @endif
                                            value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('category', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div class="form-group">
                                    <select name="course" id="course_id" required class="custom-select {{ $errors->has('course') ? 'is-invalid' : '' }}">
                                        <option value="">--Tất cả khóa học--</option>
                                        @if(isset($courses))
                                        @foreach($courses as $value)course
                                        <option @if($value->id == $course_id)
                                            selected="selected"
                                            @endif
                                            value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    {!! $errors->first('course', '<p class="invalid-feedback">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col mb-3">
                                <div id="export-click-transcrip" class="mt-0 btn-icon btn btn-success export export-sata"><i class="fa fa-download pr-1" aria-hidden="true"></i>Export</div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-3">
                                @php
                                $date_begin = request()->get('date_begin');
                                @endphp
                                <input autocomplete="off" class="form-control" name="date_begin" type="text" id="datepicker-begin"
                                    placeholder="Chọn ngày bắt đầu" value="{!! Helper::formatDateNew($date_begin) !!}"
                                    maxlength="255">
                            </div>
                            <div class="col-md-3">
                                @php
                                $date_end = request()->get('date_end');
                                @endphp
                                <input autocomplete="off" class="form-control" name="date_end" type="text" id="datepicker-end"
                                    placeholder="Chọn ngày kết thúc" value="{!! Helper::formatDateNew($date_end) !!}"
                                    maxlength="255">
                                @if(!is_null($errorDate))
                                <p class="invalid-feedback check-valid">{{$errorDate}}</p>
                                @endif
                            </div>
                            <div class="col mb-3">
                                <button class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Xem bảng điểm</button>
                                @if(isset($backUrl))
                                <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                                @endif
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên học sinh</th>
                            <th>Trường</th>
                            <th>Lớp</th>
                            <th>Khóa học</th>
                            <th>Tham gia</th>
                            <th>Kết thúc</th>
                            <th>Điểm trắc nghiệm</th>
                            <th>Điểm tự luận</th>
                            <th>Tổng điểm</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!is_null($usersCourse))
                        @foreach($usersCourse as $key => $user)
                        <tr>
                            <td>{!! Helper::indexPaginate($usersCourse,$loop) !!}</td>
                            <td>{{optional($user->student)->name}}</td>
                            <td>{{optional(optional($user->student)->school)->name}}</td>
                            <td>{{optional(optional($user->student)->classes)->name}}</td>
                            <td>{{optional($user->course)->name}}</td>
                            <td>{{date("d-m-Y", strtotime($user->join_date))}}</td>
                            <td>@if($user->status==2 && $user->date_end != null){{date("d-m-Y", strtotime($user->date_end))}}@endif</td>
                            <td>{{round($user->count_point_choose,2)}}</td>
                            <td>
                                @if($user->count_point_essay == 0)
                                --
                                @else
                                {{ $user->count_point_essay}}
                                @endif
                            </td>
                            <td>
                                {{($user->count_point_choose*$user->course->total_point_choice+
                                $user->count_point_essay*($user->course->total_point - $user->course->total_point_choice))/100}}
                            </td>
                            <td>
                                <div class=" w-100">
                                    <a href="{{route('transcript.user.delete',['id'=>$user->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    @if($user->status == 2)
                                    <form action="{{route('transcript.mark',['user_id'=>$user->student->id,'course_user_id'=>$user->id])}}" method="GET" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <button title="Chấm điểm" type="submit" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary">
                                            <i class="fas fa-pen"></i>
                                        </button>
                                        <input type="hidden" name="url" value="{{url()->full()}}">
                                    </form>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>

            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $usersCourse->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection