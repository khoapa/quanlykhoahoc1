@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    BÀI LÀM CỦA HỌC SINH
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <div class="mb-3 card">
        <form enctype="multipart/form-data" method="POST" novalidate=""  action="{{route('transcript.mark.store',['id'=>$course_id])}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
            {{csrf_field()}}
            <div class="card-body">

                <h6 class="font-weight-bold">TỔNG ĐIỂM :
                    @if($totalPoint >= 50)
                    <span class="green">{{$totalPoint}}%</span>
                    @else
                    <span class="red">{{$totalPoint}}%</span>
                    @endif
                </h6>
                <h6 class="timeline-title">I.Phần trắc nghiệm ({{$percentageChoice}}%)</h6>
                <div class="row">
                    @foreach($questionsChoose as $key=>$question)
                    <div class="col-md-3">
                        <p class="font-stt">Câu {{$key+1}} .
                            @if ($question->point==0)
                            <i class="fas fa-times"></i>
                            @else
                            <i class="fas fa-check"></i>
                            @endif
                        </p>
                    </div>
                    @endforeach
                </div>
                <p><span class="font-stt">Bạn đã đạt : </span>
                    @if($sumPointChoose >= 50)
                    <span class="green">{{$sumPointChoose}}%</span>
                    @else
                    <span class="red">{{$sumPointChoose}}%</span>
                    @endif
                </p>
                <h6 class="timeline-title">II.Phần tự luận ({{100-$percentageChoice}}%)</h6>
                @foreach($questionsEssay as $key=>$question)
                <div class="row">
                    <div class="col-1 font-stt">Câu {{$key+1}} .</div>
                    <div class="col-11 question">{{$question->question->subject}}</div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-11">
                        <div class="answer">
                            Trả lời : {{$question->answer}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-11">
                        <label for="{{$question->id}}" class="font-weight-bold">Điểm thang 100 :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="form-group col-11">
                        <input name="mark[{{$question->id}}]" id="mark" class="form control col-md-1 input-point" type="number" min="0" max="100" value="{{(optional(optional($question))->point100)}}" required>
                        <span class="text-danger">{{ $errors->first('mark.'.($question->id)) }}</span>
                    </div>
                </div>
                @endforeach
                <p><span class="font-stt">Bạn đã đạt : </span>
                    @if($sumPointEssay >= 50)
                    <span class="green">{{$sumPointEssay}}%</span>
                    @else
                    <span class="red">{{$sumPointEssay}}%</span>
                    @endif
                </p>
                <div class="form-group">
                    <div class="">
                        <a href="{{$backUrl}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                        <button type="submit" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Chấm điểm</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection