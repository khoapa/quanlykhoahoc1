@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Danh Sách Kỹ Năng Sống Lý Thuyết
                </div>
            </div>
            <div class="page-title-actions">
                <a href="{{ route('course_theory.create') }}" class="mb-2 mr-2 btn-icon btn btn-success" href=""><i
                        class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới kỹ năng sống</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row">
            <div class="col-md-3">
                <input type="search" value="<?php echo request()->get('search') ?>" class="form-control search-input" id="search_name" name="search" placeholder="Tên kỹ năng sống" aria-controls="example">
                <button class="btn btn-info search"><i class="fa fa-search"></i></button>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="search_category" name="category_id">
                        <option value="">--Chọn nhóm kỹ năng sống--</option>
                        @foreach($categories as $category)
                        <option @if($category->id == request()->category_id)
                            selected = "selected"
                            @endif
                            value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="search_author" name="author">
                        <option value="">--Chọn người tạo--</option>
                        @foreach($authors as $value)
                        <option 
                        @if($value->author == request()->author)
                            selected = "selected"
                            @endif
                            value="{{$value->author}}">{{$value->authorUser->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Hình ảnh</th>
                            <th>Tên kỹ năng sống</th>
                            <th>Nhóm kỹ năng</th>
                            <th>Thời gian bắt đầu</th>
                            @can('edit_course_theory')
                            <th>
                                <input value="1" id="selectAllCourseTheory" @if($statusAllCourse==1) checked @endif type="checkbox">
                                Hiển thị
                            </th>
                            @endcan
                            <th>Người tạo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($courseTheories as $course)
                        <tr>
                            <td>{!! Helper::indexPaginate($courseTheories,$loop) !!}</td>
                            <td class="image-cate">
                                <img class="rounded-circle" src="{{$course->image}}" alt="">
                            </td>
                            <td>{{$course->name}}</td>
                            <td>{{$course->category->name}}</td>
                            <td>{{date("d-m-Y", strtotime($course->start_date))}}</td>
                            @can('edit_course_theory')
                            <td>
                                <label class="pd-toggle-switch">
                                    @if($course->status == 1)
                                    <input type="checkbox" name='status[]' class="check" checked onclick="addStatusCourseTheory('0','{{$course->id}}')" value="{{$course->id}}">
                                    @else
                                    <input type="checkbox" name='status[]' class="check" onclick="addStatusCourseTheory('1','{{$course->id}}')" value="{{$course->id}}">
                                    @endif
                                </label>
                            </td>
                            @endcan
                            <td>{{$course->authorUser->name}}</td>
                            <td>
                            <?php
                             $user = auth()->user();
                            ?>
                            @if($user->hasRole('Admin') || $course->authorUser->id == $user->id)
                                <div class="w-100">

                                    <a class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning "
                                        href="{{route('course_theory.destroy',$course->id)}}" title="Xóa"
                                        onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <form action="{{route('course_theory.edit',['id'=>$course->id])}}" method="GET">
                                        <button class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                        <span class="pe-7s-tools" aria-hidden="true"></span>
                                        </button>
                                        <input type="hidden" name="url" value="{{url()->full()}}">
                                    </form>
                                </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $courseTheories->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection