<div class="form-group">
    <div class="form-row mb-2">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('name') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
                <label for="firstname">Tên Khóa Học (*)</label>
                <div>
                    <input type="text" value="{{ old('name', optional(optional($course))->name) }}" class="form-control"
                        id="name" name="name" required>
                    {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('course_category_id') ? 'was-validated ' : '' }}">
                <label for="sel1">Danh mục khóa học (*)</label>
                <select required class="custom-select {{ $errors->has('course_category_id') ? 'is-invalid' : '' }}"
                    name="course_category_id" id="course_category_id">

                    <option value="">--Chọn danh mục khóa học--</option>
                    @foreach($categoryCourses as $cate)
                    <option value="{{$cate->id}}" @if($cate->id == old('course_category_id',
                        optional(optional($course))->course_category_id))
                        selected = "selected"
                        @endif
                        >{{$cate->name}}</option>
                    @endforeach
                </select>
                {!! $errors->first('course_category_id', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="form-row mb-2">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('') ? 'was-validated ' : '' }}">
                <label for="sel1">Khóa học thực hành liên quan</label>
                <select class="custom-select {{ $errors->has('course_category_id') ? 'is-invalid' : '' }}"
                    name="course_id" id="course_id">
                    <option value="">--Chọn khóa học thực hành--</option>
                    @if(!is_null($course))
                    @foreach($coursePractice as $value)
                    <option @if($value->id == old('course_id', optional(optional($course))->course_id))
                        selected = "selected"
                        @endif
                        value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                    @endif
                </select>
                {!! $errors->first('', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="form-group {{ $errors->has('content') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
        <label for="content">Nội dung (*)</label>
        <textarea class="form-control" name="content" type="text" cols="8" id="editor"
            required>{{ old('content', optional(optional($course))->content)}}</textarea>
        {!! $errors->first('content', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="form-group">
        <label for="tag_ids">Hastag </label>
        <?php $arrTags = old('course_tag_ids');
            $arrNew = [];
            if(!is_null($arrTags)){
                foreach($arrTags as $arr){
                    if(!in_array($arr,$tagArr->toArray())){
                        array_push($arrNew,$arr);
                    }
                }
            }
            if($errors->any()){
                $tagsCourse = null;
            }  
             ?>
        <select id="tag_ids" class="tag_id form-control" name="course_tag_ids[]" multiple="multiple">

            @foreach($tags as $tag)
            <option @if(!is_null($tagsCourse)) @if(in_array($tag->id,$tagsCourse))
                selected="selected"
                @endif
                @endif
                @if(!is_null($arrTags)) @foreach($arrTags as $arr) @if($tag->id==$arr)
                selected
                @endif
                @endforeach
                @endif
                value="{{$tag->id}}">{{$tag->name}}
            </option>
            @endforeach
            @empty(!$arrNew)
            @foreach($arrNew as $arr)
            <option selected value="{{$arr}}">{{$arr}}
            </option>
            @endforeach
            @endempty
        </select>
    </div>
    <div class="row">
        <div class="col mb-6">
            <div class="form-group {{ $errors->has('image') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
                <label for="avatar" class="control-label">Hình ảnh (*)</label>
                <div class="col-md-3 block-single-upload">
                    {!! Helper::uploadSingleImage( old('cover_image', optional($course)->image), 'image') !!}
                </div>
                {!! $errors->first('image', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
        <div class="col mb-6">

            <div class="form-group">
                <label>Hiển thị </label>
                @php
                $new = old('new', optional(optional($course))->new);
                $offer = old('offer', optional(optional($course))->offer);
                $status = old('status', optional(optional($course))->status);
                @endphp
                <label class="customcheck">Hiển thị
                    <input type="hidden" name="status" value="0">
                    <input class="form-check-input" {!! Helper::setChecked($status, 1) !!} name="status" type="checkbox"
                        value="1" id="defaultCheck0">
                    <span class="checkmark"></span>
                </label>
                <label class="customcheck">Khóa học mới
                    <input type="hidden" name="new" value="0">
                    <input class="form-check-input" {!! Helper::setChecked($new, 1) !!} name="new" type="checkbox"
                        value="1" id="defaultCheck1">
                    <span class="checkmark"></span>
                </label>

                <label class="customcheck">Khóa học đề xuất
                    <input type="hidden" name="offer" value="0">
                    <input class="form-check-input" {!! Helper::setChecked($offer, 1) !!} name="offer" type="checkbox"
                        value="1" id="defaultCheck2">
                    <span class="checkmark"></span>
                </label>

            </div>
        </div>

    </div>
    @section('script')
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.tag_id').select2({
            placeholder: " Chọn hastag",
            tags: true
        });
    });
    $(document).ready(function() {
        $('#editor').each(function(e) {
            CKEDITOR.replace(this.id, {
                height: 350,
                filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            })
        });
    })
    </script>
    @endsection