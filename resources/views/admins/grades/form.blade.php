<div class="form-row">
    <div class="col mb-6">
        <div class="form-group">
            <label for="name">Tên khối học (*)</label>
            <input type="text" maxlength="255" value="{{ old('name', optional(optional($grade))->name) }}" class="form-control" maxlength="255" id="name" name="name" placeholder="Tên khối học" required>
            {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col mb-6">
        <div class="form-group {{ $errors->has('grade_code') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="grade_code">Mã khối học (*)</label>
            <input type="text" maxlength="255" value="{{ old('grade_code', optional(optional($grade))->grade_code) }}" class="form-control" maxlength="255" id="grade_code" name="grade_code" placeholder="Mã khối học" required>
            {!! $errors->first('grade_code', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('thumbnail') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
    <label for="thumbnail">Ảnh khối học (*)</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('thumbnail', optional($grade)->thumbnail), 'thumbnail') !!}
    </div>
    {!! $errors->first('thumbnail', '<p class="invalid-feedback">:message</p>') !!}
</div>
<br>