@extends('admin')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.slim.js"></script>
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>
                    Chatting
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="card mb-3">
        <div class="main-card card">
            <?php
            $idUser = auth()->user()->id;
            ?>
            <input type="hidden" id="id_user" value="{{$idUser}}" />
            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <div class="card-hover-shadow-2x mb-3 card">
                    <form action="" method="GET" >
                        <div class="card-header-tab card-header custom-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            <input type="search" value="<?php echo request()->get('search')  ?>" class="form-control search-chat" name="search" placeholder="Tên...." aria-controls="example">
                            <button class="btn btn-primary ml-2">Tìm</button>    
                        </div>
                        </div>
                    </form>
                        <div class="scroll-area-lg">
                            <div class="scrollbar-container ps ps--active-y">
                                <div class="p-2">
                                    <ul class="todo-list-wrapper list-group list-group-flush">
                                        <li class="list-group-item">
                                            <div class="todo-indicator bg-success"></div>
                                            <span id="list-chat"></span>
                                            @foreach($listConversionsFirst as $conversion)

                                            <div class="widget-content conversion p-0" id="conversion-{{$conversion->id}}">
                                                <div class="widget-content-wrapper                                                     <?php
                                                     if($conversion->message->isNotEmpty()){
                                                         if($conversion->message->last()->sender_id != $idUser && $conversion->message->last()->status == 1){
                                                            echo "no-send-list";
                                                         }
                                                     }
                                                    ?>">
                                                    <div class="widget-content-left ">
                                                        <div class="custom-checkbox">
                                                            <a href="{{route('chat.chat.index',$conversion->id)}}">
                                                                <div class="avatar-icon avatar-icon-lg rounded">
                                                                    <img src="{{$conversion->user2->avatar}}" alt="">
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <a href="{{route('chat.chat.index',$conversion->id)}}">
                                                        <div class="widget-content-left flex2 padding-chat">
                                                            <div class="widget-heading">{{$conversion->user2->name}}
                                                            </div>
                                                            <div class="widget-subheading" id="message-convertion-{{$conversion->id}}">
                                                                @if($conversion->message->isNotEmpty())
                                                                @if($conversion->message->last()->text)
                                                                {{$conversion->message->last()->text}}
                                                                @else
                                                                <img class="img-chat" src="{{$conversion->message->last()->images}}" />
                                                                @endif
                                                                @else
                                                                Chưa có tin nhắn
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </a>
<!-- 
                                                    <div class="widget-content-right">
                                                        <a href="{{route('chat.chat.delete_conversion',$conversion->id)}}"><i class="fa fa-trash-alt"></i></a>
                                                    </div> -->
                                                </div>
                                            </div>
                                            @endforeach
                                            @foreach($listConversionsTwo as $conversion)
                                            <div class="widget-content p-0 conversion" id="conversion-{{$conversion->id}}">
                                                <div class="widget-content-wrapper
                                                    <?php
                                                     if($conversion->message->isNotEmpty()){
                                                         if($conversion->message->last()->sender_id != $idUser && $conversion->message->last()->status == 1){
                                                            echo "no-send-list";
                                                         }
                                                     }
                                                    ?>">
                                                    <div class="widget-content-left">
                                                        <div class="custom-checkbox">
                                                            <a href="{{route('chat.chat.index',$conversion->id)}}">
                                                                <div class="avatar-icon avatar-icon-lg rounded">
                                                                    <img src="{{$conversion->user1->avatar}}" alt="">
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <a href="{{route('chat.chat.index',$conversion->id)}}">
                                                        <div class="widget-content-left flex2 padding-chat">
                                                            <div class="widget-heading">{{$conversion->user1->name}}
                                                            </div>
                                                            <div class="widget-subheading" id="message-convertion-{{$conversion->id}}">
                                                                @if($conversion->message->isNotEmpty())
                                                                @if($conversion->message->last()->text)
                                                                {{$conversion->message->last()->text}}
                                                                @else
                                                                <img class="img-chat" src="{{$conversion->message->last()->images}}" />
                                                                @endif
                                                                @else
                                                                Chưa có tin nhắn
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <!-- <div class="widget-content-right">
                                                        <a onclick="return confirm('Bạn muốn xóa ?')" href="{{route('chat.chat.delete_conversion',$conversion->id)}}"><i class="fa fa-trash-alt"></i></a>
                                                    </div> -->
                                                </div>
                                            </div>
                                            @endforeach
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                @if(!is_null($conversonId))
                <div class="col-sm-12 col-lg-8">
                    <div class="card-hover-shadow-2x mb-3 card">
                        <div class="card-header-tab card-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                <i class="header-icon lnr-printer icon-gradient bg-ripe-malin"> </i>
                                {{$nameChat}}
                            </div>
                        </div>
                        <div class="scroll-area-lg">
                            <div class="scrollbar-container content-chat  ps ps--active-y">
                                <div class="p-2">
                                    <div class="chat-wrapper p-1">

                                        @if(!is_null($messages))
                                        @foreach($messages as $message)
                                        @if($idUser != $message->sender_id)
                                        <div class="chat-box-wrapper" id="mesage-{{$message->id}}">
                                            <div>
                                                <div class="avatar-icon-wrapper mr-1">
                                                    <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg">
                                                    </div>
                                                    <div class="avatar-icon avatar-icon-lg rounded">
                                                        <img src="{{$message->sender->avatar}}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="chat-box">
                                                    <div>
                                                        @if($message->text)
                                                        {{$message->text}}
                                                        @endif
                                                    </div>
                                                    @if($message->images)
                                                    <div>
                                                        <img onclick="viewImage(this)" src="{{$message->images}}" alt="" />
                                                    </div>
                                                    @endif
                                                </div>
                                                <small class="opacity-6">
                                                    <i class="fa fa-calendar-alt mr-1"></i>
                                                    {!! Helper::formatDateChat($message->created_at) !!}
                                                </small>
                                            </div>
                                            <!-- <div class="icon-del" onclick="deleteMessage(<?php echo $message->id ?>)">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </div> -->
                                        </div>
                                        @else
                                        <div class="float-right custom-block-send" id="mesage-{{$message->id}}">
                                            <div class="chat-box-wrapper">
                                                <div class="icon-del" onclick="deleteMessage(<?php echo $message->id ?>)">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                                <div>
                                                    <div class="chat-box">
                                                        <div>
                                                            @if($message->text)
                                                            {{$message->text}}
                                                            @endif
                                                        </div>
                                                        @if($message->images)
                                                        <div>
                                                            <img onclick="viewImage(this)" src="{{$message->images}}" alt="" />
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <small class="opacity-6">
                                                        <i class="fa fa-calendar-alt mr-1"></i>
                                                        {!! Helper::formatDateChat($message->created_at) !!}
                                                    </small>
                                                </div>
                                                <div>
                                                    <div class="avatar-icon-wrapper ml-1">
                                                        <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg">
                                                        </div>
                                                        <div class="avatar-icon avatar-icon-lg rounded">
                                                            <img src="{{$message->sender->avatar}}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        @endif
                                        <div id="messages">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form enctype="multipart/form-data" onSubmit="return false;" novalidate="" accept-charset="UTF-8" name="create_exhibition_form" id="send-message">

                            <input type="hidden" name="conversion_id" id="conversion_id" value="{{$conversonId}}" />
                            <input type="hidden" id="id_user" value="{{$idUser}}" />
                            <div class="card-footer card-custom">
                                <div class="image-box">
                                    <div class="item-image">
                                        <span class="remove-image" onclick="removeImage()">
                                            <i class="fas fa-times-circle"></i>
                                        </span>
                                        <img id="preview-image" src="upload_tmp/17112020142238/Artboard 1.png">
                                    </div>
                                </div>
                                <div class="input-group">
                                    <input aria-describedby="basic-addon2" name="text" id="text" placeholder="Nhập nội dung..." type="text" class="form-control-custom form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">
                                            <div class="custom-send">
                                                <span class="item-icon" title="Tải ảnh lên">
                                                    <i class="fas fa-image" id="btn-upload_image"></i>
                                                    <input type="hidden" id="image" name="image" />
                                                    <input type="file" name="file" class="upload_image" id="upload_image">
                                                </span>
                                                <button class="btn btn-send" onclick="sendMessage(<?php echo $user2Id; ?>)">
                                                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="modal fade show" id="show-image" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-modal="true">
    <div class="modal-dialog modal-ls">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" onclick="closeModel()">×</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="urlImage" src="" />
            </div>
        </div>
    </div>
</div>
@endsection
@section('chat')
<script>
    var LINKSERVER = '{{ env('LINKSERVER') }}';
    var PATH_SOCKET = '{{ env('PATH_SOCKET') }}';
</script>
<script type="text/javascript" src="{{ asset('admins/assets/scripts/chat.js') }}"></script>
@endsection