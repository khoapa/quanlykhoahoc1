@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Thống Kê Số Liệu</div>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="row mb-2">
        @hasanyrole('Admin')
        <div class="col-md-3">
                <div class="form-group">
                    <select class="custom-select" id="school_id_search" name="school_id">
                        <option value="">--Tất cả trường--</option>
                        @foreach($schoolSearchs as $school)
                        <option @if($school->id == request()->school_id)
                            selected = "selected"
                            @endif
                            value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endhasrole
            <div class="col-md-3">
                @php
                $date_begin =  request()->get('date_begin');
                @endphp
                <input class="form-control" autocomplete="off" name="date_begin" type="text" id="datepicker-begin" placeholder="Chọn ngày bắt đầu" value="{!! Helper::formatDateNew($date_begin) !!}" maxlength="255" >
            </div>
            <div class="col-md-3">
                @php
                $date_end =  request()->get('date_end');
                @endphp
                <input autocomplete="off" class="form-control" name="date_end" type="text" id="datepicker-end" placeholder="Chọn ngày kết thúc" value="{!! Helper::formatDateNew($date_end) !!}" maxlength="255">
                @if(!is_null($errorDate))  
                <input type="hidden" name="checkeror" id="checkerror" value="1">
                <p class="invalid-feedback check-valid">{{$errorDate}}</p>
                @endif
            </div>
            <div class="col-mb-3">
                                <button style="height: 38px;" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i
                                        class="pe-7s-check btn-icon-wrapper"> </i>Xem</button>
                            </div>
                <div id="export-click-statical" class="mb-1 btn-icon btn btn-success export"><i class="fa fa-download pr-1" aria-hidden="true"></i>Export</div>
        </div>
        
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">

                    <thead>
                        <tr role="row">
                            <th>STT</th>
                            <th>Trường</th>
                            <th>Lớp</th>
                            <th>Khóa học</th>
                            <th>Danh mục</th>
                            <th>Số HS đang tham gia</th>
                            <th>Số HS đã tham gia</th>
                            <th>Tổng số truy cập</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schools as $school)
                        <tr role="row" class="even">
                            <td>{!! Helper::indexPaginate($schools,$loop) !!}</td>
                            <td>{{$school->name}}</td>
                            <td>{{$school->classes->count()}}</td>
                            <td>{{$school->course->count()}}</td>
                            <td>{{$school->course_category_count}}</td>
                            <td>{{$school->getCourseUserJoin($dateBegin,$dateEnd)}}</td>
                            <td>{{$school->getCourseUserJoined($dateBegin,$dateEnd)}}</td>
                            <td>{{$school->getCourseUserVisit($dateBegin,$dateEnd)}}</td>
                            <td>
                                <div class=" w-100">
                                    <div class="btn-group">
                                        <button type="button"
                                            class="mb-2 btn-icon btn-pill btn btn-outline-success dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button> 
                                        <div class="dropdown-menu dropdown-menu-right">
                                        <form method="GET" action="{{route('school_course.index')}}" accept-charset="UTF-8" class="dropdown-item">
                                            {{ csrf_field() }}
                                                <input type="hidden" name="school_id" value="{{$school->id}}">
                                                <input type="hidden" name="date_begin" value="{{$dateBegin}}">
                                                <input type="hidden" name="date_end" value="{{$dateEnd}}">
                                                <input type="hidden" name="check" value="1">
                                                <input type="hidden" name="url" value="{{url()->full()}}">
                                                <button class="dropdown-item" >Xem theo khóa học </button>
                                        </form>
                                        <form method="GET" action="{{route('transcript.user')}}" accept-charset="UTF-8" class="dropdown-item">
                                            {{ csrf_field() }}
                                                <input type="hidden" name="school" value="{{$school->id}}">
                                                <input type="hidden" name="date_begin" value="{{$dateBegin}}">
                                                <input type="hidden" name="date_end" value="{{$dateEnd}}">
                                                <input type="hidden" name="url" value="{{url()->full()}}">
                                                <button class="dropdown-item" href="">Xem theo học sinh </button>
                                            
                                        </form>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $schools->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection