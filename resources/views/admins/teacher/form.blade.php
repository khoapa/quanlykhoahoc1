<div class="form-row mb-2">
    <div class="col-md-6 {{ $errors->has('name') ? 'was-validated-custom' : '' }}">
        <label for="firstname">Họ và tên (*)</label>
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional(optional($teacher))->name) }}" placeholder="Họ và tên" maxlength="255" required>
        {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="col-md-6 {{ $errors->has('birthday') ? 'was-validated-custom' : '' }}">
        <label for="firstname">Ngày sinh (*)</label>
        @php
        $date = old('birthday', optional(optional($teacher))->birthday);
        @endphp
        <input class="form-control" name="birthday" type="text" id="datepicker" value="{!! Helper::formatDate1($date) !!}" maxlength="255" required>
        {!! $errors->first('birthday', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="form-row mb-2">
    <div class="col-md-6 {{ $errors->has('email') ? 'was-validated-custom' : '' }}">
        <label for="birthday">Email (*)</label>
        <input class="form-control" name="email" type="email" id="email" value="{{ old('email', optional($teacher)->email) }}" maxlength="255" placeholder="Email" required>
        {!! $errors->first('email', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="col-md-6 {{ $errors->has('address') ? 'was-validated-custom' : '' }}">
        <label for="address">Địa chỉ (*)</label>
        <input class="form-control" name="address" type="text" id="address" value="{{ old('address', optional(optional($teacher))->address) }}" placeholder="Địa Chỉ" maxlength="255" placeholder="" required>
        {!! $errors->first('address', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>
<div class="form-row mb-2">
    <div class="col-md-6">
        <div class="form-group">
            <label for="sel1">Giới tính (*)</label>
            <select class="custom-select {{ $errors->has('gender') ? 'is-invalid' : '' }}" id="gender" name="gender">
                @php
                $gender = old('gender', optional(optional($teacher))->gender)
                @endphp
                <option value="">Giới tính</option>
                <option value="1" {!! Helper::setSelected($gender, 1) !!}>Nam</option>
                <option value="2" {!! Helper::setSelected($gender, 2) !!}>Nữ</option>
            </select>
            {!! $errors->first('gender', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6 {{ $errors->has('phone') ? 'was-validated-custom' : '' }}">
        <label for="phone">Số điện thoại (*)</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">+84</span>
            </div>
            <input aria-describedby="basic-addon1" class="form-control" name="phone" type="text" id="phone" value="{{ old('phone', optional(optional($teacher))->phone) }}" placeholder="Số điện thoại" required>
            {!! $errors->first('phone', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-row mb-2">
    <div class="col-md-6">
        <div class="form-group ">
            <label for="sel1">Trường (*)</label>
            <select class="custom-select {{ $errors->has('school_id') ? 'is-invalid' : '' }}" id="school_id1" name="school_id">
            @hasanyrole('Admin')
                <option value="">Chọn trường</option>
                @endhasrole
                @foreach($schools as $valueSchool)
                <option value="{{$valueSchool->id}}" @if($valueSchool->id == old('school_id',
                    optional(optional($teacher))->school_id))
                    selected = "selected"
                    @endif>{{$valueSchool->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('school_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <?php if(!is_null(old('school_id'))){
                $classes = App\Models\Classes::where('school_id',old('school_id'))->get();
                $roles =  App\Models\Role::where('name','!=','school')->whereNull('id_school')->orWhere('id_school',old('school_id'))->get();
            } ?>
    <div class="col-md-6">
        <div class="form-group">
            <label for="sel1">Lớp (*)</label>
            <select class="custom-select {{ $errors->has('class_id') ? 'is-invalid' : '' }}" id="class_id" name="class_id">
                <option value="">--Lớp--</option>
                @foreach($classes as $class)         
                <option @if($class->id == old('class_id', optional(optional($teacher))->class_id))
                    selected = "selected"
                    @endif
                    value="{{$class->id}}">{{$class->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('class_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-row mb-2">
<div class="col-md-6">
        <div class="form-group {{ $errors->has('role_id') ? 'was-validated ' : '' }}" id="form-role_id_school">
            <label for="sel1">Nhóm phân quyền</label>
            <select class="custom-select {{ $errors->has('role_id') ? 'is-invalid' : '' }}" id="role_id_school"
                name="role_id">
                @php
                $roleId = null;
                if($teacher){
                    $role =  optional(optional($teacher))->role()->first();
                    $roleId = $role ?  $role->role_id : null;
                }
                $oldRole = old('role_id',$roleId);
                if(is_null($oldRole)){
                    $oldRole = 4;
                }
                @endphp
                @foreach($roles as $role)
                <option value="{{$role->id}}"
                    @if($role->id == $oldRole)
                    selected = "selected"
                    @endif
                    >{{$role->description}}</option>
                @endforeach
            </select>
            {!! $errors->first('role_id', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
    <div class="col-md-6" >
        <div class="form-group">
            <label for="sel1">Loại</label>
            <select class="custom-select {{ $errors->has('user_type') ? 'is-invalid' : '' }}" id="user_type" name="user_type">
                @php
                $user_type = old('user_type', optional(optional($teacher))->user_type)
                @endphp
                <option value="2" {!! Helper::setSelected($user_type, 2) !!}>Giáo viên</option>
                <option value="1" {!! Helper::setSelected($user_type, 1) !!}>Học sinh</option>
            </select>
            {!! $errors->first('user_type', '<p class="invalid-feedback">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group mb-2">
    <label for="avatar" class="control-label">Hình ảnh</label>
    <div class="col-md-3 block-single-upload">
        {!! Helper::uploadSingleImage( old('avatar', optional($teacher)->avatar), 'avatar') !!}
        {!! $errors->first('avatar', '<p class="invalid-feedback">:message</p>') !!}
    </div>
</div>