@extends('admin')
@section('content')
<div class="container">
    <div class="main-body">
        <!-- /Breadcrumb -->
        <div class="row gutters-sm mt-4">
            <div class="col-md-4 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="{{$user->avatar}}" alt="Admin" class="rounded-circle" width="150" height="150">
                            <div class="mt-3">
                                <h4>{{$user->name}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    @if(Session::has('message'))
                    <div class="alert alert-success">
                        <span class="glyphicon glyphicon-ok"></span>
                        {!! session('message') !!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="close">
                            <span aria-hidden="true">&times;</span>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('profile_user.store_password',['id'=>$user->id])}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-4">
                                    <h6 class="mb-0">Mật khẩu cũ :</h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group {{ $errors->has('old_password') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
                                        <input aria-describedby="basic-addon4" id="old_password" name="old_password" class="form-control" type="password" value="{{ old('old_password')}}" require>
                                        <div onclick="showOldPassword()" class="input-group-append">
                                            <span class="input-group-text" id="basic-addon4">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </div>
                                        {!! $errors->first('old_password', '<p class="invalid-feedback">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <h6 class="mb-0">Mật khẩu mới :</h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group {{ $errors->has('password1') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
                                        <input aria-describedby="basic-addon3" id="password1" name="password1" class="form-control" type="password" value="{{ old('password1')}}" require>
                                        <div onclick="showPassword1()" class="input-group-append">
                                            <span class="input-group-text" id="basic-addon3">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </div>
                                        {!! $errors->first('password1', '<p class="invalid-feedback">:message</p>') !!}
                                    </div>

                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <h6 class="mb-0">Nhập lại mật khẩu mới :</h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group {{ $errors->has('password1_confirmation') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
                                        <input aria-describedby="basic-addon5" id="password2" name="password1_confirmation" class="form-control" type="password" value="{{ old('password1_confirmation')}}" require>
                                        <div onclick="showPassword2()" class="input-group-append">
                                            <span class="input-group-text" id="basic-addon5">
                                                <i class="fas fa-eye"></i>
                                            </span>
                                        </div>
                                        {!! $errors->first('password1_confirmation', '<p class="invalid-feedback">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-5">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-12">
                                        <a href="{{route('profile_user.index',['id'=>$user->id])}}" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-secondary"><i class="pe-7s-back btn-icon-wrapper"> </i>Trở lại</a>
                                        <button type="submit" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection