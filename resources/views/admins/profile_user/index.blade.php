@extends('admin')
@section('content')
<div class="container">
  <div class="main-body">
    <div class="row gutters-sm mt-4">
      <div class="col-md-4 mb-3">
        <div class="card">
          <div class="card-body">
            <div class="d-flex flex-column align-items-center text-center">
              <form enctype="multipart/form-data" method="POST" novalidate="" action="{{route('profile_user.store_avatar',['id'=>$user->id])}}" accept-charset="UTF-8" id="create_exhibition_form" name="create_exhibition_form" class="form-horizontal {{ $errors->any() ? 'was-validated' : '' }}">
                {{csrf_field()}}
                <div class="form-group {{ $errors->has('avatar') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
                  <div class="col-md-3 block-single-upload">
                    {!! Helper::uploadSingleImage( old('avatar', optional($user)->avatar), 'avatar') !!}
                  </div>
                  {!! $errors->first('avatar', '<p class="invalid-feedback">:message</p>') !!}
                </div>
                <div class="mt-3">
                  <h4>{{$user->name}}</h4>
                </div>
                <button type="submit" class="mb-2 mr-2 btn-icon btn-pill btn btn-outline-success"><i class="pe-7s-check btn-icon-wrapper"> </i>Cập nhật Avatar</button>
              </form>
            </div>
          </div>
        </div>
        <div class="mt-3">
          @if(Session::has('message'))
          <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="close">
              <span aria-hidden="true">&times;</span>
          </div>
          @endif
        </div>
      </div>
      <div class="col-md-8">
        <div class="card mb-3">
          <div class="card-body">
            <div class="mb-3">
              <h5 class="text-center">Thông tin cá nhân</h5>
            </div>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Họ và tên</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                {{$user->name}}
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Email</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                {{$user->email}}
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Địa chỉ</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                @if($user->admin_flg != 1)
                {{$user->address}}
                @endif
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Số điện thoại</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                @if($user->admin_flg != 1)
                {{$user->phone}}
                @endif
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Ngày sinh</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                @if($user->admin_flg != 1 && $user->user_type != 3)
                {{$user->birthday}}
                @endif
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Giới tính</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                @if($user->admin_flg != 1 && $user->user_type != 3)
                @if($user->gender == 2)
                Nữ
                @else
                Nam
                @endif
                @endif
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Trường</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                {{optional(optional($user->school))->name}}
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-sm-3">
                <h6 class="mb-0">Lớp</h6>
              </div>
              <div class="col-sm-9 text-secondary">
                {{optional(optional($user->classes))->name}}
              </div>
            </div>
            <hr>
            <a href="{{ route('profile_user.change_password',['id'=>$user->id]) }}" class="btn-icon btn btn-info float-right"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
              </i>Đổi mật khẩu</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection