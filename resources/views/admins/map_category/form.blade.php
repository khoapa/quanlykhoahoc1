<div class="form-row">
    <div class="col mb-6">
        <div class="form-group {{ $errors->has('name') ? 'was-validated-custom' : 'was-validated-custom-valid' }}">
            <label for="name">Thể loại điểm map (*)</label>
            <input type="text" class="form-control" id="name" maxlength="255" name="name" value="{{ old('name', optional(optional($category))->name) }}" placeholder="Thể loại điểm map" required>
            {!! $errors->first('name', '<p class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('icon') ? 'was-validated-custom' : 'was-validated-custom-valid'}}">
            <label for="icon" class="control-label">Icon (*)</label>
            <div class="col-md-3 block-single-upload">
                {!! Helper::uploadSingleImage( old('icon', optional($category)->icon), 'icon') !!}
                {!! $errors->first('icon', '<p class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col mb-6">
    </div>
</div>
