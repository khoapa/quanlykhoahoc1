@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Thông Tin Chung
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-animation">
        <div class="row">
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-night-fade">
                    <div class="widget-content-wrapper text-white custom-widget">
                        <div class="widget-content-left">
                            <div class="widget-heading">Tổng Số Học Sinh</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$students}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-arielle-smile">
                    <div class="widget-content-wrapper text-white custom-widget">
                        <div class="widget-content-left ">
                            <div class="widget-heading">Tổng Số Khóa Học Thực Hành</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$courses}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            @hasrole('Admin')
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-happy-green">
                    <div class="widget-content-wrapper text-white custom-widget">
                        <div class="widget-content-left">
                            <div class="widget-heading">Tổng Số Tin Tức</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$news}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            @endhasrole
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">DANH SÁCH KỸ NĂNG SỐNG THỰC HÀNH MỚI
                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <a class="btn btn-success" href="{{route('course.index')}}">Xem Tất Cả</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Ảnh</th>
                                    <th>Tên</th>
                                    <th>Danh mục</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listCourses as $listCourse)
                                <tr role="row" class="even">
                                    <td>{!! Helper::indexPaginate($listCourses,$loop) !!}</td>
                                    <td class="image-cate">
                                        <img class="rounded-circle" src="{{$listCourse->cover_image}}" alt="">
                                    </td>
                                    <td class="sorting_1 dtr-control">{{$listCourse->name}}</td>
                                    <td>
                                        {{$listCourse->category->name}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @hasrole('Admin')
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">DANH SÁCH TIN TỨC MỚI
                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <a class="btn btn-success" href="{{route('news_index')}}">Xem Tất Cả</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Ảnh</th>
                                    <th>Tiêu đề</th>
                                    <th>Thể loại</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listNews as $valueListNews)
                                <tr role="row" class="even">
                                    <td>{!! Helper::indexPaginate($listNews,$loop) !!}</td>
                                    <td class="image-cate">
                                        <img class="rounded-circle" src="{{$valueListNews->thumbnail}}" alt="">
                                    </td>
                                    <td class="sorting_1 dtr-control">{{$valueListNews->title}}</td>
                                    <td>
                                        {{optional(optional(optional($valueListNews))->category)->name}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endhasrole
        @hasanyrole('Admin|school')
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">DANH SÁCH NGƯỜI DÙNG MỚI
                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <a class="btn btn-success" href="{{route('user.index')}}">Xem Tất Cả</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Ảnh</th>
                                    <th>Họ Tên</th>
                                    <th>Lớp</th>
                                    <th>Trường</th>
                                    <th>Loại</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $user)
                                @php
                                $date=date_create($user->birthday);
                                @endphp
                                <tr role="row" class="even">
                                    <td>{{$key+1}}</td>
                                    <td class="image-cate">
                                        <img class="rounded-circle" src="{{$user->avatar}}" alt="">
                                    </td>
                                    <td class="sorting_1 dtr-control">{{$user->name}}</td>
                                    <td>{{!is_null($user->classes) ? $user->classes->name : ''}}</td>
                                    <td>{{!is_null($user->school) ? $user->school->name : ''}}</td>
                                    <td>@if($user->user_type == 1 )
                                        Học sinh
                                        @else
                                        Giáo viên
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endhasrole
    </div>
</div>
@endsection