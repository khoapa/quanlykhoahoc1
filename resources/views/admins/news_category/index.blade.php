@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div class="mr-2">
                    Danh Sách Thể Loại Tin Tức
                    <br>
                    <h8 class="text">Với tài khoản Admin, khi bạn muốn sắp xếp vị trí danh sách thể loại tin tức  thì vui lòng chạm chuột vào danh mục và di chuyển vị trí bạn muốn. </h8>
                </div>
            </div>
            <div class="page-title-actions">
            <a href="{{route('news_category_create')}}" class="mb-2 mr-2 btn-icon btn btn-success" id="btn_create"><i class="mr-1 pe-7s-plus btn-icon-wrapper">
                    </i>Thêm mới thể loại</a>
            </div>
        </div>
    </div>
    @if(Session::has('message'))
    <div class="alert alert-success">
        <span class="glyphicon glyphicon-ok"></span>
        {!! session('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>

    </div>
    @endif
    <form action="" method="GET" id="form-search">
        <div class="col mb-6">
            <div class="row">
                <div class="form-group">
                    <input type="search" value="<?php echo request()->get('search') ?>" class="form-control" name="search" placeholder="Tên thể loại tin tức" aria-controls="example">
                </div>
                <div class="form-group ml-2">
                    <button class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div class="main-card card">
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped " id="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>STT</th>
                            <th>Thể loại tin tức</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="tablecontents">
                    @foreach($categories as $cate)
                        <tr class="row1" data-id="{{ $cate->id }}">
                        <td class="pl-3"><i class="fa fa-sort"></i></td>
                            <td>{!! Helper::indexPaginate($categories,$loop) !!}</td>
                            <td>{{$cate->name}}</td>
                            <td>
                            <div class=" w-100">
                                    <a class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning " href="{{route('news_category_delete',['id'=>$cate->id])}}" title="Xóa" onclick="return confirm('Bạn muốn xóa ?')">
                                        <span class="pe-7s-trash" aria-hidden="true"></span>
                                    </a>
                                    <a href="{{route('news_category_edit',['id'=>$cate->id])}}" class="pull-right mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-primary" title="Sửa">
                                        <span class="pe-7s-tools" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            <div class="col-12">
                <div class="pagination pull-right">
                    {{ $categories->links()}}
                </div>
            </div>
        </div>

    </div>
</div>
<?php
$user =auth()->user();
$role = null;
if($user->hasRole('Admin')){
    $role = 1;
}
?>
<input type="hidden" id="checkrole" value="{{$role}}">
@endsection
@section('chat')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
<script type="text/javascript">
$(function() {
    let checkRole = '{{$role}}';
    if(checkRole == 1){
        $("#tablecontents").sortable({
        items: "tr",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            sendOrderToServer();
        }
    });
    }
    function sendOrderToServer() {
        var order = [];
        var token = $('meta[name="csrf-token"]').attr('content');
        $('tr.row1').each(function(index, element) {
            order.push({
                id: $(this).attr('data-id'),
                position: index + 1
            });
        });

        $.ajax({

            type: "POST",
            dataType: "json",
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            url: "admin/sort-table/cate/news",
            data: {
                order: order,
            },
            success: function(response) {
                console.log(response);
                if (response.status == "success") {
                    console.log(response);
                } else {
                    console.log(response);
                }
            }
        });
    }
});
</script>
@endsection