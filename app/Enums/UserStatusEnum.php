<?php

namespace App\Enums;

use Spatie\Enum\Enum;
/**
 * @method static self admin()
 * @method static self visitor()
 * @method static self exhibitor()
 */
final class UserStatusEnum extends Enum
{
    const ACTIVE = 1;
    const DISABLE = 2;

    public static function getDescription($value): string
    {
        if ($value === self::ACTIVE) {
            return 'Hoạt động';
        }
        if ($value === self::DISABLE) {
            return 'Vô hiệu hoá';
        }
        if ($value === null) {
            return 'Vô hiệu hoá';
        }
        return parent::getDescription($value);
    }
}