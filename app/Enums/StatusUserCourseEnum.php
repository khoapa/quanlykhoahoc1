<?php

namespace App\Enums;

use Spatie\Enum\Enum;
/**
 * @method static self admin()
 * @method static self visitor()
 * @method static self exhibitor()
 */
final class StatusUserCourseEnum extends Enum
{
    const COURSE_OFF = 0;
    const STUDYING = 1;
    const FINISH = 2;

    public static function getDescription($value): string
    {
        if ($value === self::COURSE_OFF) {
            return 'Khóa học đã Off';
        }
        if ($value === self::STUDYING) {
            return 'Đang học';
        }
        if ($value === self::FINISH) {
            return 'Đã học xong';
        }
        return parent::getDescription($value);
    }
}