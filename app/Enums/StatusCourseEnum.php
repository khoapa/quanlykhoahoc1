<?php

namespace App\Enums;

use Spatie\Enum\Enum;
/**
 * @method static self admin()
 * @method static self visitor()
 * @method static self exhibitor()
 */
final class StatusCourseEnum extends Enum
{
    const HIDE = 0;
    const SHOW = 1;
}