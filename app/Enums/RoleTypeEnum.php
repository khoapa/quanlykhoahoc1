<?php

namespace App\Enums;

use Spatie\Enum\Enum;
/**
 * @method static self admin()
 * @method static self visitor()
 * @method static self exhibitor()
 */
final class RoleTypeEnum extends Enum
{
    const ADMIN = 1;
    const SCHOOL = 2;
    const TEACHER = 3;
    const STUDENT = 4;

    public static function getDescription($value): string
    {
        if ($value === self::ADMIN) {
            return 'Quản trị viên';
        }
        if ($value === self::SCHOOL) {
            return 'Trường';
        }
        if ($value === self::TEACHER) {
            return 'Giáo viên';
        }
        if ($value === self::STUDENT) {
            return 'Học sinh';
        }
        return parent::getDescription($value);
    }
}