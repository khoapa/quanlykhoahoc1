<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self admin()
 * @method static self visitor()
 * @method static self exhibitor()
 */
final class UserTypeEnum extends Enum
{
    const STUDENT = 1;
    const TEACHER = 2;
    const STAFF = 3;
    const USER_TYPE_ARRAY = [
        ['name' => 'Học Sinh', 'id' => self::STUDENT],
        ['name' => 'Giáo Viên', 'id' => self::TEACHER],
        ['name' => 'Trường', 'id' => self::STAFF]
    ];
    public static function getDescription($value): string
    {
        if ($value === self::STUDENT) {
            return 'Học sinh';
        }
        if ($value === self::TEACHER) {
            return 'Giáo viên';
        }
        if ($value === self::STAFF) {
            return 'Trường';
        }
        if ($value === NULL) {
            return 'Không xác định';
        }
        return parent::getDescription($value);
    }
}
