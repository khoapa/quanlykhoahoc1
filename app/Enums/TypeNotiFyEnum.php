<?php

namespace App\Enums;

use Spatie\Enum\Enum;
/**
 * @method static self admin()
 * @method static self visitor()
 * @method static self exhibitor()
 */
final class TypeNotiFyEnum extends Enum
{
    const COURSE = 1;
    const NEWS = 2;
    const CONVERSATION = 3;
    const GENERAL = 4;
    public static function getDescription($value): string
    {
        if ($value === self::COURSE) {
            return 'Khóa học';
        }
        if ($value === self::NEWS) {
            return 'Tin tức';
        }
        if ($value === self::CONVERSATION) {
            return 'Chat';
        }
        if ($value === self::GENERAL) {
            return 'Chung';
        }
        return parent::getDescription($value);
    }
}