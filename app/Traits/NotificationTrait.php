<?php

namespace App\Traits;

use App\Http\Resources\Notification\NotificationResource;
use FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\Models\Notification;

trait NotificationTrait
{
    // NOTIFICATION FOR USER
    public function notifcationForUser($title, $body, $user)
    {
        $tokens = $user->device_token;
        $action = 'NOTIFICATION_USER';
        if (empty($tokens)) {
            return true;
        }

        // $html = view('admin.blocks.notify', compact('notify'))->render();

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 60);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)->setSound('default')->setClickAction($action);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['data' => $user]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
    }

    // ADMIN SEND NOTIFICATION
    public function notifcationSend($title, $content, $sender, $receiver, $type, $id_type)
    {
        if (is_null($sender) || is_null($receiver)) {
            return true;
        }
        $notify = $this->createNotifcation($title, $content, $sender, $receiver, $type, $id_type);
        $tokens = [];
        if (!is_null($receiver->device_token))
            $tokens = json_decode($receiver->device_token);

        foreach ($tokens as $token) {
            $action = 'NOTIFICATION';
            if (empty($token)) {
                return true;
            }
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 60);

            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($content)->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData(json_decode((new NotificationResource($notify))->toJson(), true));

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            if ($receiver->notification_status == 1) {
                $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
            }
        }
    }

    protected function createNotifcation($title, $content, $sender, $receiver, $type, $id_type)
    {

        return Notification::create([
            'sender_id' => $sender->id,
            'receiver_id' => $receiver->id,
            'title' => $title,
            'type' => $type,
            'content' => $content,
            'id_type' => $id_type
        ]);
    }
}
