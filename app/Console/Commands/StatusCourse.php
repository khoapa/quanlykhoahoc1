<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\UserCourse;
use Carbon\Carbon;
use App\Enums\StatusCourseEnum;
use App\Enums\StatusUserCourseEnum;

class StatusCourse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'status:course';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiểm tra trạng thái khóa học của người dùng';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userCourses = UserCourse::all();
        foreach($userCourses as $userCourse){
            $dateInvite = $userCourse->join_date;
            $courseBegin = $userCourse->course->start_date;
            $longtime = $userCourse->longtime;
            $converDateInvite = Carbon::parse($dateInvite);
            $converCourseBegin = Carbon::parse($courseBegin);   
            $daysub = Carbon::now('Asia/Ho_Chi_Minh')->subDays($longtime);
           if($converDateInvite->gt($converCourseBegin)){
              if(!$converDateInvite->gt($daysub)){
                $userCourse->update(['status'=>StatusUserCourseEnum::COURSE_OFF]);
              }
           }
        }
    }
}
