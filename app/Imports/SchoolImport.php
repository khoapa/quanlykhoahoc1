<?php

namespace App\Imports;

use App\Enums\UserTypeEnum;
use App\Models\ImportError;
use App\Models\School;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SchoolImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
   
    private $num_row = 0;
    private $file_name;
    private $number_import;
    public function __construct($file_name,$number_import)
    {
        $this->file_name = $file_name;
        $this->number_import = $number_import;
    }
    public function model(array $row)
    {   
    //     ++$this->num_row;
    //     $title_error = null;
    //     $email = $row['email'];
    //     $school = School::where('email', $email)->first();
    //     $user = User::where('email', $email)->first();
    //     $phone_user = User::where('phone', $row['phone'])->first();
    //     if (!$phone_user) {
    //         if (!$school) {
    //             $createSchool = School::create([
    //                 'name' => $row['name'],
    //                 'email' => $email,
    //                 'phone' => $row['phone'],
    //                 'address' => $row['address'],
    //                 'description' => !isset($row['description']) ? null : $row['description'],
    //                 'logo' => !isset($row['logo']) ? null : $row['logo'],
    //             ]);
    //             if (!$user) {
    //                 $createUser = User::create([
    //                     'name' => $row['name'],
    //                     'email' => $email,
    //                     'phone' => $row['phone'],
    //                     'address' => $row['address'],
    //                     'password' => \Hash::make(config('settings.password')),
    //                     'school_id' => $createSchool->id,
    //                     'user_type' => UserTypeEnum::STAFF,
    //                     'avatar' => !isset($row['logo']) ? null : $row['logo'],
    //                 ]);
    //                 $createSchool = $createSchool->update(['user_id' => $createUser->id]);
    //             }
    //         }else $title_error = "Trường đã tồn tại";
    //     } else $title_error = "Số điện thoại dùng đã tồn tại";
    //     if($title_error!=null){
    //     $importError=ImportError::create([
    //         'name_file' => $this->file_name,
    //         'created_by'=>auth()->user()->id,
    //         'row' => $this->num_row,
    //         'title_error' => $title_error,
    //         'number_import' => $this->number_import
    //     ]);
    // }
    }
}
