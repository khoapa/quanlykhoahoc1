<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\User;

class School extends Model
{
    protected $table = 'schools';
    protected $fillable = [
        'name', 'address', 'phone','email','logo','description','user_id','grade_ids','status','school_code'
    ];

    public $timestamps = true;

    public function classes()
    {
        return $this->hasMany(Classes::class);
    }
    public function grade()
    {
        return $this->hasMany('App\Models\Grade');
    }
    public function Scholl_Student()
    {
        return $this->hasMany('App\User');
    }
    public function staff(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function course()
    {
        return $this->belongsToMany('App\Models\Course', 'App\Models\SchoolCourse')->withPivot('id');
    }
    public function getCourseCategoryCountAttribute(){

        $courses = $this->course()->get();
        $arr = [];
        foreach($courses as $cou){
            $course = Course::findOrFail($cou->id);
            $idCategory = $course->category->id; 
            array_push($arr,$idCategory);
        }
        $arrnew = array_unique($arr);
        $count = count($arrnew);
        return $count;
      }  
      public function getCourseUserJoin($dateBegin,$dateEnd){
       // dd($dateBegin);
        $idSchool = $this->id;
        $courses = $this->course()->get();
        $sum= 0;
        foreach($courses as $cou){
          $userCourse = UserCourse::where('course_id',$cou->id);
            if(!is_null($dateBegin)){
            $userCourse->whereDate('join_date','>=',$dateBegin);
            }
             $userCourse = $userCourse->where('status',1)->whereHas('student',function($q) use($idSchool){
                    $q->where('school_id',$idSchool);
             })->count();
          $sum += $userCourse; 
        }
        return $sum;
      } 
      public function getCourseUserJoined($dateBegin,$dateEnd){
        $idSchool = $this->id;
        $courses = $this->course()->get();
        $sum= 0;
        foreach($courses as $cou){
             $userCourse = UserCourse::where('course_id',$cou->id);
             if(!is_null($dateBegin)){
              $userCourse->whereDate('join_date','>=',$dateBegin);
              }
              if(!is_null($dateEnd)){
                $userCourse->whereDate('date_end','<=',$dateEnd);
                }
             $userCourse = $userCourse->where('status',2)->whereHas('student',function($q) use($idSchool){
                    $q->where('school_id',$idSchool);
             })->count();
             $sum += $userCourse;
        }
        return $sum;
      } 
      public function getCourseUserVisit($dateBegin,$dateEnd){
        $idSchool = $this->id;
        $courses = $this->course()->get();
        $sum= 0;
        foreach($courses as $cou){
             $userCourse = UserCourse::where('course_id',$cou->id);
             if(!is_null($dateBegin)){
              $userCourse->whereDate('join_date','>=',$dateBegin);
              }
              if(!is_null($dateEnd)){
                $userCourse->whereDate('date_end','<=',$dateEnd);
                }
                $userCourse = $userCourse->whereHas('student',function($q) use($idSchool){
                    $q->where('school_id',$idSchool);
             })->sum('number_of_visits');
             $sum += $userCourse;
        }
        return $sum;
      } 

}