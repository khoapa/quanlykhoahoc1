<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $table = 'maps';
    protected $fillable = [
        'title','map_category_id', 'icon','description','phone','hotline','location','latitude','longitude'
    ];
    public function category()
    {
        return $this->belongsTo(MapCategory::class,'map_category_id');
    }
}
