<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class LocationUser extends Model
{
    protected $table = 'location_users';
    protected $fillable = [
        'user_id','latitude','longitude','date_tracking','address'
    ];
    public function student(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
