<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportError extends Model
{
    protected $table = 'import_errors';
    protected $fillable = [
        'id','name_file','created_by','row','title_error','number_import'
    ];
}
