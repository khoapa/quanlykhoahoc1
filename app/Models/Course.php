<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class Course extends Model
{
    use SoftDeletes;
    protected $table = 'courses';
    protected $fillable = [
        'name','course_category_id','cover_image','start_date','longtime','status','new','offer','total_point','total_point_choice','author'
    ];
    public $timestamps = true;

    public function category(){
        return $this->belongsTo(CourseCategory::class,'course_category_id','id');
    }
    public function question(){
        return $this->hasMany(Question::class,'course_id','id');
    }
    public function userCourse()
    {
        return $this->belongsToMany(User::class, 'user_courses','course_id','user_id')->withPivot('join_date','status','total_point');
    }
    public function userFavourite()
    {
        return $this->belongsToMany(User::class, 'course_favorites','course_id','user_id');
    }
    public function school()
    {
        return $this->belongsToMany(School::class, 'school_course', 'course_id','school_id')->withPivot('id');
    }
    public function class()
    {
        return $this->belongsToMany(Classes::class,'App\Models\ClassCourse');
    }
    public function authorUser(){
        return $this->belongsTo(User::class,'author','id')->orderBy('name');
    }
    public function getCountUserCount($idSchool,$dateBegin,$dateEnd){
        $course = Course::findOrFail($this->id);
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
        $userCourse->whereDate('join_date','>=',$dateBegin);
        }
         $userCourse = $userCourse->where('status',1)->whereHas('student',function($q) use($idSchool){
                $q->where('school_id',$idSchool);
         })->count();
         return $userCourse; 
      }
      public function getCountUserJoin($idSchool,$dateBegin,$dateEnd){
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
         $userCourse->whereDate('join_date','>=',$dateBegin);
         }
         if(!is_null($dateEnd)){
           $userCourse->whereDate('date_end','<=',$dateEnd);
           }
        $userCourse = $userCourse->where('status',2)->whereHas('student',function($q) use($idSchool){
               $q->where('school_id',$idSchool);
        })->count();
        return $userCourse;
      }
      public function getCountUserVisit($idSchool,$dateBegin,$dateEnd){
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
         $userCourse->whereDate('join_date','>=',$dateBegin);
         }
         if(!is_null($dateEnd)){
           $userCourse->whereDate('date_end','<=',$dateEnd);
           }
           $userCourse = $userCourse->whereHas('student',function($q) use($idSchool){
               $q->where('school_id',$idSchool);
        })->sum('number_of_visits');
        return $userCourse;
      }
      public function getCountUserCountClass($idClass,$dateBegin,$dateEnd){
        $course = Course::findOrFail($this->id);
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
        $userCourse->whereDate('join_date','>=',$dateBegin);
        }
         $userCourse = $userCourse->where('status',1)->whereHas('student',function($q) use($idClass){
                $q->where('class_id',$idClass);
         })->count();
         return $userCourse; 
      }
      public function getCountUserJoinClass($idClass,$dateBegin,$dateEnd){
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
         $userCourse->whereDate('join_date','>=',$dateBegin);
         }
         if(!is_null($dateEnd)){
           $userCourse->whereDate('date_end','<=',$dateEnd);
           }
        $userCourse = $userCourse->where('status',2)->whereHas('student',function($q) use($idClass){
               $q->where('class_id',$idClass);
        })->count();
        return $userCourse;
      }
      public function getCountUserVisitClass($idClass,$dateBegin,$dateEnd){
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
         $userCourse->whereDate('join_date','>=',$dateBegin);
         }
         if(!is_null($dateEnd)){
           $userCourse->whereDate('date_end','<=',$dateEnd);
           }
           $userCourse = $userCourse->whereHas('student',function($q) use($idClass){
               $q->where('class_id',$idClass);
        })->sum('number_of_visits');
        return $userCourse;
      }
      public function getCountUserCountAll($dateBegin,$dateEnd){
        $course = Course::findOrFail($this->id);
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
        $userCourse->whereDate('join_date','>=',$dateBegin);
        }
         $userCourse = $userCourse->where('status',1)->count();
         return $userCourse; 
      }
      public function getCountUserJoinAll($dateBegin,$dateEnd){
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
         $userCourse->whereDate('join_date','>=',$dateBegin);
         }
         if(!is_null($dateEnd)){
           $userCourse->whereDate('date_end','<=',$dateEnd);
           }
        $userCourse = $userCourse->where('status',2)->count();
        return $userCourse;
      }
      public function getCountUserVisitAll($dateBegin,$dateEnd){
        $userCourse = UserCourse::where('course_id',$this->id);
        if(!is_null($dateBegin)){
         $userCourse->whereDate('join_date','>=',$dateBegin);
         }
         if(!is_null($dateEnd)){
           $userCourse->whereDate('date_end','<=',$dateEnd);
           }
           $userCourse = $userCourse->sum('number_of_visits');
        return $userCourse;
      }
      
}
