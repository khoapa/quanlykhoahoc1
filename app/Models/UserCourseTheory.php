<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCourseTheory extends Model
{
    protected $table = 'user_course_theories';
    protected $fillable = [
        'user_id','course_theory_id'
    ];
}
