<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseFavorite extends Model
{
    public $timestamps = true;
    protected $table = 'course_favorites';
    protected $fillable = [
        'course_id', 'user_id'
    ];
}
