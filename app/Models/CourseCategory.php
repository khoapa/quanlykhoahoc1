<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    use SoftDeletes;
    protected $table = 'course_categories';
    protected $fillable = [
        'name', 'thumbnail','position'
    ];
    public $timestamps = true;
    public function course(){
        return $this->hasMany(Course::class,'course_category_id','id');
    }

    public function courseTheory(){
        return $this->hasMany(CourseTheory::class,'course_category_id','id');
    }
}
