<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Conversation extends Model
{
    protected $table = 'conversations';
    protected $fillable = [
        'id','user1_id','user2_id','name','updated_at'
    ];
    public function user1(){
        return $this->belongsTo(User::class,'user1_id','id');
    }
    public function user2(){
        return $this->belongsTo(User::class,'user2_id','id');
    }
    public function message(){
        return $this->hasMany(Message::class,'conversation_id','id');
    }
}
