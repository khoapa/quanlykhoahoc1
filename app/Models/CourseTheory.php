<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class CourseTheory extends Model
{
    use SoftDeletes;
    protected $table = 'course_theories';
    protected $fillable = [
        'name','course_category_id','content','image','status','new','offer','course_tag_ids','author','course_id','start_date'
    ];
    public $timestamps = true;

    public function category(){
        return $this->belongsTo(CourseCategory::class,'course_category_id','id');
    }
    public function authorUser(){
        return $this->belongsTo(User::class,'author','id')->orderBy('name');
    }
    public function tag()
    {
        return $this->belongsToMany(CourseTag::class,'course_tag_ids');
    }
    public function course(){
        return $this->belongsTo(Course::class,'course_id','id');
    }
}
