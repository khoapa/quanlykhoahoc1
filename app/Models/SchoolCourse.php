<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolCourse extends Model
{
    protected $table = 'school_course';
    protected $fillable = [
        'school_id','course_id',
    ];
    public $timestamps = true;
}
