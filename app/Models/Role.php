<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    public $guard_name = 'web';
    protected $table = "roles";
    protected $fillable = [
            'name',
            'guard_name',
            'description',
            'id_school'
    ];
    public function permissionRole(){
        return $this->belongsToMany(Permission::class, 'role_has_permissions','role_id','permission_id');
    }
}
