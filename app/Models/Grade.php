<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grades';
    protected $fillable = [
        'name', 'thumbnail','grade_code'
    ];
    public function school()
    {
        return $this->belongsTo(School::class);
    }
    public function classes()
    {
        return $this->hasMany('App\Models\Classes');
    }
}
