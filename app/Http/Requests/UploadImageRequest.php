<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadImageRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpeg,png,jpg,gif|max:5000'
        ];
    }

    public function messages(){
        return [
            'image.required' => 'Hình ảnh không được để trống.',
            'image.mimes' => 'Hình ảnh phải có định dạng (jpeg, png, jpg, gif).',
            'image.max' => 'Hình ảnh không được vượt quá 5MB.',
        ];
    }
}
