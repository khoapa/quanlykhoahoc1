<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;
use Facade\FlareClient\Api;
use Illuminate\Foundation\Http\FormRequest;

class AddLastPositionUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'long' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/']
        ];
    }
    public function messages()
    {
        $messages = [
            'long.required' => 'Kinh độ là trường bắt buộc',
            'lat.required' => 'Vĩ là trường bắt buộc',
            'long.regex' => 'Kinh độ không đúng định dạng',
            'lat.regex' => 'Vĩ độ không đúng định dạng',
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['long', 'lat']);
        return $data;
    }
}
