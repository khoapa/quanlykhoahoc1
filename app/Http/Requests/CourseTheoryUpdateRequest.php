<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseTheoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|max:255|unique:course_theories,name,'.$this->id,
            'course_category_id' => 'required',
            //'image'  =>'nullable|mimes:jpeg,png,jpg,gif,svg|max:5012',
            'content' => 'required|max:50000',
        ];
        
    }
    public function messages()
    {
        $messages = [ 
            'name.max' => 'Tên khóa học không vượt quá 255 kí tự.', 
            'name.unique' => 'Tên khóa học đã tồn tại.', 
            'name.required' => 'Tên khóa học là trường bắt buộc.', 
            'course_category_id.required' => 'Danh mục là trường bắt buộc.', 
            'image.mimes' => 'Hình ảnh không đúng định dạng.', 
            'image.max' => 'Hình ảnh không được quá 5 MB.',
            'content.max' => 'Nội dung không vượt quá 50000 ký tự',
            'content.required' => 'Nội dung là trường bắt buộc',
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name','course_category_id','image','status','new','offer','content','course_tag_ids','course_id']);
        return $data;
    }
}
