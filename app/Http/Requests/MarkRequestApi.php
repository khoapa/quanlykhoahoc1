<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarkRequestApi extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.id_answer_essay' => 'required',
            '*.mark' => 'required|numeric|max:100|min:0',
        ];
    }
    public function messages(){
        $messages = [
            '*.id_answer_essay.required' => 'Câu trả lời của user là trường bắt buộc.',
            '*.mark.required' => 'Bạn chưa nhập điểm.',
            '*.mark.max' => 'Điểm không đúng thang 100.' ,  
            '*.mark.min' => 'Điểm không đúng thang 100.' , 
        ];
        return $messages;
    }

}
