<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupportsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  =>  'required|max:255',
            'content'  =>  'required|max:1000',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề là trường bắt buộc.',
            'title.max' => 'Tiêu đề không vượt quá 255 ký tự.',
            'content.required' => 'Nội dung là trường bắt buộc.',
            'content.max' => 'Nội dung không vượt quá 1000 kí tự.',
        ];
    }
   
    public function getData()
    {
        $data = $this->only(['title','icon','link','content']);
        return $data;
    }
}
