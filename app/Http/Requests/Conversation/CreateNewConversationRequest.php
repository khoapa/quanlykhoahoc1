<?php

namespace App\Http\Requests\Conversation;

use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateNewConversationRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user2_id' => 'required'
        ];
    }
    public function messages()
    {
        $messages = [ 
            'user2_id.required' => 'Tên người nhận là trường bắt buộc',  
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['user2_id']);
        return $data;
    }
}
