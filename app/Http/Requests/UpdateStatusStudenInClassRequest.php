<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStatusStudenInClassRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|in:1,2'
        ];
    }
    public function messages()
    {
        $messages = [
            'status.in' => 'Trạng thái không đúng định dạng.',
            'status.required' => 'Trạng thái là trường bắt buộc.'
        ];
        return $messages;
    }
}
