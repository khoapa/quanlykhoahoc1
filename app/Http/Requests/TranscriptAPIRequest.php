<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TranscriptAPIRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_begin' => 'nullable|date|before_or_equal:today',
            'date_end' => 'nullable|date|before_or_equal:today',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'date_begin.date'=> 'Ngày bắt đầu sai định dạng ngày tháng năm (dd-mm-YYYY).',
            'date_end.date'=> 'Ngày kết thúc định dạng ngày tháng năm (dd-mm-YYYY).',
            'date_begin.before_or_equal'=> 'Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày hiện tại.',
            'date_end.before_or_equal'=> 'Ngày kết thúc phải nhỏ hơn hoặc bằng ngày hiện tại.',
        ];
        return $messages;
    }
}
