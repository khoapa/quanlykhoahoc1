<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required',
            'email' => 'required|regex:/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/|unique:users', 
            'birthday'  => 'required|before:today',
           // 'avatar'  =>  'mimes:jpeg,png,jpg,gif,svg|max:5012',
            'phone'  =>  'required|numeric|min:9999999|unique:users,phone|max:999999999999',
            'address'  =>  'required',
            'gender'  =>  'required',
            'school_id'  =>  'required',
            'class_id'  =>  'required',
          
        ];
    }
    public function messages()
    {
        return [
            'required'            =>  __(':attribute không được để trống.'),
            'unique'              =>  __(':attribute đã tồn tại trong hệ thống, vui lòng nhập :attribute khác.'),
            'min'            =>  __('Đây không phải là :attribute .'),
            'mimes'            =>  __('đây không phải file :attribute .'),
            'max'            =>  __(':attribute quá dung lượng quy định.'),
            'not_in'            =>  __(':attribute Bạn chưa chọn :attribute.'),
            'regex'            =>  __('Đây không phải :attribute.'),
            'before'            =>  __('Đây không phải :attribute.'),
            'numeric'           =>  __(':attribute phải là số.'),
        ];
    }
    public function attributes()
    {
        return [
            'name'     =>  __('Họ và tên'),
            'email'     =>  __('Email'),
            'birthday'     =>  __('Ngày sinh'),
            'avatar'     =>  __('Hình'),
            'phone'     =>  __('Số điện thoại'),
            'address'     =>  __('Địa chỉ'),
            'gender'     =>  __('Giới tính'),
            'school_id'     =>  __('Trường'),
            'class_id'     =>  __('Lớp'),
        ];
    }
    public function getData()
    {
        $data = $this->only(['role_id','name','email','avatar','birthday','phone','address','gender','class_id','school_id','user_type']);
        return $data;
    }
}
