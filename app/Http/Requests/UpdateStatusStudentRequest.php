<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateStatusStudentRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.user_id' => 'required',
            '*.status' => 'required|in:1,2'
        ];
    }
    public function messages()
    {
        $messages = [
            '*.user_id.required' => 'Người dùng là trường bắt buộc',
            '*.status.required' => 'Trạng thái là trường bắt buộc',
            '*.status.in' => 'Trạng thái không đúng định dạng'
        ];
        return $messages;
    }
}
