<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StatisticAPIRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_begin' => 'date',
            'date_end' => 'date',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'date_begin.date'=> 'Ngày bắt đầu sai định dạng ngày tháng năm (dd-mm-YYYY).',
            'date_end.date'=> 'Ngày kết thúc định dạng ngày tháng năm (dd-mm-YYYY).',
        ];
        return $messages;
    }
}
