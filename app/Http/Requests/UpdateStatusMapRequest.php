<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStatusMapRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            '*.map_id' => 'required',
            '*.status' => 'required'
        ];
    }
    public function messages()
    {
        $messages = [
            '*.map_id.required' => 'Map là trường bắt buộc',
            '*.status.required' => 'Trạng thái là trường bắt buộc'
        ];
        return $messages;
    }
}
