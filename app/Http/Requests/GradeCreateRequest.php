<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GradeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'grade_code' => 'required|unique:grades,grade_code',
            'thumbnail' => 'required'
        ];
    }
    public function messages(){
        $messages = [
            'thumbnail.required' => 'Hình ảnh là trường bắt buộc.',
            'thumbnail.mimes' => 'Hình ảnh không đúng định dạng.',
            'thumbnail.max' => 'Hình ảnh không vượt quá 5Mb.',
            'name.required' => 'Tên khối là trường bắt buộc.',
            'grade_code.required' => 'Mã khối là trường bắt buộc.',
            'grade_code.unique' => 'Mã khối đã tồn tại trong hệ thống.',
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name','thumbnail','grade_code']);
        return $data;
    }
}
