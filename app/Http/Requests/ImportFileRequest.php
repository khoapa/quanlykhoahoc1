<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportFileRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:xls,xlsx|max:20000',
            'type_import' => 'required'
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages(){
        $messages = [
            'file.required' => 'Tệp tin là trường bắt buộc.',
            'file.max' => 'Tệp tin không vượt quá 20Mb.',
            'type_import.required' => 'Kiểu tệp tin là trường bắt buộc.',
        ];
        return $messages;
    }
}
