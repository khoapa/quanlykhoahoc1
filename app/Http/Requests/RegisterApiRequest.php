<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\UserTypeEnum;
use App\User;
use Illuminate\Validation\Rule;
class RegisterApiRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_type' =>'required',
            'name'  =>  'required',
            'email' => 'required|regex:/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/|unique:users',
            'password' => 'required|min:8|max:15|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
            'password_confirmation' => 'required',
            'birthday' => 'required|date|before_or_equal:now',
            'phone'  =>  ['required','numeric', function ($attribute, $value, $fail) {
                if (!is_null($value)) {
                    $user = User::where('phone', $value)->first();
                    if ($user) {
                        return $fail(__('Số điện thoại đã tồn tại trong hệ thống'));
                    }
                }
            }],
            'address'  =>  'required',
            'gender'  =>  'required',
            'school_id' =>'required',
            'class_id' => [
                Rule::requiredIf($this->user_type == UserTypeEnum::STUDENT)
            ],
            
        ];
    }
    public function messages()
    {
        $messages = [ 
            'name.required' => 'Tên đầy đủ là trường bắt buộc', 
            'email.required' => 'Email là trường bắt buộc', 
            'email.regex' => 'Email không đúng định dạng',
            'email.unique' => 'Email đã tồn tại trong hệ thống',
            'phone.required' => 'Số điện thoại là trường bắt buộc',
            'phone.numeric' => 'Số điện thoại không đúng định dạng',
            'email.email' => 'Email không đúng định dạng',
            'password.required' => 'Mật khẩu là trường bắt buộc', 
            'password.confirmed' =>'Mật khẩu xác nhận không khớp với mật khẩu đã nhập',
            'password_confirmation.required' => 'Mật khẩu xác nhận là trường bắt buộc',
            'address.required' => 'Địa chỉ là trường bắt buộc',
            'gender.required' => 'Giới tính là trường bắt buộc',
            'school_id.required' => 'Trường học là trường bắt buộc',
            'class_id.required' => 'Lớp học là trường bắt buộc',
            'user_type.required' => 'Vị trí người dùng là trường bắt buộc',
            'birthday.required' => 'Ngày sinh là trường bắt buộc',
            'birthday.date'=> 'Ngay sinh sai định dạng ngày tháng năm (dd-mm-YYYY)',
            'birthday.before_or_equal'=>'Ngày sinh phải nhỏ hơn ngày hiện tại',
            'password.min' => 'Mật khẩu có ít nhất 8 kí tự',
            'password.regex' => 'Mật khẩu phải chứa chữ viết hoa,chữ thường và số',
            'password.max' => 'Mật khẩu có ít nhất 15 kí tự',

        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name', 'email', 'password', 'avatar', 'phone', 'birthday',
        'address', 'gender', 'school_id', 'class_id', 'position_id', 'user_type',]);
        return $data;
    }
}
