<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MapUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> 'required',
            //'icon' => 'mimes:jpeg,png,jpg,gif,svg|max:5012',
            'map_category_id' => 'required',
            'description'=> 'required|max:1000',
            'phone' => 'required|numeric|min:9999999|max:999999999999|unique:maps,phone,'.$this->id,
            'hotline' => 'required|numeric|min:9999999|max:999999999999|unique:maps,hotline,'.$this->id,
            'location' => 'required'
        ];
    }
    public function messages(){
        $messages = [
            'title.required' => 'Tiêu đề là trường bắt buộc.',    
            'icon.required' => 'Icon là trường bắt buộc.',
            'icon.mimes'=>'Icon chưa đúng định dạng.',
            'icon.max' => 'Icon không vượt quá 5Mb.',
            'map_category_id.required' => 'Thể loại là trường bắt buộc.',
            'description.required' => 'Mô tả là trường bắt buộc.',
            'description.max' => 'Mô tả không vượt quá 1000 ký tự.',
            'phone.required' => 'Số điện thoại là trường bắt buộc.',
            'phone.unique' => 'Số điện thoại đã tồn tại trong hệ thống.',
            'phone.min' => 'Đây không phải số điện thoại.',         
            'phone.numeric' => 'Số điện thoại phải là số.',
            'phone.max' => 'Số điện thoại không vượt quá 12 số.',  
            'hotline.required' => 'Hotline là trường bắt buộc.',
            'hotline.unique' => 'Hotline đã tồn tại trong hệ thống.',
            'hotline.max' => 'Hotline không vượt quá 12 số.',  
            'hotline.numeric' => 'Hotline phải là số.',
            'hotline.min' => 'Đây không phải Hotline.', 
            'location.required' => 'Vị trí là trường bắt buộc.',
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['title','map_category_id', 'icon','description','phone','hotline','location','latitude','longitude']);
        return $data;
    }
}
