<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_begin'=> 'nullable|numeric',
            'date_end' => 'required|numeric',
        ];
    }
    public function messages(){
        $messages = [
            'date_begin.required' => 'Hình ảnh là trường bắt buộc.',
            'date_end.mimes' => 'Hình ảnh không đúng định dạng.',
        ];
        return $messages;
    }
}
