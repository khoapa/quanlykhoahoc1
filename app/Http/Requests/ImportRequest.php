<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'import' => 'required|mimes:xls,xlsx|max:20000',
            'type_import' => 'required'
        ];
    }

    public function messages(){
        $messages = [
            'import.required' => 'Tệp tin chưa được chọn.',
            'import.mimes' => 'Tệp tin phải là file excel.',
            'import.max' => 'Tệp tin không vượt quá 20Mb.',
            'type_import.required' => 'Kiểu tệp tin là trường bắt buộc.',
        ];
        return $messages;
    }
}
