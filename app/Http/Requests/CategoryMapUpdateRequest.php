<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryMapUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|unique:map_category,name,'.$this->id,
            //'icon' => 'mimes:jpeg,png,jpg,gif,svg|max:5012',
        ];
    }
    public function messages(){
        $messages = [
            'name.required' => 'Thể loại điểm map là trường bắt buộc.',
            'name.unique' => 'Thể loại điểm map đã tồn tại trong hệ thống.',
            'icon.mimes'=>'Icon phải đúng định dạng.',
            'icon.max' => 'Icon không vượt quá 5Mb.',
        ];
        return $messages;
        
    }
    public function getData()
    {
        $data = $this->only(['name','icon']);
        return $data;
    }
}
