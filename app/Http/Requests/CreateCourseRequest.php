<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|max:255|unique:courses,name,NULL,id,deleted_at,NULL',
            'course_category_id' => 'required',
            'cover_image'  =>'required',
            //'start_date' => 'required',
            'longtime' =>'nullable|numeric|min:1',
            'total_point_choice'=>'required|numeric|min:0|max:100',
            'status' => 'required'
        ];
    }
    public function messages()
    {
        $messages = [ 
            'name.max' => 'Tên khóa học không vượt quá 255 kí tự.', 
            'name.unique' => 'Tên khóa học đã tồn tại.', 
            'name.required' => 'Tên khóa học là trường bắt buộc.', 
            'course_category_id.required' => 'Danh mục là trường bắt buộc.', 
            'cover_image.required' => 'Hình ảnh là trường bắt buộc.', 
            'cover_image.mimes' => 'Hình ảnh không đúng định dạng.', 
            'cover_image.max' => 'Hình ảnh không được quá 5 MB.',
            'start_date.required' => 'Ngày bắt đầu là trường bắt buộc.',  
            'longtime.required' => 'Số ngày là trường bắt buộc.',  
            'total_point_choice.required' => 'Tỉ lệ điểm trắc nghiệm là trường bắt buộc.',
            'total_point_choice.numeric' => 'Tỉ lệ điểm trắc nghiệm phải là số.',
            'total_point_choice.min' => 'Tỉ lệ điểm trắc nghiệm phải lớn hơn hoặc bằng 0.',
            'total_point_choice.max' => 'Tỉ lệ điểm trắc nghiệm phải nhỏ hơn hoặc bằng 100.',
            'longtime.min' => 'Số ngày học phải lớn hơn 0.',
            'longtime.numeric' => 'Số ngày học phải là số.',
            'status.required' => 'Trạng thái là trường bắt buộc.'
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['total_point_choice','name','course_category_id','cover_image','start_date','longtime','status','new','offer']);
        return $data;
    }
}
