<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePermissionApiRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_permission'  => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'id_permission.required' => 'Quyền là trường bắt buộc.', 
            'status.required' => 'Trạng thái là trường bắt buộc.', 
        ];
        return $messages;
    }
}
