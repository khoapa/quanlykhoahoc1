<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryNewsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required|max:255|unique:category_news',
        ];
    }
    public function messages(){
        $messages = [
            'name.required' => 'Thể loại tin tức là trường bắt buộc.',
            'name.max' => 'Thể loại tin tức không vượt quá 255 kí tự.',
            'name.unique' => 'Thể loại tin tức đã tồn tại trong hệ thống.'
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name']);
        return $data;
    }
}
