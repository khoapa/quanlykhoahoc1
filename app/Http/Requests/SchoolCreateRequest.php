<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_code' =>'required|unique:schools,school_code',
            'name'=> 'required|unique:schools,name',
            'address' => 'required',
            'phone' => 'required|numeric|min:9999999|max:999999999999|unique:schools,phone',
           // 'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5000',
            'email' => 'required|regex:/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/|unique:schools,email|unique:users,email',
            'grade_ids' => 'required',
        ];
    }
    public function messages(){
        $messages = [
            'email.regex'=> 'Email phải đúng định dạng.',
            'email.unique'=> 'Email '.$this->email.' đã tồn tại trong hệ thống.',
            'email.required' => 'Email là trường bắt buộc.',
            'logo.mimes' => 'Logo không đúng định dạng.',
            'logo.max' => 'Logo không vượt quá 5Mb.',
            'name.required' => 'Tên Trường là trường bắt buộc.',
            'name.unique' => 'Trường '.$this->name.' đã tồn tại trong hệ thống.',
            'address.required' => 'Địa chỉ là trường bắt buộc.',
            'phone.required' => 'Số điện thoại là trường bắt buộc.',
            'phone.unique' => 'Số điện thoại đã tồn tại trong hệ thống.',
            'phone.numeric' => 'Số điện thoại phải là số.',
            'phone.min' => 'Đây không phải số điện thoại.',
            'phone.max' => 'Số điện thoại không vượt quá 12 số.',   
            'grade_ids.required' => 'Khối là trường bắt buộc.',
            'school_code.required' => 'Mã Trường là trường bắt buộc.',
            'school_code.unique' => 'Mã trường đã tồn tại trong hệ thống'
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name','address','logo','phone','email','description','grade_ids','school_code']);
        return $data;
    }
}
