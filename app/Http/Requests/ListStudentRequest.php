<?php

namespace App\Http\Requests;

use App\Models\Classes;
use App\Models\School;
use Illuminate\Foundation\Http\FormRequest;

class ListStudentRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id'=> [
                function ($attribute, $value, $fail) {
                    if(!is_null($value)) {
                        $school = School::find($value);
                        if (!$school) {
                            return $fail("Trường không tồn tại trong hệ thống");
                        }
                    }
                }],
            'class_id'=> [
                function ($attribute, $value, $fail) {
                    if(!is_null($value)) {
                        $class = Classes::find($value);
                        if (!$class) {
                            return $fail("Lớp không tồn tại trong hệ thống");
                        }
                    }
                }]
        ];
    }
}
