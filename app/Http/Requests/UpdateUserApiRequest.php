<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Enums\UserTypeEnum;

class UpdateUserApiRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_type' =>'required',
            'name'  =>  'required',
            'birthday' => 'required|date|before_or_equal:now',
            'address'  =>  'required',
            'gender'  =>  'required',
            'school_id' =>'required',
            'class_id' => [
                Rule::requiredIf($this->user_type == UserTypeEnum::STUDENT)
            ],
            'avatar' => 'required',
            
        ];
    }
    public function messages()
    {
        $messages = [ 
            'name.required' => 'Tên đầy đủ là trường bắt buộc.', 
            'avatar.required' => 'Ảnh đại diện là trường bắt buộc.', 
            'address.required' => 'Địa chỉ là trường bắt buộc.',
            'gender.required' => 'Giới tính là trường bắt buộc.',
            'school_id.required' => 'Trường học là trường bắt buộc.',
            'class_id.required' => 'Lớp học là trường bắt buộc.',
            'user_type.required' => 'Vị trí người dùng là trường bắt buộc.',
            'birthday.required' => 'Ngày sinh là trường bắt buộc.',
            'birthday.date'=> 'Ngay sinh sai định dạng ngày tháng năm (dd-mm-YYYY).',
            'birthday.before_or_equal'=>'Ngày sinh phải nhỏ hơn ngày hiện tại.',

        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name', 'avatar', 'birthday',
        'address', 'gender', 'school_id', 'class_id', 'position_id', 'user_type']);
        return $data;
    }
}
