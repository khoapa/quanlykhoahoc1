<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|max:255|unique:course_categories,name,'.$this->id,
           // 'thumbnail'  =>'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'name.max' => 'Tên danh mục không vượt quá 255 kí tự.', 
            'name.unique' => 'Tên danh mục đã tồn tại trong hệ thống.',
            'name.required' => 'Tên danh mục là trường bắt buộc.', 
            'thumbnail.mimes' => 'Hình ảnh không đúng định dạng.', 
            'thumbnail.max' => 'Hình ảnh không được quá 5 MB.', 
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name','thumbnail']);
        return $data;
    }
}
