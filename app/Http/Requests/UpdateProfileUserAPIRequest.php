<?php

namespace App\Http\Requests;

use App\Enums\UserTypeEnum;
use App\Models\Classes;
use App\Models\Role;
use App\Models\School;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfileUserAPIRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        return [
            'user_type' => 'required',
            'name'  =>  'required',
            'birthday' => 'date|before_or_equal:now',
            'gender'  =>  'required|in:1,2',
            'role_id' => [
                function ($attribute, $value, $fail) {
                    $role = Role::find($value);
                    if (!$role) {
                        return $fail("Quyền không tồn tại.");
                    }
                }
            ],
            'school_id' => [
                'required',
                function ($attribute, $value, $fail) {
                    $school = School::find($value);
                    if (!$school) {
                        return $fail("Trường không tồn tại.");
                    }
                }
            ],
            'email' => 'required|regex:/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/|unique:users,email,' . $id . ',id',
            'phone' => 'nullable|unique:users,phone,' . $id . ',id',
            'class_id' => [
                Rule::requiredIf($this->user_type == UserTypeEnum::STUDENT),
                function ($attribute, $value, $fail) {
                    $class = Classes::find($value);
                    if (!$class) {
                        return $fail("Lớp không tồn tại.");
                    }
                }
            ],

        ];
    }
    public function messages()
    {
        $messages = [
            'name.required' => 'Tên đầy đủ là trường bắt buộc.',
            'gender.required' => 'Giới tính là trường bắt buộc.',
            'gender.in' => 'Giới tính không đúng định dạng.',
            'school_id.required' => 'Trường học là trường bắt buộc.',
            'class_id.required' => 'Lớp học là trường bắt buộc.',
            'user_type.required' => 'Loại người dùng là trường bắt buộc.',
            'birthday.required' => 'Ngày sinh là trường bắt buộc.',
            'birthday.date' => 'Ngay sinh sai định dạng ngày tháng năm (dd-mm-YYYY).',
            'birthday.before_or_equal' => 'Ngày sinh phải nhỏ hơn ngày hiện tại.',
            'email.unique' => 'Email đã tồn tại.',
            'email.required' => 'Email là trường bắt buộc.',
            'email.regex' => 'Email không đúng định dạng.',
            'phone.unique' => 'Số điện thoại đã tồn tại.',
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only([
            'name', 'avatar', 'birthday', 'phone', 'email','role_id',
            'address', 'gender', 'school_id', 'class_id', 'user_type'
        ]);
        return $data;
    }
}
