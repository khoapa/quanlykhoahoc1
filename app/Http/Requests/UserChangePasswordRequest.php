<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Request;

class UserChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $getOldPass = Request::get('old_password'); 
        return [
            'old_password' => [
                'required',
                function ($attribute, $value, $fail) use($getOldPass) {
                    $passwordOld = auth()->user()->password;

                    if (!Hash::check($getOldPass, $passwordOld)){
                        return $fail("Mật khẩu cũ không đúng.");
                    }
                }
            ],  
            'password1' => 'required|min:8|max:15',
            'password1_confirmation' => 'required|same:password1',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'old_password.required' => 'Mật khẩu cũ là trường bắt buộc.', 
            'password1.required' => 'Mật khẩu mới là trường bắt buộc.', 
            'password1.min' => 'Mật khẩu không ngắn hơn 8 ký tự.',
            'password1.max' => 'Mật khẩu không quá 15 ký tự.',
            
            'password1_confirmation.required' => 'Mật khẩu xác nhận là trường bắt buộc.',
            'password1_confirmation.same' => 'Mật khẩu xác nhận không khớp.',
        ];
        return $messages;
    }
}
