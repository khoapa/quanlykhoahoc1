<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGetNotificationRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'notification_status'  => 'required|numeric',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'notification_status.required' => 'Trạng thái thông báo là trường bắt buộc.', 
            'notification_status.numeric' => 'Trạng thái thông báo không đúng định dạng.'
        ];
        return $messages;
    }
}
