<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> 'required|max:200',
            'content'=> 'required|max:50000',
            'thumbnail' => 'required',
            'news_category_id' => 'required',
            'link' => 'max:200'
        ];
    }
    public function messages(){
        $messages = [
            'title.max' => 'Tiêu đề không vượt quá 200 kí tự.',
            'news_category_id.required' => 'Thể loại là trường bắt buộc.',
            'thumbnail.mimes'=>'Hình ảnh phải đúng định dạng.',
            'thumbnail.max' => 'Hình ảnh không vượt quá 5Mb.',
            'thumbnail.required' => 'Hình ảnh là trường bắt buộc.',
            'title.required' => 'Tiêu đề là trường bắt buộc.',
            'content.required' => 'Nội dung là trường bắt buộc.',       
            'content.max' => 'Nội dung không vượt quá 50000 ký tự.',       
            'link.max' => 'Link không vượt quá 200 kí tự.'
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['title','thumbnail','news_category_id','content','status','priority','link','tag_ids']);
        return $data;
    }
}
