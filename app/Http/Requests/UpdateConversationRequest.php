<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateConversationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'nullable',
            'images' => 'required_without:text',
        ];
    }
    public function messages()
    {
        $messages = [ 
            'images.required_without' => 'Hình ảnh là trường bắt buộc.', 
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['text','images']);
        return $data;
    }
}
