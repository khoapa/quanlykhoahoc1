<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GradeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade_code' => 'required|unique:grades,grade_code,'.$this->id,
            'name'=> 'required',
          //  'thumbnail' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ];
    }
    public function messages(){
        $messages = [
            'thumbnail.mimes' => 'Ảnh không đúng định dạng.',
            'thumbnail.max' => 'Ảnh không vượt quá 5Mb.',
            'grade_code.required' => 'Mã khối là trường bắt buộc.',
            'name.required' => 'Tên khối là trường bắt buộc.',
            'grade_code.unique' => 'Mã khối đã tồn tại trong hệ thống.',
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['name','thumbnail','grade_code']);
        return $data;
    }
}
