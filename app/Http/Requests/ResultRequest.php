<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResultRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'id_user_course' =>'required',            
        ];
    }
    public function messages()
    {
        $messages = [ 
          'id_user_course.required' => 'ID khóa học của user là trường bắt buộc.', 
        ];
        return $messages;
    }
}