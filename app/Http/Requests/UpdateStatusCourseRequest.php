<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStatusCourseRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'offer' => 'in:0,1',
            'status' => 'in:0,1',
            'new' => 'in:0,1'
        ];
    }
    public function messages()
    {
        $messages = [
            'offer.in' => 'Offer không đúng định dạng.',
            'status.in' => 'Status không đúng định dạng.',
            'new.in' => 'New Không đúng định dạng.'
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only(['offer','status','new']);
        return $data;
    }
}
