<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class UpdateQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id'  => 'required',
            'type' => 'required',
            'subject' =>'required|max:1000',
            'answer.'.$this->id_answer.'' => [
                Rule::requiredIf($this->type == 1)
            ],
            'answer.'.$this->id_answer1.'' => [
                Rule::requiredIf($this->type == 1)
            ],
        ];
    }
    public function messages()
    {
        $messages = [ 
            'course_id.required' => 'Khóa học là trường bắt buộc.', 
            'type.required' => 'Loại câu hỏi là trường bắt buộc.', 
            'point.required' => 'Số điểm là trường bắt buộc.', 
            'subject.required' => 'Tên câu hỏi là trường bắt buộc.',  
            'subject.max' => 'Nội dung câu hỏi không vượt quá 1000 ký tự.',  
            'answer.'.$this->id_answer.'.required' => 'Câu trả lời bắt buộc.', 
            'answer.'.$this->id_answer1.'.required' => 'Câu trả lời bắt buộc.', 
        ];
        return $messages;
    }
    public function getData()
    {
        $data = $this->only([ 'course_id','type','subject','multiselect','point','rediret_answer']);
        return $data;
    }
    public function getDataAnswerOld()
    {
        $data = $this->only([ 'answer','correct']);
        return $data;
    }
    public function getDataAnswerNew()
    {
        $data = $this->only([ 'answerTo','correctTo']);
        return $data;
    }
}
