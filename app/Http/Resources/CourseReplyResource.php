<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->course->total_point_choice != 0){
            $courseResultChooseTrue = $this->courseResult()->where('point','!=',0)->whereHas('question', function ($query){
                $query->where('type', 1);
             })->get();
            $courseResultChoose = $this->courseResult()->whereHas('question', function ($query){
                $query->where('type', 1);
             })->get();
            if($courseResultChoose->count() != 0){
                $percentChoose = ($courseResultChooseTrue->count()/$courseResultChoose->count())*100;
            }else{
                $percentChoose = 0;
            }
            $resultPercentChoose = $percentChoose*$this->course->total_point_choice/100;
        }else{
            $resultPercentChoose = 0;
            $percentChoose = 0;
        }

         $courseResultEssay = $this->courseResult()->whereHas('question', function ($query){
            $query->where('type', 2);
         })->get();
    

         $totalEssay = $this->course->total_point-$this->course->total_point_choice;
         if($totalEssay != 0){
            $percentEssay = $courseResultEssay->sum('point')/$totalEssay*100;
            $resultPercentEssay = $percentEssay*$totalEssay/100;
         }else{
             $percentEssay = 0;
             $resultPercentEssay = 0;
         }

         $ratio = $this->course->total_point_choice;
         $result = $resultPercentEssay+$resultPercentChoose;
        return [
            'id' => $this->id,
            'id_course'=>$this->course->id,
            'name' =>$this->course->name,
            'cover_image' => $this->course->cover_image,
            'result' =>$result,
            'result_choose' => $percentChoose,
            'result_essay' => $percentEssay,
            'answer_choose' =>$this->course->total_point_choice != 0 ? AnswerReplyResource::collection($courseResultChoose) : null,   
            'answer_essay' =>$totalEssay != 0 ? AnswerEssayReplyResource::collection($courseResultEssay) : null,
        ];
    }
}
