<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\Permission;
use App\Models\RoleHasPermission;

class PermissionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  

        return $this->collection->map(function($n){
            return [
                'id' => $n->id,
                'name' => $n->description,
                'checked' => in_array($n->id, $this->checkPermission())?true:false,
                'subject'=> $n->subject
            ];
            })->groupBy('subject');
    }
    public function checkPermission(){
        $selected = [];
        if(!empty(request()->route('id')))
         {
             $selected = RoleHasPermission::where('role_id', request()->route('id'))->pluck('permission_id')->toArray();
         }
         return $selected;
    }
}
