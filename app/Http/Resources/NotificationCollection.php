<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\NotificationResource;

class NotificationCollection extends CollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = NotificationResource::class;

    public function toArray($request)
    {
        return $this->collection;
    }
}
