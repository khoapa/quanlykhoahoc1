<?php

namespace App\Http\Resources\Refer;

use App\Http\Resources\CollectionResource;

class ReferCollection extends CollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = ReferResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
