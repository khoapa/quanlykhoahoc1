<?php

namespace App\Http\Resources\Refer;

use Illuminate\Http\Resources\Json\JsonResource;

class ReferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'icon' => $this->icon,
            'title' => $this->title,
            'link' => $this->link
        ];
    }
}
