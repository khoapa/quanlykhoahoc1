<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Enums\StatusUserCourseEnum;
use App\Helpers\Helper;

class CouresNewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $idUser = auth()->user()->id;
        $statusFavourite = $this->course->userFavourite()->where('user_id',$idUser)->first();
        return [
            'id' => $this->course->id,
            'name' => $this->course->name,
            'category' => $this->course->category->name,
            'cover_image' => $this->course->cover_image,
            'start_date' => Helper::formatDate($this->start_date),
            'status_user' => $this->status,
            'status_user_lable' => StatusUserCourseEnum::getDescription($this->status),
            'status_favourite' => is_null($statusFavourite) ? 0 : 1,
            'id_user_course' => $this->id,
        ];
    }
}
