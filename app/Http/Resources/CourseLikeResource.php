<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Helpers\Helper;
use App\Models\UserCourse;
use App\Enums\StatusCourseEnum;
use App\Enums\StatusUserCourseEnum;

class CourseLikeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $idUser = auth()->user()->id;
        $statusFavourite = $this->userFavourite()->where('user_id',$idUser)->latest()->first();
        $statusCourse = UserCourse::where('course_id',$this->id)->where('user_id',$idUser)->latest()->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category' => $this->category->name,
            'cover_image' => $this->cover_image,
            'start_date' => Helper::formatDate($this->start_date),
            'longtime' => $this->longtime,
            'status_user' => is_null($statusCourse) ? null : $statusCourse->status,
            'status_user_lable' => !is_null($statusCourse) ? StatusUserCourseEnum::getDescription($statusCourse->status) : "Tham gia",
            'status_favourite' => is_null($statusFavourite) ? 0 : 1,
        ];
    }
}
