<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AnswerEssayReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'question' => !is_null($this->question) ? $this->question->subject : null, 
            'answer' => $this->answer,
            'percent' => $this->point/$this->question->point*100,
        ];
    }
}
