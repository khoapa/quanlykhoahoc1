<?php

namespace App\Http\Resources\Map;

use App\Http\Resources\CollectionResource;

class MapCollection extends CollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = MapResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
