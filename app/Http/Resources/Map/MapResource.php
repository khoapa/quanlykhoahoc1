<?php

namespace App\Http\Resources\Map;

use App\Helpers\Helper;
use App\Models\Map;
use App\Models\UserMap;
use Illuminate\Http\Resources\Json\JsonResource;

class MapResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'map_category_id' => $this->map_category_id,
            'icon' => $this->icon,
            'description' => $this->description,
            'phone' => $this->phone,
            'hotline' => $this->hotline,
            'location' => $this->location,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'distance' => !is_null($this->distance)?number_format((float)$this->distance, 3, '.', ''):null
        ];
    }
}
