<?php

namespace App\Http\Resources\Conversation;

use Illuminate\Http\Resources\Json\JsonResource;

class CreateNewConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $receiver;
    public function __construct($resource, $receiver)
    {
        parent::__construct($resource);
        $this->receiver = $receiver;
    }

    public function toArray($request)
    {   
        return [
            'conversation_id' => $this->id,
            'user2_id' => $this->receiver->id,
            'name_chat' => $this->receiver->name,
            'avatar' => $this->receiver->avatar,
        ];
    }
}
