<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ListConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth('api')->user();
        $userChat = $this->userChat();
        return [
            'id' => $this->id,
            'name_chat' =>$user->name,
            'avatar' => $user->avatar,
            'user_id' =>$user->id,
            'message'  => new MessageResource($this->message()->latest()->first())        
        ];
    }
    public function userChat(){
        $user = auth('api')->user();
        $userId = $user->id;
        if($this->user1_id == $userId){
            $nameChat = $this->user2->name; 
            $avatar = $this->user2->avatar; 
        }else{
            $nameChat = $this->user1->name;
            $avatar = $this->user2->avatar; 
        }
        $arrUser = [
            'name' => $nameChat,
            'avatar' => $avatar
        ];
        return $arrUser;
    }
}
