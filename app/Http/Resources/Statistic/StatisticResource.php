<?php

namespace App\Http\Resources\Statistic;

use Illuminate\Http\Resources\Json\JsonResource;

class StatisticResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = (object) $this->resource;
        return [
            'stt' => $item->stt,
            'school_id' => $item->school_id,
            'school_code' => $item->school_code,
            'school_name' => $item->school_name,
            'class_count' => $item->class_count,
            'course_course' => $item->course_course,
            'course_category_count' => $item->course_category_count,
            'student_count' => $item->student_count,
            'student_join_count' => $item->student_join_count,
            'number_visits' => $item->number_visits
        ];
    }
}
