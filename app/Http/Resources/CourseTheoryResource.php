<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Helpers\Helper;
use Illuminate\Support\Str;
use App\Models\UserCourseTheory;

class CourseTheoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        $userCourseTheory  = UserCourseTheory::where('user_id',$user->id)->where('course_theory_id',$this->id)->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => $this->course_category_id,
            'category_name' => $this->category->name,
            'content' => $this->content,
            'image' => $this->image,
            'status'=> !is_null($userCourseTheory) ? 1 : 0,
            'is_new' => $this->new,
            'is_offer' => $this->offer,
            'description' =>Str::limit(preg_replace( "/\r|\n/", "", strip_tags(html_entity_decode($this->content))),150) ,
            'created_at' => Helper::formatDateDiffForHumans($this->created_at),
            'course' =>!is_null($this->course_id) ? new CouresNewResource($this) : null
        ];
    }
}
