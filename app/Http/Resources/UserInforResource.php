<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Helpers\Helper;
use App\Enums\UserTypeEnum;

class UserInforResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'phone' => $this->phone,
            'birthday' => Helper::formatDate($this->birthday),
            'address' => $this->address,
            'id_school' => $this->school_id,
            'id_class' => $this->class_id,
            'school'=> !is_null($this->school) ? $this->school->name : null,
            'class'=> !is_null($this->classes) ? $this->classes->name : null,
            'teacher' => $this->checkTeacher(),
            'user_type' => $this->user_type,
            'type_lable' => UserTypeEnum::getDescription($this->user_type),
            'gender' => $this->gender,
            'notification_status' => $this->notification_status
        ];
    }
    public function checkTeacher(){
        $class = $this->classes;
        $teacher = null ;
        if(!is_null($class)){
          $teacher = !is_null($class->teacher) ? $class->teacher->name : null;
        }
        return $teacher;
    }
}
