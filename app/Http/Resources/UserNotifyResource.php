<?php

namespace App\Http\Resources;

use App\Enums\UserTypeEnum;
use Illuminate\Http\Resources\Json\JsonResource;

class UserNotifyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'avatar' =>$this->avatar,
            'status' => $this->status,
            'user_type' => $this->user_type,
            'type_lable' => UserTypeEnum::getDescription($this->user_type),
          ];
    }
}
