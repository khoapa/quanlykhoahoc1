<?php

namespace App\Http\Resources;

use App\Models\Conversation;
use App\Models\Message;
use Illuminate\Http\Resources\Json\JsonResource;

class ConversionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    // public function toArray($request)
    // {
    //     $user = auth('api')->user();
    //     $user_school_id = -1;
    //     if ($user->admin_flg != 1)
    //     $user_school_id = $user->school()->firstOrFail()->user_id;
    //     $dataUser = [$this->id, $user->id];
    //     $conversation = Conversation::whereIn('user1_id', $dataUser)->whereIn('user2_id', $dataUser)->first();
    //     return [
    //         'conversation_id' => !is_null($conversation) ? $conversation->id : null,
    //         'user_id' => $this->id,
    //         'name_chat' => $this->name,
    //         'avatar' => $this->avatar,
    //         'admin_flg' => $this->admin_flg,
    //         'user_type' => ($this->id != $user_school_id) ? $this->user_type : 4, //user_type = 4 is school
    //         'message'  => !is_null($conversation) ? new MessageResource($conversation->message()->latest()->first()) : null
    //     ];
    // }

    // public function toArray($request)
    // {
    //     $user = auth('api')->user();
    //     $userChat = $this->userChat();
    //     return [
    //         'id' => $this->id,
    //         'name_chat' =>$userChat['name'],
    //         'avatar' => $userChat['avatar'],
    //         'user2_id' =>$userChat['id'],
    //         'message'  => new MessageResource($this->message()->latest()->first())        
    //     ];
    // }
    // public function userChat(){
    //     $user = auth('api')->user();
    //     $userId = $user->id;
    //     if($this->user1_id == $userId){
    //         $nameChat = $this->user2->name; 
    //         $avatar = $this->user2->avatar; 
    //         $id = $this->user2->id;
    //     }else{
    //         $nameChat = $this->user1->name;
    //         $avatar = $this->user1->avatar; 
    //         $id = $this->user1->id;
    //     }
    //     $arrUser = [
    //         'name' => $nameChat,
    //         'avatar' => $avatar,
    //         'id'=> $id
    //     ];
    //     return $arrUser;
    // }
    public function toArray($request)
    {
        $user = auth('api')->user();
        $userChat = $this->userChat();
        $user_school_id = -1;
        if ($user->admin_flg != 1)
            $user_school_id = $user->school()->firstOrFail()->user_id;
        $number_messages_viewed = Message::where('conversation_id', $this->id)->where('status', 1)->count();
        return [
            'conversation_id' => $this->id,
            'number_messages_viewed' => $number_messages_viewed,
            'name_chat' => $userChat['name'],
            'avatar' => $userChat['avatar'],
            'user_id' => $userChat['id'],
            'user_type' => ($userChat['id'] != $user_school_id) ? $userChat['user_type'] : 4, //user_type = 4 is school,
            'admin_flg' => $userChat['admin_flg'],
            'message'  => new MessageResource($this->message()->latest()->first())
        ];
    }
    public function userChat()
    {
        $user = auth('api')->user();
        $userId = $user->id;
        if ($this->user1_id == $userId) {
            if ($this->user2 == null) {
                $nameChat = 'Không xác định';
                $avatar = config('settings.image');
                $id = null;
                $admin_flg = null;
                $user_type = null;
            } else {
                $nameChat = $this->user2->name;
                $avatar = $this->user2->avatar;
                $id = $this->user2->id;
                $admin_flg = $this->user2->admin_flg;
                $user_type = $this->user2->user_type;
            }
        } else {
            if ($this->user1 == null) {
                $nameChat = 'Không xác định';
                $avatar = config('settings.image');
                $id = null;
                $admin_flg = null;
                $user_type = null;
            } else {
                $nameChat = $this->user1->name;
                $avatar = $this->user1->avatar;
                $id = $this->user1->id;
                $admin_flg = $this->user1->admin_flg;
                $user_type = $this->user1->user_type;
            }
        }
        $arrUser = [
            'name' => $nameChat,
            'avatar' => $avatar,
            'id' => $id,
            'user_type' => $user_type,
            'admin_flg' => $admin_flg
        ];
        return $arrUser;
    }
}
