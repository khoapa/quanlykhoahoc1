<?php

namespace App\Http\Resources\Transcript;

use Illuminate\Http\Resources\Json\JsonResource;

class TranscriptResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = (object) $this->resource;
        return [
            'stt' => $item->stt,
            'id' => $item->id,
            'student_name' => $item->student_name,
            'school_name' => $item->school_name,
            'class_name' => $item->class_name,
            'course_name' => $item->course_name,
            'join_date' => $item->join_date,
            'end_date' => $item->end_date,
            'multiple_choice_scores' => $item->multiple_choice_scores,
            'essay_score' => $item->essay_score,
            'total_score' => $item->total_score,
        ];
    }
}
