<?php

namespace App\Http\Resources\CategoryMap;

use App\Http\Resources\CollectionResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoryMapCollection extends CollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = CategoryMapResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
