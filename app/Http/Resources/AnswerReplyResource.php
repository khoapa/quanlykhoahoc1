<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Ramsey\Collection\Collection;

class AnswerReplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id, 
            'question_id' => $this->question_id,
            'question' => !is_null($this->question) ? $this->question->subject : null, 
            'answer_detail'=> !is_null($this->question) ? AnswerReource::collection($this->question->answer) : null,
            'answer_user'=> $this->answer,
            'status' => $this->point != 0 ? 1 : 0,
            'multiselect' =>!is_null($this->question) ? $this->question->multiselect : null, 
        ];
    }
}
