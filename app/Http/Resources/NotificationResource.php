<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Enums\TypeNotiFyEnum;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->load(['sender', 'receiver']);
        return [
          'id' => $this->id,
          'sender' => new UserNotifyResource($this->sender),
          'receiver' => new UserNotifyResource($this->receiver),
          'status' => $this->status,
          'title' => $this->title,
          'type' => $this->type,
          'lable_type' => TypeNotiFyEnum::getDescription($this->type),
          'id_type' => $this->id_type,
          'created_at' => Helper::formatDate($this->created_at),
          'moment_string' => ' '.$this->momentDate($this->created_at),
        ];
    }
        // Protected fucntion
        protected function momentDate($date){
            Carbon::setLocale('vi');
            $dt = new Carbon($date);
            return $dt->diffForHumans();
          }
}
