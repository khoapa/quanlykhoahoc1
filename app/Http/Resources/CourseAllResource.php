<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Enums\StatusCourseEnum;
use App\Models\UserCourse;

class CourseAllResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        $user = auth()->user();

        $limit = request()->get('limit');
        $course_id = UserCourse::where('user_id', auth()->user()->id)->pluck('course_id');
        $course = $this->course()->whereNotIn('id', $course_id)->where(function ($query) use ($user) {
            $query = $query->whereHas('question');
            if ($user->admin_flg != 1) {
                $school_id = $user->school_id;
                if ($school_id) {
                    $query = $query->whereHas('school', function ($q) use ($school_id) {
                        $q = $q->where('school_id', $school_id);
                    });
                }
            }
        })->where('status', StatusCourseEnum::SHOW)->take($limit)->get();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'coursers' => CourseResource::collection($course),
        ];
    }
}
