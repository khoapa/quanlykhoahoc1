<?php

namespace App\Http\Resources\News;

use App\Http\Resources\CollectionResource;

class NewsCollection extends CollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = NewsResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
