<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SchoolCourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item = (object) $this->resource;
        return [
            'stt' => $item->stt,
            'id' => $item->id,
            'course_name' => $item->course_name,
            'category_course_name' => $item->category_course_name,
            'total_student_participating' => $item->total_student_participating,
            'total_student_joined' => $item->total_student_joined,
            'total_student_visit' => $item->total_student_visit,
        ];
    }
}
