<?php

namespace App\Http\Resources\Course;

use App\Helpers\Helper;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseAPIAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => $this->course_category_id,
            'category' => $this->category->name,
            'content' => $this->content,
            'image' => $this->cover_image,
            'status' => $this->status,
            'is_new' => $this->new,
            'is_offer' => $this->offer,
        ];
    }
}
