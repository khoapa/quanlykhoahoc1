<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\CollectionResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CourseAPIAdminCollection extends CollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects = CourseAPIAdminResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
