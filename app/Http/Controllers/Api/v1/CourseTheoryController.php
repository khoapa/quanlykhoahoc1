<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\CourseCategory;
use App\Enums\StatusCourseEnum;
use App\Http\Resources\CourseTheoryAllCollection;
use App\Models\CourseTheory;
use App\Http\Resources\CourseTheoryCollection;
use App\Http\Resources\CourseTheoryResource;
use App\Models\UserCourseTheory;
use App\Models\CourseTag;

class CourseTheoryController extends ApiController
{
    public function listAll(Request $request)
    {
      $perPage = $request->get('per_page');
      $query = CourseCategory::orderBy('position', 'ASC')->whereHas('courseTheory', function ($q) {
        $q->where('status',StatusCourseEnum::SHOW);
      });
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $query = $query->paginate($perPage);
      return $this->formatJson(CourseTheoryAllCollection::class, $query);
    }
    public function index(Request $request)
    {
        $perPage = $request->get('per_page');
        $courseCategoryId = $request->get('course_category_id');
        $searchText = $request->get('search_text');
        $query = CourseTheory::orderBy('id', 'DESC')->where('status', 1);
        $query_all = CourseTheory::orderBy('id', 'DESC');
        if (!is_null($courseCategoryId)){
          $query->where('course_category_id', $courseCategoryId);
        }
        $data = [];
        if (!is_null($searchText)) {
            $query_tags = CourseTag::where('name', 'LIKE', '%' . $searchText . '%')->pluck('id')->toArray();
            foreach ($query_all->get() as $news) {
                foreach (!is_null($news->course_tag_ids) ? json_decode($news->course_tag_ids) : [] as $news_tag_id) {
                    $value = in_array($news_tag_id, $query_tags);
                    if ($value) {
                        $data[] = $news->id;
                        break;
                    }
                }
            }
            $query = $query->where(function($q) use($data,$searchText){
              $q->whereIn('id', $data)->orWhere('name', 'LIKE', '%' . $searchText . '%');
            });
          }
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->latest()->paginate($perPage);
        return $this->formatJson(CourseTheoryCollection::class, $query);
    }
    public function show($id){
      $courseTheory = CourseTheory::findOrFail($id);
      $user = auth()->user();
      $userCourseTheory  = UserCourseTheory::where('user_id',$user->id)->where('course_theory_id',$id)->first();
      if(is_null($userCourseTheory)){
        $data = [
          'user_id' => $user->id,
          'course_theory_id' => $id
        ];
        UserCourseTheory::create($data);
      }
      return $this->formatJson(CourseTheoryResource::class, $courseTheory);  
    }
    public function listOffer(Request $request)
    {
      $perPage = $request->get('per_page');
      $searchText = $request->get('search_text');
      $query = CourseTheory::orderBy('id', 'DESC')->where('offer', 1)->where('status',1);
      $query_all = CourseTheory::orderBy('id', 'DESC');
      $data = [];
      if (!is_null($searchText)) {
          $query_tags = CourseTag::where('name', 'LIKE', '%' . $searchText . '%')->pluck('id')->toArray();
          foreach ($query_all->get() as $news) {
              foreach (!is_null($news->course_tag_ids) ? json_decode($news->course_tag_ids) : [] as $news_tag_id) {
                  $value = in_array($news_tag_id, $query_tags);
                  if ($value) {
                      $data[] = $news->id;
                      break;
                  }
              }
          }
          $query = $query->where(function($q) use($data,$searchText){
            $q->whereIn('id', $data)->orWhere('name', 'LIKE', '%' . $searchText . '%');
          });
      }
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $query = $query->latest()->paginate($perPage);
      return $this->formatJson(CourseTheoryCollection::class, $query);
    }
    public function listNew(Request $request)
    {
      $perPage = $request->get('per_page');
      $searchText = $request->get('search_text');
      $query = CourseTheory::orderBy('id', 'DESC')->where('new', 1)->where('status',1);
      $query_all = CourseTheory::orderBy('id', 'DESC');
      $data = [];
      if (!is_null($searchText)) {
          $query_tags = CourseTag::where('name', 'LIKE', '%' . $searchText . '%')->pluck('id')->toArray();
          foreach ($query_all->get() as $news) {
              foreach (!is_null($news->course_tag_ids) ? json_decode($news->course_tag_ids) : [] as $news_tag_id) {
                  $value = in_array($news_tag_id, $query_tags);
                  if ($value) {
                      $data[] = $news->id;
                      break;
                  }
              }
          }
          $query = $query->where(function($q) use($data,$searchText){
            $q->whereIn('id', $data)->orWhere('name', 'LIKE', '%' . $searchText . '%');
          });
      }
      $perPage = $perPage ? $perPage : config('settings.countPaginate');
      $query = $query->latest()->paginate($perPage);
      return $this->formatJson(CourseTheoryCollection::class, $query);
    }
}
