<?php

namespace App\Http\Controllers\Api\v1;

use App\Helpers\Helper;
use App\Http\Controllers\ApiController;
use App\Http\Requests\TranscriptAPIRequest;
use App\Http\Resources\Transcript\TranscriptResource;
use App\Models\UserCourse;

class TranscriptController extends ApiController
{
    public function index(TranscriptAPIRequest $request)
    {
        $category_id = $request->get('category_id');
        $school_id = $request->get('school_id');
        $class_id = $request->get('class_id');
        $searchText = $request->get('search_text');
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $perPage = $request->get('per_page');
        $page = $request->get('page');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);

        if (is_null($school_id) && !is_null($class_id)) {
            $class = Classes::findOrFail($class_id);
            $school_id = $class->school->id;
        }
        // if (!is_null($course_id)) {
        //     $course = Course::findOrFail($course_id);
        //     $category_id = $course->category->id;
        // }
        $usersCourses = UserCourse::whereIn('status', [1, 2])->orderBy('course_id');
        if (!is_null($school_id)) {
            $usersCourses = $usersCourses->whereHas('student', function ($query) use ($school_id) {
                $query = $query->where('school_id', $school_id);
            });
        }
        if (!is_null($category_id)) {
            $usersCourses = $usersCourses->whereHas('course', function ($query) use ($category_id) {
                $query = $query->where('course_category_id', $category_id);
            });
        }
        if (!is_null($class_id)) {
            $usersCourses = $usersCourses->whereHas('student', function ($query) use ($class_id) {
                $query = $query->where('class_id', $class_id);
            });
        }
        if (!is_null($searchText)) {
            $usersCourses = $usersCourses->whereHas('student', function ($query) use ($searchText) {
                $query = $query->where('name', 'LIKE','%'.$searchText.'%');
            });
        }  
        // if (!is_null($course_id)) {
        //     $usersCourses = $usersCourses->whereHas('course', function ($query) use ($course_id) {
        //         $query = $query->where('course_id', $course_id);
        //     });
        // }
        if (!is_null($dateBegin)) {
            $usersCourses->whereDate('join_date', '>=', $dateBegin);
        }
        if (!is_null($dateEnd)) {
            $usersCourses->whereDate('date_end', '<=', $dateEnd);
        }
        
        $stt = 1;
        $result =[];
        foreach ($usersCourses->get() as $usersCourse) {
            $data['stt'] = $stt;
            $data['id'] = $usersCourse->id;
            $data['student_name'] = !is_null($usersCourse->student) ? $usersCourse->student->name : null;
            $data['school_name'] = !is_null($usersCourse->student) ? (!is_null($usersCourse->student->school) ? $usersCourse->student->school->name : null) : null;
            $data['class_name'] = !is_null($usersCourse->student) ? !is_null($usersCourse->student->classes) ? $usersCourse->student->classes->name : null : null;
            $data['course_name'] = !is_null($usersCourse->course) ? $usersCourse->course->name : null;
            $data['join_date'] = !is_null($usersCourse->join_date) ? Helper::formatDate1($usersCourse->join_date) : null;
            $data['end_date'] = $usersCourse->status == 2 && !is_null($usersCourse->date_end) ? Helper::formatDate1($usersCourse->date_end) : null;
            $data['multiple_choice_scores'] = round($usersCourse->count_point_choose, 2);
            $data['essay_score'] = $usersCourse->count_point_essay == 0 ? null : $usersCourse->count_point_essay;
            $data['total_score'] = ($usersCourse->count_point_choose * (!is_null($usersCourse->course) ? $usersCourse->course->total_point_choice : 0) + $usersCourse->count_point_essay * ((!is_null($usersCourse->course) ? $usersCourse->course->total_point : 0) - $usersCourse->course->total_point_choice)) / 100;
            $stt++;
            array_push($result,$data);
        }
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $dataResponse = collect($result)->forPage($page, $perPage);
        return $this->formatCollectionJson(TranscriptResource::class,$dataResponse);
    }
}
