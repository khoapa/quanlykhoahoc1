<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateStatusMapRequest;
use App\Http\Requests\User\AddLastPositionUserRequest;
use App\Http\Resources\Map\MapCollection;
use App\Http\Resources\Map\MapResource;
use App\Models\Map;
use App\Models\UserCategoryMap;
use App\Models\UserMap;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use phpDocumentor\Reflection\Location;
use App\Models\LocationUser;

class MapController extends ApiController
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $perPage = $request->get('per_page');
        $searchText = $request->get('search_text');
        $distance = $request->get('distance');
        $query = Map::orderBy('id', 'DESC')
            ->select(
                'id',
                'title',
                'map_category_id',
                'icon',
                'description',
                'phone',
                'hotline',
                'location',
                'latitude',
                'longitude'
            );
        if (!is_null($user->latitude) && !is_null($user->longitude)) {
            $query = $query->addSelect(DB::raw('(6371 * acos (
                    cos ( radians(' . $user->latitude . ') )
                    * cos( radians( latitude ) )
                    * cos( radians( longitude ) - radians(' . $user->longitude . ') )
                    + sin ( radians(' . $user->latitude . ') )
                    * sin( radians( latitude ) )
                  )
                ) AS distance'));
                
                if (!is_null($distance)) {
                    if ($distance <= 20)
                        $query = $query->having('distance', '<', $distance);
                    else {
                        $query = $query->having('distance', '>', 20);
                    }
                }
        }
        
        if (!is_null($searchText)) {
            $query = $query->where('title', 'LIKE', '%' . $searchText . '%');
        }
        

        $userCategoryMap = UserCategoryMap::where('user_id', $user->id)->where('status', 2)->pluck('category_map_id');
        $query = $query->whereNotIn("map_category_id", $userCategoryMap);
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->latest()->paginate($perPage);
        return $this->formatJson(MapCollection::class, $query);
    }

    public function show($id)
    {
        $map = Map::findOrFail($id);
        return $this->formatJson(MapResource::class, $map);
    }

    public function addTheLastPosition(AddLastPositionUserRequest $request)
    {
        try {
            $data = $request->getData();
            DB::beginTransaction();
            $user = auth()->user();
            $date = Carbon::now('Asia/Ho_Chi_Minh');
            $user->update(['longitude' => $data['long'], 'latitude' => $data['lat'],'date_tracking'=>$date]);
            $lat = $data['lat'];
            $long = $data['long'];
            $address=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&key=AIzaSyAZlHcoPe33iRtvrqFr1o_ZU50pV6JRVJs");
            $json_data=json_decode($address);
            $full_address=$json_data->results[0]->formatted_address;

            $dataLocation = [
                'user_id' => $user->id,
                'longitude' => $data['long'], 
                'latitude' => $data['lat'],
                'date_tracking'=>$date,
                'address' => $full_address
            ];
            LocationUser::create($dataLocation);
            DB::commit();
            return $this->sendMessage('Cập nhật vị trí thành công.');
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
