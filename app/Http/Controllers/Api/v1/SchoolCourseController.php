<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SchoolCourseApiRequest;
use App\Http\Controllers\ApiController;
use App\Helpers\Helper;
use App\Models\Course;
use App\Http\Resources\SchoolCourseResource;
use App\Models\School;

class SchoolCourseController extends ApiController
{
    public function index(SchoolCourseApiRequest $request){
            $schoolId = $request->get('school_id');
            $searchText = $request->get('search_text');
            $dateBegin = $request->get('date_begin');
            $dateEnd = $request->get('date_end');
            $perPage = $request->get('per_page');
            $page = $request->get('page');
            $dateBegin = Helper::formatSqlDate($dateBegin);
            $dateEnd = Helper::formatSqlDate($dateEnd);
            $stt = 1;
            $result =[];
            if(is_null($schoolId)){
                $listCourses = Course::orderBy('name');
                if(!is_null($searchText)){
                    $listCourses = $listCourses->where('name','LIKE','%'.$searchText.'%');
                }
                foreach ($listCourses->get() as $course) {
                    $data['stt'] = $stt;
                    $data['id'] = $course->id;
                    $data['course_name'] = $course->name;
                    $data['category_course_name'] =$course->category->name;
                    $data['total_student_participating'] = $course->getCountUserCountAll($dateBegin, $dateEnd);
                    $data['total_student_joined'] = $course->getCountUserJoinAll($dateBegin, $dateEnd);
                    $data['total_student_visit'] = $course->getCountUserVisitAll($dateBegin, $dateEnd);
                    $stt++;
                    array_push($result,$data);
                }
            }else{
                    $listCourses = Course::with('school');
                    if(!is_null($searchText)){
                        $listCourses = $listCourses->where('name','LIKE','%'.$searchText.'%');
                    }
                    $listCourses = $listCourses->whereHas('school', function ($q) use ($schoolId){
                        $q->where('school_id',$schoolId)->orderBy('course_id');
                    });
                    foreach($listCourses->get() as $course) {
                        $data['stt'] = $stt;
                        $data['id'] = $course->id;
                        $data['course_name'] = $course->name;
                        $data['category_course_name'] =$course->category->name;
                        $data['total_student_participating'] =  $course->getCountUserCount($schoolId, $dateBegin, $dateEnd);
                        $data['total_student_joined'] = $course->getCountUserJoin($schoolId, $dateBegin, $dateEnd);
                        $data['total_student_visit'] = $course->getCountUserVisit($schoolId, $dateBegin, $dateEnd);
                        $stt++;
                        array_push($result,$data);
                }
            }
            $perPage = $perPage ? $perPage : config('settings.countPaginate');
            $dataResponse = collect($result)->forPage($page, $perPage);
            return $this->formatCollectionJson(SchoolCourseResource::class,$dataResponse);
    }
}