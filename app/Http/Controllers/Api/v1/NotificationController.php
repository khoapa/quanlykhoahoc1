<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Requests\UpdateGetNotificationRequest;
use App\Models\Notification;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\NotificationResource;
use App\User;

class NotificationController extends ApiController
{
    public function index(Request $request)
    {
        $perPage = $request->get('per_page');
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $notifications = Notification::ofUser(auth('api')->user())->paginate($perPage);
        // $notification_update = Notification::ofUser(auth('api')->user())->update(['status' => 1]);
        return $this->formatJson(NotificationCollection::class, $notifications);
    }

    public function notifcations()
    {
        $notifications = Notification::unread(auth('api')->user())->get();
        if (count($notifications) < 10) {
            $notifications = Notification::ofUser(auth('api')->user())->get()->take(10);
        }
        return $this->formatCollectionJson(NotificationResource::class, $notifications);
    }

    public function unread()
    {
        $notifications = Notification::unread(auth('api')->user())->get()->count();
        return  response()->json([
            'total' => $notifications
        ]);
    }

    public function readAll()
    {
        $notifications = Notification::where('receiver_id', auth('api')->user()->id)->get();
        $notifications_update = Notification::where('receiver_id', auth('api')->user()->id)->update(['status' => 1]);
        return $this->formatCollectionJson(NotificationResource::class, $notifications);
    }
    public function readDetail($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->update(['status' => 1]);
        return $this->formatJson(NotificationResource::class, $notification);
    }
    public function userUpdateGetNotification(UpdateGetNotificationRequest $request)
    {
        $user = auth()->user();
        $notification_status = $request->get('notification_status');
        $user = $user->update(['notification_status' => $notification_status]);
        if ($notification_status == 1) {
            return $this->sendMessage('Bạn đã bật nhận thông báo.');
        } else {
            return $this->sendMessage('Bạn đã tắt nhận thông báo.');
        }
    }
}
