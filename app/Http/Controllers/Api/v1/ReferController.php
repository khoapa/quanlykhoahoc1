<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\CourseCollection;
use App\Http\Resources\Refer\ReferCollection;
use App\Http\Resources\Refer\ReferResource;
use App\Models\Refer;
use Illuminate\Http\Request;

class ReferController extends ApiController
{
    public function index(Request $request){
        $perPage = $request->get('per_page');
        $query = Refer::orderBy('id','DESC');
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->latest()->paginate($perPage);
        return $this->formatJson(ReferCollection::class, $query);
    }

    public function show($id){
        $refer = Refer::findOrFail($id);
        return $this->formatJson(ReferResource::class,$refer);
      }
}
