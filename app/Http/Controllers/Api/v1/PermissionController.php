<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Role;
use App\Http\Resources\RoleResource;
use App\Models\Permission;
use App\Http\Resources\PermissionCollection;
use App\Http\Resources\PermissionResource;
use App\Models\RoleHasPermission;
use App\Http\Requests\UpdatePermissionApiRequest;
use App\Http\Requests\CheckAllPermissionApiRequest;

class PermissionController extends ApiController
{
    public function index(){
        $roles = Role::where('name','!=','Admin');
        $user = auth('api')->user();
        if($user->hasRole('school')){
            $schoolID = $user->school_id;
            $roles->where('id_school',$schoolID);
        }
        if($user->hasRole('Admin')){
            $schoolID = $user->school_id;
            $roles->whereNull('id_school');
        }
        $roles = $roles->get();
        return $this->formatCollectionJson(RoleResource::class,$roles);
    }
    public function list($id){
        $role = Role::findOrFail($id);
        $permissions = Permission::select('id','subject','name','description','group')->whereNotNull('group')->get()->groupBy('group');
        return $this->formatCollectionJson(PermissionCollection::class,$permissions);
    }
    public function update($id,UpdatePermissionApiRequest $request){
        $role = Role::findOrFail($id);
        $dataPermission = $request->get('id_permission');
        $rolePermission = RoleHasPermission::where('role_id', $id)
        ->where('permission_id', $dataPermission)->first();
        $status = $request->get('status');
        if($status == 1 && empty($rolePermission)){
            $permission = Permission::findOrFail($dataPermission);
            $role->givePermissionTo($permission->name);
        }
        if($status == 2 && !empty($rolePermission)){
            $permission = Permission::findOrFail($dataPermission);
            $role->revokePermissionTo($permission->name);
        }
         $permissions = Permission::select('id','subject','name','description','group')->whereNotNull('group')->get()->groupBy('group');
         return $this->formatCollectionJson(PermissionCollection::class,$permissions);
    }
    public function checkAll($id,CheckAllPermissionApiRequest $request){
        $role = Role::findOrFail($id);
        $type = $request->get('type');
        $status = $request->get('status');
        if($type == 1){
            $subject  = "Thêm";
        }elseif($type == 2){
            $subject = "Sửa";
        }elseif($type==3){
           $subject = "Xem";
        }else {
            $subject = "Xóa";
        }
        $dataPermission = Permission::where('description','LIKE','%'.$subject.'%')->get();
        if(!empty($dataPermission)){
            foreach ($dataPermission as $value) {
                $rolePermission = RoleHasPermission::where('role_id', $id)
                ->where('permission_id', $value->id)->first();
                if($status == 1 && empty($rolePermission)){
                    $permission = Permission::findOrFail($value->id);
                    $role->givePermissionTo($permission->name);
                }
                if($status == 2 && !empty($rolePermission)){
                    $permission = Permission::findOrFail($value->id);
                    $role->revokePermissionTo($permission->name);
                }
                }
            }
         $permissions = Permission::select('id','subject','name','description','group')->whereNotNull('group')->get()->groupBy('group');
        return $this->formatCollectionJson(PermissionCollection::class,$permissions);
    }
}