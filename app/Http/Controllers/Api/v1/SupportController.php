<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Support;
use App\Http\Resources\SupportCollection;
use App\Http\Resources\SupportResource;

class SupportController extends ApiController
{
    public function index(Request $request){
        $perPage = $request->get('per_page');
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $supports = Support::latest()->paginate($perPage);
        return $this->formatJson(SupportCollection::class,$supports);
    }
    public function show($id){
        $support = Support::findOrFail($id);
        return $this->formatJson(SupportResource::class,$support);
    }
}
