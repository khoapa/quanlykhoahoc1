<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\CourseCategory;
use App\Models\Course;
use App\Http\Resources\CourseCollection;
use App\Enums\StatusCourseEnum;
use App\Enums\UserTypeEnum;
use App\Http\Resources\CourseResource;
use App\Http\Resources\CourseDetailResource;
use App\Models\Question;
use App\Http\Resources\CourseAllCollection;
use Facade\FlareClient\Api;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Models\UserCourse;
use App\Http\Requests\ReplyRequest;
use App\Models\UserCourseResult;
use App\Http\Requests\UpdateUserApiRequest;
use App\Http\Resources\CourseResultResource;
use App\Http\Resources\CouresNewCollection;
use App\Http\Requests\ResultRequest;
use App\Http\Requests\CourseHistoryRequest;
use App\Http\Requests\UpdateStatusCourseRequest;
use App\Http\Resources\Course\CourseAPIAdminCollection;
use App\Http\Resources\Course\CourseAPIAdminResource;
use App\Http\Resources\CourseLikeCollection;
use App\Http\Resources\CourseReplyResource;
use App\Http\Requests\MarkRequestApi;

class CourseController extends ApiController
{
  public function index(Request $request)
  {
    $categoryId = $request->get('category_id');
    $searchText = $request->get('search_text');
    $perPage = $request->get('per_page');
    $course_id = UserCourse::where('user_id', auth()->user()->id)->pluck('course_id');
    $query = Course::with('category')->whereNotIn('id', $course_id);
    $user = auth()->user();
    if ($user->admin_flg != 1) {
      $school_id = $user->school_id;
      if ($school_id) {
        $query = $query->whereHas('school', function ($q) use ($school_id) {
          $q = $q->where('school_id', $school_id);
        });
      }
    }
    if (!is_null($categoryId)) {
      $query = $query->where('course_category_id', '=', $categoryId);
    }
    if (!is_null($searchText)) {
      $query = $query->where('name', 'LIKE', '%' . $searchText . '%');
    }
    $perPage = $perPage ? $perPage : config('settings.countPaginate');
    $query = $query->whereHas('question')->where('status', 1)->latest()->paginate($perPage);
    return $this->formatJson(CourseCollection::class, $query);
  }
  public function listOffer(Request $request)
  {
    $searchText = $request->get('search_text');
    $perPage = $request->get('per_page');
    $course_id = UserCourse::where('user_id', auth()->user()->id)->pluck('course_id');
    $query = Course::orderBy('id', 'DESC')->whereNotIn('id', $course_id);
    $user = auth()->user();
    if ($user->admin_flg != 1) {
      $school_id = $user->school_id;
      if ($school_id) {
        $query = $query->whereHas('school', function ($q) use ($school_id) {
          $q = $q->where('school_id', $school_id);
        });
      }
    }
    if (!is_null($searchText)) {
      $query = $query->where('name', 'LIKE', '%' . $searchText . '%');
    }
    $perPage = $perPage ? $perPage : config('settings.countPaginate');
    $query = $query->whereHas('question')->where('offer', 1)->where('status', StatusCourseEnum::SHOW)->paginate($perPage);
    return $this->formatJson(CourseCollection::class, $query);
  }
  public function listNew(Request $request)
  {
    $searchText = $request->get('search_text');
    $perPage = $request->get('per_page');
    $course_id = UserCourse::where('user_id', auth()->user()->id)->pluck('course_id');
    $query = Course::orderBy('id', 'DESC')->whereNotIn('id', $course_id);
    $user = auth()->user();
    if ($user->admin_flg != 1) {
      $school_id = $user->school_id;
      if ($school_id) {
        $query = $query->whereHas('school', function ($q) use ($school_id) {
          $q = $q->where('school_id', $school_id);
        });
      }
    }
    if (!is_null($searchText)) {
      $query = $query->where('name', 'LIKE', '%' . $searchText . '%');
    }
    $perPage = $perPage ? $perPage : config('settings.countPaginate');
    $query = $query->whereHas('question')->where('new', 1)->where('status', StatusCourseEnum::SHOW)->paginate($perPage);
    return $this->formatJson(CourseCollection::class, $query);
  }
  public function show($id)
  {
    $user = auth()->user();
    $course = Course::findOrFail($id);
    $data  = [
      'join_date' => Helper::formatSqlDate(Carbon::now()),
      'course_id' => $id,
      'user_id' => $user->id,
      'status' => 1,
      'number_of_visits' => 1
    ];
    $user_course = UserCourse::where('user_id', $user->id)->where('course_id', $id)->where('status', 1)->first();
    if (!$user_course) {
      $user_course = UserCourse::create($data);
    }
    if ($user_course) {
      $number_of_visits = !is_null($user_course->number_of_visits) ? $user_course->number_of_visits : 0;
      $user_course = $user_course->update(['number_of_visits' => $number_of_visits + 1]);
    }
    return $this->formatJson(CourseDetailResource::class, $course);
  }
  public function listAll(Request $request)
  {
    $searchText = $request->get('search_text');
    $perPage = $request->get('per_page');
    $course_id = UserCourse::where('user_id', auth()->user()->id)->pluck('course_id');
    $query = CourseCategory::orderBy('position', 'ASC')->whereHas('course', function ($q) use ($course_id) {
      $q->whereNotIn('id', $course_id)->where(function ($query) {
        $query->whereHas('question');
      })->where('status', StatusCourseEnum::SHOW);
    });
    if (!is_null($searchText)) {
      $query = $query->where('name', 'LIKE', '%' . $searchText . '%');
    }
    $perPage = $perPage ? $perPage : config('settings.countPaginate');
    $query = $query->paginate($perPage);
    return $this->formatJson(CourseAllCollection::class, $query);
  }
  public function join($id)
  {
    $user = auth()->user();
    $data  = [
      'join_date' => Helper::formatSqlDate(Carbon::now()),
      'course_id' => $id,
      'user_id' => $user->id,
      'status' => 1,
      'number_of_visits' => 1
    ];
    $course = UserCourse::where('user_id', $user->id)->where('course_id', $id)->where('status', 1)->first();
    if ($course) {
      return $this->sendMessage('Tham gia khóa học thành công.');
    }
    UserCourse::create($data);
    return $this->sendMessage('Tham gia khóa học thành công.');
  }
  public function favourite($id)
  {
    $course = Course::findOrFail($id);
    $user = auth()->user();
    $favorite = $course->userFavourite()->firstOrNew(['user_id' => $user->id]);
    $status = is_null($favorite->id);
    $status ? $user->userFavourite()->attach($id) : $user->userFavourite()->detach($id);
    if ($status == 1) {
      return $this->sendMessage('Khóa học đã được thêm vào danh sách yêu thích.');
    } else {
      return $this->sendMessage('Bỏ yêu thích khóa học thành công.');
    }
  }
  public function history(Request $request)
  {
    $searchText  = $request->get('search_text');
    $status = $request->get('status');
    $user = auth()->user();
    $perPage = $request->get('per_page');
    $perPage = $perPage ? $perPage : config('settings.countPaginate');
    $query = UserCourse::where('user_id', $user->id);
    if (!is_null($status)) {
      $query = $query->where('status', $status);
    }

    if (is_null($status)) {
      $query = $query->where(function ($q2) {
        $q2->where('status', 1)
          ->orWhere('status', 2);
      });
    }
    if (!is_null($searchText)) {
      $query = $query->whereHas('course', function ($q) use ($searchText) {
        $q->where('name', 'LIKE', '%' . $searchText . '%');
      });
    }
    $courses = $query->latest()->paginate($perPage);
    return $this->formatJson(CouresNewCollection::class, $courses);
  }
  public function listFavourite(Request $request)
  {
    $user = auth()->user();
    $perPage = $request->get('per_page');
    $perPage = $perPage ? $perPage : config('settings.countPaginate');
    $courses = $user->userFavourite()->paginate($perPage);
    return $this->formatJson(CourseLikeCollection::class, $courses);
  }
  public function reply(ReplyRequest $request, $id)
  {
    $course = Course::findOrFail($id);
    $user = auth()->user();
    $userCourse = UserCourse::where('user_id', $user->id)->where('course_id', $id)->where('status', 1)->first();
    if (!$userCourse) {
      $dataCourse = [
        'join_date' => Helper::formatSqlDate(Carbon::now()),
        'course_id' => $id,
        'user_id' => $user->id,
        'status' => 1,
        'number_of_visits' => 1
      ];
      $userCourse = UserCourse::create($dataCourse);
    }
    $data = $request->all();
    $point = 0;
    foreach ($data as $d) {
      $question = Question::findOrFail($d['id_question']);
      if ($d['type'] == 1 && $question->multiselect == 0) {
        $answerCorrect = $question->answer()->where('correct', 1)->first();
        if ($d['answer'] == $answerCorrect->id) {
          $point = $question->point;
        }
      } elseif ($d['type'] == 1 && $question->multiselect == 1) {
        $answerCorrect = $question->answer()->where('correct', 1)->pluck('id')->toArray();
        $arrAnswer = explode(',', $d['answer']);
        $diffArr = array_diff($answerCorrect, $arrAnswer);
        if (empty($diffArr) && count($answerCorrect) == count($arrAnswer)) {
          $point = $question->point;
        }
      } else {
        $point = 0;
      }
      $arr = [
        'user_course_id' => $userCourse->id,
        'question_id' => $d['id_question'],
        'answer' => $d['answer'],
        'point' => $point
      ];
      $create = UserCourseResult::create($arr);
    }
    $date = Carbon::now('Asia/Ho_Chi_Minh');
    $dataStatus  = [
      'status' => 2,
      'date_end' => $date,
    ];
    $dataUserCourse  = [
      'join_date' => $userCourse->join_date,
      'course_id' => $id,
      'user_id' => $user->id,
      'status' => 1,
      'number_of_visits' => 1
    ];
    $userCourse->update($dataStatus);
    // UserCourse::create($dataUserCourse);
    return $this->formatJson(CourseResultResource::class, $userCourse);
  }
  public function result(ResultRequest $request)
  {
    $user = auth()->user();
    $id_user_course = $request->id_user_course;
    $userCourse = UserCourse::findOrFail($id_user_course);
    return $this->formatJson(CourseResultResource::class, $userCourse);
  }
  public function listCourseAdmin(Request $request)
  {
    $categoryId = $request->get('category_id');
    $perPage = $request->get('per_page');
    $searchText  = $request->get('search_text');
    $query = Course::with('category');
    $user = auth()->user();
    if (
      $user->user_type == UserTypeEnum::STUDENT ||
      $user->hasRole('user') ||
      $user->user_type == UserTypeEnum::TEACHER ||
      $user->hasRole('teacher')
    ) {
      return $this->sendMessage('Không có quyền thực hiện.', 403);
    }

    if ($user->admin_flg != 1) {
      $school_id = $user->school_id;
      if ($school_id) {
        $query = $query->whereHas('school', function ($q) use ($school_id) {
          $q = $q->where('school_id', $school_id);
        });
      }
    }
    if (!is_null($categoryId)) {
      $query = $query->where('course_category_id', '=', $categoryId);
    }
    if (!is_null($searchText)) {
      $query = $query->where('name', 'LIKE', '%' . $searchText . '%');
    }
    $perPage = $perPage ? $perPage : config('settings.countPaginate');
    $query = $query->latest()->paginate($perPage);
    return $this->formatJson(CourseAPIAdminCollection::class, $query);
  }
  public function updateStatusCourse(UpdateStatusCourseRequest $request, $id)
  {
    $user = auth()->user();
    if (
      $user->user_type == UserTypeEnum::STUDENT ||
      $user->hasRole('user') ||
      $user->user_type == UserTypeEnum::TEACHER ||
      $user->hasRole('teacher')
    ) {
      return $this->sendMessage('Không có quyền thực hiện.', 403);
    }

    $data = $request->getData();
    $course = Course::find($id);
    if ($course) {
      $course = $course->update($data);
      return $this->sendMessage('Cập nhật trạng thái thành công.');
    } else {
      return $this->sendError404('Khoá học không tồn tại.');
    }
  }
  public function showMark($id){
    $userCourse = UserCourse::findOrFail($id);
    return $this->formatJson(CourseReplyResource::class, $userCourse);
  }
  public function mark(MarkRequestApi $request,$id){
    $userCourse = UserCourse::findOrFail($id);
    $course = Course::findOrFail($userCourse->course_id);
    $countQuestionEssay = $course->question->where('type', 2)->count();
    $percentageChoice = $course->total_point_choice;
    $percentageEssay = 100 - $percentageChoice;

    if ($countQuestionEssay >= 1) {
        $pointOfOneQuestionsEssay = $percentageEssay / $countQuestionEssay;
    };
    $data = $request->all();
    foreach ($data as $d) {
            $pointEssay = round(($d['mark'] / 100) * $pointOfOneQuestionsEssay, 2);
            UserCourseResult::where('id', $d['id_answer_essay'])->update(['point' => $pointEssay]);
    };
    return $this->formatJson(CourseReplyResource::class, $userCourse);
  }
}
