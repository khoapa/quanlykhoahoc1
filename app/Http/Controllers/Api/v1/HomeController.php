<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Resources\Home\HomeResource;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\News;
use App\Models\UserCourse;
use Illuminate\Support\Str;

class HomeController extends ApiController
{
    public function getHome()
    {
        $user = auth()->user();

        $queryCourseCategory = CourseCategory::select("id", 'name', 'thumbnail', 'position')->orderBy('position', 'asc')->get();
        $queryNews = News::select("id", 'title', 'thumbnail', 'news_category_id', 'content', 'priority', 'link', 'status')
            ->where("priority", 1)->where("status", 1)->orderBy('id', 'DESC')->get();
        $queryOldNews = News::select("id", 'title', 'thumbnail', 'news_category_id', 'content', 'priority', 'link', 'status')
        ->where("priority", 2)->where("status", 1)->orderBy('id', 'DESC')->take(6)->get();

        $queryNewCourse = Course::select("id", 'name', 'course_category_id', 'cover_image', 'start_date', 'longtime', 'status', 'new', 'offer', 'total_point', 'total_point_choice')
            ->where("status", 1)->whereHas('question')->where("new", 1);

        $queryOfferCourse = Course::select("id", 'name', 'course_category_id', 'cover_image', 'start_date', 'longtime', 'status', 'new', 'offer', 'total_point', 'total_point_choice')
            ->where("status", 1)->whereHas('question')->where("offer", 1);

        if ($user->admin_flg != 1) {
            $school_id = $user->school_id;
            $queryNewCourse = $queryNewCourse->whereHas('school', function ($q) use ($school_id) {
                $q = $q->where('school_id', $school_id);
            });
            $queryOfferCourse = $queryOfferCourse->whereHas('school', function ($q) use ($school_id) {
                $q = $q->where('school_id', $school_id);
            });
        }

        $queryNewCourse = $queryNewCourse->with('category:id,name')->orderBy('id', 'DESC')->take(5)->get();
        $queryOfferCourse = $queryOfferCourse->with('category:id,name')->orderBy('id', 'DESC')->take(5)->get();
        $data['new_courses'] = null;
        $data['offer_courses'] = null;
        $data['news'] = null;
        $data['old_news'] = null;
        foreach ($queryNews as $key => $news) {
            $data['news'][$key] = $news->setAttribute('description', Str::limit(preg_replace("/\r|\n/", "", strip_tags(html_entity_decode($news->content))), 150));
        }
        foreach ($queryOldNews as $key => $news) {
            $data['old_news'][$key] = $news->setAttribute('description', Str::limit(preg_replace("/\r|\n/", "", strip_tags(html_entity_decode($news->content))), 150));
        }
        foreach ($queryNewCourse as $key => $course) {
            $status_user = UserCourse::where('course_id', $course->id)->where('user_id', $user->id)->first();
            $data['new_courses'][$key] = $course->setAttribute('status_user', is_null($status_user) ? null : $status_user->status);
        }
        foreach ($queryOfferCourse as $key => $offerCourse) {
            $status_user = UserCourse::where('course_id', $offerCourse->id)->where('user_id', $user->id)->first();
            $data['offer_courses'][$key] = $offerCourse->setAttribute('status_user', is_null($status_user) ? null : $status_user->status);
        }

        $data['course_categories'] = $queryCourseCategory;
        $data['user']["id"] = $user->id;
        $data['user']["name"] = $user->name;
        return $data;
    }
}
