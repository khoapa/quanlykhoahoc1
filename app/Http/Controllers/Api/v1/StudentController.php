<?php

namespace App\Http\Controllers\Api\v1;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\ListStudentRequest;
use App\Http\Requests\UpdateStatusStudenInClassRequest;
use App\Http\Requests\UpdateStatusStudentRequest;
use App\Http\Resources\Student\StudentResource;
use App\Http\Resources\UserListStudentsCollection;
use App\Http\Resources\UserNotifyResource;
use App\Models\Classes;
use App\Models\School;
use App\User;
use Illuminate\Http\Request;

class StudentController extends ApiController
{

    public function index(ListStudentRequest $request)
    {
        $user = auth('api')->user();
        $class = null;
        $perPage = $request->get('per_page');
        $searchText = $request->get('search_text');
        $school_id = $request->get('school_id');
        $class_id = $request->get('class_id');

        if ($user->user_type == UserTypeEnum::STUDENT || $user->hasRole('user')) {
            return $this->sendMessage('Không có quyền thực hiện.', 403);
        }

        $query = User::orderBy('id', 'DESC')->where('user_type', UserTypeEnum::STUDENT);
        if ($user->user_type == UserTypeEnum::TEACHER) {
            if ($user->hasRole('teacher')) {
                $school_id = $user->school_id;
                $class_id = $user->class_id;
                $class = Classes::find($user->class_id);
                if (!$class) {
                    return $this->sendError404('Lớp của người dùng không tồn tại.');
                }
            } else {
                return $this->sendMessage('Không có quyền thực hiện.', 403);
            }
        }

        if ($user->hasRole('school')) {
            $school_id = $user->school_id;
            $school = School::find($user->school_id);
            if (!$school) {
                return $this->sendError404('Trường của người dùng không tồn tại.');
            }
        }
        if ($school_id) {
            $query = $query->where('school_id', $school_id);
        }
        if ($class_id) {
            $query = $query->where('class_id', $class_id);
        }
        if (!is_null($searchText)) {
            $query = $query->where('name', 'LIKE', '%' . $searchText . '%');
        }

        if (!is_null($perPage)) {
            $query = $query->latest()->paginate($perPage);
            return $this->formatJson(UserListStudentsCollection::class, $query);
        } else {
            return ['data' => $this->formatCollectionJson(UserNotifyResource::class, $query->get()), 'total' => $query->count()];
        }
    }

    public function show($id)
    {
        $user = auth('api')->user();

        if ($user->user_type == UserTypeEnum::STUDENT || $user->hasRole('user')) {
            return $this->sendMessage('Không có quyền thực hiện.', 403);
        }

        $user = User::find($id);
        if (!$user) {
            return $this->sendMessage404("Người dùng không tồn tại trong hệ thống.");
        } else {
            return $this->formatJson(StudentResource::class, $user);
        }
    }

    public function updateStatusStudent(UpdateStatusStudentRequest $request)
    {
        $user = auth('api')->user();
        $data = $request->all();
        if ($user->user_type == UserTypeEnum::STUDENT || $user->hasRole('user')) {
            return $this->sendMessage('Không có quyền thực hiện.', 403);
        }

        foreach ($data as $value) {
            $user =  User::find($value['user_id']);
            if ($user)
                $user->update(["status" => $value["status"]]);
        }

        return $this->sendMessage("Cập nhật trạng thái học sinh thành công.");
    }
    public function updateStatusStudentFollowClass(UpdateStatusStudenInClassRequest $request, $id)
    {
        $user = auth('api')->user();
        if ($user->user_type == UserTypeEnum::STUDENT || $user->hasRole('user')) {
            return $this->sendMessage('Không có quyền thực hiện.', 403);
        }
        $users = User::where('class_id', $id)
            ->where('user_type', UserTypeEnum::STUDENT)
            ->update(['status' => $request->get('status')]);

        return $this->sendMessage("Cập nhật trạng thái học sinh thành công.");
    }
}
