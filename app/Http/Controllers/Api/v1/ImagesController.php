<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UploadImageRequest;
use App\Services\UploadService;

class ImagesController extends Controller
{
    public function uploadImage(UploadImageRequest $request){
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $image = UploadService::uploadImage('avatar',$file);
        }
        return response()->json($image);
    }
}
