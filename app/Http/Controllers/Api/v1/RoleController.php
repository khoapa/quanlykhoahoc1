<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Role\RoleSelectResource;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends ApiController
{
    public function listSelect()
    {
        $role = Role::select('id','name')->get();
        return $role;
    }
}
