<?php

namespace App\Http\Controllers\Api\v1;

use App\Helpers\Helper;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\StatisticAPIRequest;
use App\Http\Requests\TranscriptAPIRequest;
use App\Http\Resources\Statistic\StatisticResource;
use App\Http\Resources\Transcript\TranscriptResource;
use App\Models\School;
use Illuminate\Http\Request;

class StatisticController extends ApiController
{
    public function index(StatisticAPIRequest $request)
    {
        $schoolId = $request->get('school_id');
        $perPage = $request->get('per_page');
        $page = $request->get('page');
        $dateBegin = null;
        $dateEnd = null;
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);
        $query = School::orderBy('id', 'ASC');

        if (!is_null($schoolId)) {
            $query->where('id', $schoolId);
        }

        $schools = $query->get();
        $stt = 1;
        $result = [];
        foreach ($schools as $key => $school) {
            $data['stt'] = $stt;
            $data['school_id'] = $school->id;
            $data['school_code'] = $school->school_code;
            $data['school_name'] = $school->name;
            $data['class_count'] = $school->classes->count();
            $data['course_course'] = $school->course->count();
            $data['course_category_count'] = $school->course_category_count;
            $data['student_count'] = $school->getCourseUserJoin($dateBegin, $dateEnd);
            $data['student_join_count'] = $school->getCourseUserJoined($dateBegin, $dateEnd);
            $data['number_visits'] = $school->getCourseUserVisit($dateBegin, $dateEnd);
            $stt++;
            array_push($result, $data);
        }
        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $dataResponse = collect($result)->forPage($page, $perPage);
        return $this->formatCollectionJson(StatisticResource::class, $dataResponse);
    }
}
