<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ApiLoginRequest;
use App\Http\Resources\UserResource;
use JWTAuth;
use App\Http\Controllers\ApiController;
use App\Http\Resources\UserInforResource;
use App\Http\Requests\RegisterApiRequest;
use App\Helpers\Helper;
use App\User;
use App\Models\School;
use App\Http\Resources\ListSchoolResource;
use App\Enums\UserTypeEnum;
use App\Http\Requests\ActionVerifyRequest;
use App\Http\Resources\ListTeacherResource;
use App\Models\Classes;
use App\Http\Requests\ApiForgetPassRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ChangePassRequest;
use App\Http\Requests\ChangePassVerifyRequest;
use App\Http\Requests\ListStudentRequest;
use App\Http\Requests\UpdateProfileUserAPIRequest;
use App\Http\Requests\UpdateUserApiRequest;
use App\Http\Requests\User\AddDeviceTokenRequest;
use App\Http\Resources\Role\RoleSelectResource;
use App\Http\Resources\Student\StudentResource;
use App\Http\Resources\UserListStudentsCollection;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use MarkWilson\XmlToJson\XmlToJsonConverter;
use SimpleXMLElement;

use function GuzzleHttp\json_decode;

class UserController extends ApiController
{
    public function signIn(ApiLoginRequest $request)
    {

        $credentials = $request->only('email', 'password');
        $userCheck = User::where('email', $request->email)->first();
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'Email hoặc mật khẩu không hợp lệ'], 422);
            }
            if (!is_null($userCheck) && $userCheck->status == 2) {
                return response()->json(['message' => 'Tài khoản của bạn đã bị khóa.'], 422);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['message' => 'Không thể tạo token'], 500);
        }
        $user = auth()->user();
        $user = $user->setAttribute('token', $token);
        return $this->formatJson(UserResource::class, $user);
    }
    public function me()
    {
        $user = auth('api')->user();
        return $this->formatJson(UserInforResource::class, $user);
    }
    public function signUp(RegisterApiRequest $request)
    {
        $data = $request->getData();
        if (isset($data['birthday'])) {
            $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        }
        $data['password'] = bcrypt($data['password']);
        $data['avatar'] = config('settings.image');
        if(isset($data['school_id'])){
           $school = School::findOrFail($data['school_id']);
           $data['school_code'] = $school->school_code;
        }
        if(isset($data['class_id'])){
            $class = Classes::findOrFail($data['class_id']);
            $data['class_code'] = $class->class_code;
        }
        $createUser = User::create($data);
        if ($createUser) {
            return $this->sendMessage('Tạo tài khoản thành công.', 200);
        } else {
            return $this->sendError404('Có lỗi khi tạo tài khoản!');
        }
    }

    public function forgetPass(ApiForgetPassRequest $request)
    {
        $data_verify = $request->data_verify;
        try {
            DB::beginTransaction();
            if (filter_var($data_verify, FILTER_VALIDATE_EMAIL)) {
                $user = User::where('email', $data_verify)->first();
                if (is_null($user)) {
                    return $this->sendError404('Email không có trong hệ thống');
                } else {
                    $email_authentication_code = rand(1000, 9999);
                    Mail::send('emails.forgot_password', compact('user', 'email_authentication_code'), function ($message) use ($user) {
                        $message->to($user->email)->subject('Mã xác thực!');
                    });
                    $user->update(["email_authentication_code" => $email_authentication_code]);
                    DB::commit();
                    return $this->sendMessage("Mã xác thực đã được gửi qua email của bạn.");
                }
            } else {
                $user = User::where('phone', $data_verify)->first();
                if (is_null($user)) {
                    return $this->sendError404('Số điện thoại không có trong hệ thống.');
                } else {
                    $phone_authentication_code = rand(1000, 9999);
                    //send sms right now
                    $school_code = config('config_web_service.MaTruong');
                    $user_service = config('config_web_service.User');
                    $pass_service = config('config_web_service.pass');
                    $company_code = config('config_web_service.CompanyCode');
                    $sms_type = config('config_web_service.SMSType');

                    $response_login = Helper::postApi(
                        '<Login xmlns="http://tempuri.org/">
                    <maTruong>' . $school_code . '</maTruong>
                    <userName>' . $user_service . '</userName>
                    <password>' . $pass_service . '</password>
                  </Login>'
                    );
                    // return $response_login->getBody()->getContents();
                    $xml_string = $response_login->getBody()->getContents();
                    $soap = simplexml_load_string($xml_string);
                    $response = $soap->children('http://schemas.xmlsoap.org/soap/envelope/')
                        ->Body->children()
                        ->LoginResponse;

                    $customer_sent_id = (string) $response->LoginResult;
                    $phone = substr_replace($data_verify, "84", 0, 1);
                    $response_send = Helper::postApi(
                        '<SendSMS xmlns="http://tempuri.org/">
                        <aSMS_Input>
                            <SmsType>' . $sms_type . '</SmsType>
                            <IdCustomerSent>' . $customer_sent_id . '</IdCustomerSent>
                            <CompanyCode>' . $company_code . '</CompanyCode>
                            <Mobile>' . $phone . '</Mobile>
                            <SMSContent>' . $phone_authentication_code . '</SMSContent>
                        </aSMS_Input>
                        <userName>' . $user_service . '</userName>
                        <password>' . $pass_service . '</password>
                    </SendSMS>
    '
                    );
                    $user->update(["phone_authentication_code" => $phone_authentication_code]);
                    DB::commit();
                    return $this->sendMessage("Mã xác thực đã được gửi đến số điện thoại.");
                }
            }
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    public function actionVerifyPhone(ActionVerifyRequest $request)
    {
        $data_verify = $request->data_verify;
        $verify_code = $request->verify_code;
        if (filter_var($data_verify, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Email không có trong hệ thống');
            } else {
                if ($user->email_authentication_code === $verify_code) {
                    return $this->sendMessage("Xác thực thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        } else {
            $user = User::where('phone', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Số điện thoại không có trong hệ thống.');
            } else {
                if ($user->phone_authentication_code === $verify_code) {
                    return $this->sendMessage("Xác thực thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        }
    }
    public function changePassVerifyPhone(ChangePassVerifyRequest $request)
    {
        $data_verify = $request->data_verify;
        $verify_code = $request->verify_code;
        $password = bcrypt($request->password);
        if (filter_var($data_verify, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Email không có trong hệ thống');
            } else {
                if ($user->email_authentication_code === $verify_code) {
                    $user->update(["email_authentication_code" => null, 'password' => $password]);
                    return $this->sendMessage("Thay đổi mật khẩu thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        } else {
            $user = User::where('phone', $data_verify)->first();
            if (is_null($user)) {
                return $this->sendError404('Số điện thoại không có trong hệ thống.');
            } else {
                if ($user->phone_authentication_code === $verify_code) {
                    $user->update(["phone_authentication_code" => null, 'password' => $password]);
                    return $this->sendMessage("Thay đổi mật khẩu thành công.");
                } else return $this->sendError404('Mã xác thực không đúng.');
            }
        }
    }

    public function addDeviceToken(AddDeviceTokenRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $device_token = $user->device_token;
        if (is_null($device_token)) {
            $data_tokens[] = (string) $data['device_token'];
            $device_token = json_encode($data_tokens);
            $user->device_token = $device_token;
            $user->save();
        } else {
            $device_token = json_decode($device_token);
            if (!in_array((string) $data['device_token'], $device_token)) {
                array_push($device_token, (string) $data['device_token']);
                $user->device_token = $device_token;
                $user->save();
            }
        }
        return $this->sendMessage("Thêm Device Token thành công!");
    }

    public function logout(AddDeviceTokenRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $device_token = $user->device_token;
        if (!empty($device_token)) {
            $arr_token = json_decode($device_token);
            $index = array_search((string) $data['device_token'], $arr_token);
            unset($arr_token[$index]);
            $device_token = json_encode(array_values($arr_token));
            $user->update(['device_token' => $device_token]);
        }
        JWTAuth::invalidate(JWTAuth::getToken());
        return $this->sendMessage('Đăng xuất thành công!');
    }

    public function changePass(ChangePassRequest $request)
    {
        $password = bcrypt($request->get('password'));
        $user = auth('api')->user()->update([
            'password' => $password
        ]);
        return $this->sendMessage("Thay đổi mật khẩu thành công.");
    }

    public function update(UpdateUserApiRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $update = $user->update($data);
        return $this->sendMessage('Cập nhập thông tin thành công.');
    }

    public function updateUser(UpdateProfileUserAPIRequest $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return $this->sendError404('Người dùng không tồn tại.');
        }
        $data = $request->getData();
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $update = $user->update($data);
        if (isset($data['role_id'])){
        $user->syncRoles([$data['role_id']]);
    }
        return $this->sendMessage('Cập nhập thông tin thành công.');
    }

    public function index(ListStudentRequest $request)
    {
        $user = auth('api')->user();
        $perPage = $request->get('per_page');
        $searchText = $request->get('search_text');
        $school_id = $request->get('school_id');
        $class_id = $request->get('class_id');

        if (
            $user->user_type == UserTypeEnum::STUDENT ||
            $user->hasRole('user') ||
            $user->user_type == UserTypeEnum::TEACHER ||
            $user->hasRole('teacher')
        ) {
            return $this->sendMessage('Không có quyền thực hiện.', 403);
        }

        $query = User::orderBy('user_type', 'DESC')->whereIn('user_type', [UserTypeEnum::STUDENT, UserTypeEnum::TEACHER]);

        if ($user->hasRole('school') && UserTypeEnum::STAFF) {
            $school_id = $user->school_id;
            $school = School::find($user->school_id);
            if ($school) {
                return $this->sendError404('Trường của người dùng không tồn tại.');
            }
        }
        if ($school_id) {
            $query = $query->where('school_id', $school_id);
        }
        if ($class_id) {
            $query = $query->where('class_id', $class_id);
        }
        if (!is_null($searchText)) {
            $query = $query->where('name', 'LIKE', '%' . $searchText . '%');
        }

        $perPage = $perPage ? $perPage : config('settings.countPaginate');
        $query = $query->latest()->paginate($perPage);
        return $this->formatJson(UserListStudentsCollection::class, $query);
    }
    public function show($id)
    {
        $user = auth('api')->user();

        if (
            $user->user_type == UserTypeEnum::STUDENT ||
            $user->hasRole('user') ||
            $user->user_type == UserTypeEnum::TEACHER ||
            $user->hasRole('teacher')
        ) {
            return $this->sendMessage('Không có quyền thực hiện.', 403);
        }

        $user = User::find($id);
        if (!$user) {
            return $this->sendError404("Người dùng không tồn tại trong hệ thống.");
        } else {
            return $this->formatJson(StudentResource::class, $user);
        }
    }
    
    public function resetPass($id)
    {
        $password = bcrypt(config('settings.passwordReset'));

        $current_user = auth()->user();
        if (
            $current_user->user_type == UserTypeEnum::STUDENT ||
            $current_user->hasRole('user') ||
            $current_user->user_type == UserTypeEnum::TEACHER ||
            $current_user->hasRole('teacher')
        ) {
            return $this->sendMessage('Không có quyền thực hiện.', 403);
        }
        $user = User::find($id);
        if ($user) {
            $user->update([
                'password' => $password
            ]);
        } else {
            return $this->sendError404('Người dùng không tồn tại.');
        }
        return $this->sendMessage("Đặt lại mật khẩu thành công.");
    }

    public function listUsertype()
    {
        $user_types = UserTypeEnum::USER_TYPE_ARRAY;
        return $user_types;
    }
}
