<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateStatusMapRequest;
use App\Http\Resources\CategoryMap\CategoryMapResource;
use App\Models\MapCategory;
use App\Models\UserCategoryMap;
use Illuminate\Http\Request;

class CategoryMapController extends ApiController
{
    public function index(){
        $categories = MapCategory::all();
        return $this->formatCollectionJson(CategoryMapResource::class,$categories);
    }

    public function update(UpdateStatusMapRequest $request)
    {
        $data = $request->all();
        foreach ($data as $value) {
            $userId = auth()->user()->id;
        $map =  UserCategoryMap::where("user_id", $userId)->where("category_map_id", $value["map_id"])->first();
        if (is_null($map)) UserCategoryMap::create([
            "user_id" => $userId,
            "category_map_id" => $value["map_id"],
            "status" => $value["status"]
        ]);
        else $map->update(["status" => $value["status"]]);
        }
        
        return $this->sendMessage('Cập nhật trạng thái map thành công.'); 
    }
}
