<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ListClassResource;
use App\Http\Controllers\ApiController;
use App\Models\Classes;

class ClassController extends ApiController
{
    public function index(Request $request){
        $searchText = $request->get('search_text');
        $schoolId = $request->get('school_id');
        $query = Classes::orderBy('id','ASC');
        if(!is_null($schoolId)){
            $query = $query->where('school_id',$schoolId);
        }
        if(!is_null($searchText)){
            $query = $query->where('name','LIKE','%'.$searchText.'%');
        }
        $teachers = $query->get();
        return $this->formatCollectionJson(ListClassResource::class,$teachers);
    }
}
