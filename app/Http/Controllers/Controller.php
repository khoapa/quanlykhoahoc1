<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $back_url;
    public function callAction($method, $parameters)
    {
        // code that runs before any action
        if (in_array($method, ['index','listClass','listUser','listStudent'])) {
            Session::put('back_url', url()->full());
        }
        return parent::callAction($method, $parameters);
    }
}
