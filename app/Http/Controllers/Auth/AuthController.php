<?php

namespace App\Http\Controllers\Auth;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Auth;

class AuthController extends Controller
{
    public function getLogin()
    {
        return view('admins.login');
    }
    public function postLogin(LoginRequest $request)
    {
        $data = $request->getData();
        $email = $data['email'];
        $password = $data['password'];

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = auth()->user();
            $role =$user->role()->first();
            if($user->status == 2){
                Auth::logout();
                return redirect()->route('login')->with('msg', 'Tài khoản của bạn bị khóa.');
            }
            if ($user->user_type != UserTypeEnum::STUDENT && (!is_null($role) && $role->role_id!=4)) {
                return redirect()->route('home.index');
            } else {
                Auth::logout();
         redirect()->route('login');
                return view('errors.403');}
        } else {
            return redirect()->route('login')->with('msg', 'Email hoặc mật khẩu không đúng');
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
