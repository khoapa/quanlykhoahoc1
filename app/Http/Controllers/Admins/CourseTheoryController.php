<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseTheoryRequest;
use App\Http\Requests\CourseTheoryUpdateRequest;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseTag;
use App\Models\CourseTheory;
use App\Services\UploadService;
use Carbon\Carbon;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Models\UserCourseTheory;

class CourseTheoryController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $query = CourseTheory::orderBy('id', 'DESC');
        $categoryId = $request->get('category_id');
        $authId = $request->get('author');
        $name = $request->get('search');
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        if (!is_null($categoryId)) {
            $query = $query->where('course_category_id',$categoryId);
        }
        if (!is_null($authId)) {
            $query->where('author','=',$authId);
         }
        if ($user->admin_flg != 1 && is_null($authId)) {
            $query = $query->where(function($q1) use($user){
                $q1->where('author', $user->id)->orWhereHas('authorUser',function($q){
                $q->where('admin_flg',1);
            });
        });
        }
    
        $statusAllCourse = 1;  
        foreach($query->get() as $course){
            if($course->status==0){
                $statusAllCourse = 2;
            }
        }
        $authors = CourseTheory::with('authorUser');
        if(!$user->hasRole('Admin')){
            $schoolId = $user->school_id;
            $authors->whereHas('authorUser',function($q) use($schoolId){
                $q->where('school_id',$schoolId)->orWhere('admin_flg',1);
            });
        }
        $authors = $authors->get()->sortBy('authorUser.name')->unique('author');
        $courseTheories = $query->paginate(config('settings.countPaginate'))->appends(request()->query());
        $categories = CourseCategory::orderBy('name')->get();
        return view('admins.course_theory.index', compact('courseTheories','categories','statusAllCourse','authors'));
    }
    public function create()
    {
        $categoryCourses = CourseCategory::orderBy('name')->get();
        $tags = CourseTag::get();
        $tagArr = CourseTag::pluck('id');
        return view('admins.course_theory.create', compact('categoryCourses', 'tags','tagArr'));
    }

    public function store(CourseTheoryRequest $request)
    {
        $data = $request->getData();
        $tags = $request->input('course_tag_ids');
        $creator = auth()->user();
        $arrayTag = [];
        if (!is_null($tags)) {
            foreach ($tags as $value) {
                if (CourseTag::find($value)) {
                    $arrayTag[] = $value;
                    continue;
                } else {
                    $tag = new CourseTag();
                    $tag->name = $value;
                    $tag->save();
                    $arrayTag[] = $tag->id;
                }
            };
        }
        $data['course_tag_ids'] = json_encode($arrayTag);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $data_image = UploadService::uploadImage('course', $file);
            $data['image'] = $data_image;
        }
        $data['author'] = $creator->id;
        $data['start_date'] = Carbon::now();
        $data['start_date'] = Helper::formatSqlDate($data['start_date']);
        $insertData = CourseTheory::create($data);
        if ($insertData) {
            return redirect()->route('course_theory.index')->with('message', 'Thêm mới kỹ năng sống lý thuyết thành công');
        } else {
            return back()->with('message', 'Thêm mới kỹ năng sống lý thuyết không thành công');
        }
    }

    public function edit(Request $request,$id)
    {
        $backUrl = $request->url;
        $course = CourseTheory::findOrFail($id);
        $categoryCoursesId = $course->course_category_id;
        $categoryCourses = CourseCategory::orderBy('name')->get();
        $tagsCourse = json_decode($course->course_tag_ids);
        $tags = CourseTag::get();
        $tagArr = CourseTag::pluck('id');
        $coursePractice = Course::where('course_category_id', $categoryCoursesId)->orderBy('name')->get();
        
        return view('admins.course_theory.edit', compact('course', 'categoryCourses', 'tagsCourse', 'tags', 'coursePractice','tagArr','backUrl'));
    }

    public function update(CourseTheoryUpdateRequest $request, $id)
    {
        $course = CourseTheory::findOrFail($id);
        $data = $request->getData();
        $data_image = $course->image;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $data_image = UploadService::uploadImage('course', $file);
        }
        $data['image'] = $data_image;
        $tags = $request->input('course_tag_ids');
        $arrayTag = [];
        if (!is_null($tags)) {
            foreach ($tags as $value) {
                if (CourseTag::find($value)) {
                    $arrayTag[] = $value;
                    continue;
                } else {
                    $tag = new CourseTag();
                    $tag->name = $value;
                    $tag->save();
                    $arrayTag[] = $tag->id;
                }
            };
        }
        $data['course_tag_ids'] = json_encode($arrayTag);
        $update = $course->update($data);
        if ($update) {
            return redirect()->route('course_theory.index')->with('message', 'Cập nhật kỹ năng sống lý thuyết thành công');
        } else {
            return back()->with('message', 'Cập nhật kỹ năng sống lý thuyết không thành công');
        }
    }
    public function destroy($id)
    {
        $course = CourseTheory::findOrFail($id);
        $userCourrse = UserCourseTheory::where('course_theory_id')->delete();
        $delCourse = $course->delete();
        if ($delCourse) {
            return redirect(session('back_url'))->with('message', 'Xóa kỹ năng sống lý thuyết thành công');
        } else {
            return redirect()->back()->with('message', 'Xóa kỹ năng sống lý thuyết không thành công');
        }
    }
    public function addStatus(Request $request)
    {
        if ($request->ajax()) {
            $course = CourseTheory::findOrFail($request->id);
            $course->status = $request->status;
            if ($course->save()) {
                $message = 'Chuyển trạng thái thành công';
            } else {
                $message = 'Chuyển trạng thái thất bại';
            }
            return response()->json($message);
        }
    }
    public function listCourse($id, Request $request)
    {
        if ($request->ajax()) {
            $course = Course::where('course_category_id',$id)->where('status',1)->orderBy('name')->get();

            return response()->json($course);
        }
    }
    public function enableCourseTheory(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $category = $request->category;
            $type = $request->type;         
            $courseTheory = CourseTheory::orderBy('id','DESC');
            if(!is_null($search_name)){
                $courseTheory = $courseTheory->where('name', 'LIKE', '%'.$search_name.'%');
            }
            if(!is_null($category)){
                $courseTheory = $courseTheory->where('course_category_id',$category);
            }
            $courseTheory->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}
