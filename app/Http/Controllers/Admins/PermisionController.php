<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RoleHasPermission;
use Spatie\Permission\Models\Role as SpatieRole;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UpdatePermissionRequest;
use App\Models\ModelHasRole;
use App\User;

class PermisionController extends Controller
{
    public function index(){
        $roles = Role::where('name','!=','Admin');
        $user = auth()->user();
        if($user->hasRole('school')){
            $schoolID = $user->school_id;
            $roles->where('id_school',$schoolID);
        }
        if($user->hasRole('Admin')){
            $schoolID = $user->school_id;
            $roles->whereNull('id_school');
        }
        $roles = $roles->paginate(config('settings.countPaginate'));
        return view('admins.permissions.index',compact('roles'));
    }
    public function show($id){
        $role = Role::findOrFail($id);
        $roles = $role->permissionRole()->get();
        $permissions = Permission::select('id','subject','name','description')->get()->groupBy('subject'); 
        $rolesForm = Role::whereNull('id_school')->where('description','!=','Admin')->get();
        $role_id = null;
        $description = $role->description;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        return view('admins.permissions.add',compact('permissions','roles','id','role','rolesForm','role_id','description'));
    }
    public function update($id,UpdatePermissionRequest $request){
        $role = Role::findOrFail($id);
        //dd($request->description);
        $role->update(['description' => $request->description]);
        $dataPermission = $request->get('permision');
        if(!empty($dataPermission)){
            foreach ($dataPermission as $key => $value) {
                $rolePermission = RoleHasPermission::where('role_id', $id)
                ->where('permission_id', $value)->first();
                if(empty($rolePermission)){
                    $permission = Permission::findOrFail($value);
                    $role->givePermissionTo($permission->name);
                }
            }
            RoleHasPermission::where('role_id', $id)->whereNotIn('permission_id', $dataPermission)->delete();
            
        }else{
            RoleHasPermission::where('role_id', $id)->delete();
        }
        return redirect()->route('permission.permission.index')->with('message','Cập nhập phân quyền thành công.');
    }
    public function create(Request $request,$role_id=null){
        $roles = [];
        $description = null;
        $description = $request->description;
        if(!is_null($role_id)){
            $role = Role::findOrFail($role_id);
            $roles = $role->permissionRole()->get();
        }
        $permissions = Permission::select('id','subject','name','description')->get()->groupBy('subject');
        $rolesForm = Role::whereNull('id_school')->where('description','!=','Admin')->get(); 
        //return response()->json($rolesForm);                       
         return view('admins.permissions.create',compact('permissions','roles','rolesForm','role_id','description'));
    }
    public function store(Request $request){ 
        $user = auth()->user();
        $description = $request->description;
        if(is_null($description)){
            $role_id = $request->role_form_id;
            $roles = [];
            if(!is_null($role_id)){
                $role = Role::findOrFail($role_id);
                $roles = $role->permissionRole()->get();
            }
            $description = $request->description;
            $permissions = Permission::select('id','subject','name','description')->get()->groupBy('subject');
            $rolesForm = Role::whereNull('id_school')->where('description','!=','Admin')->get();
            $check = "error";                     
            return view('admins.permissions.create',compact('permissions','roles','rolesForm','role_id','description','check'));
        }else{

        $data = [
            'name' => $description,
            'description' => $description,
            'id_school' => $user->school_id,
        ];
        $role =  Role::create($data);
        $dataPermission = $request->get('permision');

        if(!empty($dataPermission)){
            foreach ($dataPermission as $key => $value) {
                $rolePermission = RoleHasPermission::where('role_id', $role->id)
                ->where('permission_id', $value)->first();
                if(empty($rolePermission)){
                    $permission = Permission::findOrFail($value);
                    $role->givePermissionTo($permission->name);
                }
            }
        }
        return redirect()->route('permission.permission.index')->with('message','Tạo nhóm phân quyền thành công.');
    }
    }
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $userRoles = ModelHasRole::where ('role_id',$id)->delete();
        $role->delete();
        return redirect()->route('permission.permission.index')->with('message','Xóa nhóm phân quyền thành công.');
    }
    public function createAjax(Request $request,$role_id=null){
        
        $roles = [];
        if(!is_null($role_id)){
            $role = Role::findOrFail($role_id);
            $roles = $role->permissionRole()->get();
        }
        $description = $request->description;
        $permissions = Permission::select('id','subject','name','description')->get()->groupBy('subject');
        $rolesForm = Role::whereNull('id_school')->where('description','!=','Admin')->get();                     
        return view('admins.permissions.create',compact('permissions','roles','rolesForm','role_id','description'));
    }
}
