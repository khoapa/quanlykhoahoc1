<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Enums\UserTypeEnum;
use App\Models\School;
use App\Models\Classes;
use App\Helpers\Helper;

class TrackingController extends Controller
{
    public function index(Request $request){
        $schoolId = $request->get('school_id');
        $classId = $request->get('class_id');
        $name = $request->get('name');
        $query = User::where('user_type',UserTypeEnum::STUDENT);
        $schools = School::orderBy('name')->get();
        $classes = []; 
        $user = auth()->user();
        if($user->hasRole('school')){
            $schoolId = $user->school_id;
            $schools = School::where('id',$user->school_id)->orderBy('name')->get();
            $classes = Classes::where('id',$user->class_id)->orderBy('name')->get(); 
        }
        if(!$user->hasanyrole('school','Admin')){
            $classId = $user->class_id;
            $schools = School::where('id',$user->school_id)->orderBy('name')->get();
            $classes = Classes::where('user_id',$user->id)->orderBy('name')->get(); 
        }
        if(!is_null($name)){
            $query = $query->where('name','LIKE','%'.$name.'%');
        }
        if(!is_null($schoolId)){
            $query = $query->where('school_id','=',$schoolId);
            $classes = Classes::where('school_id',$schoolId)->orderBy('name')->get(); 
        }
        if(!is_null($classId)){
            $query = $query->where('class_id','=',$classId);
        }
        $perPage = config('settings.countPaginate');
        if(is_null($classId) && is_null($schoolId) && is_null($name)){
            $perPage = 5;
        }
        $students = $query->where('longitude','!=','')->where('latitude','!=','')->where('status',1)->orderBy('date_tracking','DESC')->paginate(config('settings.countPaginate'))->appends(request()->query());
        $arr = [];
        foreach($students as $student){
            if(!is_null($student->school)){
                $nameSchool = $student->school->name;
            }else{
                $nameSchool = null;
            }
            if(!is_null($student->classes)){
                $nameClass = $student->classes->name;
            }else{
                $nameClass = null;
            }
            $data = [
                'name' => $student->name,
                'school' => $nameSchool.'-'.$nameClass,
                'long' => $student->longitude,
                'lat' => $student->latitude,
                'id' => $student->id,
            ];
            array_push($arr,$data);
        }
        $arrs = json_encode($arr);
        return view('admins.tracking.index',compact('arrs','schools','classes','students'));
    }
    public function detail($id){
        
        $student = User::findOrFail($id);
        $locations  = $student->locationUser()->orderBy('id','DESC')->paginate(config('settings.countPaginate'));
        $arr = [];
        foreach($locations as $location){
            $data = [
                'long' => $location->longitude,
                'lat' => $location->latitude,
                'time' => Helper::formatDateChat($location->date_tracking)
            ];
            array_push($arr,$data);
        }
        // $lat="16.061055740569326";
        // $long="108.16171022387016";
        // $address=file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&key=AIzaSyAZlHcoPe33iRtvrqFr1o_ZU50pV6JRVJs");
        // $json_data=json_decode($address);
        // $full_address=$json_data->results[0]->formatted_address;

        $arrs = json_encode($arr);
        return view('admins.tracking.detail',compact('arrs','locations'));
    }
    
}
