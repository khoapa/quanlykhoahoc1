<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UploadService;

class UploadImageController extends Controller
{
    public function upload(Request $request){
        $validator = validator()->make(request()->all(), [
            'file' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:5012'
            ]);
        if ($validator->fails()) {
            echo json_encode(array('success'=>false, 'message' => 'Có lỗi khi tải hình ảnh.'));
        }else{
            $file = $request->file('file');
            $imageUrl = UploadService::uploadImage('code',$file);
            echo json_encode(array('success'=>true, 'message' => $imageUrl));
        }

    }
}
