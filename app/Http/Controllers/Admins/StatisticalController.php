<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Helpers\Helper;
use Illuminate\Support\Carbon;
use App\Http\Requests\CreateSearchRequest;
use Illuminate\Support\Facades\Auth;

class StatisticalController extends Controller
{
    public function index(Request $request){
        $user = Auth::user();
        $schoolId = $request->get('school_id');
        $dateBegin = null;
        $dateEnd = null;
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);
        $errorDate = null;
        if(!is_null($dateBegin) && !is_null($dateEnd)){
            if($dateBegin > $dateEnd){
                $dateBegin = null;
                $dateEnd = null;
                $errorDate = "Ngày kết thúc phải lớn hơn Ngày bắt đầu";
            }
        }
        $schoolSearchs = School::orderBy('name');
        if (!$user->hasRole('Admin')) {
            $schools = $schoolSearchs->where('id', $user->school_id);
            $schoolId = $user->school_id;
        }
        $schoolSearchs = $schoolSearchs->get();
        $query = School::orderBy('id','ASC');
        if(!is_null($schoolId)){
            $query->where('id',$schoolId);
        }
        $schools = $query->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.statistical.index',compact('schools','dateBegin','dateEnd','errorDate','schoolSearchs'));
    }
}
