<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Support;
use App\Http\Requests\SupportsUpdateRequest;

class PolicyController extends Controller
{
    public function index()
    {
        $policy = Support::find(5);
        if(is_null($policy)){
            $data = [ 
                'title'=> 'Chính sách',
                'content' =>'Nội dung',
            ];
            $policy = Support::create($data);
        }
      
        return view('admins.policy.index', compact('policy'));
    }
    public function update(SupportsUpdateRequest $request, $id)
    {
        $policy = Support::find($id);
        $data = $request->getData();
        
        try {
            $policy->update($data);
            return redirect()->route('policy.index')->with('message', 'Đã cập nhập chính sách thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Cập nhật chính sách không thành công.');
        }
    }
}
