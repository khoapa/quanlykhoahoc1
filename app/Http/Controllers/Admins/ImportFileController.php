<?php

namespace App\Http\Controllers\Admins;

use App\Enums\RoleTypeEnum;
use App\Enums\UserStatusEnum;
use App\Enums\UserTypeEnum;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImportRequest;
use App\Models\Classes;
use App\Models\Grade;
use App\Models\School;
use App\User;
use SimpleXLSX;

class ImportFileController extends Controller
{
    public function importFile(ImportRequest $request)
    {
        $file = $request->file('import');
        $type_import = $request->get('type_import');
        // $user_type = $request->get('user_type');
        $row = 0;
        $response = [];
        $dataResponse = [];
        $numberError = 0;
        $data = null;
        if ($type_import == 1) {
            if ($xlsx = SimpleXLSX::parse($file)) {
                $number_record = count($xlsx->rows());
                if (count($xlsx->rows()) < 4 || count($xlsx->rows()[4]) != 10) {
                    return redirect()->back()->with('error', 'File Import không đúng định dạng.');
                }
                for ($i = 4; $i < $number_record; $i++) {
                    $name = $xlsx->rows()[$i][2];
                    $school_code = $xlsx->rows()[$i][1];
                    $phone = $xlsx->rows()[$i][3];
                    $address = ($xlsx->rows()[$i][4] != '') ? $xlsx->rows()[$i][4] : 'unknown';
                    $email = $xlsx->rows()[$i][5];
                    $grade_code = ($xlsx->rows()[$i][6] != '') ? explode(',', $xlsx->rows()[$i][6]) : [];
                    $description = ($xlsx->rows()[$i][7] != '') ? $xlsx->rows()[$i][7] : null;
                    $logo = $xlsx->rows()[$i][8];
                    $status = (($xlsx->rows()[$i][9] != '') ? ((str_replace(' ', '', $xlsx->rows()[$i][9]) == 'Active') ? UserStatusEnum::ACTIVE : UserStatusEnum::DISABLE) : UserStatusEnum::DISABLE);
                    if ($email != '' && $phone != '') {
                        //data import
                        $school = null;
                        $user = null;
                        $grade_ids = null;

                        if ($email != '' && $school_code != '')
                            $school = School::where('email', $email)->orWhere('school_code', $school_code)->first();
                        if ($email != '' && $phone != '')
                            $user = User::where('email', $email)->orWhere('phone', $phone)->first();
                        if ($xlsx->rows()[$i][6] != '')
                            $grade_ids = Grade::whereIn('grade_code', $grade_code)->pluck('id');

                        $dataImport = [
                            'name' => $name,
                            'email' => $email,
                            'phone' => $phone,
                            'address' => $address,
                            'grade_ids' => $grade_ids,
                            'description' => $description,
                            'logo' => ($logo != '') ? $logo : config('settings.image'),
                            'status' => $status,
                            'school_code' => $school_code
                        ];
                        //check empty string email, phone
                        if (is_null($school) && is_null($user) && $this->checkPhone($phone) && $this->checkEmail($email) && $email != '' && $school_code != '' && $phone != '') {
                            $createSchool = School::create($dataImport);
                            $dataUser = [
                                'name' => $name,
                                'email' => $email,
                                'phone' => $phone,
                                'address' => $address,
                                'password' => \Hash::make(config('settings.password')),
                                'school_id' => $createSchool->id,
                                'user_type' => UserTypeEnum::STAFF,
                                'avatar' => ($logo != '') ? $logo : config('settings.image'),
                                'school_code' => $school_code
                            ];
                            $createUser = User::create($dataUser);
                            //add role school
                            $roleId = RoleTypeEnum::SCHOOL;
                            $createUser->syncRoles([$roleId]);
                            $createSchool = $createSchool->update(['user_id' => $createUser->id]);
                        } else {
                            $numberError++;
                        }
                    } else {
                        $numberError++;
                    }
                }
            } else {
                return redirect()->back()->with('error', 'Import thất bại');
            }
        } elseif ($type_import == 2) {
            if ($xlsx = SimpleXLSX::parse($file)) {
                $number_record = count($xlsx->rows());
                if (count($xlsx->rows()) < 4 || count($xlsx->rows()[4]) != 12) {
                    return redirect()->back()->with('error', 'File Import không đúng định dạng.');
                }
                for ($i = 4; $i < $number_record; $i++) {
                    //data import
                    $dataImport = [
                        'name' => isset($xlsx->rows()[$i][1]) ? $xlsx->rows()[$i][1] : null,
                        'email' => isset($xlsx->rows()[$i][3]) ? $xlsx->rows()[$i][3] : null,
                        'phone' => isset($xlsx->rows()[$i][6]) ? $xlsx->rows()[$i][6] : null,
                        'address' => isset($xlsx->rows()[$i][4]) ? $xlsx->rows()[$i][4] : null,
                        'password' => \Hash::make(config('settings.password')),
                        'user_type' => (str_replace(' ', '', $xlsx->rows()[$i][10]) == "Trường") ? UserTypeEnum::STAFF : (str_replace(' ', '', $xlsx->rows()[$i][10]) == 'Giáoviên' ? UserTypeEnum::TEACHER : UserTypeEnum::STUDENT),
                        'birthday' => isset($xlsx->rows()[$i][2]) ? Helper::formatSqlDate($xlsx->rows()[$i][2]) : null,
                        'gender' => isset($xlsx->rows()[$i][5]) ? $xlsx->rows()[$i][5] == 'Nam' ? 1 : 2 : 1,
                        'avatar' => config('settings.image'),
                        'status' => isset($xlsx->rows()[$i][9]) ? ((str_replace(' ', '', $xlsx->rows()[$i][9]) == 'Active') ? UserStatusEnum::ACTIVE : UserStatusEnum::DISABLE) : UserStatusEnum::DISABLE
                    ];


                    // if(!is_null($user_type)){
                    //     $dataImport['user_type'] =  $user_type;
                    // }
                    //check empty string email, phone
                    if ($xlsx->rows()[$i][3] != '' && $xlsx->rows()[$i][1] != '' && $this->checkPhone($dataImport['phone']) && $this->checkEmail($dataImport['email'])) {
                        $school = null;
                        if ($xlsx->rows()[$i][7] != '') {
                            $school = School::where('school_code', $xlsx->rows()[$i][7])->first();
                            $dataImport['school_code'] = $xlsx->rows()[$i][7];
                        }
                        if ($school) {
                            $dataImport['school_id'] = $school->id;
                            $user = null;
                            if ($xlsx->rows()[$i][3] != '' && $xlsx->rows()[$i][6] != '')
                                $user = User::where('email', $xlsx->rows()[$i][3])->orWhere('phone', $xlsx->rows()[$i][6])->first();
                            if (is_null($user)) {
                                $class = null;
                                if (str_replace(' ', '', $xlsx->rows()[$i][10]) == 'Giáoviên' && $xlsx->rows()[$i][8] != '') {
                                    $class = Classes::where('class_code', $xlsx->rows()[$i][8])->first();
                                }
                                $dataImport['class_id'] = !is_null($class) ? $class->id : null;
                                $dataImport['class_code'] = !is_null($class) ? $xlsx->rows()[$i][8] : null;
                                $createUser = User::create($dataImport);
                                $roleId = (isset($xlsx->rows()[$i][11]) ? ($xlsx->rows()[$i][11] == 'Trường' ? RoleTypeEnum::SCHOOL : ($xlsx->rows()[$i][11] == 'Giáo viên' ? RoleTypeEnum::TEACHER : RoleTypeEnum::STUDENT)) : RoleTypeEnum::STUDENT);
                                $createUser->syncRoles([$roleId]);
                            } else {
                                $numberError++;
                            }
                        } else {
                            $numberError++;
                        }
                    } else {
                        $numberError++;
                    }
                }
            } else {
                return redirect()->back()->with('error', 'Import thất bại');
            }
        } else {
            if ($xlsx = SimpleXLSX::parse($file)) {
                $number_record = count($xlsx->rows());
                if (count($xlsx->rows()) < 4 || count($xlsx->rows()[4]) != 12) {
                    return redirect()->back()->with('error', 'File Import không đúng định dạng.');
                }
                for ($i = 4; $i < $number_record; $i++) {
                    // dd($xlsx->rows()[$i]);
                    $class_code = $xlsx->rows()[$i][1];
                    $name = $xlsx->rows()[$i][2];
                    $school_code = $xlsx->rows()[$i][3];
                    $grade_code = $xlsx->rows()[$i][4];
                    $status = (($xlsx->rows()[$i][5] != '') ? ((str_replace(' ', '', $xlsx->rows()[$i][5]) == 'Active') ? UserStatusEnum::ACTIVE : UserStatusEnum::DISABLE) : UserStatusEnum::DISABLE);
                    $name_teacher = $xlsx->rows()[$i][6];
                    $phone_teacher = $xlsx->rows()[$i][8];
                    $address_teacher = ($xlsx->rows()[$i][9] != '') ? $xlsx->rows()[$i][9] : 'Unknown';
                    $gender = ($xlsx->rows()[$i][5] == 'Nam') ? 1 : 2;
                    $email_teacher = $xlsx->rows()[$i][7];
                    $birthday_teacher = (($xlsx->rows()[$i][11] != '') ? explode(',', $xlsx->rows()[$i][11]) : []);
                    $password = \Hash::make(config('settings.password'));
                    $class = null;

                    if ($school_code != '' && $grade_code != '' && $class_code != '') {
                        $dataImport = [
                            'name' => $name,
                            'class_code' => $class_code,
                            'school_code' => $school_code,
                            'grade_code' => $grade_code,
                            'status' => $status,
                            'thumbnail' => config('settings.image')
                        ];

                        $school = School::where('school_code', $school_code)->first();
                        $grade = Grade::where('grade_code', $grade_code)->first();


                        if ($school && $grade) {
                            $grade_ids = $school->grade_ids;
                            //check grade in school
                            if (!is_null($grade_ids) && count(json_decode($grade_ids)) != 0) {

                                $check_grade = in_array($grade->id, json_decode($grade_ids));
                                if ($check_grade) {
                                    $class = Classes::where('class_code', $class_code)
                                        ->where('school_code', $school_code)
                                        ->where('grade_code', $grade_code)
                                        ->first();
                                    //check class
                                    if (is_null($class)) {
                                        $dataImport['grade_id'] = $grade->id;
                                        $dataImport['school_id'] = $school->id;
                                        $createClass = Classes::create($dataImport);
                                        $user = null;
                                        if ($this->checkPhone($phone_teacher) && $this->checkEmail($email_teacher)) {
                                            if ($email_teacher != '') {
                                                $user = User::where('email', $email_teacher)->orWhere('phone', $phone_teacher)->first();

                                                if (!$user) {
                                                    $dataUser = [
                                                        'name' => $name_teacher,
                                                        'email' => $email_teacher,
                                                        'phone' => $phone_teacher,
                                                        'address' => $address_teacher,
                                                        'gender' => $gender,
                                                        'password' => \Hash::make(config('settings.password')),
                                                        'user_type' => UserTypeEnum::TEACHER,
                                                        'avatar' => config('settings.image'),
                                                        'school_id' => $school->id,
                                                        'school_code' => $school_code
                                                    ];
                                                    $user = User::create($dataUser);
                                                }

                                                $roleId = RoleTypeEnum::TEACHER;
                                                $user->syncRoles([$roleId]);
                                                $createClass->update(['user_id' => $user->id]);
                                                $user->update([
                                                    'class_id' => $createClass->id,
                                                    'class_code' => $class_code
                                                ]);
                                            }
                                        }
                                    } else {
                                        $numberError++;
                                    }
                                } else {
                                    $numberError++;
                                }
                            } else {
                                $numberError++;
                            }
                        } else {
                            $numberError++;
                        }
                    } else {
                        $numberError++;
                    }
                }
            } else {
                return redirect()->back()->with('error', 'Import thất bại');
            }
        }
        $dataReturn =  [
            'numberError' => $numberError,
            'total' =>  $number_record - 4,
        ];
        return redirect()->back()->with('data', $dataReturn);
    }

    public function checkEmail($email): bool
    {
        $pattern = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';
        if (preg_match($pattern, $email) === 1) {
            return true;
        }
        return false;
    }
    public function checkPhone($phone): bool
    {
        $pattern = '/^[0-9]{10}+$/';
        if (preg_match($pattern, $phone) === 1) {
            return true;
        }
        return false;
    }
}
