<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCategoryCourseRequest;
use App\Models\CourseCategory;
use App\Http\Requests\UpdateCategoryCourseRequest;
use App\Models\Course;
use App\Services\UploadService;
use App\Models\CourseTheory;
use App\Models\UserCourse;
use App\Models\SchoolCourse;
use App\Models\ClassCourse;
use App\Models\CourseFavorite;
use App\Models\UserCourseTheory;

class CourseCategoryController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $query = CourseCategory::orderBy('position', 'ASC');
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $categoryCourses = $query->paginate(20)->appends(request()->query());
        return view('admins.category_course.index', compact('categoryCourses'));
    }
    public function create()
    {
        return view('admins.category_course.add');
    }
    public function store(CreateCategoryCourseRequest $request)
    {
        $data = $request->getData();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_logo = UploadService::uploadImage('category', $file);
            $data['thumbnail'] = $data_logo;
        }
      
        $allCates = CourseCategory::all();
        foreach($allCates as $cate){
            $dataUpdate = [
                'position' => $cate->position + 1
            ];
            $cate->update($dataUpdate);
        }
        $data['position']= CourseCategory::min('position')-1;
        $insertData = CourseCategory::create($data);
        if ($insertData) {
            return redirect()->route('category_course.index')->with('message', trans('category_course.create_success'));
        } else {
            return redirect()->back()->with('message', trans('category_course.create_fail'));
        }
    }
    public function edit($id)
    {
        $categoryCourse = CourseCategory::findOrFail($id);
        return view('admins.category_course.edit', compact('categoryCourse'));
    }
    public function update(UpdateCategoryCourseRequest $request, $id)
    {
        $categoryCourse = CourseCategory::findOrFail($id);
        $data = $request->getData();
        $data_logo = $categoryCourse->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_logo = UploadService::uploadImage('category', $file);
        }
        $data['thumbnail'] = $data_logo;
        $update = $categoryCourse->update($data);
        if ($update) {
            return redirect()->route('category_course.index')->with('message', trans('category_course.update_success'));
        } else {
            return redirect()->back()->with('message', trans('category_course.update_fail'));
        }
    }
    public function destroy($id)
    {
        $courseCate = CourseCategory::findOrFail($id);
        $coursesTheories = CourseTheory::where('course_category_id', $id)->get();
        $courses = Course::where('course_category_id', $id)->get();
        foreach($coursesTheories as $theory){
            $id_course = $theory->id;
            $courseTheory = $theory->delete();
            $courseTheory = UserCourseTheory::where('course_theory_id',$id_course)->delete();
        }
        foreach($courses as $course){
        $courseID = $course->id;
        $usercourse = UserCourse::where('course_id',$courseID)->delete();
        $schoolCourse = SchoolCourse::where('course_id',$courseID)->delete(); 
        $classCourse = ClassCourse::where('course_id',$courseID)->delete(); 
        $course_favorites = CourseFavorite::where('course_id',$courseID)->delete();
        $delCourse = $course->delete();
        }
        $delCate = $courseCate->delete();
        if ($delCate) {
            return redirect()->route('category_course.index')->with('message', trans('category_course.delete_success'));
        } else {
            return redirect()->back()->with('message', trans('category_course.delete_fail'));
        }
    }
    public function sortTable(Request $request)
    {
        $posts = CourseCategory::all();

        foreach ($posts as $post) {
            foreach ($request->order as $order) {
                if ($order['id'] == $post->id) {
                    $post->update(['position' => $order['position']]);
                }
            }
        }
        
        return response('Update Successfully.', 200);
    }
}
