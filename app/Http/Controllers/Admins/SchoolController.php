<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\SchoolCreateRequest;
use App\Http\Requests\SchoolUpdateRequest;
use App\Models\Classes;
use App\Models\School;
use Illuminate\Http\Request;
use App\Services\UploadService;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Enums\UserTypeEnum;
use App\Http\Requests\ClassCreateRequest;
use App\Http\Requests\ClassUpdateRequest;
use App\Models\Grade;
use App\Models\Role;
use App\Models\Conversation;
use App\Models\Course;
use App\Models\Permission;
use App\Models\CourseTheory;
use App\Models\SchoolCourse;
use App\Models\UserCourse;
use App\Models\CourseFavorite;
use App\Models\UserCourseTheory;
use App\Models\UserMap;
use App\Models\UserCategoryMap;

class SchoolController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        if ($user->admin_flg == 1) {
            $name = $request->get('search');
            $schools = School::orderBy('id', 'DESC');
            if (!is_null($name)) {
                $schools = $schools->where('name', 'LIKE', '%' . $name . '%');
            }
            $statusAllSchool = 1;  
            foreach($schools->get() as $school){
                if($school->status==2){
                    $statusAllSchool = 2;
                }
            }
            $schools = $schools->paginate(config('settings.countPaginate'))->appends(request()->query());
            return view('admins.schools.index', compact('schools','statusAllSchool'));
        }
        return view('errors.403');
    }
    public function create()
    {
        $roles = Role::where('name', '!=', 'Admin')->get();
        $grades = Grade::orderBy('name')->get();
        return view('admins.schools.create', compact('roles', 'grades'));
    }

    public function store(SchoolCreateRequest $request)
    {
        $data = $request->getData();
        if(is_null($data['logo'])){
            $data['logo'] = config('settings.image');
        }
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $dataImage = UploadService::uploadImage('school', $file);
            $data['logo'] = $dataImage;
        }
        $grade_ids  = $request->grade_ids;
        $data['grade_ids'] = json_encode($grade_ids);
        $data['email'] = str_replace(' ', '', $data['email']);
        $insertData = School::create($data);
        $dataUser = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make(config('settings.password')),
            'phone' => $data['phone'],
            'user_type' => UserTypeEnum::STAFF,
            'school_id' => $insertData->id,
            'avatar' => $data['logo']
        ];
        $user = User::create($dataUser);
        $roleId = 2;
        $user->syncRoles([$roleId]);
        $updateSchool = $insertData->update(['user_id' => $user->id]);
        if ($insertData) {
            return redirect()->route('school_index')->with('message', 'Tạo trường học thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo trường học thất bại');
        }
    }
    public function delete($id)
    {
        $school = School::findOrFail($id);
        $idUser = $school->user_id;
        $classes = Classes::where('school_id',$school->id)->get();
        foreach($classes as $class){
            User::where('class_id',$class->id)->update(['class_id'=>NULL]);
        }
        Classes::where('school_id',$school->id)->delete();
        $conversition = Conversation::where('user1_id',$idUser)->orWhere('user2_id',$idUser)->delete();
        $users = User::where('school_id',$id)->get();
        foreach($users as $user){
            $userId = $user->id;
            Conversation::where('user1_id', $userId)->orWhere('user2_id', $userId)->delete();
            UserCourse::where('user_id',$userId)->delete();
            UserMap::where('user_id',$userId)->delete();
            UserCategoryMap::where('category_map_id',$userId)->delete();
            $courses = Course::where('author',$userId)->get();
            $coursesTheory = CourseTheory::where('author',$userId)->get();
            foreach($courses as $course){
                $idCourse = $course->id;
                $schoolCourse = SchoolCourse::where('course_id',$idCourse)->delete();
                $userCourse = UserCourse::where('course_id',$idCourse)->delete();
                $courseFavouris = CourseFavorite::where('course_id',$idCourse)->delete();
            }  
            foreach($coursesTheory as $theory){
                $idTheory = $theory->id;
                $userTheory = UserCourseTheory::where('course_theory_id',$idTheory)->delete();
            }  
            $classesUser = Classes::where('user_id',$userId)->get();
            foreach($classesUser as $class){
                $classUpdate = Classes::where('user_id',$class->id)->update(['user_id'=>NULL]);
            }
           Course::where('author',$userId)->delete();
           CourseTheory::where('author',$userId)->delete();  
        }
        User::where('school_id',$id)->delete();
        $school->delete();
        return redirect(session('back_url'))->with('message', 'Xóa trường học thành công');
    }
    public function edit($id)
    {
        $grades = Grade::orderBy('name')->get();
        $school = School::find($id);
        $gradesOfSchool = json_decode($school->grade_ids);
        $roles = Role::where('name', '!=', 'Admin')->get();
        return view('admins.schools.edit', compact('school', 'roles', 'grades', 'gradesOfSchool'));
    }

    public function update(SchoolUpdateRequest $request, $id)
    {
        $school = School::find($id);
        $data = $request->getData();
        $dataLogo = $school->logo;
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $dataLogo = UploadService::uploadImage('school', $file);
        }
        $data['logo'] = $dataLogo;
        $user = $school->staff;
        $data['email'] = str_replace(' ', '', $data['email']);
        // $roleId = $data['role_id'];
        // $roleOld = $user->role()->first();
        // if(!is_null($roleId)){
        //     $user->syncRoles([$roleId]);
        // }else{
        //     if($roleOld){
        //         $user->removeRole($roleOld->role_id);   
        //     }
        // }
        $grade_ids  = $request->grade_ids;
        $data['grade_ids'] = json_encode($grade_ids);
        $update = $school->update($data);
       
        $users = User::where('school_id',$id)->update(['school_code'=>$data['school_code']]);
        $userSchool = User::where("school_id", $school->id)->where('user_type', UserTypeEnum::STAFF)->first();
       if($userSchool){
            $userSchool->update(['name'=>$data['name'],'email'=>$data['email'],'avatar'=>$data['logo']]);
        }

        if ($update) {
            return redirect(session('back_url'))->with('message', 'Đã cập nhật trường học thành công.');
        } else {
            return redirect(session('back_url'))->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function listClass(Request $request,$id)
    {
        $backUrl = $request->url;
        $query = Classes::where('school_id', '=', $id);
        $name = $request->get('search');
        if(!is_null($name)){
            $query = $query->where('name','LIKE','%'.$name.'%');
        }
        $statusAllClass = 1;  
        foreach($query->get() as $class){
            if($class->status==2){
                $statusAllClass = 2;
            }
        }
        $classes = $query->paginate(config('settings.countPaginate'));
        $school = School::find($id);
        return view('admins.schools.list_class', compact('classes', 'school','backUrl','statusAllClass'));
    }
    public function listGrade(Request $request,$id)
    {
        $backUrl = $request->url;
        $school = School::find($id);
        $gradeIds = $school->grade_ids;
        $gradesArray = json_decode($gradeIds);
        $gradeOfSchool = [];
        if (!is_null($gradesArray)) {
            foreach ($gradesArray as $value) {
                $gradeOfSchool[] = Grade::findOrFail($value);
            };
        }
        return view('admins.schools.list_grade', compact('school','gradeOfSchool','backUrl'));
    }
    public function listUser(Request $request,$id)
    {
        $backUrl = $request->url;
        $school = School::find($id);
        $query = User::whereIn('user_type', [UserTypeEnum::TEACHER, UserTypeEnum::STUDENT])->where('school_id',$id)->orderBy('id', 'DESC');
        $name = $request->get('search');
        if(!is_null($name)){
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $statusAllUser = 1;  
        foreach($query->get() as $user){
            if($user->status==2){
                $statusAllUser = 2;
            }
        }
        $users = $query->paginate(config('settings.countPaginate'));
        return view('admins.schools.list_user', compact('school','users','backUrl','statusAllUser'));
    }
    public function listCourse(Request $request,$id)
    {
        $backUrl = $request->url;
        $nameSearch = $request->get('search');
        $school = School::findOrFail($id);
        $courseOfSchool = Course::orderBy('id','DESC');
        if(!is_null($nameSearch)){
            $courseOfSchool =   $courseOfSchool->where('name','LIKE','%'.$nameSearch.'%');
        }
        $courseOfSchool = $courseOfSchool->whereHas('school',function($q)use($id){
            $q->where('school_id',$id);
        })->paginate(config('settings.countPaginate'));
        $statusAllCourse = 1;  
        foreach($courseOfSchool as $course){
            if($course->status==0){
                $statusAllCourse = 2;
            }
        }
        return view('admins.schools.list_course', compact('school','courseOfSchool','backUrl','statusAllCourse'));
    }
    public function resetPassword($id)
    {
        $school = School::findOrFail($id);
        $userId = $school->user_id;
        $user = User::findOrFail($userId);
        $user->password = bcrypt('Aa12345678');
        $user->save();
        return back()->with('message', 'Reset mật khẩu thành công (Aa12345678)');
    }
    public function updateSchool(Request $request){
        if ($request->ajax()) {
            $school = School::findOrFail($request->id);
            $userSchool = User::findOrFail($school->user_id);
            $data = [
                'status' => $request->status,
            ];
            if($userSchool){
                $userSchool->update($data);
            }
            $school->update($data);
            $usersSchool = User::where('school_id',$school->id)->update($data);
            $classes = Classes::where('school_id',$school->id)->update($data);
            $courseOfSchool = Course::where('author',$userSchool->id)->update(['status'=>0]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
    public function permission($id){
        $roles = Role::where('name','!=','Admin');
        $user = auth()->user();
        if(!$user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$id);
        }
        $roles = $roles->get();
        return response()->json($roles);
    }
    public function enableAllSchool(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $type = $request->type;
            $schools = School::get();
            if(!is_null($search_name)){
                $schools = $schools->where('name', 'LIKE', '%' . $search_name . '%');
            }
            if(!is_null($schools)){
                foreach($schools as $school){
                    $userSchool = User::where('id',$school->user_id)->update(['status'=>$type]);
                    $courseOfSchool = Course::where('author',$school->user_id)->update(['status'=>$type]);              
                        $data = [
                            'status' => $type ,
                        ];
                    $school->update($data);
                    $usersSchool = User::where('school_id',$school->id)->update($data);
                    $classes = Classes::where('school_id',$school->id)->update($data);
                }
            }
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
    public function enableUserOfSchool(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $schoolId = $request->school_id;
            $type = $request->type;
            $users = User::whereIn('user_type',[UserTypeEnum::TEACHER, UserTypeEnum::STUDENT])->where('school_id',$schoolId);
            if(!is_null($search_name)){
                $users = $users->where('name', 'LIKE', '%' . $search_name . '%');
            }
            $users->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
    public function enableClassOfSchool(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $schoolId = $request->school_id;
            $type = $request->type;
            $classes = Classes::where('school_id',$schoolId)->get();
            if (!is_null($search_name)) {
                $classes = $classes->where('name', 'LIKE', '%' . $search_name . '%');
            }
            
            if (!is_null($classes)) {
                foreach ($classes as $class) {
                    $data = [
                        'status' => $type,
                    ];
                    $class->update($data);
                    $usersClass = User::where('class_id', $class->id)->update($data);
                }
            }
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
    public function enableCourseOfSchool(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $schoolId = $request->school_id;
            $type = $request->type;
            $courseOfSchool = Course::whereHas('school',function($q)use($schoolId){
                $q->where('school_id',$schoolId);
            });
            if (!is_null($search_name)) {
                $courseOfSchool = $courseOfSchool->where('name', 'LIKE', '%' . $search_name . '%');
            }
            $courseOfSchool->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}