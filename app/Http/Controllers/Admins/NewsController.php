<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryNewsCreateRequest;
use App\Http\Requests\CategoryNewsUpdateRequest;
use App\Http\Requests\NewsCreateRequest;
use App\Http\Requests\NewsUpdateRequest;
use App\Models\CategoryNews;
use App\Models\News;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Services\UploadService;
use App\User;
use App\Enums\TypeNotiFyEnum;

class NewsController extends Controller
{
    use \App\Traits\NotificationTrait;
    public function index(Request $request)
    {
        $title = $request->get('search');
        $categoryId = $request->get('category_id');
        $query = News::orderBy('id', 'DESC');
        if (!is_null($title)) {
            $query = $query->where('title', 'LIKE', '%' . $title . '%');
        }
        if (!is_null($categoryId)) {
            $query = $query->where('news_category_id', $categoryId);
        }
        $categories = CategoryNews::orderBy('name')->get();
        $statusAllNews = 1;  
        foreach($query->get() as $news){
            if($news->status==2){
                $statusAllNews = 2;
            }
        }
        $news = $query->paginate(config('settings.countPaginate'));
        return view('admins.news.index', compact('news', 'categories','statusAllNews'));
    }
    public function create()
    {
        $categories = CategoryNews::orderBy('name')->get();
        $tags = Tag::get();
        $tagArr = Tag::pluck('id');
        return view('admins.news.create', compact('categories', 'tags','tagArr'));
    }
    public function store(NewsCreateRequest $request)
    {
        $data = $request->getData();
        $tags = $request->input('tag_ids');
        $creator = auth()->user();
        $arrayTag = [];
        if (!is_null($tags)) {
            foreach ($tags as $value) {
                if (Tag::find($value)) {
                    $arrayTag[] = $value;
                    continue;
                } else {
                    $tag = new Tag();
                    $tag->name = $value;
                    $tag->save();
                    $arrayTag[] = $tag->id;
                }
            };
        }
        $data['tag_ids'] = json_encode($arrayTag);
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('news', $file);
            $data['thumbnail'] = $data_image;
        }
        $insertData = News::create($data);
        $users = User::where('id', "!=", $creator->id)->get();
        $title_notification = 'Bạn có một tin tức mới.';
        $type = TypeNotiFyEnum::NEWS;
        $id_type = $insertData->id;
        foreach ($users as $user)
            $this->notifcationSend($title_notification, "", $creator, $user, $type, $id_type);
        if ($insertData) {
            return redirect()->route('news_index')->with('message', 'Tạo tin tức thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo tin tức thất bại');
        }
    }
    public function delete($id)
    {
        $news = News::findOrFail($id);
        $news->delete();
        return redirect(session('back_url'))->with('message', 'Xóa tin tức thành công');
    }
    public function edit(Request $request,$id)
    {
        $backUrl = $request->url;
        $news = News::find($id);
        $tagsNews = json_decode($news->tag_ids);
        $categories = CategoryNews::orderBy('name')->get();
        $tags = Tag::get();
        $tagArr = Tag::pluck('id');
        return view('admins.news.edit', compact('news', 'categories', 'tags', 'tagsNews','tagArr','backUrl'));
    }
    public function update(NewsUpdateRequest $request, $id)
    {
        $news = News::find($id);
        $updater = auth()->user();
        $data = $request->getData();
        $tags = $request->input('tag_ids');
        $arrayTag = [];
        if (!is_null($tags)) {
            foreach ($tags as $value) {
                if (Tag::find($value)) {
                    $arrayTag[] = $value;
                    continue;
                } else {
                    $tag = new Tag();
                    $tag->name = $value;
                    $tag->save();
                    $arrayTag[] = $tag->id;
                }
            };
        }
        $data['tag_ids'] = json_encode($arrayTag);
        $data_image = $news->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('news', $file);
        }
        $data['thumbnail'] = $data_image;
        $update = $news->update($data);
        $users = User::where('id', "!=", $updater->id)->get();
        $title_notification = 'Có một tin tức được cập nhập.';
        $type = TypeNotiFyEnum::NEWS;
        $id_type = $id;
        foreach ($users as $user)
            $this->notifcationSend($title_notification, "", $updater, $user, $type, $id_type);
        if ($update) {
            return redirect()->route('news_index')->with('message', 'Đã cập nhật tin tức thành công.');
        } else {
            return redirect()->route('news_index')->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function updateStatusAjax(Request $request)
    {
        if ($request->ajax()) {
            $news = News::findOrFail($request->id);
            $news->status = $request->status;
            if ($news->save()) {
                $message = 'chuyển trạng thái thành công';
            } else {
                $message = 'chuyển trạng thái thất bại';
            }
            return response()->json($message);
        }
    }
    public function enableNews(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $categoryId = $request->category;
            $type = $request->type;
            $news = News::orderBy('id','DESC');
            if (!is_null($search_name)) {
                $news = $news->where('title', 'LIKE', '%' . $search_name . '%');
            }
            if (!is_null($categoryId)) {
                $news = $news->where('news_category_id',$categoryId);
            }
           $news=  $news->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
    public function disableNews(Request $request)
    {
        if ($request->ajax()) {
            $ids = $request->ids;
            foreach ($ids as $id) {
                $news = News::findOrFail($id);
                $news->status = 2;
                $news->save();
            }
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}
