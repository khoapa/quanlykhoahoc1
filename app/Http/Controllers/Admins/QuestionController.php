<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course;
use App\Http\Requests\CreateQuestionRequest;
use App\Models\Question;
use App\Http\Requests\UpdateQuestionRequest;
use App\Models\Answer;
use App\Models\UserCourseResult;

class QuestionController extends Controller
{
    public function index($id = null,Request $request){
        $urlCourse = Session('urlCourse');
        $backUrl = $request->url;
        $searchCrouse = $request->get('course_name');
        $query = Question::orderBy('id','DESC');
        $course = Course::findOrFail($id);
        $questions = $query->where('course_id',$id)->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.questions.index',compact('questions','id','course','backUrl','urlCourse'));
    }
    public function create($id = null){
        $query = Course::orderBy('id','DESC');
        if(!is_null($id)){
            $query = $query->where('id',$id);
        }
        $courses = $query->get();
        return view('admins.questions.add',compact('courses','id'));
    }
    public function store(CreateQuestionRequest $request){
        $data = $request->getData();
        $createQuestion = Question::create($data);
        $dataAnswer = $request->getDataAnswer();
        $course = Course::findOrFail($createQuestion->course_id);
        $countQuestionChoose = $course->question->where('type',1)->count();
        $countQuestionEssay = $course->question->where('type',2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if($countQuestionChoose >= 1){
            $point = $totalPointChoice/$countQuestionChoose;
            $course->question()->where('type',1)->update(['point'=>$point]);
        }
        if($countQuestionEssay >= 1){
            $point = ($totalPoint-$totalPointChoice)/$countQuestionEssay;
            $course->question()->where('type',2)->update(['point'=>$point]);
        }
        if($request->type == 1){   
      
        if(!($dataAnswer['correct'][0] == 0 && count($dataAnswer['correct']) == 1)){
          //  dd(  $dataAnswer['correct'] );
             $dataAnswerCorrectNew = $dataAnswer['correct'];
            for($i=0;$i<count($dataAnswer['correct']);$i++){
                if($dataAnswer['correct'][$i] == 1){
                    unset($dataAnswerCorrectNew[$i-1]);
                }
            }
            $dataAnswer['correct'] = array_values($dataAnswerCorrectNew);
        }
        foreach($dataAnswer['answer'] as $key=>$an){
            if(!is_null($dataAnswer['answer'][$key])){
                if($dataAnswer['correct'][0] == 0 && count($dataAnswer['correct']) == 1 || empty($dataAnswer['correct'])){
                    $correct = 0;
                }else{
                    $correct = $dataAnswer['correct'][$key];
                    
                }
               $dataInsert = [
                   "question_id" => $createQuestion->id,
                   "answer" => $an,
                   "correct" => $correct
               ];
               Answer::create($dataInsert);
            }
        }
        }
        return redirect()->route('question.question.index',$createQuestion->course_id)->with('message','Tạo câu hỏi thành công.');

    }
    public function edit($id){
        $question = Question::findOrFail($id);
        $courses = Course::all();
        $type = $question->type;
        $answers = null;
        if($type == 1){
            $answers = $question->answer()->get();
        }else{
            return view('admins.questions.edit',compact('courses','question','answers','type'));
        }
        return view('admins.questions.edit-question',compact('courses','question','answers','type'));
    }
    public function update(UpdateQuestionRequest $request,$id){
        $question = Question::findOrFail($id);
        $data = $request->getData();
        $updateQuestion = $question->update($data);
        $course = Course::findOrFail($question->course_id);
        $countQuestionChoose = $course->question->where('type',1)->count();
        $countQuestionEssay = $course->question->where('type',2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if($countQuestionChoose >= 1){
            $point = $totalPointChoice/$countQuestionChoose;
            $course->question()->where('type',1)->update(['point'=>$point]);
        }
        if($countQuestionEssay >= 1){
            $point = ($totalPoint-$totalPointChoice)/$countQuestionEssay;
            $course->question()->where('type',2)->update(['point'=>$point]);
        }
        if($request->type == 1){
            $dataAnswersOld = $request->getDataAnswerOld();

            foreach($dataAnswersOld['answer'] as $key=>$dataAnswerOld){

                $answerOld = Answer::findOrFail($key);
                $dataUpdate = [
                    'answer' => $dataAnswerOld,
                    'correct' => $dataAnswersOld['correct'][$key]
                ];
           // dd($dataAnswersOld['correct']);
                $answerOld->update($dataUpdate);
            }

            //Add  answer new
            $dataAnswer = $request->getDataAnswerNew();
            if(!empty($dataAnswer)){
                if(!($dataAnswer['correctTo'][0] == 0 && count($dataAnswer['correctTo']) == 1)){
                    $dataAnswerCorrectNew = $dataAnswer['correctTo'];
                    for($i=0;$i<count($dataAnswer['correctTo']);$i++){
                        if($dataAnswer['correctTo'][$i] == 1){
                            unset($dataAnswerCorrectNew[$i-1]);
                        }
                    }
                    $dataAnswer['correctTo'] = array_values($dataAnswerCorrectNew);
                }
            foreach($dataAnswer['answerTo'] as $key=>$an){
                if(!is_null($dataAnswer['answerTo'][$key])){
                    if($dataAnswer['correctTo'][0] == 0 && count($dataAnswer['correctTo']) == 1 || empty($dataAnswer['correctTo'])){
                        $correct = 0;
                    }else{
                        $correct = $dataAnswer['correctTo'][$key];
                        
                    }
                   $dataInsert = [
                       "question_id" => $id,
                       "answer" => $an,
                       "correct" => $correct
                   ];
                   Answer::create($dataInsert);
                }
            }
        }
        }
        return redirect()->route('question.question.index',$question->course_id)->with('message','Cập nhập câu hỏi thành công.');
    }
    public function destroy($id){
        $question = Question::findOrFail($id);
        $answer = $question->answer()->delete();
        UserCourseResult::where('question_id',$id)->delete();
        $delQuestion = $question->delete();
        $course = Course::findOrFail($question->course_id);
        $countQuestionChoose = $course->question->where('type',1)->count();
        $countQuestionEssay = $course->question->where('type',2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if($countQuestionChoose >= 1){
            $point = $totalPointChoice/$countQuestionChoose;
            $course->question()->where('type',1)->update(['point'=>$point]);
        }
        if($countQuestionEssay >= 1){
            $point = ($totalPoint-$totalPointChoice)/$countQuestionEssay;
            $course->question()->where('type',2)->update(['point'=>$point]);
        }
        if ($delQuestion) {
            return redirect()->route('question.question.index',$question->course_id)->with('message','Xóa câu hỏi thành công.');
	    }else {                        
            return redirect()->back()->with('message','Xóa câu hỏi thất bại.');
	    }
    }
}
