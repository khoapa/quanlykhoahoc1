<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Support;
use App\Http\Requests\SupportsCreateRequest;
use App\Services\UploadService;
use App\Http\Requests\SupportsUpdateRequest;

class SupportsController extends Controller
{
    public function index()
    {
        $supports = Support::orderBy('id', 'DESC')->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.supports.index', compact('supports'));
    }
    public function create()
    {
        return view('admins.supports.create');
    }
    public function store(SupportsCreateRequest $request)
    {
        $data = $request->getData();
        $insertData = Support::create($data);
        if ($insertData) {
            return redirect()->route('supports.index')->with('message', trans('Thêm mới hỗ trợ thành công'));
        } else {
            return redirect()->back()->with('message', trans('Đã có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại.'));
        }
    }
    public function edit($id){
        $supports = Support::findOrFail($id);
        return view('admins.supports.update',compact('supports'));
    }
    public function update(SupportsUpdateRequest $request, $id)
    {   
        $supports = Support::findOrFail($id);
        $data = $request->getData();
        $update = $supports->update($data);
	    if ($update) {
            return redirect()->route('supports.index')->with('message',trans('Cập Nhật hỗ trợ thành công'));
	    }else {                        
            return redirect()->back()->with('message',trans('Đã có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại.'));
	    }
    }
    public function destroy($id){
        $supports = Support::findOrFail($id);
        $delSupports = $supports->delete();
        if ($delSupports) {
            return redirect()->route('supports.index')->with('message',trans('Đã xóa hỗ trợ thành công'));
	    }else {                        
            return redirect()->back()->with('message',trans('Đã có lỗi xảy ra trong quá trình thêm mới. Vui lòng thử lại.'));
	    }
    }
    public function view($id){
        $support = Support::findOrFail($id);
        return view('admins.supports.view',compact('support'));
    }
}
