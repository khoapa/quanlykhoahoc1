<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;

use App\Http\Requests\UserChangeAvatarRequest;
use App\Http\Requests\UserChangePasswordRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Services\UploadService;

class ProfileUserController extends Controller
{
    public function indexTeacher($id)
    {
        $user = User::findOrFail($id);
        return view('admins.profile_user.index', compact('user'));
    }
    public function changePassword($id)
    {
        $user = User::findOrFail($id);
        return view('admins.profile_user.change_password', compact('user'));
    }
    public function storePassword(UserChangePasswordRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $password = Hash::make($request->get('password'));
            $user->update(['password' => $password]);
            return redirect()->route('profile_user.index', $id)->with('message', 'Đổi mật khẩu thành công');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    public function storeAvatar(UserChangeAvatarRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $data_image = $request->get('avatar');
            if(is_null($data_image )){
                $data_image = config('settings.image');
            }
            if ($request->hasFile('avatar')) {
                $file = $request->file('avatar');
                $data_image = UploadService::uploadImage('user', $file);
            }
            $user->update(['avatar' => $data_image]);          
            return redirect()->route('profile_user.index', $id)->with('message', 'Cập nhật avatar thành công');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
