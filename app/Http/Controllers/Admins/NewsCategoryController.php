<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryNewsCreateRequest;
use App\Http\Requests\CategoryNewsUpdateRequest;
use App\Models\CategoryNews;
use Illuminate\Http\Request;
use App\Models\News;

class NewsCategoryController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $query = CategoryNews::orderBy('position', 'ASC');
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $categories = $query->paginate(20)->appends(request()->query());
        return view('admins.news_category.index',compact('categories'));
    }
    public function create()
    {
        return view('admins.news_category.create');
    }
    public function store(CategoryNewsCreateRequest $request)   
    {
        $data = $request->getData();
        $allCates = CategoryNews::all();
        foreach($allCates as $cate){
            $dataUpdate = [
                'position' => $cate->position + 1
            ];
            $cate->update($dataUpdate);
        }
        $data['position']= CategoryNews::min('position')-1;
        CategoryNews::create($data);
        return redirect()->route('news_category_index')
            ->with('message', 'Tạo thể loại tin tức thành công');
    }
    public function edit($id)
    {
        $category = CategoryNews::find($id);
        return view('admins.news_category.edit',compact('category'));
    }
    public function update(CategoryNewsUpdateRequest $request, $id)
    {   
        $category = CategoryNews::find($id);
        $data = $request->getData();
        $category->update($data);
        return redirect()->route('news_category_index')->with('message', 'Cập nhập thể loại tin tức thành công');
    }
    public function delete($id)
    {
        $category = CategoryNews::find($id);
        $category->delete();
        News::where('news_category_id',$id)->delete();
        return redirect()->route('news_category_index')->with('message', 'Xóa thể loại tin tức thành công');
    }
    public function sortTable(Request $request){
        $posts = CategoryNews::all();

        foreach ($posts as $post) {
            foreach ($request->order as $order) {
                if ($order['id'] == $post->id) {
                    $post->update(['position' => $order['position']]);
                }
            }
        }
        
        return response('Update Successfully.', 200);
    }
}
