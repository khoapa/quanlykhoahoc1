<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Answer;
use App\Http\Requests\CreateAnswerRequest;
use App\Http\Requests\UpdateAnswerRequest;
use App\Models\Question;

class AnswerController extends Controller
{
    public function index($id){
        $question = Question::findOrFail($id);
        $answers = Answer::where('question_id',$id)->orderBy('id','DESC')->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.answers.index',compact('answers','id','question'));
    }
    public function create($id){
        return view('admins.answers.add',compact('id'));
    }
    public function store(Request $request,$id){
        $data = $request->all();  
   
        if(!($data['correct'][0] == 0 && count($data['correct']) == 1)){
            for($i= 0;$i<=count($data['correct']);$i++){
                if($data['correct'][$i] == 1){
                    unset($data['correct'][$i-1]);
                }
            }
            $data['correct'] = array_values($data['correct']);
        }
        foreach($data['answer'] as $key=>$an){
             if($data['correct'][0] == 0 && count($data['correct']) == 1 || empty($data['correct'])){
                 $correct = 0;
             }else{
                 $correct = $data['correct'][$key];
                 
             }
            $dataInsert = [
                "question_id" => $id,
                "answer" => $an,
                "correct" => $correct
            ];
            Answer::create($dataInsert);
        }
        return redirect()->route('answer.answer.index',$id)->with('message','Thêm câu trả lời thành công.');
    }
    public function edit($id){
        $answer = Answer::findOrFail($id);
        return view('admins.answers.edit',compact('answer'));
    }
    public function update(UpdateAnswerRequest $request,$id){
            $data = $request->getData();
            $answer = Answer::findOrFail($id);
            $questionId = $answer->question_id;
            $updateAnswer = $answer->update($data);
            if ($updateAnswer) {
                return redirect()->route('answer.answer.index',$questionId)->with('message','Cập nhập câu trả lời thành công.');
            }else {                        
                return redirect()->back()->with('message','Cập nhập câu trả lời thất bại.');
            }
        }
        public function destroy($id){
            $answer = Answer::findOrFail($id);
            $delAnswer = $answer->delete();
            if ($delAnswer) {
                return redirect()->back()->with('message','Xóa câu trả lời thành công.');
            }else {                        
                return redirect()->back()->with('message','Xóa câu trả lời thất bại.');
            }
        }
}