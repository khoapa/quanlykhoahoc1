<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClassCreateRequest;
use App\Http\Requests\ClassUpdateRequest;
use App\Models\Classes;
use App\Models\Grade;
use App\Models\School;
use App\User;
use Illuminate\Http\Request;
use App\Services\UploadService;
use App\Enums\UserTypeEnum;
use App\Helpers\Helper;
use App\Http\Requests\StudentRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Models\Conversation;
use App\Models\UserCourse;
use App\Models\UserMap;
use App\Models\Course;
use App\Models\CourseTheory;
use App\Models\SchoolCourse;
use App\Models\CourseFavorite;
use App\Models\UserCourseTheory;
use App\Models\UserCategoryMap;

class ClassController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $class = Classes::orderBy('id', 'DESC');
        $schoolID = $request->get('school_id');
        $gradeID = $request->get('grade_id');
        $user = auth()->user();
        $grades = [];
        $schools = [];
        $class = Classes::orderBy('id', 'DESC');
        if (!is_null($name)) {
            $class = $class->where('name', 'LIKE', '%' . $name . '%');
        }
        $schools = School::orderBy('name')->get();
        if ($user->hasRole('school')) {
            $schoolID = $user->school_id;
            $class = $class->where('school_id',$user->school_id);
        };
        if (!$user->hasanyrole('school','Admin')) {
            $schoolID = $user->school_id;
            $class = $class->where('id',$user->class_id);
        };
        if (!is_null($schoolID)) {
            $class = $class->where('school_id', '=', $schoolID);
            $gradesIds = School::findOrFail($schoolID)->grade_ids;
            $gradesArray = json_decode($gradesIds);
            if (!is_null($gradesArray)) {
                foreach ($gradesArray as $key => $value) {
                    $grades[$key]['id'] = $value;
                    $grades[$key]['name'] = Grade::findOrFail($value)->name;
                };
            }
        }  
        if (!is_null($gradeID)) {
            $class = $class->where('grade_id', $gradeID);
        }
        $statusAllClass= 1;  
        foreach($class->get() as $cl){
            if($cl->status==2){
                $statusAllClass = 2;
            }
        }
        $classes = $class->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.classes.index', compact('classes', 'schools', 'grades','statusAllClass'));
    }
    public function create()
    {
        $gradeOfSchool = [];
        $user = auth()->user();
        $schools = School::orderBy('name');
        $teachers = User::where('user_type', UserTypeEnum::TEACHER);
        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id', $user->school_id);
            $teachers = $teachers->where('id', $user->school_id);
            $schoolID = $user->school_id;
            if (!is_null($schoolID)) {
                $gradesIds = School::findOrFail($schoolID)->grade_ids;
                $gradesArray = json_decode($gradesIds);
                if (!is_null($gradesArray)) {
                    foreach ($gradesArray as $key => $value) {
                        $gradeOfSchool[$key]['id'] = $value;
                        $gradeOfSchool[$key]['name'] = Grade::findOrFail($value)->name;
                    };
                }
        }
    }
        $schools = $schools->get();
        $grades = Grade::orderBy('name')->get();
       $teachers = $teachers->orderBy('name')->get();
        return view('admins.classes.create', compact('schools', 'grades', 'teachers','gradeOfSchool'));
    }
    public function store(ClassCreateRequest $request)
    {
        $data = $request->getData();
        if(is_null($data['thumbnail'])){
            $data['thumbnail'] = config('settings.image');
        }

        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('class', $file);
            $data['thumbnail'] = $data_image;
        }

        $insertData = Classes::create($data);
        if(!is_null($data['user_id'])){
            $userTeacher = User::where('id',$data['user_id'])->first();
            if(!is_null($userTeacher)){
                $userTeacher->update(['class_id'=>$insertData->id]);
            }
        }
        if ($insertData) {
            return redirect(session('back_url'))->with('message', 'Tạo lớp học thành công');
        } else {
            return redirect()->back()->with('message', 'Tạo lớp học thất bại');
        }
    }
    public function delete($id)
    {
        $class = Classes::findOrFail($id);
        $usersClass = User::where('class_id', $id)->get();
        foreach($usersClass as $user){
            $userId = $user->id;
            Conversation::where('user1_id', $userId)->orWhere('user2_id', $userId)->delete();
            UserCourse::where('user_id',$userId)->delete();
            UserMap::where('user_id',$userId)->delete();
            $courses = Course::where('author',$userId)->get();
            $coursesTheory = CourseTheory::where('author',$userId)->get();
            foreach($courses as $course){
                $idCourse = $course->id;
                $schoolCourse = SchoolCourse::where('course_id',$idCourse)->delete();
                $userCourse = UserCourse::where('course_id',$idCourse)->delete();
                $courseFavouris = CourseFavorite::where('course_id',$idCourse)->delete();
            }  
            foreach($coursesTheory as $theory){
                $idTheory = $theory->id;
                $userTheory = UserCourseTheory::where('course_theory_id',$idTheory)->delete();
            }  
           Course::where('author',$userId)->delete();
           CourseTheory::where('author',$userId)->delete();
           UserCategoryMap::where('user_id',$userId)->delete();
           $user->delete();
        }
        $class->delete();
        return redirect(session('back_url'))->with('message', 'Xóa lớp học thành công');
    }
    public function edit(Request $request,$id)
    {
        $user = auth()->user();
        $backUrl = $request->url;
        $class = Classes::find($id);
        $idSchoolOfClass = $class->school->id;
        $gradesIds = School::findOrFail($idSchoolOfClass)->grade_ids;
        $gradesArray = json_decode($gradesIds);
        $gradeOfSchool = [];
        if (!is_null($gradesArray)) {
            foreach ($gradesArray as $key => $value) {
                $gradeOfSchool[$key]['id'] = $value;
                $gradeOfSchool[$key]['name'] = Grade::findOrFail($value)->name;
            };
        }
        $schools = School::orderBy('name');
        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id',$user->school_id);
        }
        $schools = $schools->get();
        $teachers = User::join('model_has_roles', function ($join) {
            $join->on('users.id', '=', 'model_has_roles.model_id');
        })
            ->where('school_id', $class->school_id)->where('user_type', UserTypeEnum::TEACHER)
            ->where('class_id','=',NULL)
            ->where('model_has_roles.role_id', 3)
            ->orWhere('class_id',$id)
            ->get();
        return view('admins.classes.edit', compact('class', 'schools', 'gradeOfSchool', 'teachers','backUrl'));
    }
    public function update(ClassUpdateRequest $request, $id)
    {
        $class = Classes::find($id);
        $data = $request->getData();
        $data_image = $class->thumbnail;
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $data_image = UploadService::uploadImage('class', $file);
        }
        $data['thumbnail'] = $data_image;
        if(!is_null($data['user_id']) && $data['user_id'] != $class->user_id){
            $userTeacher = User::where('id',$class->user_id)->first();
            if(!is_null($userTeacher)){
                $userTeacher->update(['class_id'=>NULL]);
            }
        }
        $update = $class->update($data);
        $users = User::where('class_id',$id)->update(['class_code'=>$data['class_code']]);
        if ($update) {
            return redirect(session('back_url'))->with('message', 'Đã cập nhật lớp học thành công.');
        } else {
            return redirect()->route('class_index')->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function listStudent(Request $request, $id, $idSchool = null)
    {
        $name = $request->get('search');
        $backUrl = $request->url;
        $query = User::where('class_id', $id)->where('user_type', UserTypeEnum::STUDENT)->orderBy('id', 'DESC');
        if (!is_null($idSchool)) {
            $query = $query->where('school_id', $idSchool);
        }
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $class = Classes::find($id);
        $statusAllStudent = 1;  
        foreach($query->get() as $student){
            if($student->status==2){
                $statusAllStudent = 2;
            }
        }
        $students = $query->paginate(config('settings.countPaginate'));
        return view('admins.classes.list_students', compact('students', 'class', 'backUrl','statusAllStudent'));
    }
    public function classSchool($id)
    {

        $class = Classes::where('school_id', $id)->orderBy('name')->get();
        $user = auth()->user();
        if ($user->hasRole('teacher')) {
            $class = Classes::where('id', $user->class_id)->orderBy('name')->get();
        }
        return response()->json($class);
    }
    public function enableClass(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $school_id = $request->school_id;
            $grade_id = $request->grade_id;
            $type = $request->type;
            $classes = Classes::orderBy('id', 'DESC');

            if (!is_null($search_name)) {
                $classes = $classes->where('name', 'LIKE', '%' . $search_name . '%');
            }
            if (!is_null($school_id)) {
                $classes = $classes->where('school_id', $school_id);
            }
            if (!is_null($grade_id)) {
                $classes = $classes->where('grade_id', $grade_id);
            }
            $classes = $classes->get();
            if (!is_null($classes)) {
                foreach ($classes as $class) {
                    $data = [
                        'status' => $type,
                    ];
                    $class->update($data);
                    $usersClass = User::where('class_id', $class->id)->update($data);
                }
            }
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
    public function updateStatusClass(Request $request)
    {
        if ($request->ajax()) {
            $class = Classes::findOrFail($request->id);
            $users = User::whereIn('user_type',[UserTypeEnum::TEACHER, UserTypeEnum::STUDENT])->where('class_id',$class->id);
            $class->status = $request->status;
            $users->update(['status'=>$request->status]);
            if ($class->save()) {
                $message = 'chuyển trạng thái thành công';
            } else {
                $message = 'chuyển trạng thái thất bại';
            }
            return response()->json($message);
        }
    }
    public function enableUser(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $class_id = $request->class_id;
            $type = $request->type;
            $users = User::where('user_type',UserTypeEnum::STUDENT)->where('class_id',$class_id);
            if (!is_null($search_name)) {
                $users = $users->where('name', 'LIKE', '%' . $search_name . '%');
            }
           $users->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}