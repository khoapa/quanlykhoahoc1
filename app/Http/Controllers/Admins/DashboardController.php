<?php

namespace App\Http\Controllers\Admins;
use App\Http\Controllers\AdminController;

class DashboardController extends AdminController
{
	public function index(){
		return view('admins.dashboard.index');
	}
}
