<?php

namespace App\Http\Controllers\Admins;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCourseRequest;
use App\Models\CourseCategory;
use App\Models\Course;
use App\Http\Requests\UpdateCourseRequest;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\Models\School;
use App\User;
use Carbon\Carbon;
use App\Enums\TypeNotiFyEnum;
use App\Models\UserCourse;
use App\Models\CourseTheory;
use App\Models\SchoolCourse;
use App\Models\ClassCourse;
use App\Models\CourseFavorite;

class CourseController extends Controller
{
    use \App\Traits\NotificationTrait;
    public function index(Request $request)
    {
        $user = auth()->user();
        $user = auth()->user();
        $categoryId = $request->get('category_id');
        $schoolId = $request->get('author');
        $authId = $request->get('author');
        $courses = Course::orderBy('id', 'DESC');
        $name = $request->get('search');
        if (!is_null($name)) {
            $courses = $courses->where('name', 'LIKE', '%' . $name . '%');
        }
        if (!is_null($categoryId)) {
            $courses = $courses->where('course_category_id',$categoryId);
        }
        if (!is_null($authId)) {
            $courses->where('author','=',$authId);
        }
        if ($user->admin_flg != 1 && is_null($authId)) {
            $courses = $courses->where(function($q1) use($user){
                $q1->where('author', $user->id)->orWhereHas('authorUser',function($q){
                $q->where('admin_flg',1);
            });
        });
        }
        $authors = Course::with('authorUser');
        if(!$user->hasRole('Admin')){
            $schoolId = $user->school_id;
            $authors->whereHas('authorUser',function($q) use($schoolId){
                $q->where('school_id',$schoolId)->orWhere('admin_flg',1);
            });
        }
        $authors = $authors->get()->sortBy('authorUser.name')->unique('author');
        $categories = CourseCategory::orderBy('name')->get();
        $statusAllCourse = 1;  
        foreach($courses->get() as $course){
            if($course->status==0){
                $statusAllCourse = 2;
            }
        }
        $courses = $courses->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.course.index', compact('courses','categories','authors','statusAllCourse'));
    }
    public function create()
    {
        $categoryCourses = CourseCategory::orderBy('name')->get();
        return view('admins.course.add', compact('categoryCourses'));
    }
    public function store(CreateCourseRequest $request)
    {

        $data = $request->getData();
        $creator = auth()->user();
        $users = [];
        if ($data['status'] == 1)
            if ($creator->admin_flg == 1) {
                $users = User::where('id', "!=", $creator->id)->get();
            } else {
                $users = User::where('id', "!=",  $creator->id)->where('school_id', $creator->school_id)->get();
            }
        if ($request->hasFile('cover_image')) {
            $file = $request->file('cover_image');
            $data_image = UploadService::uploadImage('course', $file);
            $data['cover_image'] = $data_image;
        }
            $data['start_date'] = Carbon::now();
            $data['start_date'] = Helper::formatSqlDate($data['start_date']);
        $data['author'] = $creator->id;
        $data['total_point'] = config('settings.totalPoint');
        $insertData = Course::create($data);
        if($data['status'] == 1){
            if ($creator->admin_flg == 1) {
                $schools = School::all();
            } else {
                $schools = School::where('id',$creator->school_id)->get();
            }
            foreach($schools as $school){
                $data = [
                    'school_id' => $school->id,
                    'course_id' => $insertData->id,
                ];
                SchoolCourse::create($data);
            }    
        }
        $title_notification = '' . $creator->name . ' đã tạo một khoá học.';
        $type = TypeNotiFyEnum::COURSE;
        $id_type = $insertData->id;
        foreach ($users as $user)
            $this->notifcationSend($title_notification, "", $creator, $user,$type,$id_type);
        if ($insertData) {
            return redirect()->route('course.index', $insertData->id)->with('message', trans('course.create_success'));
        } else {
            return redirect()->back()->with('message', trans('course.create_fail'));
        }
    }
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $categoryCourses = CourseCategory::orderBy('name')->get();
        return view('admins.course.edit', compact('course', 'categoryCourses'));
    }
    public function update(UpdateCourseRequest $request, $id)
    {
        $course = Course::findOrFail($id);
        $data = $request->getData();
        $data_image = $course->cover_image;
        if ($request->hasFile('cover_image')) {
            $file = $request->file('cover_image');
            $data_image = UploadService::uploadImage('course', $file);
        }
        $data['cover_image'] = $data_image;
        // if (!is_null($data['start_date'])) {
        //     $data['start_date'] = Helper::formatSqlDate($data['start_date']);
        // }
        $update = $course->update($data);
        $countQuestionChoose = $course->question->where('type', 1)->count();
        $countQuestionEssay = $course->question->where('type', 2)->count();
        $totalPointChoice = $course->total_point_choice;
        $totalPoint = $course->total_point;
        if ($countQuestionChoose >= 1) {
            $point = $totalPointChoice / $countQuestionChoose;
            $course->question()->where('type', 1)->update(['point' => $point]);
        }
        if ($countQuestionEssay >= 1) {
            $point = ($totalPoint - $totalPointChoice) / $countQuestionEssay;
            $course->question()->where('type', 2)->update(['point' => $point]);
        }
        if ($update) {
            return redirect()->route('question.question.index', $course->id)->with('message', trans('course.update_success'));
        } else {
            return redirect()->back()->with('message', trans('course.update_fail'));
        }
    }
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $usercourse = UserCourse::where('course_id',$id)->delete();
        $schoolCourse = SchoolCourse::where('course_id',$id)->delete(); 
        $classCourse = ClassCourse::where('course_id',$id)->delete(); 
        $course_favorites = CourseFavorite::where('course_id',$id)->delete();
        $delCourse = $course->delete();
        if ($delCourse) {
            return redirect(session('urlCourse'))->with('message', trans('course.delete_success'));
        } else {
            return redirect()->back()->with('message', trans('course.delete_fail'));
        }
    }
    public function addStatusCourse(Request $request)
    {
        if ($request->ajax()) {
            $course = Course::findOrFail($request->id);
            $course->status = $request->status;
            if ($course->save()) {
                $message = 'Chuyển trạng thái thành công';
            } else {
                $message = 'Chuyển trạng thái thất bại';
            }
            return response()->json($message);
        }
    }
    public function enableCourse(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $category = $request->category;
            $author = $request->author;
            $type = $request->type;         
            $courseTheory = Course::orderBy('id','DESC');
            if(!is_null($search_name)){
                $courseTheory = $courseTheory->where('name', 'LIKE', '%'.$search_name.'%');
            }
            if(!is_null($category)){
                $courseTheory = $courseTheory->where('course_category_id',$category);
            }
            if(!is_null($author)){
                $courseTheory = $courseTheory->where('author',$author);
            }
            $courseTheory->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}