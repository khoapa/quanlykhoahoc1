<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\StudentRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StudentUpdateRequest;
use App\Models\School;
use App\Models\Classes;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\Enums\UserTypeEnum;
use App\Models\Role;
use App\Models\Conversation;
use App\Models\UserCourse;
use App\Models\UserMap;
use App\Models\Course;
use App\Models\CourseTheory;
use App\Models\SchoolCourse;
use App\Models\CourseFavorite;
use App\Models\UserCourseTheory;
use App\Models\UserCategoryMap;

class StudentController extends Controller
{
    public function index(Request $request)
    {  
        $name = $request->get('search');
        $schoolID = $request->get('school_id');
        $classID = $request->get('class_id');
        $query=User::where('user_type',UserTypeEnum::STUDENT)->orderBy('id', 'DESC');
        $classes = null;
        $schools = School::orderBy('name')->get();
        $user = auth()->user();
        if($user->hasRole('school')){
            $schoolID = $user->school_id;
            $schools = School::where('id',$user->school_id)->get();
        }
        $user = auth()->user();
        if($user->hasRole('teacher')){
            $classID = $user->class_id;
        }
        if ($user->user_type == UserTypeEnum::STAFF){
            $query = $query->where('school_id',$user->school_id);
        }
        if (!is_null($schoolID)) {
            $query = $query->where('school_id',$schoolID);
            $classes = Classes::where('school_id',$schoolID)->orderBy('name')->get();   
        }
        if (!is_null($classID)) {
            $query = $query->where('class_id',$classID);
        }
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $statusAllStudent = 1;  
        foreach($query->get() as $student){
            if($student->status==2){
                $statusAllStudent = 2;
            }
        }
        $student = $query->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.student.index', compact('student','schools','classes','statusAllStudent'));
    }
    public function create()
    {
        $user = auth()->user();
        $schools = School::orderBy('name');
        $classes = Classes::orderBy('name');

        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id',$user->school_id);
            $classes = $classes->where('school_id',$user->school_id);
        }
        $schools = $schools->get();
        $classes = $classes->get();
        if($user->hasRole('Admin')){
            $classes =[];
        }
        $roles = Role::where('name','!=','Admin');
        if(!$user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school');
        }
        $roles = $roles->get();
        return view('admins.student.create', compact('schools', 'classes','roles'));
    }
    public function store(StudentRequest $request)
    {
        $data = $request->getData();
        $data['school_code'] = School::findOrFail($data['school_id'])->school_code;
        $data['class_code'] = Classes::findOrFail($data['class_id'])->class_code;
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['password'] = Hash::make(config('settings.password'));
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_image  = $data['avatar'];
        if(is_null($data_image)){
            $data_image = config('settings.image');
        }
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('student', $file);
        }
        $data['avatar'] = $data_image; 
        try {
            $userCreate = User::create($data);
            $roleId = $data['role_id'];
            if(!is_null($roleId)){
                $userCreate->syncRoles([$roleId]);
            }
            if($userCreate->user_type == UserTypeEnum::TEACHER && $userCreate->class_id != null){
                $dataClass = [
                    'user_id' => $userCreate->id,
                ];
                $class = Classes::findOrFail($userCreate->class_id);
                if(!is_null($class->user_id)){
                    $userTeacher = User::where('id',$class->user_id)->first();
                    if(!is_null($userTeacher)){
                        $userTeacher->update(['class_id'=>NULL]);
                    }
                }
                $class->update($dataClass);
            }
            return redirect(session('back_url'))->with('message',  'Thêm mới học sinh thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Thêm mới học sinh không thành công');
        }
    }
    public function edit(Request $request,$id)
    {
        $backUrl = $request->url;
        $student = User::findOrFail($id);
        $schoolId = $student->school_id;
        $user = auth()->user();
        $schools = School::orderBy('name');
        $classes = Classes::orderBy('name');

        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id',$user->school_id);
            $classes = $classes->where('school_id',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $classes =$classes->where('school_id',$student->school_id);
        }
        $schools = $schools->get();
        $classes = $classes->get();
        $roles = Role::where('name','!=','Admin');
        if(!$user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school');
        }
        $roles = $roles->get();
        return view('admins.student.update', compact('student', 'schools', 'classes','roles','backUrl'));
    }
    public function update(StudentUpdateRequest $request, $id)
    {
        try {
            $student = User::findOrFail($id);
            $class_id = $student->class_id;
            $data = $request->getData();
            $data['school_code'] = School::findOrFail($data['school_id'])->school_code;
            $data['class_code'] = Classes::findOrFail($data['class_id'])->class_code;
            $data['birthday'] = Helper::formatSqlDate($data['birthday']);
            $data['email'] = str_replace(' ', '', $data['email']);
            $data_logo = $student->avatar;
            if ($request->hasFile('avatar')) {
                $file = $request->file('avatar');
                $data_logo = UploadService::uploadImage('avatar', $file);
            }
            $data['avatar'] = $data_logo;
            $roleId = $data['role_id'];
            $roleOld = $student->role()->first();
            if(!is_null($roleId)){
                $student->syncRoles([$roleId]);
            }else{
                if($roleOld){
                    $student->removeRole($roleOld->role_id);   
                }
            }
            $student->update($data);
            if($student->user_type == UserTypeEnum::TEACHER && $student->class_id != $class_id){
                $dataClass = [
                    'user_id' => $student->id,
                ];
                $class = Classes::findOrFail($student->class_id);
                if(!is_null($class->user_id)){
                    $userTeacher = User::where('id',$class->user_id)->first();
                    if(!is_null($userTeacher)){
                        $userTeacher->update(['class_id'=>NULL]);
                    }
                }
                $class->update($dataClass);
            }
            return redirect(session('back_url'))->with('message', 'Đã cập nhật học sinh thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function searchClassAjax(Request $request)
    {
        if ($request->ajax()) {
            $classes = Classes::where('school_id', '=', $request->id_School)->orderBy('name')->get();

            return response()->json($classes);
        }
    }
    public function updateStatusAjax(Request $request)
    {
        if ($request->ajax()) {
            $student = User::findOrFail($request->id);
            $student->status = $request->status;
            if ($student->save()) {
                $message = 'chuyển trạng thái thành công';
            } else {
                $message = 'chuyển trạng thái thất bại';
            }
            return response()->json($message);
        }
    }
    public function destroy($id)
    {
        $student = User::findOrFail($id);
        try {
            Conversation::where('user1_id', $id)->orWhere('user2_id', $id)->delete();
            UserCourse::where('user_id',$id)->delete();
            UserMap::where('user_id',$id)->delete();
            $courses = Course::where('author',$id)->get();
            $coursesTheory = CourseTheory::where('author',$id)->get();
            foreach($courses as $course){
                $idCourse = $course->id;
                $schoolCourse = SchoolCourse::where('course_id',$idCourse)->delete();
                $userCourse = UserCourse::where('course_id',$idCourse)->delete();
                $courseFavouris = CourseFavorite::where('course_id',$idCourse)->delete();
            }  
            foreach($coursesTheory as $theory){
                $idTheory = $theory->id;
                $userTheory = UserCourseTheory::where('course_theory_id',$idTheory)->delete();
            }  
           Course::where('author',$id)->delete();
           CourseTheory::where('author',$id)->delete();
           UserCategoryMap::where('user_id',$id)->delete();
           $student->delete();
            $classes = Classes::where('user_id',$id)->get();
            foreach($classes as $class){
                $classUpdate = Classes::where('user_id',$class->id)->update(['user_id'=>NULL]);
            }
            return redirect(session('back_url'))->with('message', 'Đã xóa học sinh thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
    public function enableStudent(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $school_id = $request->school_id;
            $class_id = $request->class_id;
            $type = $request->type;

            $user = User::where('user_type',UserTypeEnum::STUDENT);
            if(!is_null($search_name)){
                $user = $user->where('name', 'LIKE', '%' . $search_name . '%');
            }
            if(!is_null($school_id)){
                $user = $user->where('school_id', $school_id);
            }
            if(!is_null($class_id)){
                $user = $user->where('class_id', $class_id);
            }
            $user->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}
