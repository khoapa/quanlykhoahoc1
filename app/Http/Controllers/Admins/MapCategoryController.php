<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryMapCreateRequest;
use App\Http\Requests\CategoryMapUpdateRequest;
use App\Models\Map;
use App\Models\MapCategory;
use App\Services\UploadService;
use Illuminate\Http\Request;
use App\Models\UserCategoryMap;

class MapCategoryController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $query = MapCategory::orderBy('id','DESC');
        if(!is_null($name)){
            $query = $query->where('name','LIKE','%'.$name.'%');
        }
        $categories = $query->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.map_category.index',compact('categories'));
    }
    public function create()
    {
        return view('admins.map_category.create');
    }
    public function store(CategoryMapCreateRequest $request)   
    {
        $data = $request->getData();
        if ($request->hasFile('icon')) {
           $file = $request->file('icon');
           $data_image = UploadService::uploadImage('map_category', $file);
           $data['icon'] = $data_image;
       }
        MapCategory::create($data);
        return redirect()->route('map_category_index')
            ->with('message', 'Tạo thể loại điểm map thành công');
    }
    public function edit(Request $request,$id)
    {
        $backUrl = $request->url;
        $category = MapCategory::find($id);
        return view('admins.map_category.edit',compact('category','backUrl'));
    }
    public function update(CategoryMapUpdateRequest $request, $id)
    {   
        $category = MapCategory::find($id);
        $data = $request->getData();
        $data_image = $category->icon;
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $data_image = UploadService::uploadImage('map_category', $file);
        }
        $data['icon'] = $data_image;
        $category->update($data);
        return redirect()->route('map_category_index')->with('message', 'Cập nhật thể loại điểm map thành công');
    }
    public function delete($id)
    {
        $category = MapCategory::find($id);
        $maps = Map::where('map_category_id',$id);
        UserCategoryMap::where('category_map_id',$id)->delete(); 
        $maps->delete();
        $category->delete();
        return redirect(session('back_url'))->with('message', 'Xóa thể loại điểm map thành công');
    }
    
}
