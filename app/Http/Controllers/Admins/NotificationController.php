<?php

namespace App\Http\Controllers\Admins;

use App\Enums\TypeNotiFyEnum;
use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\School;
use App\User;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    use \App\Traits\NotificationTrait;
    public function index()
    {
        $user = auth()->user();
        $schools = School::orderBy('name');
        if(!$user->hasRole('Admin')){
            $schools->where('id',$user->school_id);
        }
        $schools = $schools->get();
        $arrSchool = [];
        foreach ($schools as $school) {
            $arrClass = [];
            
            $classes = Classes::where('school_id', $school->id);
            if(!$user->hasRole('Admin') && !$user->hasRole('school') ){
                $classes->where('id',$user->class_id);
            }
            $classes = $classes->get();
            foreach ($classes as $class) {
                $arr1 = [
                    'id' => $class->id . ".class",
                    'text' => $class->name,
                ];
                array_push($arrClass, $arr1);
            }
            $arr = [
                'id' => $school->id . ".school",
                'text' => $school->name,
                'children' => $arrClass
            ];
            array_push($arrSchool, $arr);
        }
        $arrSchool = json_encode($arrSchool);
        return view('admins.notification.index', compact('schools', 'arrSchool'));
    }
    public function showUser(Request $request)
    {
        $user = auth()->user();
        if ($request->ajax()) {
            $arrId = $request->checkedIds;
            $arrStudent = [];
            if (!is_null($arrId)) {
                foreach ($arrId as $id) {
                    $cut =  explode('.', $id);
                    $arrStudentNew = [];
                    if (in_array("class", $cut)) {
                        $students = User::where('class_id', $cut[0]);
                        if(!$user->hasRole('Admin|school')){
                            $students->where('id','!=',$user->id);
                        }
                        $students = $students->get();
                        foreach ($students as $student) {
                            $arrTemp = [
                                'id' => $student->id,
                                'name' => $student->name,
                                'school_code' => $student->school->school_code,
                                'class_code' => $student->classes->class_code,
                                'user_type' => $student->user_type,
                            ];
                            if ($arrTemp) {
                                array_push($arrStudentNew, $arrTemp);
                            }
                        }
                    }
                    if ($arrStudentNew) {
                        array_push($arrStudent, $arrStudentNew);
                    }
                }
            }
            return response()->json($arrStudent);
        }
    }
    public function pushNotificationGeneral(Request $request)
    {
        $user_ids = $request->list_id;
        $content = $request->content;
        $user = auth()->user();
        if ($user_ids) {
            $user_ids = explode(',', $request->list_id);
            $users = User::whereIn('id', $user_ids)->get();
            foreach ($users as $receiver) {
                $title_notification = "Bạn có một thông báo chung.";
                $this->notifcationSend($title_notification, $content, $user, $receiver, TypeNotiFyEnum::GENERAL, null);
            }
            return redirect()->back()->with('message', 'Gửi thông báo thành công.');
        } else {
            return redirect()->back()->with('message', 'Vui lòng chọn người gửi.');
        }
    }
}
