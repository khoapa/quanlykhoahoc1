<?php

namespace App\Http\Controllers\Admins;

use App\Enums\RoleTypeEnum;
use App\Enums\UserStatusEnum;
use App\Enums\UserTypeEnum;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\Grade;
use App\Models\School;
use App\Models\UserCourse;
use App\User;
use PHPExcel;
use Symfony\Component\HttpFoundation\Request;

use function GuzzleHttp\json_decode;

class ExportFileController extends Controller
{
    public function exportUser(Request $request)
    {
        $user = auth()->user();
        $name = $request->get('search');
        $schoolID = $request->get('school_id');
        $classID = $request->get('class_id');
        $type = $request->get('type');
        $query = User::orderBy('id', 'DESC');
        $file_name = 'result.xlsx';
        if ($user->hasRole('school')) {
            $schoolID = $user->school_id;
        }
        if (!$user->hasRole('school|Admin')) {
            $schoolID = $user->school_id;
            $classID = $user->class_id;
        }
        if ($type == UserTypeEnum::STUDENT) {
            $file_name = 'Học_Sinh.xlsx';
            $title_table = 'Học Sinh';
            $title_sheet = 'STUDENT_SHEET';
            $query = $query->where('user_type', UserTypeEnum::STUDENT);
        } elseif ($type == UserTypeEnum::TEACHER) {
            $file_name = 'Giáo_Viên.xlsx';
            $title_table = 'Giáo Viên';
            $title_sheet = 'TEACHER_SHEET';
            $query = $query->where('user_type', UserTypeEnum::TEACHER);
        } else {
            $file_name = 'user.xlsx';
            $title_table = 'USER_TABLE';
            $title_sheet = 'USER_SHEET';
            $query = $query->whereIn('user_type', [UserTypeEnum::TEACHER, UserTypeEnum::STUDENT]);
        }

        if (!is_null($schoolID)) {
            $query = $query->where('school_id', $schoolID);
        }
        if (!is_null($classID)) {
            $query = $query->where('class_id', $classID);
        }
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }

        $objPHPExcel = new PHPExcel();
        $users = $query->get();

        $objPHPExcel->createSheet(); //create new sheet
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setTitle($title_sheet);
        $activeSheet->setCellValue('A1', $title_table);
        $activeSheet->mergeCells("A1:M1");
        $activeSheet->setCellValue('A4', 'STT')
            ->setCellValue('B4', 'HỌ VÀ TÊN')
            ->setCellValue('C4', 'EMAIL')
            ->setCellValue('D4', 'HÌNH ĐẠI DIỆN')
            ->setCellValue('E4', 'SỐ ĐIỆN THOẠI')
            ->setCellValue('F4', 'NGÀY SINH')
            ->setCellValue('G4', 'ĐỊA CHỈ')
            ->setCellValue('H4', 'GIỚI TÍNH')
            ->setCellValue('I4', 'MÃ TRƯỜNG')
            ->setCellValue('J4', 'MÃ LỚP')
            ->setCellValue('K4', 'LOẠI USER')
            ->setCellValue('L4', 'TRẠNG THÁI')
            ->setCellValue('M4', 'NHÓM QUYỀN'); // set title cho dòng đầu tiên
        $stt = 1;
        $row = 5;
        foreach ($users as $key => $user) {
            $activeSheet
                ->setCellValue("A$row", (string) $stt)
                ->setCellValue("B$row", (string) $user->name)
                ->setCellValue("C$row", (string) $user->email)
                ->setCellValue("D$row", (string) $user->avatar)
                ->setCellValue("E$row", (string) $user->phone)
                ->setCellValue("F$row", (string) (!is_null($user->birthday) ? Helper::formatDate1($user->birthday) : ""))
                ->setCellValue("G$row", (string) $user->address)
                ->setCellValue("H$row", (string) (!is_null($user->gender)) ? $user->gender == 1 ? "Nam" : "Nữ" : "Không xác định")
                ->setCellValue("I$row", (string) $user->school_code)
                ->setCellValue("J$row", (string) $user->class_code)
                ->setCellValue("K$row", (string) (!is_null($user->user_type)) ? UserTypeEnum::getDescription($user->user_type) : "Không xác định")
                ->setCellValue("L$row", (string) (!is_null($user->status)) ? UserStatusEnum::getDescription($user->status) : "Không xác định")
                ->setCellValue("M$row", (string) (count($user->roles->pluck('description')) != 0) ? $user->roles->pluck('description')[0] : "Không xác định" );
            $stt++;
            $row++;
        }
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(storage_path('app/' . $file_name));
        return response()->download(storage_path('app/' . $file_name), $file_name, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $file_name . '"'
        ]);
    }

    public function exportStatistics(Request $request)
    {
        $user = auth()->user();
        $schoolId = $request->get('school_id');
        $dateBegin = null;
        $dateEnd = null;
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);
        $query = School::orderBy('id', 'ASC');
        if ($user->hasRole('school')) {
            $schoolId = $user->school_id;
        }
        if (!$user->hasRole('school|Admin')) {
            $schoolId = $user->school_id;
            $classID = $user->class_id;
        }
        if (!is_null($schoolId)) {
            $query->where('id', $schoolId);
        }
        $file_name = 'Statistics_school.xlsx';
        $title_table = 'BẢNG THỐNG KÊ SỐ LIỆU';
        $title_sheet = 'THỐNG KÊ SỐ LIỆU';
        $objPHPExcel = new PHPExcel();
        $schools = $query->get();

        $objPHPExcel->createSheet(); //create new sheet
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setTitle($title_sheet);
        $activeSheet->setCellValue('A1', $title_table);
        $activeSheet->mergeCells("A1:I1");
        $activeSheet->setCellValue('A4', 'STT')
            ->setCellValue('B4', 'MÃ TRƯỜNG')
            ->setCellValue('C4', 'TRƯỜNG')
            ->setCellValue('D4', 'LỚP')
            ->setCellValue('E4', 'KHOÁ HỌC')
            ->setCellValue('F4', 'DANH MỤC')
            ->setCellValue('G4', 'SỐ HỌC SINH ĐANG THAM GIA')
            ->setCellValue('H4', 'SỐ HỌC SINH ĐÃ THAM GIA')
            ->setCellValue('I4', 'TỔNG SỐ TRUY CẬP'); // set title
        $stt = 1;
        $row = 5;
        foreach ($schools as $key => $school) {
            $activeSheet
                ->setCellValue("A$row", (string) $stt)
                ->setCellValue("B$row", (string) $school->school_code)
                ->setCellValue("C$row", (string) $school->name)
                ->setCellValue("D$row", (string) $school->classes->count())
                ->setCellValue("E$row", (string) $school->course->count())
                ->setCellValue("F$row", (string) $school->course_category_count)
                ->setCellValue("G$row", (string) $school->getCourseUserJoin($dateBegin, $dateEnd))
                ->setCellValue("H$row", (string) $school->getCourseUserJoined($dateBegin, $dateEnd))
                ->setCellValue("I$row", (string) $school->getCourseUserVisit($dateBegin, $dateEnd));
            $stt++;
            $row++;
        }
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(storage_path('app/' . $file_name));
        return response()->download(storage_path('app/' . $file_name), $file_name, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $file_name . '"'
        ]);
    }

    public function exportTranscrip(Request $request)
    {
        $user = auth()->user();
        $category_id = $request->get('category_id');
        $school_id = $request->get('school_id');
        $class_id = $request->get('class_id');
        $course_id = $request->get('course_id');
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);
        if ($user->hasRole('school')) {
            $school_id = $user->school_id;
        }
        if (!$user->hasRole('school|Admin')) {
            $school_id = $user->school_id;
            $class_id = $user->class_id;
        }
        if (is_null($school_id) && !is_null($class_id)) {
            $class = Classes::findOrFail($class_id);
            $school_id = $class->school->id;
        }
        if (!is_null($course_id)) {
            $course = Course::findOrFail($course_id);
            $category_id = $course->category->id;
        }
        $usersCourses = UserCourse::whereIn('status', [1, 2])->orderBy('course_id');
        if (!is_null($school_id)) {
            $usersCourses = $usersCourses->whereHas('student', function ($query) use ($school_id) {
                $query = $query->where('school_id', $school_id);
            });
        }
        if (!is_null($category_id)) {
            $usersCourses = $usersCourses->whereHas('course', function ($query) use ($category_id) {
                $query = $query->where('course_category_id', $category_id);
            });
        }
        if (!is_null($class_id)) {
            $usersCourses = $usersCourses->whereHas('student', function ($query) use ($class_id) {
                $query = $query->where('class_id', $class_id);
            });
        }
        if (!is_null($course_id)) {
            $usersCourses = $usersCourses->whereHas('course', function ($query) use ($course_id) {
                $query = $query->where('course_id', $course_id);
            });
        }
        if (!is_null($dateBegin)) {
            $usersCourses->whereDate('join_date', '>=', $dateBegin);
        }
        if (!is_null($dateEnd)) {
            $usersCourses->whereDate('date_end', '<=', $dateEnd);
        }

        $file_name = 'Kết_Quả_Học_Tập.xlsx';
        $title_table = 'Kết Quả Học Tập';
        $title_sheet = 'Transcript_SHEET';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet(); //create new sheet
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setTitle($title_sheet);
        $activeSheet->setCellValue('A1', $title_table);

        $activeSheet->mergeCells("A1:J1");
        $activeSheet->setCellValue('A4', 'STT')
            ->setCellValue('B4', 'TÊN HỌC SINH')
            ->setCellValue('C4', 'TRƯỜNG')
            ->setCellValue('D4', 'LỚP')
            ->setCellValue('E4', 'KHOÁ HỌC')
            ->setCellValue('F4', 'THAM GIA')
            ->setCellValue('G4', 'KẾT THÚC')
            ->setCellValue('H4', 'ĐIỂM TRẮC NGHIỆM')
            ->setCellValue('I4', 'ĐIỂM TỰ LUẬN')
            ->setCellValue('J4', 'TỔNG ĐIỂM'); // set title
        $stt = 1;
        $row = 5;
        foreach ($usersCourses->get() as $usersCourse) {
            $activeSheet
                ->setCellValue("A$row", (string) $stt)
                ->setCellValue("B$row", (string) (!is_null($usersCourse->student) ? $usersCourse->student->name : ""))
                ->setCellValue("C$row", (string) (!is_null($usersCourse->student) ? (!is_null($usersCourse->student->school) ? $usersCourse->student->school->name : "") : ""))
                ->setCellValue("D$row", (string) (!is_null($usersCourse->student) ? !is_null($usersCourse->student->classes) ? $usersCourse->student->classes->name : "" : ""))
                ->setCellValue("E$row", (string) (!is_null($usersCourse->course) ? $usersCourse->course->name : ""))
                ->setCellValue("F$row", (string) (!is_null($usersCourse->join_date) ? Helper::formatDate1($usersCourse->join_date) : ""))
                ->setCellValue("G$row", (string) ($usersCourse->status == 2 && !is_null($usersCourse->date_end) ? Helper::formatDate1($usersCourse->date_end) : ""))
                ->setCellValue("H$row", (string) round($usersCourse->count_point_choose, 2))
                ->setCellValue("I$row", (string) ($usersCourse->count_point_essay == 0 ? "--" : $usersCourse->count_point_essay))
                ->setCellValue("J$row", (string) (($usersCourse->count_point_choose * (!is_null($usersCourse->course) ? $usersCourse->course->total_point_choice : 0) + $usersCourse->count_point_essay * ((!is_null($usersCourse->course) ? $usersCourse->course->total_point : 0) - $usersCourse->course->total_point_choice)) / 100));
            $stt++;
            $row++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(storage_path('app/' . $file_name));
        return response()->download(storage_path('app/' . $file_name), $file_name, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $file_name . '"'
        ]);
    }

    public function exportSchool(Request $request)
    {
        $user = auth()->user();
        $name = $request->get('search');
        $schools = School::orderBy('id', 'DESC');
        if (!is_null($name)) {
            $schools = $schools->where('name', 'LIKE', '%' . $name . '%');
        }
        $file_name = 'Danh_Sách_Trường.xlsx';
        $title_table = 'Trường';
        $title_sheet = 'Trường_SHEET';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet(); //create new sheet
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setTitle($title_sheet);
        $activeSheet->setCellValue('A1', $title_table);

        $activeSheet->mergeCells("A1:J1");
        $activeSheet->setCellValue('A4', 'STT')
            ->setCellValue('B4', 'MÃ TRƯỜNG')
            ->setCellValue('C4', 'TÊN TRƯỜNG')
            ->setCellValue('D4', 'LOGO')
            ->setCellValue('E4', 'ĐỊA CHỈ')
            ->setCellValue('F4', 'SỐ ĐIỆN THOẠI')
            ->setCellValue('G4', 'MÔ TẢ')
            ->setCellValue('H4', 'EMAIL')
            ->setCellValue('I4', 'MÃ KHỐI')
            ->setCellValue('J4', 'TRẠNG THÁI'); // set title
        $stt = 1;
        $row = 5;
        foreach ($schools->get() as $school) {
            $grades_code = [];
            if (!is_null($school->grade_ids)) {
                $grade_ids = json_decode($school->grade_ids);
                $grades_code = Grade::whereIn('id', $grade_ids)->whereNotNull('grade_code')->pluck("grade_code");
            }
            $activeSheet
                ->setCellValue("A$row", (string) $stt)
                ->setCellValue("B$row", (string) (!is_null($school->school_code) ? $school->school_code : ""))
                ->setCellValue("C$row", (string) (!is_null($school->name) ? $school->name : ""))
                ->setCellValue("D$row", (string) (!is_null($school->logo) ? $school->logo : ""))
                ->setCellValue("E$row", (string) (!is_null($school->address) ? $school->address : ""))
                ->setCellValue("F$row", (string) (!is_null($school->phone) ? $school->phone : ""))
                ->setCellValue("G$row", (string) (!is_null($school->description) ? $school->description : ""))
                ->setCellValue("H$row", (string) (!is_null($school->email) ? $school->email : ""))
                ->setCellValue("I$row", (string) (count($grades_code) != 0 ? str_replace(['[', ']', '"'], '', json_encode($grades_code)) : ""))
                ->setCellValue("J$row", (string) (!is_null($school->status) ? $school->status : ""));
            $stt++;
            $row++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(storage_path('app/' . $file_name));
        return response()->download(storage_path('app/' . $file_name), $file_name, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $file_name . '"'
        ]);
    }

    public function exportClass(Request $request)
    {
        $user = auth()->user();
        if ($user->hasRole('school')) {
            $school_id = $user->school_id;
        }
        if (!$user->hasRole('school|Admin')) {
            $school_id = $user->school_id;
            $class_id = $user->class_id;
        }
        $name = $request->get('search');
        $classes = Classes::orderBy('id', 'DESC')->with('school', 'grade');
        if (!$user->hasRole('school|Admin')) {
            $class_id = $user->class_id;
            $classes = $classes->where('id',$class_id);    
        }
        $school_id = $request->get('school_id');
        $grade_id = $request->get('grade_id');
        if ($user->user_type == UserTypeEnum::STAFF && !is_null($user->school_id)) {
            $classes = $classes->where('school_id', $user->school_id);
        } else {
            if (!is_null($school_id)) {
                $classes = $classes->where('school_id', $school_id);
            }
        }
        if (!is_null($grade_id)) {
            $classes = $classes->where('grade_id', $grade_id);
        }
        if (!is_null($name)) {
            $classes = $classes->where('name', 'LIKE', '%' . $name . '%');
        }
        $file_name = 'Danh_Sách_Lớp.xlsx';
        $title_table = 'Lớp';
        $title_sheet = 'Lớp_SHEET';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet(); //create new sheet
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);
        $activeSheet->setTitle($title_sheet);
        $activeSheet->setCellValue('A1', $title_table);

        $activeSheet->mergeCells("A1:G1");
        $activeSheet->setCellValue('A4', 'STT')
            ->setCellValue('B4', 'MÃ LỚP')
            ->setCellValue('C4', 'TÊN LỚP')
            ->setCellValue('D4', 'TRẠNG THÁI')
            ->setCellValue('E4', 'KHỐI')
            ->setCellValue('F4', 'TÊN TRƯỜNG')
            ->setCellValue('G4', 'HÌNH ẢNH'); // set title
        $stt = 1;
        $row = 5;
        foreach ($classes->get() as $class) {
            $activeSheet
                ->setCellValue("A$row", (string) $stt)
                ->setCellValue("B$row", (string) (!is_null($class->class_code) ? $class->class_code : ""))
                ->setCellValue("C$row", (string) (!is_null($class->name) ? $class->name : ""))
                ->setCellValue("D$row", (string) (!is_null($class->status) ? $class->status : ""))
                ->setCellValue("E$row", (string) (!is_null($class->grade) ? (!is_null($class->grade->grade_code) ? $class->grade->grade_code : "") : ""))
                ->setCellValue("F$row", (string) (!is_null($class->school) ? (!is_null($class->school->name) ? $class->school->name : "") : ""))
                ->setCellValue("G$row", (string) (!is_null($class->thumbnail) ? $class->thumbnail : ""));
            $stt++;
            $row++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(storage_path('app/' . $file_name));
        return response()->download(storage_path('app/' . $file_name), $file_name, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $file_name . '"'
        ]);

    }

    public function exportCourseSchool(Request $request)
    {
        $user = auth()->user();
        $school_id = $request->get('school_id');
        $class_id = $request->get('class_id');
        if ($user->hasRole('school')) {
            $school_id = $user->school_id;
        }
        if (!$user->hasRole('school|Admin')) {
            $school_id = $user->school_id;
            $class_id = $user->class_id;
        }
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->createSheet(); //create new sheet
        $activeSheet = $objPHPExcel->setActiveSheetIndex(1);

        $file_name = "Danh_Sách_Chương_Trình_Học.xlsx";
        $title_table = "Chương Trình Học";
        $title_sheet = "Chương_Trình_Học";
        $activeSheet->mergeCells("A1:F1");
        $activeSheet->setCellValue('A4', 'STT')
            ->setCellValue('B4', 'TÊN KHOÁ HỌC')
            ->setCellValue('C4', 'DANH MỤC')
            ->setCellValue('D4', 'SỐ HỌC SINH ĐANG THAM GIA')
            ->setCellValue('E4', 'SỐ HỌC SINH ĐÃ THAM GIA')
            ->setCellValue('F4', 'TỔNG LƯỢT TRUY CẬP'); // set title
        $stt = 1;
        $row = 5;
        if (!is_null($school_id)) {
            $school = School::findOrFail($school_id);
            $file_name = "Danh_Sách_Chương_Trình_Học_" . $school->name . ".xlsx";
            $title_table = "Chương Trình Học " . $school->name;
            $title_sheet = "Chương_Trình_Học";


            if (!is_null($class_id)) {
                $class = Classes::findOrFail($class_id);
                $file_name = "Danh_Sách_Chương_Trình_Học_" . $class->name . $school->name . ".xlsx";
                $title_table = "Chương Trình Học " . $school->name;
                $title_class_table = 'Lớp ' . $class->name;
                $title_sheet = "Chương_Trình_Học";
                $activeSheet->mergeCells("A2:F2");
                $activeSheet->setCellValue('A2', $title_class_table);
            }

            if (!is_null($dateBegin) && !is_null($dateEnd)) {
                if ($dateBegin > $dateEnd) {
                    $dateBegin = null;
                    $dateEnd = null;
                }
            }
            $activeSheet->setTitle($title_sheet);
            $activeSheet->setCellValue('A1', $title_table);
            if (!is_null($school->course)) {
                foreach ($school->course as $course) {
                    $activeSheet
                        ->setCellValue("A$row", (string) $stt)
                        ->setCellValue("B$row", (string) (!is_null($course->name) ? $course->name : ""))
                        ->setCellValue("C$row", (string) (!is_null($course->category) ? (!is_null($course->category->name) ? $course->category->name : "") : ""));
                    if (is_null($class_id)) {
                        $activeSheet->setCellValue("D$row", (string) $course->getCountUserCount($school->id, $dateBegin, $dateEnd))
                            ->setCellValue("E$row", (string) $course->getCountUserJoin($school->id, $dateBegin, $dateEnd))
                            ->setCellValue("F$row", (string) $course->getCountUserVisit($school->id, $dateBegin, $dateEnd));
                    } else {
                        $activeSheet->setCellValue("D$row", (string) $course->getCountUserCountClass($class_id, $dateBegin, $dateEnd))
                            ->setCellValue("E$row", (string) $course->getCountUserJoinClass($class_id, $dateBegin, $dateEnd))
                            ->setCellValue("F$row", (string) $course->getCountUserVisitClass($class_id, $dateBegin, $dateEnd));
                    }
                    $stt++;
                    $row++;
                }
            }
        } else {
            $listCourses = Course::orderBy('name');
            $schoolId = $user->school_id;
            $listCourses = $listCourses->whereHas('authorUser', function ($q) use ($schoolId) {
                $q->where('school_id', $schoolId)->orWhere('admin_flg', 1);
            })->get();
            if (!$user->hasRole('Admin')) {
                $listCourses = School::where('id', $user->school_id)->get();
            }
            foreach ($listCourses as $course) {
                $activeSheet
                    ->setCellValue("A$row", (string) $stt)
                    ->setCellValue("B$row", (string) (!is_null($course->name) ? $course->name : ""))
                    ->setCellValue("C$row", (string) (!is_null($course->category) ? (!is_null($course->category->name) ? $course->category->name : "") : ""))
                    ->setCellValue("D$row", (string) $course->getCountUserCountAll($dateBegin, $dateEnd))
                    ->setCellValue("E$row", (string) $course->getCountUserJoinAll($dateBegin, $dateEnd))
                    ->setCellValue("F$row", (string) $course->getCountUserVisitAll($dateBegin, $dateEnd));
                $stt++;
                $row++;
            }
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(storage_path('app/' . $file_name));
        return response()->download(storage_path('app/' . $file_name), $file_name, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $file_name . '"'
        ]);
    }
}
