<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Http\Requests\ServiceRequest;
class ServiceController extends Controller
{
    public function edit()
    {
        $service = Service::first();
        if(is_null($service)){
            $data = [ 
                'link'=> 'http://svc.netplus.vn/WSSendSMS.asmx',
                'user' =>'apineplus',
                'pass' => '123456',
                'key' =>'23424',
            ];
            $service = Service::create($data);
        }
        return view('admins.service.edit', compact('service'));
    }
    public function update(ServiceRequest $request, $id)
    {
        $service = Service::find($id);
        $data = $request->getData();
        try {
            $service->update($data);
            return redirect()->route('service.update')->with('message', 'Đã cập nhập web service thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã cập nhập web service thành công.');
        }
    }
}
