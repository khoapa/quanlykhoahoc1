<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\School;
use App\Models\Classes;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\Enums\UserTypeEnum;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Grade;
use App\Models\Role;
use App\Models\Conversation;
use App\Models\UserCourse;
use App\Models\Course;
use App\Models\UserMap;
use App\Models\CourseTheory;
use App\Models\SchoolCourse;
use App\Models\CourseFavorite;
use App\Models\UserCourseTheory;
use App\Models\UserCategoryMap;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $name = $request->get('search');
        $user_type = $request->get('user_type');
        $schoolID = $request->get('school_id');
        if (!$user->hasRole('Admin')) {
            $schoolID = $user->school_id;
        }
        $schools = School::orderBy('name')->get();
        $user = auth()->user();
        if($user->hasRole('school')){
            $schoolID = $user->school_id;
            $schools = School::where('id', $user->school_id)->get();
        }
        $classID = $request->get('class_id');

        $query = User::whereIn('user_type', [UserTypeEnum::TEACHER, UserTypeEnum::STUDENT])->orderBy('id', 'DESC');
        $classes = null;
        if ($user->hasRole('teacher')) {
            $classID = $user->class_id;
            $query = $query->where('user_type', 1);
        }
        if ($user->user_type == UserTypeEnum::STAFF) {
            $query = $query->where('school_id', $user->school_id);
        }
        if (!is_null($schoolID)) {
            $query = $query->where('school_id', $schoolID);
            $classes = Classes::where('school_id', $schoolID)->orderBy('name')->get();
        }
        if (!is_null($classID)) {
            $query = $query->where('class_id', $classID);
        }
        if (!is_null($user_type)) {
            $query = $query->where('user_type', $user_type);
        }
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $statusAllUser = 1;  
        foreach($query->get() as $user){
            if($user->status==2){
                $statusAllUser = 2;
            }
        }
        $users = $query->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.users.index', compact('users', 'schools', 'classes','statusAllUser'));
    }
    public function create()
    {
        $user = auth()->user();
        $schools = School::orderBy('name');
        $classes = Classes::orderBy('name');

        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id',$user->school_id);
            $classes = $classes->where('school_id',$user->school_id);
        }
        $schools = $schools->get();
        $classes = $classes->get();
        if($user->hasRole('Admin')){
            $classes = [];
        }
        $roles = Role::where('name','!=','Admin');
        if(!$user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school');
        }
        $roles = $roles->get();
        return view('admins.users.create', compact('schools', 'classes', 'roles'));
    }
    public function store(UserCreateRequest $request)
    {

        $data = $request->getData();
        $data['school_code'] = School::findOrFail($data['school_id'])->school_code;
        $data['class_code'] = Classes::findOrFail($data['class_id'])->class_code;
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['password'] = Hash::make(config('settings.password'));
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_image  = $data['avatar'];
        if(is_null($data_image)){
            $data_image = config('settings.image');
        }
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('user', $file);
        }
        $data['avatar'] = $data_image;
         try {
            $userCreate = User::create($data);
            $roleId = $data['role_id'];
            if (!is_null($roleId)) {
                $userCreate->syncRoles([$roleId]);
            }
            if($userCreate->user_type == UserTypeEnum::TEACHER && $userCreate->class_id != null){
                $dataClass = [
                    'user_id' => $userCreate->id,
                ];
                $class = Classes::findOrFail($userCreate->class_id);
                if(!is_null($class->user_id)){
                    $userTeacher = User::where('id',$class->user_id)->first();
                    if(!is_null($userTeacher)){
                        $userTeacher->update(['class_id'=>NULL]);
                    }
                }
                $class->update($dataClass);
            }
            return redirect()->route('user.index')->with('message', 'Thêm mới user thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Thêm mới user không thành công');
        }
    }
    public function edit(Request $request,$id)
    {
        $backUrl = $request->url;
        $user = User::findOrFail($id);
        $schools = School::orderBy('name');
        $classes = Classes::orderBy('name');
  
        $user_login = auth()->user();
        if (!$user_login->hasRole('Admin')) {
            $schools = $schools->where('id',$user_login->school_id);
            $classes = $classes->where('school_id',$user_login->school_id);
        }
        if($user_login->hasRole('Admin')){
            $classes =$classes->where('school_id',$user->school_id);
        }
        $schools = $schools->get();
        $classes = $classes->get();
        $roles = Role::where('name','!=','Admin');
        if(!$user_login->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$user->school_id);
        }
        if($user_login->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school');
        }
        $roles = $roles->get();
        return view('admins.users.update', compact('user', 'schools', 'classes', 'roles','backUrl'));
    }
    public function update(UserUpdateRequest $request, $id)
    {

        $user = User::findOrFail($id);
        $class_id = $user->class_id;
        $data = $request->getData();
        $data['school_code'] = School::findOrFail($data['school_id'])->school_code;
        $data['class_code'] = Classes::findOrFail($data['class_id'])->class_code;
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_logo = $user->avatar;
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_logo = UploadService::uploadImage('avatar', $file);
        }
        $data['avatar'] = $data_logo;
        $roleId = $data['role_id'];
        $roleOld = $user->role()->first();
        if (!is_null($roleId)) {
            $user->syncRoles([$roleId]);
        } else {
            if ($roleOld) {
                $user->removeRole($roleOld->role_id);
            }
        }
        try {
            $user->update($data);
            if($user->user_type == UserTypeEnum::TEACHER && $user->class_id != $class_id){
                $dataClass = [
                    'user_id' => $user->id,
                ];
                $class = Classes::findOrFail($user->class_id);
                if(!is_null($class->user_id)){
                    $userTeacher = User::where('id',$class->user_id)->first();
                    if(!is_null($userTeacher)){
                        $userTeacher->update(['class_id'=>NULL]);
                    }
                }
                $class->update($dataClass);
            }
            return redirect(session('back_url'))->with('message', 'Đã cập nhật user thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        try {
            $userId = $id;
            Conversation::where('user1_id', $userId)->orWhere('user2_id', $userId)->delete();
            UserCourse::where('user_id',$userId)->delete();
            UserMap::where('user_id',$userId)->delete();
            $courses = Course::where('author',$userId)->get();
            $coursesTheory = CourseTheory::where('author',$userId)->get();
            foreach($courses as $course){
                $idCourse = $course->id;
                $schoolCourse = SchoolCourse::where('course_id',$idCourse)->delete();
                $userCourse = UserCourse::where('course_id',$idCourse)->delete();
                $courseFavouris = CourseFavorite::where('course_id',$idCourse)->delete();
            }  
            foreach($coursesTheory as $theory){
                $idTheory = $theory->id;
                $userTheory = UserCourseTheory::where('course_theory_id',$idTheory)->delete();
            }  
           Course::where('author',$userId)->delete();
           CourseTheory::where('author',$userId)->delete();
           UserCategoryMap::where('category_map_id',$userId)->delete();
           $classes = Classes::where('user_id',$id)->get();
           foreach($classes as $class){
               $classUpdate = Classes::where('user_id',$class->id)->update(['user_id'=>NULL]);
           }
           $user->delete();
          return redirect(session('back_url'))->with('message', 'Xóa user thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
    public function resetPassword($id)
    {
        $user = User::findOrFail($id);
        $user->password = bcrypt('Aa12345678');
        $user->save();
        return back()->with('message', 'Reset mật khẩu thành công (Aa12345678)');
    }
    public function enableUser(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $school_id = $request->school_id;
            $class_id = $request->class_id;
            $user_type = $request->user_type;
            $type = $request->type;

            $user = User::whereIn('user_type',[UserTypeEnum::TEACHER, UserTypeEnum::STUDENT]);
            if(!is_null($search_name)){
                $user = $user->where('name', 'LIKE', '%' . $search_name . '%');
            }
            if(!is_null($school_id)){
                $user = $user->where('school_id', $school_id);
            }
            if(!is_null($class_id)){
                $user = $user->where('class_id', $class_id);
            }
            if(!is_null($user_type)){
                $user = $user->where('user_type', $user_type);
            }
            $user->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}
