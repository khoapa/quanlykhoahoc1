<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\School;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Services\UploadService;
use App\Helpers\Helper;
use App\Http\Requests\StudentUpdateRequest;
use App\Http\Requests\TeacherUpdateRequest;
use App\Enums\UserTypeEnum;
use App\Http\Requests\TeacherCreateRequest;
use App\Models\Role;
use App\Models\Conversation;
use Illuminate\Support\Facades\Auth;
use App\Models\UserCourse;
use App\Models\UserMap;
use App\Models\Course;
use App\Models\CourseTheory;
use App\Models\SchoolCourse;
use App\Models\CourseFavorite;
use App\Models\UserCourseTheory;
use App\Models\UserCategoryMap;

class TeacherController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->get('search');
        $schoolID = $request->get('school_id');
        $schools = School::orderBy('name')->get();
        $user = auth()->user();
        if($user->hasRole('school')){
            $schoolID = $user->school_id;
            $schools = School::where('id',$user->school_id)->get();
        }
        $classID = $request->get('class_id');
        $query=User::where('user_type',UserTypeEnum::TEACHER)->orderBy('id', 'DESC');
        $classes = null;
        $user = auth()->user();
        if ($user->user_type == UserTypeEnum::STAFF){
            $query = $query->where('school_id',$user->school_id);
        }
        if (!is_null($schoolID)) {
            $query = $query->where('school_id',$schoolID);
            $classes = Classes::where('school_id',$schoolID)->orderBy('name')->get();   
        }
        if (!is_null($classID)) {
            $query = $query->where('class_id',$classID);
        }
        if (!is_null($name)) {
            $query = $query->where('name', 'LIKE', '%' . $name . '%');
        }
        $statusAllTeacher = 1;  
        foreach($query->get() as $teacher){
            if($teacher->status==2){
                $statusAllTeacher = 2;
            }
        }
        $teacher = $query->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.teacher.index', compact('teacher','schools','classes','statusAllTeacher'));
    }
    public function create()
    {
        $user = auth()->user();
        $schools = School::orderBy('name');
        $classes = Classes::orderBy('name');

        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id',$user->school_id);
            $classes = $classes->where('school_id',$user->school_id);
        }
        $schools = $schools->get();
        $classes = $classes->get();
        if($user->hasRole('Admin')){
            $classes =[];
        }
        $roles = Role::where('name','!=','Admin');
        if(!$user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school');
        }
        $roles = $roles->get();
        return view('admins.teacher.create', compact('schools', 'classes','roles'));
    }
    public function store(TeacherCreateRequest $request)
    {
        $data = $request->getData();
        $data['school_code'] = School::findOrFail($data['school_id'])->school_code;
        $data['class_code'] = Classes::findOrFail($data['class_id'])->class_code;
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['password'] = Hash::make(config('settings.password'));
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_image  = $data['avatar'];
        if(is_null($data_image)){
            $data_image = config('settings.image');
        }
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_image = UploadService::uploadImage('teacher', $file);
        }
        $data['avatar'] = $data_image;
        try {
            $userCreate = User::create($data);
            $roleId = $data['role_id'];
            if(!is_null($roleId)){
                $userCreate->syncRoles([$roleId]);
            }
            if($userCreate->user_type == UserTypeEnum::TEACHER && $userCreate->class_id != null){
                $dataClass = [
                    'user_id' => $userCreate->id,
                ];
                $class = Classes::findOrFail($userCreate->class_id);
                if(!is_null($class->user_id)){
                    $userTeacher = User::where('id',$class->user_id)->first();
                    if(!is_null($userTeacher)){
                        $userTeacher->update(['class_id'=>NULL]);
                    }
                }
                $class->update($dataClass);
            }
            return redirect()->route('teacher.index')->with('message',  'Thêm mới giáo viên thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Thêm mới giáo viên không thành công');
        }
    }
    public function edit(Request $request,$id)
    {
        $backUrl = $request->url;
        $teacher = User::findOrFail($id);
        $schools = School::orderBy('name');
        $classes = Classes::orderBy('name');
        $user = auth()->user();
        $schools = School::orderBy('name');
        $classes = Classes::orderBy('name');

        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id',$user->school_id);
            $classes = $classes->where('school_id',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $classes =$classes->where('school_id',$teacher->school_id);
        }
        $schools = $schools->get();
        $classes = $classes->get();
        $roles = Role::where('name','!=','Admin');
        if(!$user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school')->orWhere('id_school',$user->school_id);
        }
        if($user->hasRole('Admin')){
            $roles->where('name','!=','school')->whereNull('id_school');
        }
        $roles = $roles->get();
        return view('admins.teacher.update', compact('teacher', 'schools', 'classes','roles','backUrl'));
    }
    public function update(TeacherUpdateRequest $request, $id)
    {

        $teacher = User::findOrFail($id);
        $class_id = $teacher->class_id;
        $data = $request->getData();
        $data['school_code'] = School::findOrFail($data['school_id'])->school_code;
        $data['class_code'] = Classes::findOrFail($data['class_id'])->class_code;
        $data['birthday'] = Helper::formatSqlDate($data['birthday']);
        $data['email'] = str_replace(' ', '', $data['email']);
        $data_logo = $teacher->avatar;
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $data_logo = UploadService::uploadImage('avatar', $file);
        }
        $data['avatar'] = $data_logo;
        $roleId = $data['role_id'];
        $roleOld = $teacher->role()->first();
        if(!is_null($roleId)){
            $teacher->syncRoles([$roleId]);
        }else{
            if($roleOld){
                $teacher->removeRole($roleOld->role_id);   
            }
        }
        try {
            $teacher->update($data);
            if($teacher->user_type == UserTypeEnum::TEACHER && $teacher->class_id != $class_id){
                $dataClass = [
                    'user_id' => $teacher->id,
                ];
                $class = Classes::findOrFail($teacher->class_id);
                if(!is_null($class->user_id)){
                    $userTeacher = User::where('id',$class->user_id)->first();
                    if(!is_null($userTeacher)){
                        $userTeacher->update(['class_id'=>NULL]);
                    }
                }
                $class->update($dataClass);
            }
            return redirect(session('back_url'))->with('message', 'Đã cập nhật giáo viên thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');
        }
    }
    public function destroy($id)
    {
        $teacher = User::findOrFail($id);
        try {
            Conversation::where('user1_id', $id)->orWhere('user2_id', $id)->delete();
            UserCourse::where('user_id',$id)->delete();
            UserMap::where('user_id',$id)->delete();
            $courses = Course::where('author',$id)->get();
            $coursesTheory = CourseTheory::where('author',$id)->get();
            foreach($courses as $course){
                $idCourse = $course->id;
                $schoolCourse = SchoolCourse::where('course_id',$idCourse)->delete();
                $userCourse = UserCourse::where('course_id',$idCourse)->delete();
                $courseFavouris = CourseFavorite::where('course_id',$idCourse)->delete();
            }  
            foreach($coursesTheory as $theory){
                $idTheory = $theory->id;
                $userTheory = UserCourseTheory::where('course_theory_id',$idTheory)->delete();
            }  
           Course::where('author',$id)->delete();
           CourseTheory::where('author',$id)->delete();
           UserCategoryMap::where('user_id',$id)->delete();
           $teacher->delete();
            $classes = Classes::where('user_id',$id)->get();
            foreach($classes as $class){
                $classUpdate = Classes::where('user_id',$class->id)->update(['user_id'=>NULL]);
            }
            return redirect(session('back_url'))->with('message', 'Đã xóa giáo viên thành công.');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại');
        }
    }
    public function listTeacher($id)
    {
        $class = User::join('model_has_roles', function($join) {
            $join->on('users.id', '=', 'model_has_roles.model_id');
          })
          ->where('school_id', $id)->where('user_type', UserTypeEnum::TEACHER)
          ->where('model_has_roles.role_id',3)->get();
        return response()->json($class);
    }
    public function enableTeacher(Request $request)
    {
        if ($request->ajax()) {
            $search_name = $request->search_name;
            $school_id = $request->school_id;
            $class_id = $request->class_id;
            $type = $request->type;

            $user = User::where('user_type',UserTypeEnum::TEACHER);
            if(!is_null($search_name)){
                $user = $user->where('name', 'LIKE', '%' . $search_name . '%');
            }
            if(!is_null($school_id)){
                $user = $user->where('school_id', $school_id);
            }
            if(!is_null($class_id)){
                $user = $user->where('class_id', $class_id);
            }
            $user->update(['status'=>$type]);
            $message = 'Chuyển trạng thái thành công';
            return response()->json($message);
        }
    }
}
