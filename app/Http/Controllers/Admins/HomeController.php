<?php

namespace App\Http\Controllers\Admins;

use App\Enums\UserTypeEnum;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\News;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $user = auth()->user();
        $students = User::where("user_type", '=', UserTypeEnum::STUDENT);
        $news = News::count();
        $courses = Course::orderBy('id','DESC');
        $listCourses = Course::orderBy('id','DESC')->where('new',1);
        $users = User::whereIn('user_type', [UserTypeEnum::TEACHER, UserTypeEnum::STUDENT])->orderBy('id', 'DESC');
        if ($user->admin_flg != 1) {
            $courses = $courses->where('author', $user->id)->orWhereHas('authorUser',function($q){
                $q->where('admin_flg',1);
            });
            $listCourses = $listCourses->where('author', $user->id)->orWhereHas('authorUser',function($q){
                $q->where('admin_flg',1);
            });
        }
        if($user->user_type == UserTypeEnum::STAFF){
            $students = $students->where('school_id',$user->school_id);
            $users = $users->where('school_id',$user->school_id);
        }
        if($user->user_type == UserTypeEnum::TEACHER){  
            $students = $students->where('class_id',$user->class_id);
            $users = $users->where('school_id',$user->school_id);
        }
        $students = $students->count();
        $courses = $courses->count();
        $users = $users->limit(10)->get();
       // dd($users);
        $listNews = News::orderBy('id', 'DESC')->paginate(config('settings.countPaginateHome'));
       $listCourses = $listCourses->paginate(config('settings.countPaginateHome'));
        return view('admins.dashboard.index',compact('courses','students','news','users','listNews','listCourses'));
    }
}
