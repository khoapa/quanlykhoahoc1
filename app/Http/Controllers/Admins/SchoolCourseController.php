<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Course;
use App\Models\SchoolCourse;
use App\Http\Requests\SchoolCourseRequest;
use App\Models\ClassCourse;
use App\Models\Classes;
use App\Models\UserCourse;
use App\Helpers\Helper;

class SchoolCourseController extends Controller
{
    public function index(Request $request)
    {
        $backUrl = $request->url;
        $school_id = null;
        $school_id = $request->school_id;
        $schools = null;
        $classes = null;
        $listClasses = null;
        $class_id = null;
        $userCourses = null;
        $user = auth()->user();
        if($user->hasRole('school')){
            $school_id = $user->school_id;
        }
        if(is_null($request->school)){
            session()->forget('schoolId');
        }
        if (!is_null($request->school_id) && is_null($request->class_id)) {
            $school_id = $request->school_id;
            session()->forget('classId');
            $listClasses = Classes::where('school_id', '=', $school_id)->get();
            $userCourses = UserCourse::whereHas('student', function ($query) use ($school_id) {
                $query = $query->where('school_id', $school_id);
            })->get();
            $schools = School::findOrFail($school_id);
        }
        if (!is_null($school_id)) {
            Session()->put('schoolId', $school_id);
        }
        if (Session()->has('schoolId') && is_null($request->school_id) && is_null($request->class_id)) {
            $school_id = Session('schoolId');
            $listClasses = Classes::where('school_id', '=', $school_id)->get();
            $userCourses = UserCourse::whereHas('student', function ($query) use ($school_id) {
                $query = $query->where('school_id', $school_id);
            })->get();
            $schools = School::findOrFail($school_id);
        }

        $listSchools = School::orderBy('name')->get();
        
        if($user->hasRole('school')){
            $listSchools = School::where('id',$user->school_id)->get();
        
        }
        if(!$user->hasAnyRole('school','Admin')){
            $class_id = $user->class_id;
        }
        if (!is_null($request->class_id)) {
            $class_id = $request->class_id;
            $school_id = $request->school_id;
            $listClasses = Classes::where('school_id', '=', $school_id)->get();

            $userCourses = UserCourse::whereHas('student', function ($query) use ($class_id,$school_id) {
                $query = $query->where('school_id',$school_id)->where('class_id', $class_id);
            })->get();
            $classes = Classes::findOrFail($class_id);
        }
        if (!is_null($school_id) && !is_null($class_id)) {
            Session()->put('schoolId', $school_id);
            Session()->put('classId', $class_id);
        }

        if (Session()->has('schoolId') && Session()->has('classId') && is_null($request->school_id) && is_null($request->class_id)) {
            $class_id = Session('classId');
            $school_id = Session('schoolId');
            $listClasses = Classes::where('school_id', '=', $school_id)->get();

            $userCourses = UserCourse::whereHas('student', function ($query) use ($class_id,$school_id) {
                $query = $query->where('school_id',$school_id)->where('class_id', $class_id);
            })->get();
            $classes = Classes::findOrFail($class_id);
        }

        if(!is_null($class_id)){
            $classes = Classes::findOrFail($class_id);
            $schools = School::findOrFail($classes->school_id);
            $listClasses = Classes::where('id',$class_id)->get();
        }
        if(!is_null($school_id)){
            $schools = School::findOrFail($school_id);
        }
        $dateBegin = null;
        $dateEnd = null;
        $check = null;
        $check = $request->get('check');
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);
        $errorDate = null;
        if(!is_null($dateBegin) && !is_null($dateEnd)){
            if($dateBegin > $dateEnd){
                $dateBegin = null;
                $dateEnd = null;
                $errorDate = "Ngày kết thúc phải lớn hơn Ngày bắt đầu";
            }
        }
        $listCourses = null;
        if(is_null($schools) && is_null($classes)){
            $listCourses = Course::orderBy('name');
            $schoolId = $user->school_id;
            $listCourses = $listCourses->whereHas('authorUser',function($q) use($schoolId){
                $q->where('school_id',$schoolId)->orWhere('admin_flg',1);
            })->paginate(10)->appends(request()->query());;
        }
        if(is_null($schools) && is_null($classes)){
            $listCourses = Course::orderBy('name');
            $schoolId = $user->school_id;
            $listCourses = $listCourses->whereHas('authorUser',function($q) use($schoolId){
                if(!is_null($schoolId)){
                    $q->where('school_id',$schoolId);
                }
                $q->orWhere('admin_flg',1);
            })->paginate(10)->appends(request()->query());
        }
        if(!$user->hasRole('Admin') || !is_null($schools)){
            $listCourses = $schools->course()->orderBy('id')->paginate(10)->appends(request()->query());
        }
        return view('admins.school_course.index', compact('check','schools', 'listSchools', 'school_id', 'classes', 'listClasses', 'class_id', 'userCourses','dateBegin','dateEnd','errorDate','listCourses','backUrl'));
    }
    public function create($id,Request $request)
    {
        $search = $request->get('search');
        $schools = School::with("course")->where('id', $id)->get();
        $Courses_id  = [];
        $school =  School::find($id);
        $userSchool = $school->user_id;
        foreach ($schools as $valueSchool) {
            foreach ($valueSchool->course as $value) {
                $Courses_id[] = $value->pivot->course_id;
            }
        }
        $courses = Course::whereNotIn('id', $Courses_id);
        if(!is_null($search)){
            $courses = $courses->where('name','LIKE','%'.$search.'%');
        }
        $courses = $courses->whereHas('authorUser',function($q) use($userSchool){
            $q->where('admin_flg',1)->orWhere('id',$userSchool);
        })->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view('admins.school_course.create', compact('school', 'courses'));
    }
    public function store(SchoolCourseRequest $request, $id)
    {
        $course_id = $request->course_id;
        $data['school_id'] = $id;
        $school =  School::findOrFail($id);
        try {
            foreach ($course_id as $valueCourse) {
                $data['course_id'] = $valueCourse;
                SchoolCourse::create($data);
                $dataClass['course_id'] = $valueCourse;
                // foreach ($school->classes as $class) {
                //     $dataClass['classes_id'] = $class->id;
                //     ClassCourse::create($dataClass);
                // }
            }
            return redirect()->route('school_course.index')->with('school_id',$id)->with('message',  'Thêm mới khóa học trường ' . '"' . $school->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi trong quá trình thực hiện vui long thử lại');
        }
    }
    public function destroy($school_id,$id)
    {

        $schoolCourses = SchoolCourse::where('school_id',$school_id)->where('course_id',$id)->first();
        $course_id = $schoolCourses->course_id;
        $schools =  School::find($schoolCourses->school_id);
        try {
            $schoolCourses->delete();
            foreach ($schools->classes as $value) {
                ClassCourse::where([
                    ['classes_id', $value->id],
                    ['course_id', $course_id],
                ])->delete();
            }
            return redirect()->route('school_course.index')->with('school_id',$school_id)->with('message', 'Đã xóa Khóa Học trường  ' . '"' . $schools->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
    public function createClass($id)
    {
        $classCourse_id = [];
        $schoolCourse_id = [];
        $classes = Classes::find($id);
        $classCourse = ClassCourse::where('classes_id', $id)->get();
        foreach ($classCourse as $value) {
            $classCourse_id[] = $value->course_id;
        }
        $schools = School::with("course")->where('id', $classes->school_id)->get();
        foreach ($schools as $valueSchool) {
            foreach ($valueSchool->course as $valueCourse) {
                $schoolCourse_id[] = $valueCourse->id;
            }
        }
        $course = array_diff($schoolCourse_id, $classCourse_id);
        $userSchool = $classes->school_id;
        $courses = Course::whereIn('id', $course)->whereHas('authorUser',function($q) use($userSchool){
            $q->where('admin_flg',1)->orWhere('id',$userSchool);
        })->get();
        return view('admins.class_course.create', compact('classes', 'courses'));
    }
    public function storeClass(SchoolCourseRequest $request, $id)
    {
        $data['classes_id'] = $id;
        $class = Classes::find($id);
        $course_id = $request->course_id;
        try {
            foreach ($course_id as $value) {
                $data['course_id'] = $value;
                ClassCourse::create($data);
            }
            return redirect()->route('school_course.index')->with('message',  'Thêm mới khóa học lớp ' . '"' . $class->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi trong quá trình thực hiện vui long thử lại');
        }
    }
    public function destroyClass($id)
    {
        $classCourse = ClassCourse::find($id);
        $class = Classes::find($classCourse->classes_id);
        try {
            $classCourse->delete();
            return redirect()->route('school_course.index')->with('message', 'Đã xóa khóa học lớp ' . '"' . $class->name . '"' . ' thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.');
        }
    }
    // public function edit($id)
    // {
    //     $schoolCourses = SchoolCourse::find($id);
    //     $schools = School::with("course")->where('id', $id)->get();
    //     $Courses_id  = [];
    //     $school =  School::find($schoolCourses->school_id);
    //     foreach($schools as $valueSchool){
    //         foreach ($valueSchool->course as $value) {
    //             if($value->pivot->id == $id ){
    //                 continue;
    //             }
    //             $Courses_id[] = $value->pivot->course_id;
    //         }
    //     }
    //     $courses = Course::whereNotIn('id', $Courses_id)->get();
    //     return view('admins.school_course.update', compact('schoolCourses', 'courses','school'));
    // }
    // public function update(SchoolCourseRequest $request,$id)
    // {
    //     $schoolCourses = SchoolCourse::find($id);
    //     $data = $request->getData();
    //     $schools =  School::find($schoolCourses->school_id);
    //     $data['school_id'] = $schools->id;
    //     try {
    //         $schoolCourses->update($data);
    //         return redirect()->route('school_course.index')->with('message',  'Cập Nhập khóa học trường '."/ $schools->name /".' thành công');
    //     } catch (\Exception $e) {
    //         return redirect()->back()->with('message', 'Đã có lỗi trong quá trình thực hiện vui long thử lại');
    //     }
    // }
}
