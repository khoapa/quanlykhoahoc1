<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LRedis;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use function GuzzleHttp\json_decode;
use App\Models\Message;
use App\Models\Conversation;
use App\Http\Requests\UpdateConversationRequest;
use App\Http\Resources\MessageCollection;
use App\Http\Resources\ConversionCollection;
use App\Http\Resources\MessageResource;
use App\Http\Resources\ConversionResource;
use App\Services\UploadService;
use File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Enums\TypeNotiFyEnum;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Enums\UserTypeEnum;

class ChatController extends Controller
{
    use \App\Traits\NotificationTrait;
    public function index(Request $request,$id = null){
        $nameSearch = $request->get('search');
        $user = Auth::user();
        $users = User::where('id', '!=', $user->id);
        if ($user->admin_flg != 1) {            //if is't admin
            $users = $users->where('school_id', $user->school_id);
            $user_school_id = $user->school()->firstOrFail()->user_id;
            //dd($user_school_id);         //school
            if ($user->id == $user_school_id) {       //if is school
                $users = $users->whereIn('user_type', [UserTypeEnum::TEACHER, UserTypeEnum::STUDENT])->orWhere('admin_flg', 1);
            } else {
                if ($user->user_type == UserTypeEnum::STUDENT) { //if is student
                    $homeroom_teacher_id = $user->classes()->firstOrFail()->user_id;    //teacher
                    $users = $users->whereIn('user_type', [UserTypeEnum::STAFF])->orWhere('id', $homeroom_teacher_id)->orWhere('admin_flg', 1);
                }
                if ($user->user_type == UserTypeEnum::TEACHER) { //if is teacher
                    if (!is_null($user->class_id))
                    $users = $users->where('user_type', UserTypeEnum::STUDENT)->where('class_id', $user->class_id)->orWhere(function ($q) use ($user) {
                        $q->where('user_type', UserTypeEnum::STAFF)->where('school_id', $user->school_id);
                    })->orWhere('admin_flg', 1);
                    else
                        $users = $users->whereIn('user_type', [UserTypeEnum::STAFF])->orWhere('admin_flg', 1);
                }
            }
        }
        try {
            DB::beginTransaction();
            foreach ($users->get() as $userCreate) {
                $dataConversition = ['user1_id' => $user->id, 'user2_id' => $userCreate->id];
                $conversation = Conversation::whereIn('user1_id', [$user->id, $userCreate->id])->whereIn('user2_id', [$user->id, $userCreate->id])->first();
                if (!$conversation)
                    $conversation[] = Conversation::create($dataConversition);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
        $listConversionsFirst = Conversation::where('user1_id',$user->id)->where('user2_id','!=',$user->id);
        $listConversionsTwo = Conversation::where('user2_id',$user->id)->where('user1_id','!=',$user->id);
        if(!is_null($nameSearch)){
            $listConversionsFirst->whereHas('user2',function($q) use ($nameSearch){
                $q->where('name','LIKE','%'.$nameSearch.'%');
            });
            $listConversionsTwo->whereHas('user1',function($q) use ($nameSearch){
                $q->where('name','LIKE','%'.$nameSearch.'%');
            });
        }
        if(is_null($nameSearch)){
            $listConversionsFirst->whereHas('message');
            $listConversionsTwo->whereHas('message');
        }
        $listConversionsFirst = $listConversionsFirst->orderBy('updated_at', 'DESC')->get();
        $listConversionsTwo = $listConversionsTwo->orderBy('updated_at', 'DESC')->get();
        $messages = null;
        $user2Id = null;
        $conversonId = null;
        if(!is_null($id)){
            $converson = Conversation::findOrFail($id);
            if(is_null($converson)){
                $user2Id = $id;
            }else{
                $messages = $converson->message()->get();
                $updateMessage = Message::where('conversation_id',$id)->where('sender_id','!=',$user->id)->update(['status'=>2]);
                $user1Id = $converson->user1_id;
                $idUser2 = $converson->user2_id;
                $user2Id = ($user1Id == $user->id) ? $idUser2 : $user1Id; 
                $conversonId = $id;  
            }
    

        }
        $nameChat = "";
        if(!is_null($user2Id)){
            $nameChat  = User::findOrFail($user2Id)->name;
        }

        return view('admins.chat.index',compact('listConversionsFirst','listConversionsTwo','messages','conversonId','user2Id','nameChat'));
    }
    public function store(Request $request){
         $user = auth()->user();
         $text = $_POST['text'];
         $image = $_POST['image'];
         $conversonId = $_POST['conversion_id'];
         $user2Id = $_POST['user2_id'];
         $date = Carbon::now();
         $data = [
             'text' => $text,
             'user2_id' => $user2Id,
             'images' => $image,
             'created_at' => $date
         ];
         if(!is_null($conversonId)){
            $converson = Conversation::findOrFail($conversonId);
            $data['sender_id'] = $user->id;
            $data['conversation_id'] = $converson->id;
            $createMessage = Message::create($data);
        }else{
            $dataConversition['user1_id'] = $user->id;
            $dataConversition['user2_id'] = $data['user2_id'];
            $converson = Conversation::create($dataConversition);
            $data['sender_id'] = $user->id;
            $data['conversation_id'] = $converson->id;
            $createMessage = Message::create($data);
            $this->pushSocketListConversation($dataConversition['user1_id'], $converson);
            $this->pushSocketListConversation($dataConversition['user2_id'], $converson);
        }
        $converson->update(["updated_at" => Carbon::now()]);
        $title_notification = '' . $user->name . ' đã gửi tin nhắn cho bạn.';
        $type = TypeNotiFyEnum::CONVERSATION;
        $id_type = $converson->id;
        $content = "";
        $receiver = User::findOrFail($user2Id);
        $this->notifcationSend($title_notification, $content, $user, $receiver,$type,$id_type);
        $this->pushSocket($converson->id, $createMessage);
        echo json_encode($data);
    }
    protected function pushSocket($id, $message) 
    {
        $redis = LRedis::connection();
        $redis->publish('conversation-'.$id, json_encode(new MessageResource($message)));
    }

    protected function pushSocketListConversation($id, $conversation) {
        $redis = LRedis::connection();
        $redis->publish('list-conversation-'.$id, json_encode(new ConversionResource($conversation)));
    }
    public function upload(Request $request){
        $file = $request->file('file');
        $imageUrl = UploadService::uploadImage('code',$file);
         echo $imageUrl;
    }
    public function deleteConversion($id){
        $converson = Conversation::findOrFail($id);
        $converson->delete();
        return redirect()->route('chat.chat.index')->with('message','Xóa hội thoại thành công.');
    }
    public function deleteMessage($id){
        $message = Message::findOrFail($id);
        $countMessage = Message::where('conversation_id',$message->conversation_id)->get()->count();
        if($countMessage == 0){
            $converson = Conversation::findOrFail($message->conversation_id);
            $converson->delete();
        }
        $message->delete();
        echo json_encode(array('success'=>true, 'message' => 'Xóa tin nhắn thành công.'));
    }
}