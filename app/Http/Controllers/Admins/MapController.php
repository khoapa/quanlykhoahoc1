<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\MapCreateRequest;
use App\Http\Requests\MapUpdateRequest;
use App\Models\Map;
use App\Models\MapCategory;
use Illuminate\Http\Request;
use App\Services\UploadService;

class MapController extends Controller
{
    public function index(Request $request)
    {
        $title = $request->get('search');
        $categoryId = $request->get('category_id');
        $query = Map::orderBy('id','DESC');
        if(!is_null($title)){
            $query = $query->where('title','LIKE','%'.$title.'%');
        }
        if(!is_null($categoryId)){
            $query = $query->where('map_category_id',$categoryId);
        }
        $categories = MapCategory::orderBy('name')->get();
        $maps = $query->paginate(config('settings.countPaginate'))->appends(request()->query());
        return view ('admins.maps.index',compact('maps','categories'));
    }
    public function create()
    {
        $categories=MapCategory::orderBy('name')->get();
        return view('admins.maps.create',compact('categories'));
    }
    public function store(MapCreateRequest $request)
    {       
        $data = $request->getData();
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $data_image = UploadService::uploadImage('map', $file);
            $data['icon'] = $data_image;
            
        }
       
        
        $insertData = Map::create($data);
	    if ($insertData) {
            return redirect()->route('map_index')->with('message','Tạo điểm map thành công');
	    }else {                        
            return redirect()->back()->with('message','Tạo điểm map thất bại');
	    }
    }
    public function delete($id)
    {
        $map = Map::findOrFail($id);
        $map->delete();
        return redirect(session('back_url'))->with('message','Xóa điểm map thành công');
    }
    public function edit(Request $request,$id)
    {
        $backUrl = $request->url;
        $map = Map::find($id);
        $categories=MapCategory::orderBy('name')->get();
        return view ('admins.maps.edit',compact('map','categories','backUrl'));      
    }
    public function update(MapUpdateRequest $request, $id)
    {   
        $map=Map::find($id);
        $data = $request->getData(); 
        $data_image = $map->icon;
        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $data_image = UploadService::uploadImage('map', $file);
        }
        $data['icon'] = $data_image;
        $update = $map->update($data);
        if ($update) {
            return redirect()->route('map_index')->with('message','Đã cập nhật điểm map thành công.');
        } else{
            return redirect()->route('map_index')->with('message','Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.');     
        }
    }
}
