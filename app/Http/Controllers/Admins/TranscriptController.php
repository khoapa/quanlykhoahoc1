<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarkRequest;
use App\Http\Requests\TranscriptRequest;
use App\Http\Resources\CategoryCourseResource;
use App\Http\Resources\UserResource;
use App\Models\Classes;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\Question;
use App\Models\School;
use App\Models\UserCourse;
use App\Models\UserCourseResult;
use App\User;
use Illuminate\Http\Request;
use App\Helpers\Helper;

class TranscriptController extends Controller
{
    public function index()
    {
        $errorDate = null;
        $schools = School::orderBy('name');
        $user = auth()->user();
        $classes = Classes::orderBy('name');        
        $categoryCourses = CourseCategory::orderBy('name')->get();
        $usersCourse = UserCourse::whereIn('status', [1, 2]);
        if (!$user->hasRole('Admin')) {
            $schools = $schools->where('id', $user->school_id);
            $classes = $classes->where('school_id', $user->school_id);
            if($user->hasRole('teacher')){
                $classes = $classes->where('id',$user->class_id);
            }

            $school_id = $user->school_id;
            $userCourse = $usersCourse->whereHas('student',function($q)use($school_id){
                $q->where('school_id',$school_id);
            });
    
            if($user->hasRole('teacher')){
                $class_id = $user->class_id;
                $userCourse = $usersCourse->whereHas('student',function($q)use($class_id){
                    $q->where('class_id',$class_id);
                });
            }
        }
        $classes = $classes->get();  
        $schools = $schools->get();
        $usersCourse = $usersCourse->orderBy('course_id')->paginate(config('settings.countPaginate'))->appends(request()->query());

        return view('admins.transcripts.index', compact('schools', 'categoryCourses', 'usersCourse','errorDate','classes'));
    }

    public function searchUser(Request $request)
    {
        $backUrl = $request->url;
        $schools = School::orderBy('name')->get();
        $user = auth()->user();
        $categoryCourses = CourseCategory::orderBy('name')->get();
        $school_id = $request->school;
        $class_id = $request->class;
        $course_id = $request->course;
        $category_id = $request->category;
        if ($user->hasRole('school')) {
            $school_id = $user->school_id;
        };
        if (!$user->hasanyrole('school','Admin')) {
            $school_id = $user->school_id;
            $class_id = $user->class_id;
        };
        if(is_null($school_id) && !is_null($class_id)){
            $class = Classes::findOrFail($class_id);
            $school_id = $class->school->id;
        }
        if(!is_null($course_id)){
            $course = Course::findOrFail($course_id);
            $category_id = $course->category->id;
        }
        $usersCourse = UserCourse::whereIn('status', [1, 2])->orderBy('id','DESC');
        if (!is_null($school_id)) {
            $usersCourse = $usersCourse->whereHas('student', function ($query) use ($school_id) {
                $query = $query->where('school_id', $school_id);
            });
        }
        if (!is_null($category_id)) {
            $usersCourse = $usersCourse->whereHas('course', function ($query) use ($category_id) {
                $query = $query->where('course_category_id', $category_id);
            });
        }
        if (!is_null($class_id)) {
            $usersCourse = $usersCourse->whereHas('student', function ($query) use ($class_id) {
                $query = $query->where('class_id', $class_id);
            });
        }
        if (!is_null($course_id)) {
            $usersCourse = $usersCourse->whereHas('course', function ($query) use ($course_id) {
                $query = $query->where('course_id', $course_id);
            });
        }
        $dateBegin = null;
        $dateEnd = null;
        $dateBegin = $request->get('date_begin');
        $dateEnd = $request->get('date_end');
        $dateBegin = Helper::formatSqlDate($dateBegin);
        $dateEnd = Helper::formatSqlDate($dateEnd);
        $errorDate = null;
        if(!is_null($dateBegin) && !is_null($dateEnd)){
            if($dateBegin > $dateEnd){
                $dateBegin = null;
                $dateEnd = null;
                $errorDate = "Ngày kết thúc phải lớn hơn Ngày bắt đầu";
            }
        }
        if(!is_null($dateBegin)){
            $usersCourse->whereDate('join_date','>=',$dateBegin);
            }
        if(!is_null($dateEnd)){
            $usersCourse->whereDate('date_end','<=',$dateEnd);
        }
        $usersCourse = $usersCourse->paginate(config('settings.countPaginate'))->appends(request()->query());
       
        if (!$user->hasRole('Admin')) {
            $school_id = $user->school_id;
        }
        $classes = Classes::where('school_id', $school_id)->get();
        $courses = Course::where('course_category_id', $category_id)->get();

        return view('admins.transcripts.index', compact('schools', 'classes', 'courses', 'categoryCourses', 'school_id', 'course_id', 'category_id', 'class_id', 'usersCourse','backUrl','dateBegin','dateEnd','errorDate'));
    }

    public function mark(Request $request, $user_id, $course_user_id)
    {
        $backUrl = $request->url;
        $userCourse = UserCourse::findOrFail($course_user_id);
        $course_id = $userCourse->course_id;
        $course = Course::findOrFail($course_id);
        $countQuestionEssay = $course->question->where('type', 2)->count();
        $percentageChoice = $course->total_point_choice;
        $percentageEssay = 100 - $percentageChoice;
        if ($countQuestionEssay >= 1) {
            $pointOfOneQuestionsEssay = $percentageEssay / $countQuestionEssay;
        };
        $questionsChoose = $userCourse->courseResult()->whereHas('question', function ($query) {
            $query->where('type', 1);
        })->get();
        $sumPointChoose = 0;
        foreach ($questionsChoose as $value) {
            $sumPointChoose = $sumPointChoose + $value->point;
        };
        if ($percentageChoice != 0) {
            $sumPointChoose = round($sumPointChoose * 100 / $percentageChoice, 2);
        } else {
            $sumPointChoose = 0;
        }
        $questionsEssay = $userCourse->courseResult()->whereHas('question', function ($query) {
            $query->where('type', 2);
        })->get();
        foreach ($questionsEssay as $value) {
            $value->point100 = round($value->point * 100 / $pointOfOneQuestionsEssay, 0);
        };
        $sumPointEssay = 0;
        foreach ($questionsEssay as $value) {
            if (!is_null($value->point)) {
                $sumPointEssay = $sumPointEssay + $value->point;
            }
        };
        if ($sumPointEssay != 0) {
            $sumPointEssay = round($sumPointEssay * 100 / $percentageEssay, 2);
        } else {
            $sumPointEssay = 0;
        }
        $totalPoint = ($sumPointEssay * $percentageEssay + $sumPointChoose * $percentageChoice) / 100;
        return view('admins.transcripts.mark', compact('questionsChoose', 'questionsEssay', 'sumPointChoose', 'sumPointEssay', 'totalPoint', 'percentageChoice', 'backUrl','course_id'));
    }
    public function markStore(MarkRequest $request, $id)
    {
        $course = Course::findOrFail($id);
        $countQuestionEssay = $course->question->where('type', 2)->count();
        $percentageChoice = $course->total_point_choice;
        $percentageEssay = 100 - $percentageChoice;

        if ($countQuestionEssay >= 1) {
            $pointOfOneQuestionsEssay = $percentageEssay / $countQuestionEssay;
        };
        $data = $request->getData();

        foreach ($data as $data) {
            foreach ($data as $id => $mark) {
                $pointEssay = round(($mark / 100) * $pointOfOneQuestionsEssay, 2);
                UserCourseResult::where('id', $id)->update(['point' => $pointEssay]);
            }
        };
        return redirect(session('back_url'))->with('message', 'Chấm điểm thành công');
    }
    public function deleteUser($id)
    {
        $userCourse = UserCourse::findOrFail($id);
        $userCourse->delete();
        return redirect()->back()->with('message', 'Xóa người học thành công');
    }
    public function searchClassAjax(Request $request)
    {
        if ($request->ajax()) {
            $classes = Classes::where('school_id', '=', $request->id_School)->orderBy('name');
            $user = auth()->user();
            if ($user->hasRole('teacher')) {
                $classes->where('id', $user->class_id)->get();
            }
            $classes = $classes->get();
            return response()->json($classes);
        }
    }
    public function searchCourseAjax(Request $request)
    {
        if ($request->ajax()) {
            $courses = Course::where('course_category_id', '=', $request->id_category)->orderBy('name');
            $user = auth()->user();
            if ($user->hasRole('school') || $user->hasRole('teacher')) {
                $schoolId = $user->school_id;
                $courses->whereHas('authorUser', function ($q) use ($schoolId) {
                    $q->where('school_id', $schoolId)->orWhere('admin_flg',1);
                });
            }
            $courses = $courses->get();
            return response()->json($courses);
        }
    }
}
