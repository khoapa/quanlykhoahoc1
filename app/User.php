<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Course;
use Spatie\Permission\Traits\HasRoles;
use App\Models\ModelHasRole;
use App\Models\LocationUser;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'phone', 'birthday','admin_flg','device_token','longitude','latitude','notification_status','school_code','class_code',
        'address', 'gender', 'school_id', 'class_id', 'position_id', 'user_type','status','phone_authentication_code','email_authentication_code','date_tracking'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function school()
    {
        return $this->belongsTo('App\Models\School','school_id');
    }
    public function classes()
    {
        return $this->belongsTo('App\Models\Classes','class_id');
    }

    public static function list($name = null, $user_type = null, $limit = 10)
    {
        if ($name == null) {
            return User::with('school', 'classes')->where('user_type', '=', $user_type)->orderBy('id', 'DESC')->paginate($limit);
        } else {
            return User::with('school', 'classes')->where([
                ['name', "like", '%' . mb_strtolower($name, 'UTF-8') . '%'],
                ["user_type", '=', $user_type]
            ])->orderBy('id', 'DESC')->paginate($limit);
        }
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function userCourse()
    {
        return $this->belongsToMany(Course::class, 'user_courses','user_id','course_id')->withPivot('join_date','status','total_point');
    }
    public function userFavourite()
    {
        return $this->belongsToMany(Course::class, 'course_favorites','user_id','course_id');
    }
    public function role(){
        return $this->morphMany(ModelHasRole::class, 'model');
    }
    public function locationUser(){
        return $this->hasMany(LocationUser::class,'user_id','id');
    }
}
