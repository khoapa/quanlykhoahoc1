<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $admin = Role::create(['name'=>'Admin','description'=>'Admin']);
      $school = Role::create(['name' => 'school' ,'description' =>'Trường học']);
      $teacher = Role::create(['name' => 'teacher' ,'description'=>'Giáo viên']);
      $user = Role::create(['name' => 'user', 'description'=>'Người dùng']);
    }
}
