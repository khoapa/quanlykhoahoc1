<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class UpdatePermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::where('name','Admin')->first();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('role_has_permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Permission::where('subject','LIKE','%'.'Trường học'.'%')->delete();
        DB::table('permissions')->insert([
            [
                'name' => 'edit_school_course', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Chương trình học'
            ],
            [
                'name' => 'delete_school_course', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Chương trình học'
            ],
            [
                'name' => 'export_result', 
                'description' => 'Export', 
                'guard_name' => 'web',
                'subject' => 'Kết quả học tập'
            ],
            [
                'name' => 'export_statistics', 
                'description' => 'Export', 
                'guard_name' => 'web',
                'subject' => 'Thống kê số liệu'
            ],
        ]);

         $permissions = Permission::all();
         foreach($permissions as $permission){
             $roleAdmin->givePermissionTo($permission->name);
         }        
    }
}
