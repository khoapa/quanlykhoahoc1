<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class CreatePermissionNewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //truncate table
        DB::table ('permissions')->truncate();
      //  DB::table('roles')->truncate();
        DB::table('role_has_permissions')->truncate();
      // Create role for user
      DB::statement('SET FOREIGN_KEY_CHECKS=1;');
       $roleAdmin = Role::where('name','Admin')->first();

         Model::unguard();
   
        DB::table('permissions')->insert([
            //School
            [
                'name' => 'create_school', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            [
                'name' => 'edit_school', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            [
                'name' => 'view_school', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            [
                'name' => 'delete_school', 
                'description' => 'Xóa',
                'guard_name' => 'web',
                'subject' => 'Trường học'
            ],
            //Grade
            [
                'name' => 'create_grade', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            [
                'name' => 'edit_grade', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            [
                'name' => 'view_grade', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            [
                'name' => 'delete_grade', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Khối học'
            ],
            //Class
            [
                'name' => 'create_class', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'edit_class', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'view_class', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'delete_class', 
                'description' => 'Xóa',
                'guard_name' => 'web', 
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'import_class', 
                'description' => 'Import', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            [
                'name' => 'export_class', 
                'description' => 'Export', 
                'guard_name' => 'web',
                'subject' => 'Lớp học'
            ],
            //All user
            [
                'name' => 'create_user', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Tất cả User'
            ],
            [
                'name' => 'edit_user', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Tất cả User'
            ],
            [
                'name' => 'view_user', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Tất cả User'
            ],
            [
                'name' => 'delete_user', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Tất cả User'
            ], 
            [
                'name' => 'import_user', 
                'description' => 'Import', 
                'guard_name' => 'web',
                'subject' => 'Tất cả User'
            ], 
            [
                'name' => 'export_user', 
                'description' => 'Export', 
                'guard_name' => 'web',
                'subject' => 'Tất cả User'
            ], 
            //Teacher
            [
                'name' => 'create_teacher', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'edit_teacher', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'view_teacher', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'delete_teacher', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'import_teacher', 
                'description' => 'Import',
                'guard_name' => 'web', 
                'subject' => 'Giáo viên'
            ],
            [
                'name' => 'export_teacher', 
                'description' => 'Export', 
                'guard_name' => 'web',
                'subject' => 'Giáo viên'
            ],
             //Student
            [
                'name' => 'create_student', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'edit_student', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'view_student', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'delete_student', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'import_student', 
                'description' => 'Import',
                'guard_name' => 'web', 
                'subject' => 'Học sinh'
            ],
            [
                'name' => 'export_student', 
                'description' => 'Export', 
                'guard_name' => 'web',
                'subject' => 'Học sinh'
            ],
            
            //Tracking student
            [
                'name' => 'view_tracking', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Tracking học sinh'
            ],
            //Category course
            [
                'name' => 'create_category_course', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Nhóm kỹ năng sống'
            ],
            [
                'name' => 'edit_category_course', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Nhóm kỹ năng sống'
            ],
            [
                'name' => 'view_category_course', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Nhóm kỹ năng sống'
            ],
            [
                'name' => 'delete_category_course', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Nhóm kỹ năng sống'
            ],
            //Course
            [
                'name' => 'create_course', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Kỹ năng sống thực hành'
            ],
            [
                'name' => 'edit_course', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Kỹ năng sống thực hành'
            ],
            [
                'name' => 'view_course', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Kỹ năng sống thực hành'
            ],
            [
                'name' => 'delete_course', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Kỹ năng sống thực hành'
            ],
            //Course Theory
            [
                'name' => 'create_course_theory', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Kỹ năng sống lý thuyết'
            ],
            [
                'name' => 'edit_course_theory', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Kỹ năng sống lý thuyết'
            ],
            [
                'name' => 'view_course_theory', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Kỹ năng sống lý thuyết'
            ],
            [
                'name' => 'delete_course_theory', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Kỹ năng sống lý thuyết'
            ],
            //Category new
            [
                'name' => 'create_category_new', 
                'description' => 'Thêm', 
                'guard_name' => 'web',
                'subject' => 'Danh mục tin tức'
            ],
            [
                'name' => 'edit_category_new', 
                'description' => 'Sửa',
                'guard_name' => 'web', 
                'subject' => 'Danh mục tin tức'
            ],
            [
                'name' => 'view_category_new', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Danh mục tin tức'
            ],
            [
                'name' => 'delete_category_new', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Danh mục tin tức'
            ], 
            //Statistics
            [
                'name' => 'view_statistics', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Thống kê số liệu'
            ],
            //Study program
            [
                'name' => 'view_study_program', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Chương trình học'
            ],
            [
                'name' => 'create_study_program', 
                'description' => 'Thêm khóa học cho trường', 
                'guard_name' => 'web',
                'subject' => 'Chương trình học'
            ],
            //Mark
            [
                'name' => 'view_mark', 
                'description' => 'Xem', 
                'guard_name' => 'web',
                'subject' => 'Kết quả học tập'
            ],
            [
                'name' => 'create_mark', 
                'description' => 'Chấm điểm', 
                'guard_name' => 'web',
                'subject' => 'Kết quả học tập'
            ],
            [
                'name' => 'delete_mark', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Kết quả học tập'
            ],
            // //new
            // [
            //     'name' => 'create_new', 
            //     'description' => 'Thêm', 
            //     'guard_name' => 'web',
            //     'subject' => 'Tin tức'
            // ],
            // [
            //     'name' => 'edit_new', 
            //     'description' => 'Sửa',
            //     'guard_name' => 'web', 
            //     'subject' => 'Tin tức'
            // ],
            // [
            //     'name' => 'view_new', 
            //     'description' => 'Xem',
            //     'guard_name' => 'web', 
            //     'subject' => 'Tin tức'
            // ],
            // [
            //     'name' => 'delete_new', 
            //     'description' => 'Xóa', 
            //     'guard_name' => 'web',
            //     'subject' => 'Tin tức'
            // ],
            // //Category map
            // [
            //     'name' => 'create_category_map', 
            //     'description' => 'Thêm', 
            //     'guard_name' => 'web',
            //     'subject' => 'Danh mục map'
            // ],
            // [
            //     'name' => 'edit_category_map', 
            //     'description' => 'Sửa',
            //     'guard_name' => 'web', 
            //     'subject' => 'Danh mục map'
            // ],
            // [
            //     'name' => 'view_category_map', 
            //     'description' => 'Xem',
            //     'guard_name' => 'web', 
            //     'subject' => 'Danh mục map'
            // ],
            // [
            //     'name' => 'delete_category_map', 
            //     'description' => 'Xóa', 
            //     'guard_name' => 'web',
            //     'subject' => 'Danh mục map'
            // ], 
             //Sys nottification
             [
                'name' => 'send_nottification', 
                'description' => 'Gửi thông báo',
                'guard_name' => 'web', 
                'subject' => 'Thông báo(Sys Notiffication)'
            ],
            //Permission     
            [
                'name' => 'create_permission', 
                'description' => 'Thêm',
                'guard_name' => 'web', 
                'subject' => 'Phân quyền'
            ],
            [
                'name' => 'edit_permission', 
                'description' => 'Sửa', 
                'guard_name' => 'web',
                'subject' => 'Phân quyền'
            ],      
            [
                'name' => 'view_permission', 
                'description' => 'Xem',
                'guard_name' => 'web', 
                'subject' => 'Phân quyền'
            ],
            [
                'name' => 'delete_permission', 
                'description' => 'Xóa', 
                'guard_name' => 'web',
                'subject' => 'Phân quyền'
            ],            
              
            ]);
    $permissions = Permission::all();
    foreach($permissions as $permission){
        $roleAdmin->givePermissionTo($permission->name);
    }        
    }
}
