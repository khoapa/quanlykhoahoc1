<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class UpdatePermissionNew extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::where('name','Admin')->first();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('role_has_permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Permission::where('subject','LIKE','%'.'Danh mục tin tức'.'%')->delete();
        $permissions = Permission::all();
        foreach($permissions as $permission){
            $roleAdmin->givePermissionTo($permission->name);
        } 
    }
}
