<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class PermissionAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $admin =  User::where('email','admin@gmail.com')->first();
      $data= [
        'name' => 'Admin',
        'phone' => '012345678',
        'email' => 'admin@gmail.com',
        'password' => bcrypt('Aa12345678'),
        'admin_flg' => 1,
        'address' => 'Đà Nẵng',
       ];  
      if(!$admin){
        
           $admin = User::create($data);
      }else{
        $admin->update($data);
      }
        $admin->syncRoles([1]);
    }
}