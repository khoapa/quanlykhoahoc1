<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
class SupportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('supports')->truncate();
        DB::table('supports')->insert([
            // Danh sách danh web support
            [
                'id' => 1,
                'title' => 'Hướng dẫn sử dụng',
                'content' => 'Hướng dẫn sử dụng',
            ],
            [
                'id' => 2,
                'title' => 'Thông tin ứng dụng',
                'content' => 'Thông tin ứng dụng',
            ],
            [
                'id' => 4,
                'title' => 'Liên hệ nhà phát triển',
                'content' => 'Liên hệ nhà phát triển',
            ],
            [
                'id' => 3,
                'title' => 'Cập nhật ứng dụng',
                'content' => 'Cập nhật ứng dụng',
            ],
        ]);
    }
}