<?php

use Illuminate\Database\Seeder;
use App\Models\Conversation;

class ConversionDBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conversions = Conversation::all();
        foreach($conversions as $con){
            $user1 = $con->user1;
            $user2 = $con->user2;
            if(is_null($user1) || is_null($user2)){
                $con->delete();
            }
        }
    }
}
