<?php

use Illuminate\Database\Seeder;
use App\Models\UserCourse;
use App\Models\Course;

class CourseDBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = UserCourse::all();
        foreach($courses as $con){
            $course = $con->course;
            if(is_null($course)){
                $con->delete();
            }
        }
    }
}
