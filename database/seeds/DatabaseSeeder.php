<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\School;
use App\User;
use App\Http\Resources\SupportResource;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(RoleUserSeed::class);
        //$this->call(UserPermissionSeed::class);
        // $this->call(StudentSeeder::class);
        // factory(User::class, 20)->create();
        // factory(App\Models\School::class, 10)->create();
        // factory(App\Models\Classes::class, 10)->create();
        // factory(App\Models\Grade::class, 12)->create();
        // factory(App\Models\News::class, 10)->create();
        // factory(App\Models\CategoryNews::class, 10)->create();
        // factory(App\Models\Map::class, 10)->create();
        // factory(App\Models\MapCategory::class, 10)->create();
        // factory(App\Models\Support::class, 10)->create();
        // factory(App\Models\Tag::class, 10)->create();
        // factory(App\Models\SchoolCourse::class, 10)->create();
        //$this->call(PermissionAdmin::class);
        //$this->call(SupportSeeder::class);
        //$this->call(ConversionDBSeeder::class);
        //$this->call(CourseDBSeeder::class);
        //$this->call(CreatePermissionNewSeeder::class);
        //$this->call(UpdatePermission::class);
       // $this->call(UpdatePermissionUser::class);
        //$this->call(UpdatePermissionNew::class);
        $this->call(UpdatePermissionGroup::class);
    }
}
