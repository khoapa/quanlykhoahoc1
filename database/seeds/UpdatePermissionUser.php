<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;

class UpdatePermissionUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::where('name','Admin')->first();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('role_has_permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Permission::where('subject','LIKE','%'.'Nhóm kỹ năng sống'.'%')->delete();
        Permission::where('name','LIKE','%'.'edit_school_course'.'%')->delete();
        DB::table('permissions')->insert([
            [
                'name' => 'export_school_course', 
                'description' => 'Export', 
                'guard_name' => 'web',
                'subject' => 'Chương trình học'
            ]
        ]);

         $permissions = Permission::all();
         foreach($permissions as $permission){
             $roleAdmin->givePermissionTo($permission->name);
         } 
    }
}
