<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class UpdatePermissionGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();
        foreach($permissions as $permission){
          if($permission->subject == "Khối học" || ($permission->subject == "Lớp học" && $permission->description != "Import" && $permission->description != "Export")){
              $permission->update(["group"=>"Hệ Thống"]);
          }
          if($permission->subject == "Tất cả User" && $permission->description != "Import" && $permission->description != "Export"){
            $permission->update(["group"=>"Quản lý User"]);
          }
          if($permission->subject == "Giáo viên" && $permission->description != "Import" && $permission->description != "Export"){
            $permission->update(["group"=>"Quản lý User"]);
          }
          if($permission->subject == "Học sinh" && $permission->description != "Import" && $permission->description != "Export"){
            $permission->update(["group"=>"Quản lý User"]);
          }
          if($permission->subject == "Tất cả User" || $permission->subject == "Giáo viên" || $permission->subject == "Học sinh"){
            $permission->update(["group"=>"Quản lý User"]);
          }
          if($permission->subject == "Tracking học sinh"){
            $permission->update(["group"=>"Quản lý User"]);
          }
          if($permission->subject == "Kỹ năng sống thực hành"){
            $permission->update(["group"=>"Khóa học"]);
          }
          if($permission->subject == "Kỹ năng sống lý thuyết"){
            $permission->update(["group"=>"Khóa học"]);
          }
          if($permission->subject == "Thống kê số liệu" && $permission->description != "Export"){
            $permission->update(["group"=>"Khóa học"]);
          }
          if($permission->subject == "Chương trình học" && $permission->description != "Export"){
            $permission->update(["group"=>"Khóa học"]);
          }
          if($permission->subject == "Kết quả học tập" && $permission->description != "Export"){
            $permission->update(["group"=>"Khóa học"]);
          }
          if($permission->subject == "Phân quyền"){
            $permission->update(["group"=>"Phân Quyền"]);
          }
          if($permission->subject == "Thông báo(Sys Notiffication)"){
            $permission->update(["group"=>"Thông báo(Sys Notiffication)"]);
          }
          if($permission->description == "Gửi thông báo"){
            $permission->update(["description"=>"Thêm"]);
          }
          if($permission->description == "Chấm điểm"){
            $permission->update(["description"=>"Sửa"]);
          }
          if($permission->description == "Thêm khóa học cho trường"){
            $permission->update(["description"=>"Thêm"]);
          }
        }
    }
}