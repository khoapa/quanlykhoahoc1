<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\School;

$factory->define(App\Models\School::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone' => rand(1000000000, 9999999999),
        'email'=>$faker->email,
    ];
});
