<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Support::class, function (Faker $faker) {
    return [
        'icon'=>'https://picsum.photos/200/300',
        'title'=>'Link đường dẫn tới trang web',
        'link' => 'https://picsum.photos/200/300',
    ];
});
