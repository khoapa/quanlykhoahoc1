<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\News::class, function (Faker $faker) {
    return [
        'title'=>$faker->name,
        'news_category_id'=>rand(1,10),
        'content'=> $faker->address,
    ];
});
