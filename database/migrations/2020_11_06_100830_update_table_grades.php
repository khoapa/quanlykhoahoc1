<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableGrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('grades', 'thumbnail')){
            return true;
        }else{
            Schema::table('grades', function (Blueprint $table){
                $table->string('thumbnail')->nullable();
            });
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('grades', 'thumbnail')){
            Schema::table('grades', function (Blueprint $table){
                $table->dropColumn('thumbnail');
            });
        }
    }
}
