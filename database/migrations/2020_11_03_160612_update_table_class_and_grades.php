<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableClassAndGrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class', function(Blueprint $table){
            $table->integer('user_id')->nullable();
        });

        if (Schema::hasColumn('grades', 'school_id')){
            Schema::table('grades', function (Blueprint $table){
                $table->dropColumn('school_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('grades', function(Blueprint $table){
            $table->integer('school_id')->nullable();
        });

        if (Schema::hasColumn('class', 'user_id')){
            Schema::table('class', function (Blueprint $table){
                $table->dropColumn('user_id');
            });
        }
    }
}
