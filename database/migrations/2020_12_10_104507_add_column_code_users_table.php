<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCodeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('school_code')->after("school_id")->nullable();
            $table->string('class_code')->after("class_id")->nullable();
        });
        Schema::table('class', function (Blueprint $table) {
            $table->string('class_code')->after("school_id")->nullable();
        });
        Schema::table('schools', function (Blueprint $table) {
            $table->string('school_code')->after("name")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['school_code','class_code']);
        });
        Schema::table('class', function (Blueprint $table) {
            $table->dropColumn(['class_code']);
        });
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn(['school_code']);
        });
    }
}
