<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCourseTheories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_theories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->integer('course_category_id');
            $table->text('content');
            $table->string('image')->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('new');
            $table->tinyInteger('offer');
            $table->string('course_tag_ids',1024)->nullable();
            $table->integer('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_theories');
    }
}
