<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_map', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('map_id');
            $table->integer('status')->default(2)->comment = '1:Enable, 2:Disable';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_map');
    }
}
