<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnLastPositionUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // $table->float('longitude')->after("user_type")->nullable();
            // $table->float('latitude')->after("user_type")->nullable();
            $table->decimal('latitude', 10, 8)->after("user_type")->nullable();;
            $table->decimal('longitude', 11, 8)->after("user_type")->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['longitude','latitude']);
        });
    }
}
