<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStatusTableSchoolAndClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('class', function (Blueprint $table){
            $table->integer('status')->default(1);
        });
        Schema::table('schools', function (Blueprint $table){
            $table->integer('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class', function (Blueprint $table){
            $table->dropColumn('status');
        });
        Schema::table('schools', function (Blueprint $table){
            $table->dropColumn('status');
        });
    }
}
