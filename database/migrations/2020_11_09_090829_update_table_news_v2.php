<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableNewsV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->string('tag_ids', 1024)->nullable();
            $table->tinyInteger('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('news', 'tag_ids', 'status', 'audio')) {
            Schema::table('news', function (Blueprint $table) {
                $table->dropColumn('tag_ids');
                $table->dropColumn('status');
                $table->dropColumn('audio');
            });
        }
    }
}
