var map;
var markers = [];

function initMap() {
    var locations = new Array();
    let students = document.getElementById('studentsMap').value;
    let arrstudent = JSON.parse(students);
    for (var i = 0; i < arrstudent.length; i++) {
        arr = [arrstudent[i]['name'], arrstudent[i]['school'], arrstudent[i]['lat'], arrstudent[i]['long'], arrstudent[i]['id'], i];
        if (arrstudent[i]['long'] != null || arrstudent[i]['lat'] != null) {
            locations.push(arr)
        }
    }
    window.map = new google.maps.Map(document.getElementById('mapstudent'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var bounds = new google.maps.LatLngBounds();

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][2], locations[i][3]),
            map: map
        });
        markers.push(marker);
        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                var html = "<div class='maps_popover-content' onclick=detailTracking(" + locations[i][4] + ")><h4>" + locations[i][0] + "</h4><span>" + locations[i][1] + "</span></div>";
                infowindow.setContent(html);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

    map.fitBounds(bounds);

    var listener = google.maps.event.addListener(map, "click", function() {
        map.setZoom(15);
        google.maps.event.removeListener(listener);
    });

}

function detailTracking(id) {
    url = 'admin/tracking/detail/' + id;
    window.location.replace(url);
}

function moveMap(id) {
    let studen = document.getElementById(id).value;
    let student = JSON.parse(studen);
    clearOverlays()
    var pt = new google.maps.LatLng(student['lat'], student['long']);
    map.setCenter(pt);
    var marker = new google.maps.Marker({
        position: pt,
        map: map
    });
    markers.push(marker);
    var infowindow = new google.maps.InfoWindow();

    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
            var html = "<div class='maps_popover-content' onclick=detailTracking(" + student['id'] + ")><h4>" + student['name'] + "</h4><span>" + student['school'] + "-" + student['class'] + "</span></div>";
            "</span></div>";
            infowindow.setContent(html);
            infowindow.open(map, marker);
        }
    })(marker));
}

function clearOverlays() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers.length = 0;
}