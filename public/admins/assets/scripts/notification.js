$(document).ready(function() {
    let school = document.getElementById('schools').value;
    let arrSchool = JSON.parse(school);
    console.log(arrSchool);
    var tree = $('#tree').tree({
        primaryKey: 'id',
        uiLibrary: 'bootstrap',
        dataSource: [{

            value: 2,
            text: 'Trường',
            children: arrSchool
        }],
        checkboxes: true
    });
    $('#tree').on('click', function() {
        var checkedIds = tree.getCheckedNodes();
        var url = 'admin/notification/show-user';
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: {
                'checkedIds': checkedIds
            },
            success: function(data) {
                $("#list_user").html("");
                $.each(data, function(key, values) {
                    $.each(values, function(key, value) {

                        if (value.user_type == 1) {
                            user_type = "Học sinh";
                        } else {
                            user_type = "Giáo viên";
                        }
                        $("#list_user").append(
                            '<tr id="tr_custom"><td><input type="checkbox" name="user[]" class="check' + value.user_type +
                            '" id="check" value="' + value.id + '"></td><td>' + value.name + '</td><td>' + value.school_code +
                            '</td><td>' + value.class_code + '</td><td><input type="hidden" name="user_type" value="' + value.user_type + '">' + user_type + '</td></tr>'
                        );
                    });
                });
            }
        });
    });
});
$("#check_all").click(function() {
    value = $('#check_all:checked').val();

    if (value == 1) {
        $("#check").prop('checked', true);
        var checkboxes = document.getElementsByName('user[]');
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = true;
        }

    } else {
        $("#check").prop('checked', false);
        var checkboxes = document.getElementsByName('user[]');
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].checked = false;

        }
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#check_all").prop("checked", false);
    }
});
$("#checkStudent").click(function() {
    value = $('#checkStudent:checked').val();
    if (value == 1) {

        $('.check1').prop('checked', true);
    } else {
        $('.check1').prop('checked', false);
    }
});
$("#checkTeacher").click(function() {
    value = $('#checkTeacher:checked').val();
    if (value == 1) {

        $('.check2').prop('checked', true);
    } else {
        $('.check2').prop('checked', false);
    }
});
$(".check1").click(function() {
    if (!$(this).prop("checked")) {
        $("#checkStudent").prop("checked", false);
    }
});
$("#click").click(function() {

    var checkboxes = document.getElementsByName('user[]');
    var ids = [];
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked == true) {
            ids.push(checkboxes[i].value);
        }
    }
    $("#hidden").append(
        '<input type="hidden" name="list_id" value="' + ids +
        '">'
    );
});
$(document).ready(function() {
    let check = document.getElementById('test').value;
    if (check == "") {
        $('#btn-notifi').css('pointer-events', 'none')
    } else {
        $('#btn-notifi').css('pointer-events', 'auto')
    }

})
$(document).on('keydown', '#test', function() {
    let check = document.getElementById('test').value;
    if (check == "") {
        $('#btn-notifi').css('pointer-events', 'none')
    } else {
        $('#btn-notifi').css('pointer-events', 'auto')
    }
});