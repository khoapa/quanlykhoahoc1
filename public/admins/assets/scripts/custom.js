$(document).on('click', '.block-image-upload', function() {
    var tag = event.target.nodeName.toLowerCase();
    if (tag == 'button' || tag == 'img' || tag == 'i') {
        return;
    } else {
        $('#image-upload').click();
    }
})


$(document).on('change', '#upload', function() {
    let formData = new FormData();
    formData.append('file', $('#image-upload')[0].files[0]);
    let url = $(this).attr('url');
    let images = $('#images').val();
    images = (images) ? JSON.parse(images) : [];
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        processData: false,
        contentType: false,
        success: function(data) {
            images = (images) ? images : [];
            images.push(data.path);
            let preview = '<div class="preview-image"><img src="' + data.path + '" />';

            preview += '<button type="button" class="delete-preview-image mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-dashed btn btn-outline-warning"><i class="pe-7s-trash btn-icon-wrapper"> </i></button></div>';

            $('.block-image-upload').append(preview);
            $('#images').val(JSON.stringify(images));
        }
    });
});
$(document).on('click', '.delete-preview-image', function() {
    let path = $(this).parent().find("img").attr('src');
    let images = $('#images').val();
    images = (images) ? JSON.parse(images) : [];
    if (images.includes(path)) {
        images = images.filter(function(ele) {
            return ele != path;
        });
        images = (images.length) ? JSON.stringify(images) : '';
        $('#images').val(images);
        $(this).parent().remove();
    }
});

$(document).on('click', '.btn-upload', function() {
    $('#image-upload').click();
});

$(document).on('click', '.btn-photo-image', function() {
    $('#upload-single-image').click();
});
$(document).on('change', '#upload-single-image', function() {
    var fd = new FormData();
    var files = $('#upload-single-image')[0].files;
    // Check file selected or not
    if (files.length > 0) {
        fd.append('file', files[0]);

        $.ajax({
            url: '/admin/upload/image',
            type: 'post',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                var dataResponse = JSON.parse(response);
                if (dataResponse.success) {
                    $('#img-sigle-preview')
                        .attr('src', dataResponse.message);
                    $("#value-input").val(dataResponse.message);
                } else {
                    alert(dataResponse.message);
                }
            },
        });
    } else {
        alert("Please select a file.");
    }
    //readURL(this, 'img-sigle-preview');
});

function readURL(input, idPreview) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#' + idPreview)
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function showPreview(event) {
    if (event.target.files.length > 0) {
        var src = URL.createObjectURL((event.target.files[0]));
        var preview = document.getElementById('upload-img-single-show');
        preview.src = src;
        preview.style.display = "block";
    }
}


$(document).on('click', '#btn-upload_image', function() {
    $('#upload_image').click();
});

function readURLNew(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            imgElem = document.getElementById('upload_image');
            data = e.target.result.base64;
            console.log(data);
            $('#preview-image').attr('src', e.target.result);
            $(".image-box").css("display", "block");

        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#upload_image").change(function() {
    readURLNew(this);
});

function removeImage() {
    $(".image-box").css("display", "none");
    $('#upload_image').removeAttr('value');
}
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

function showPassword1() {
    var x = document.getElementById("password1");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function showPassword2() {
    var x = document.getElementById("password2");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function showOldPassword() {
    var x = document.getElementById("old_password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

$("#selectAll").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả user");
    if (result1) {
        value = $('#selectAll:checked').val();
        var search_name = document.getElementById('search_name').value;
        var school_id = document.getElementById('search_school');
        if (school_id) {
            school_id = school_id.value;
        }
        var class_id = document.getElementById('search_class');
        if (class_id) {
            class_id = class_id.value;
        }
        var user_type = document.getElementById('user_type_search');
        if (user_type) {
            user_type = user_type.value;
        }
        var url = 'admin/user/enable-all';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'class_id': class_id,
                    'user_type': user_type,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'class_id': class_id,
                    'user_type': user_type,
                    'type': 2
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAll").prop("checked", false);
    }
});

$("#selectAllSchool").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả trường học");
    if (result1) {
        value = $('#selectAllSchool:checked').val();
        var search_name = document.getElementById('search_name').value;
        var url = 'admin/school/enable-all';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'type': 2
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllSchool").prop("checked", false);
    }
});
$("#selectAllCourseTheory").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả kỹ năng sống lý thuyết");
    if (result1) {
        value = $('#selectAllCourseTheory:checked').val();
        var search_name = document.getElementById('search_name').value;
        var category = document.getElementById('search_category');
        if (category) {
            category = category.value;
        }
        var url = 'admin/course_theory/enable-all';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'category': category,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'category': category,
                    'type': 0
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllCourseTheory").prop("checked", false);
    }
});
$("#selectAllCourse").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả kỹ năng sống thực hành");
    if (result1) {
        value = $('#selectAllCourse:checked').val();
        var search_name = document.getElementById('search_name').value;
        var category = document.getElementById('search_category');
        if (category) {
            category = category.value;
        }
        var author = document.getElementById('search_author');
        if (author) {
            author = category.value;
        }
        var url = 'admin/course/enable-all';

        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'category': category,
                    'search_name': search_name,
                    'author': author,
                    'type': 1,
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'category': category,
                    'search_name': search_name,
                    'author': author,
                    'type': 0,
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllCourse").prop("checked", false);
    }
});
$("#selectAllClass").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả lớp học");
    if (result1) {
        value = $('#selectAllClass:checked').val();
        var search_name = document.getElementById('search_name').value;
        var school_id = document.getElementById('search_school');
        if (school_id) {
            school_id = school_id.value;
        }
        var grade_id = document.getElementById('grade_search');
        if (grade_id) {
            grade_id = grade_id.value;
        }
        var url = 'admin/class/enable-all';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'grade_id': grade_id,
                    'type': 1,
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'grade_id': grade_id,
                    'type': 2,
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllClass").prop("checked", false);
    }
});
$("#selectAllNews").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả tin tức");
    if (result1) {
        value = $('#selectAllNews:checked').val();
        var search_name = document.getElementById('search_name').value;
        var category = document.getElementById('search_category').value;
        var url = 'admin/news/enable-all';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'category': category,
                    'type': 1,
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'category': category,
                    'type': 2,
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllNews").prop("checked", false);
    }
});

$("#selectAllUserOfSchool").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả user");
    if (result1) {
        value = $('#selectAllUserOfSchool:checked').val();
        var search_name = document.getElementById('search_name').value;
        var school_id = document.getElementById('school_id');
        if (school_id) {
            school_id = school_id.value;
        }
        var url = 'admin/school/enable-user';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'type': 2
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllUserOfSchool").prop("checked", false);
    }
});

$("#selectAllClassOfSchool").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả lớp học của trường");
    if (result1) {
        value = $('#selectAllClassOfSchool:checked').val();
        var search_name = document.getElementById('search_name').value;
        var school_id = document.getElementById('school_id').value;
        var url = 'admin/school/enable-class';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'type': 2
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllClassOfSchool").prop("checked", false);
    }
});
$("#selectAllCourseOfSchool").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả khóa học của trường");
    if (result1) {
        value = $('#selectAllCourseOfSchool:checked').val();
        var search_name = document.getElementById('search_name').value;
        var school_id = document.getElementById('school_id').value;
        var url = 'admin/school/enable-course';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'type': 0
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllCourseOfSchool").prop("checked", false);
    }
});
$("#selectAllStudent").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả khóa học của trường");
    if (result1) {
        value = $('#selectAllStudent:checked').val();
        var search_name = document.getElementById('search_name').value;
        var school_id = document.getElementById('search_school');
        if (school_id) {
            school_id = school_id.value;
        }
        var class_id = document.getElementById('search_class');
        if (class_id) {
            class_id = class_id.value;
        }
        var url = 'admin/student/enable-student';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'class_id': class_id,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'class_id': class_id,
                    'type': 2
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllStudent").prop("checked", false);
    }
});
$("#selectAllTeacher").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả khóa học của trường");
    if (result1) {
        value = $('#selectAllTeacher:checked').val();
        var search_name = document.getElementById('search_name').value;
        var school_id = document.getElementById('search_school');
        if (school_id) {
            school_id = school_id.value;
        }
        var class_id = document.getElementById('search_class');
        if (class_id) {
            class_id = class_id.value;
        }

        var url = 'admin/teacher/enable-teacher';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'class_id': class_id,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'school_id': school_id,
                    'class_id': class_id,
                    'type': 2
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("#selectAllUserOfClass").click(function() {
    result1 = confirm("Bạn có muốn thay đổi trạng thái tất cả học sinh của lớp");
    if (result1) {
        value = $('#selectAllUserOfClass:checked').val();
        var search_name = document.getElementById('search_name').value;
        var class_id = document.getElementById('class_id').value;
        var url = 'admin/class/enable-user';
        if (value == 1) {
            $(".check").prop('checked', true);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'class_id': class_id,
                    'type': 1
                },
                success: function(data) {
                    alert(data);
                }
            });
        } else {
            $(".check").prop('checked', false);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                data: {
                    'search_name': search_name,
                    'class_id': class_id,
                    'type': 2
                },
                success: function(data) {
                    alert(data);
                }
            });
        }
    } else {
        return false
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllTeacher").prop("checked", false);
    }
});
$("#selectAllSchoolCourse").click(function() {
    value = $('#selectAllSchoolCourse:checked').val();
    if (value == 1) {
        $(".checkSchoolCourse").prop('checked', true);
    } else {
        $(".checkSchoolCourse").prop('checked', false);
    }
});
$("input[type=checkbox]").click(function() {
    if (!$(this).prop("checked")) {
        $("#selectAllSchoolCourse").prop("checked", false);
    }
});
$(".check").click(function() {
    location.reload();
});