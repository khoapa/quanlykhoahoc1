var map;
var markers = [];

function initMap() {
    var geocode = {
        lat: 16.067139,
        lng: 108.213954
    };

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15, // Set the zoom level manually
        center: geocode,
        mapTypeId: 'terrain'
    });

    initMarker(map)
    var input = document.getElementById('searchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        console.log(autocomplete.getPlace);
        var place = autocomplete.getPlace();
        document.getElementById('lat').value = place.geometry.location.lat();
        document.getElementById('long').value = place.geometry.location.lng();

        let lat = place.geometry.location.lat();
        let lng = place.geometry.location.lng();
        if (lat != null && lng != null) {
            let latLng = {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
            };
            map.setCenter(latLng);
            marker = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }
        markers.push(marker);
    });
    // map.addListener('click', function(event) {
    //     if (markers.length >= 1) {
    //         deleteMarkers();
    //     }

    //     addMarker(event.latLng);
    //     document.getElementById('lat').value = event.latLng.lat();
    //     document.getElementById('long').value = event.latLng.lng();
    // });
}

// Adds a marker to the map and push to the array.
function addMarker(location) {
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

function initMarker(map) {
    let lat = $('#lat').attr('value');
    let lng = $('#long').attr('value');
    if (lat != null && lng != null) {
        let latLng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };
        map.setCenter(latLng);
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
    }
    markers.push(marker);
}
$('.search-location').keypress(function(e) {
    if (e.which == 13) {
        google.maps.event.trigger(autocomplete, 'place_changed');
        return false;
    }
});