function sendMessage(user2_id) {
    var url = 'admin/chat/create';
    var data = getFormObj("send-message");
    text = data['text'];
    image = data['image'];
    if (text == "" && image == "") {
        return false;
    }
    data['user2_id'] = user2_id;
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: data,
        success: function(data) {
            removeImage();
            $('#text').val('');
            $('#image').val('');
            var dataNew = JSON.parse(JSON.stringify(data));
            $("#conversion_id").val(dataNew['conversation_id']);
            console.log(dataNew['conversation_id'])
        }
    });
}

function getFormObj(formId) {
    var formObj = {};
    var inputs = $("#" + formId).serializeArray();
    $.each(inputs, function(i, input) {
        formObj[input.name] = input.value;
    });
    return formObj;
}
$(document).on("change", "#upload_image", function() {
    var fd = new FormData();
    var files = $('#upload_image')[0].files;

    // Check file selected or not
    if (files.length > 0) {
        fd.append('file', files[0]);

        $.ajax({
            url: '/admin/chat/upload',
            type: 'post',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response);
                $("#image").val(response);
            },
        });
    } else {
        alert("Please select a file.");
    }
});

function removeImage() {
    $(".image-box").css("display", "none");
    $('#upload_image').removeAttr('value');
    $('#image').val('');
}

function deleteMessage(id) {
    var url = 'admin/chat/message/delete/' + id;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            $('#mesage-' + id).css('display', 'none');
        }
    });
}

function viewImage(element) {
    let url = element.getAttribute('src');
    document.getElementById("urlImage").src = url;
    $('#show-image').css('display', 'block');
}

function closeModel() {
    document.getElementById("urlImage").src = '';
    $('#show-image').css('display', 'none');
}
var socket = io.connect(LINKSERVER, { path: PATH_SOCKET });
//var socket = io.connect("http://localhost:8080");

var idUser = document.getElementById("id_user").value;

socket.on('list-conversation-' + idUser, function(data) {
    console.log(data)
    var dataList = JSON.parse(data);
    avatarList = dataList['message']['avatar'];
    nameList = dataList['message']['sender'];
    textList = dataList['message']['text'];
    textList = textList != null ? textList : '';
    idConversion = dataList['conversation_id'];
    imageList = dataList['message']['images'];
    var conversions = document.querySelectorAll(".conversion");
    idConversionCheck = "conversion-" + idConversion;
    console.log("text" + idConversionCheck);
    for (i = 0; i < conversions.length; i++) {
        console.log('id' + conversions[i].id);
        if (idConversionCheck == conversions[i].id) {
            $('#' + idConversionCheck).remove();
        }

    }
    $("#list-chat").prepend('<div class="widget-content p-0 conversion" id="conversion-' + idConversion + '">' +
        '<div class="widget-content-wrapper no-send-list">' +
        '<div class="widget-content-left">' +
        '<div class="custom-checkbox">' +
        '<a href="/admin/chat/' + idConversion + '">' +
        '<div class="avatar-icon avatar-icon-lg rounded">' +
        '<img src="' + avatarList + '" alt="">' +
        '</div>' +
        '</a>' +
        '</div>' +
        '</div>' +
        '<a href="/admin/chat/' + idConversion + '">' +
        '<div class="widget-content-left flex2 padding-chat">' +
        '<div class="widget-heading">' + nameList + '</div>' +
        '<div class="widget-subheading">' + textList +
        '<img class="img-chat" src="' + imageList + '" />' +
        '</div>' +
        '</div>' +
        '</a>' +
        // '<div class="widget-content-right">' +
        // '<i class="fa fa-trash-alt"></i>' +
        // '</div>' +
        '</div>' +
        '</div>');
});
var id = document.getElementById("conversion_id").value;
socket.on('conversation-' + id, function(data) {
    var dataNew = JSON.parse(data);
    var avatar = dataNew['avatar'];
    var content = dataNew['text'];
    var image = dataNew['images'];
    var idMessage = dataNew['id'];
    var time = dataNew['time_chat'];
    if (idUser == dataNew['sender_id']) {
        $("#messages").append('<div id ="mesage-' + idMessage + '" class="float-right custom-block-send">' +
            '<div class="chat-box-wrapper">' +
            '<div class="icon-del" onclick="deleteMessage(' + idMessage + ')">' +
            '<i class="fa fa-times" aria-hidden="true"></i>' +
            '</div>' +
            '<div>' +
            '<div class="chat-box"><div>' + content +
            '</div>' +
            '<img onclick="viewImage(this)" src="' + image + '">' +
            '</div>' +
            '<small class="opacity-6">' +
            '<i class="fa fa-calendar-alt mr-1"></i>' + time +
            '</small>' +
            '</div>' +
            '<div>' +
            '<div class="avatar-icon-wrapper ml-1">' +
            '<div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg">' +
            '</div>' +
            '<div class="avatar-icon avatar-icon-lg rounded">' +
            '<img src="' + avatar + '" alt="">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div>');
    } else {
        $("#messages").append('<div id ="mesage-' + idMessage + '" class="chat-box-wrapper"><div><div class="avatar-icon-wrapper mr-1">' +
            '<div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg"></div>' +
            '<div class="avatar-icon avatar-icon-lg rounded">' +
            '<img src="' + avatar + '" alt="">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div>' +
            '<div class="chat-box">' + content +
            '<img onclick="viewImage(this)" src="' + image + '" alt="">' +
            '</div>' +
            '<small class="opacity-6">' +
            '<i class="fa fa-calendar-alt mr-1"></i>' + time +
            '</small>' +
            '</div>' +
            '</div>');
    }
    console.log('idmessage' + id);
    $messageNew = content +
        '<img class="img-chat" src="' + image + '" />';
    $('#message-convertion-' + id).html($messageNew);
});