$("#school_id").change(function() {
    var id_School = $(this).val();
    var url = 'admin/transcript/search-class';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'id_School': id_School
        },
        success: function(data) {
            $("#class_id").html('');
            $('#form-school-id').css('pointerEvents', 'auto')
            $.each(data, function(key, value) {
                $("#class_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});
$("#category_id").change(function() {
    var id_category = $(this).val();
    var url = 'admin/transcript/search-course';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'id_category': id_category
        },
        success: function(data) {
            $("#course_id").html("<option value=''>--Tất cả khóa học--</option>");
            $.each(data, function(key, value) {
                $("#course_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});

function checkFormQuestion(type) {
    document.getElementById("main-question").style.display = "block";
    document.getElementById("form-open").value = 1;
    if (type == 1) {
        document.getElementById("form-choose").style.display = "block";
        $("#add-answer-choose").css("display", "inline-block");
        document.getElementById("type-question").value = 1;
        let check = document.getElementById('multiselect-choose').value;
        if (check == 0) {
            $('.form-check-input-choose').css('display', 'none')
            $('#form-check-input-choose-first').attr('checked', true);
        } else {
            $('.form-check-input-choose').css('display', 'block')
            $('.add-form-check-input-choose').css('display', 'block')
            $('#form-check-input-choose-first').prop("checked", false);
        }
        $("#create_exhibition_form").removeClass("was-validated");
    } else {
        $("#add-answer-choose").css("display", "none");
        document.getElementById("form-choose").style.display = "none";
        document.getElementById("type-question").value = 2;
    }
    document.getElementById("create_exhibition_form").reset();
    return false;
}
$(document).ready(function() {
    if ($("#create_exhibition_form").hasClass("was-validated")) {
        let typeQuestion = document.getElementById("type-question").value;
        document.getElementById("main-question").style.display = "block";
        if (typeQuestion == 1) {
            document.getElementById("form-choose").style.display = "block";
            document.getElementById("type-question").value = 1;
        } else {
            document.getElementById("form-choose").style.display = "none";
            document.getElementById("type-question").value = 2;
        }
    }
    let classId = document.getElementById('class_id');
    if (classId) {
        valueClass = classId.value;
        if (valueClass != '') {
            $('#form-school-id').css('pointerEvents', 'auto')
        }
    }
    let schoolId = document.getElementById('school_id1');
    if (schoolId) {
        valueSchool = schoolId.value;
        if (valueSchool != '') {
            $('#form-role_id_school').css('pointerEvents', 'auto')
        }
    }
});

function redirectCourse(school_id) {
    let url = 'admin/school/course';
    let date_begin = document.getElementById('datepicker-begin').value;
    let date_end = document.getElementById('datepicker-end').value;
    $.redirect(url, {
        school_id: school_id,
        date_begin: date_begin,
        date_end: date_end,
        check: 1
    }, "GET");

};

//   $('#datepicker-begin').datepicker({
//     format: 'mm/dd/yyyy',
//     uiLibrary: 'bootstrap4',
// })
// $('#datepicker-begin').datepicker({
//     //footer: true,
//     format: 'dd-mm-yyyy',
//     uiLibrary: 'bootstrap4',
//     change: function (e) {
//         $('#form-search').submit();
//     }
// });
$(document).ready(function() {
    $('#datepicker-begin').datepicker({
        format: 'dd-mm-yyyy',
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
    }).on('changeDate', function(e) {
        var date1 = $(this).val();
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var date2 = dd + '-' + mm + '-' + yyyy;
        var startDate = new Date(date1)
        var year = date1.split("-", 3);
        if (date1 > date2 || startDate.getTime() > today.getTime() || year[2] > yyyy) {
            $("#datepicker-begin").datepicker("setDate", new Date());
        }
    });
    $('#datepicker-end').datepicker({
        format: 'dd-mm-yyyy',
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
    }).on('changeDate', function(e) {
        var date1 = $(this).val();
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var date2 = dd + '-' + mm + '-' + yyyy;
        var startDate = new Date(date1)
        var year = date1.split("-", 3);
        if (date1 > date2 || startDate.getTime() > today.getTime() || year[2] > yyyy) {
            $("#datepicker-end").datepicker("setDate", new Date());
        }
    });
});
$(document).ready(function() {
    $('#datepicker-end').datepicker({
        format: 'dd-mm-yyyy',
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        // }).on('changeDate', function(e) {
        //     $(this).datepicker('hide');
        //     $('#form-search').submit();
    });
});

$(document).on('click', '#export-click-statical', function() {
    var url = 'admin/export/statistical';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var dateBegin = document.getElementById('datepicker-begin');
    if (dateBegin) {
        dateBegin = dateBegin.value;
    }
    var dateEnd = document.getElementById('datepicker-end');
    if (dateEnd) {
        dateEnd = dateEnd.value;
    }
    var school_id = document.getElementById('school_id_search');
    if (school_id) {
        school_id = school_id.value;
    }
    $.redirect(url, {
        _token: _token,
        dateBegin: dateBegin,
        dateEnd: dateEnd,
        school_id: school_id,
    }, "GET");

});
$(document).on('click', '#export-click-transcrip', function() {
    var url = 'admin/export/transcript';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var school_id = document.getElementById('school_id1');
    if (school_id) {
        school_id = school_id.value;
    }
    var class_id = document.getElementById('class_id');
    if (class_id) {
        class_id = class_id.value;
    }
    var category_id = document.getElementById('category_id');
    if (category_id) {
        category_id = category_id.value;
    }
    var course_id = document.getElementById('course_id');
    if (course_id) {
        course_id = course_id.value;
    }
    var date_begin = document.getElementById('datepicker-begin').value;
    var date_end = document.getElementById('datepicker-end').value;
    $.redirect(url, {
        _token: _token,
        school_id: school_id,
        class_id: class_id,
        category_id: category_id,
        course_id: course_id,
        date_begin: date_begin,
        date_end: date_end,
    }, "GET");

});
$(document).on('click', '#export-click-all-user', function() {
    var type = document.getElementById('type').value;
    var url = 'admin/user/export';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var class_id = document.getElementById('search_class');
    if (class_id) {
        class_id = class_id.value;
    }
    var school_id = document.getElementById('search_school');
    if (school_id) {
        school_id = school_id.value;
    }
    var user_type_search = document.getElementById('user_type_search');
    if (user_type_search) {
        user_type_search = user_type_search.value;
    }
    $.redirect(url, {
        _token: _token,
        type: type,
        class_id: class_id,
        school_id: school_id,
        user_type_search: user_type_search,
    }, "GET");

});

$(document).on('click', '#export-click-school', function() {
    var url = 'admin/school/export';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var search = document.getElementById('search_name').value;
    $.redirect(url, {
        _token: _token,
        search: search,
    }, "GET");

});
$(document).on('click', '#export-click-class', function() {
    let url = 'admin/class/export';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var search = document.getElementById('search_name');
    if (search) {
        search = search.value;
    }
    var grade_id = document.getElementById('grade_search');
    if (grade_id) {
        grade_id = grade_id.value;
    }
    var school_id = document.getElementById('search_school');
    if (school_id) {
        school_id = school_id.value;
    }
    $.redirect(url, {
        _token: _token,
        search: search,
        grade_id: grade_id,
        school_id: school_id,
    }, "GET");

});

$(document).on('click', '#export-click-school-course', function() {
    var url = 'admin/school/course/export/course-school';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var class_id = document.getElementById('class_id');
    if (class_id) {
        class_id = class_id.value;
    }
    var school_id = document.getElementById('school_id1');
    if (school_id) {
        school_id = school_id.value;
    }
    var date_begin = document.getElementById('datepicker-begin');
    if (date_begin) {
        date_begin = date_begin.value;
    }
    var date_end = document.getElementById('datepicker-end');
    if (date_end) {
        date_end = date_end.value;
    }
    $.redirect(url, {
        _token: _token,
        class_id: class_id,
        date_begin: date_begin,
        date_end: date_end,
        school_id: school_id,
    }, "GET");

});