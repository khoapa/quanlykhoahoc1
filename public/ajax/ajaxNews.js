function addStatusFunction(status, id) {
    var url = 'admin/news/update-status';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}

$(document).on('change', '#search_category', function() {
    $('#form-search').submit();
});
$(document).on('change', '#search_author', function() {
    $('#form-search').submit();
});

function addPositionFunction(id) {
    var url = 'admin/category_course/update-position';
    var position = document.getElementById('position' + id).value;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'position': position,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
    location.reload()
};

function addStatusCourseFunction(status, id) {
    var url = 'admin/course/update-status';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}

function addStatusCourseTheory(status, id) {
    var url = 'admin/course_theory/update-status';

    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}
$("#course_category_id").change(function() {
    if ($(this).val() == "") {
        $("#course_id").html("<option value=''>--Chọn khóa học thực hành--</option>");
    } else {
        var id = $(this).val();
        var url = 'admin/course_theory/list-course/' + id;
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $("#course_id").html('');
                $("#course_id").html("<option value=''>--Chọn khóa học thực hành--</option>");
                $.each(data, function(key, value) {
                    $("#course_id").append(
                        "<option value=" + value.id + ">" + value.name + "</option>"
                    );
                });
            }
        });
    }
});

// if (document.getElementsByName('selectAll').value == checked) {
//     $("#selectAll").click(function() {

//         var checkboxes = document.getElementsByName('status[]');

//         // Lặp và thiết lập checked
//         for (var i = 0; i < checkboxes.length; i++) {
//             checkboxes[i].checked = false;
//         }


//     });
// }
$("#role_form_id").change(function() {
    var role_id = document.getElementById('role_form_id').value;
    if (role_id == '') {
        var url = 'admin/permission/ajax/create/';
    } else {
        var url = 'admin/permission/ajax/create/' + role_id;
    }

    var _token = $('meta[name="csrf-token"]').attr('content');
    var description = document.getElementById('description');
    if (description) {
        description = description.value;
    }
    $.redirect(url, {
        _token: _token,
        description: description,
    }, "POST");
});
$(document).on('click', '#export-click-student', function() {
    var type = document.getElementById('type').value;
    var url = 'admin/student/export';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var class_id = document.getElementById('search_class')
    if (class_id) {
        class_id = class_id.value;
    }
    var school_id = document.getElementById('search_school');
    if (school_id) {
        school_id = school_id.value;
    }
    var search = document.getElementById('search_name');
    if (search) {
        search = search.value;
    }
    // $.post(url, {
    //     _token : _token,
    //     user : $('#num').val(),
    //     passwd : $('#passwd').val()
    // }, function (data) {
    //     window.location = data;
    // });
    //location.href = url;
    $.redirect(url, {
        _token: _token,
        class_id: class_id,
        school_id: school_id,
        search: search,
        type: type,
    }, "GET");

});
$(document).on('click', '#export-click-teacher', function() {
    var type = document.getElementById('type').value;
    var url = 'admin/teacher/export';
    var _token = $('meta[name="csrf-token"]').attr('content');
    var class_id = document.getElementById('search_class')
    if (class_id) {
        class_id = class_id.value;
    }
    var school_id = document.getElementById('search_school');
    if (school_id) {
        school_id = school_id.value;
    }
    var search = document.getElementById('search_name');
    if (search) {
        search = search.value;
    }
    // $.post(url, {
    //     _token : _token,
    //     user : $('#num').val(),
    //     passwd : $('#passwd').val()
    // }, function (data) {
    //     window.location = data;
    // });
    //location.href = url;
    $.redirect(url, {
        _token: _token,
        class_id: class_id,
        school_id: school_id,
        search: search,
        type: type,
    }, "GET");

});