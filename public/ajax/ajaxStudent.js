$("#school_id1").change(function() {
    var id_School = $(this).val();
    var url = 'admin/student/search-class';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'id_School': id_School
        },
        success: function(data) {
            $('#form-school-id').css('pointerEvents', 'auto')
            $("#class_id").html("<option value=''>--Tất cả lớp--</option>");
            $.each(data, function(key, value) {
                $("#class_id").append(
                    "<option value=" + value.id + ">" + value.name + "</option>"
                );
            });
        }
    });
});

function addStatusUserFunction(status, id) {
    var url = 'admin/student/update-status';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}


$('#datepicker').datepicker({
    format: 'dd-mm-yyyy',
    showOtherMonths: true,
    selectOtherMonths: true,
    changeMonth: true,
    changeYear: true,
});
$("#school").change(function() {
    var id = $(this).val();
    var url = 'admin/teacher/list-teacher/' + id;
    if (id == "") {
        $("#user_id").html("<option value=''>--Chọn giáo viên chủ nhiệm--</option>");
    } else {
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $("#user_id").html('');
                $("#user_id").html("<option value=''>--Chọn giáo viên chủ nhiệm--</option>");
                $.each(data, function(key, value) {
                    $("#user_id").append(
                        "<option value=" + value.id + ">" + value.name + "</option>"
                    );
                });
            }
        });
    }
});
$("#school").change(function() {
    var id = $(this).val();
    var url = 'admin/grades/list-grade/' + id;
    if (id == "") {
        $("#grade_id").html("<option value=''>--Chọn khối--</option>");
    } else {
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $("#grade_id").html('');
                $("#grade_id").html("<option value=''>--Chọn khối--</option>");
                $.each(data, function(key, value) {
                    $("#grade_id").append(
                        "<option value=" + value.id + ">" + value.name + "</option>"
                    );
                });
            }
        });
    }
});

function addStatusFunction(status, id) {
    var url = '/student/update-status';
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}

$(document).on('change', '#search_school', function() {
    $('#form-search').submit();
});
$(document).on('change', '#grade_search', function() {
    $('#form-search').submit();
});
$(document).on('change', '#search_class', function() {
    $('#form-search').submit();
});
$(document).on('change', '#user_type_search', function() {
    $('#form-search').submit();
});

$("#search_school").change(function() {
    var id = $(this).val();
    var url = 'admin/grades/list-grade/' + id;
    if (id == "") {
        $("#search_grade").html("<option value=''>--Tất cả khối--</option>");
    } else {
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $("#grade_search").html('');
                $("#grade_search").html("<option value=''>--Tất cả khối--</option>");
                $.each(data, function(key, value) {
                    $("#grade_search").append(
                        "<option value=" + value.id + ">" + value.name + "</option>"
                    );
                });
            }
        });
    }
});
$("#search_school").change(function() {
    document.getElementById('grade_search').value = '';
});
$("#search_school").change(function() {
    document.getElementById('search_class').value = '';
});



function addStatusSchoolFunction(status, id) {
    var url = 'admin/school/update-status/' + id;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}

function addStatusClassFunction(status, id) {
    var url = 'admin/class/update-status/' + id;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        data: {
            'status': status,
            'id': id
        },
        success: function(data) {
            alert(data);
        }
    });
}
$("#school_id1").change(function() {
    var id_School = $(this).val();
    var url = 'admin/school/permission/' + id_School;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function(data) {
            $('#form-role_id_school').css('pointerEvents', 'auto')
            $("#role_id_school").html('');
            $.each(data, function(key, value) {
                selected = "";
                if (value.id == 4) {
                    selected = 'selected = "selected"'
                }
                $("#role_id_school").append(
                    "<option value=" + value.id + " " + selected + ">" + value.description + "</option>"
                );
            });
        }
    });
});

function addAnswer() {
    let check = document.getElementById('multiselect-choose').value;
    if (check == 1) {
        var preview = '<div class="col-md-6" id="form-select-D">' +
            '<div class="form-group">' +
            '<label for="sel1">Đáp án</label>' +
            '<input type="text" class="form-control" maxlength="255" name="answer[]">' +
            '<div class="form-group mt-1 form-check-input-choose">' +
            '<label class="customcheck">Đáp án đúng' +
            '<input type="hidden" name="correct[]" type="checkbox" value="0">' +
            '<input class="form-check-input" name="correct[]" type="checkbox" value="1">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>';
    } else {
        var preview = '<div class="col-md-6" id="form-select-D">' +
            '<div class="form-group">' +
            '<label for="sel1">Đáp án</label>' +
            '<input type="text" class="form-control" maxlength="255" name="answer[]">' +
            '<div class="form-group mt-1 add-form-check-input-choose form-check-input-choose">' +
            '<label class="customcheck">Đáp án đúng' +
            '<input type="hidden" name="correct[]" type="checkbox" value="0">' +
            '<input class="form-check-input" name="correct[]" type="checkbox" value="1">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>';
    }
    $('#add-answer').append(preview);

    return false;
}
$("#multiselect-choose").change(function() {
    let check = document.getElementById('multiselect-choose').value;
    if (check == 0) {
        $('.form-check-input-choose').css('display', 'none')
        $('.form-check-input').prop('checked', false);
        $('.form-check-input:first-child').attr('checked', true);
        $('#form-check-input-choose-first').prop("checked", true);
    } else {
        $('.form-check-input-choose').css('display', 'block')
        $('.add-form-check-input-choose').css('display', 'block')
        $('.form-check-input').attr('checked', false);
        $('#form-check-input-choose-first').attr("checked", true);
        $('.form-check-input:first-child').attr('checked', true);
        $('.form-check-input-choose-second-div').css('pointer-events', 'none');
        $('#form-check-input-choose-second').prop("checked", true);
        $('#form-check-input-choose-second').attr("checked", true);
    }

});

function addAnswerEdit() {
    let check = document.getElementById('multiselect-choose').value;
    if (check == 1) {
        var preview = '<div class="col-md-6" id="form-select-D">' +
            '<div class="form-group">' +
            '<label for="sel1">Đáp án</label>' +
            '<input type="text" class="form-control" maxlength="255" name="answerTo[]">' +
            '<div class="form-group mt-1 form-check-input-choose">' +
            '<label class="customcheck">Đáp án đúng' +
            '<input type="hidden" name="correctTo[]" type="checkbox" value="0">' +
            '<input class="form-check-input" name="correctTo[]" type="checkbox" value="1">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>';
    } else {
        var preview = '<div class="col-md-6" id="form-select-D">' +
            '<div class="form-group">' +
            '<label for="sel1">Đáp án</label>' +
            '<input type="text" class="form-control" maxlength="255" name="answerTo[]">' +
            '<div class="form-group mt-1 add-form-check-input-choose form-check-input-choose">' +
            '<label class="customcheck">Đáp án đúng' +
            '<input type="hidden" name="correctTo[]" type="checkbox" value="0">' +
            '<input class="form-check-input" name="correctTo[]" type="checkbox" value="1">' +
            '<span class="checkmark"></span>' +
            '</label>' +
            '</div>' +
            '</div>' +
            '</div>';
    }
    $('#add-answer').append(preview);

    return false;
}