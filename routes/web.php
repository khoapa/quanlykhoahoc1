<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'namespace' => 'Admins',
    'middleware' =>'auth'
], function () {
    Route::get('/', 'HomeController@index')->name('home.index');
});
Route::group(['prefix' => 'admin','middleware' =>'auth'], function () {

    Route::group([
        'prefix' => 'school',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'SchoolController@index')->name('school_index');
        Route::get('/create', 'SchoolController@create')->name('school_create');
        Route::post('/create', 'SchoolController@store');
        Route::get('/delete/{id}', 'SchoolController@delete')->name('school_delete');
        Route::get('/edit/{id}', 'SchoolController@edit')->name('school_edit');
        Route::post('/update/{id}', 'SchoolController@update')->name('school_update');
        Route::get('/class/{id}', 'SchoolController@listClass')->name('list_class');
        Route::get('/grade/{id}', 'SchoolController@listGrade')->name('list_grade');
        Route::get('/user/{id}', 'SchoolController@listUser')->name('list_user');
        Route::get('/course/{id}', 'SchoolController@listCourse')->name('list_course');
        Route::get('class/create/{id}', 'SchoolController@createClass')->name('school_create.class')->middleware('permission:create_class');
        Route::post('class/create', 'SchoolController@storeClass')->name('school_store.class');
        Route::get('class/edit/{id}', 'SchoolController@editClass')->name('school_edit.class');
        Route::post('class/update/{id}', 'SchoolController@updateClass')->name('school_update.class');
        Route::get('/reset-password/{id}', 'SchoolController@resetPassword')->name('school.reset_password');
        Route::get('/enable-all', 'SchoolController@enableAllSchool');
        Route::get('/search-multiple-class', 'SchoolController@multipleClass');
        Route::get('/export', 'ExportFileController@exportSchool')->name('school.school.export');
        Route::get('/enable-user', 'SchoolController@enableUserOfSchool');
        Route::get('/enable-class', 'SchoolController@enableClassOfSchool');
        Route::get('/enable-course', 'SchoolController@enableCourseOfSchool');



    });
    Route::group([
        'prefix' => 'class',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'ClassController@index')->name('class_index')->middleware('permission:view_class');
        Route::get('/create', 'ClassController@create')->name('class_create')->middleware('permission:create_class');
        Route::post('/create', 'ClassController@store');
        Route::get('/delete/{id}', 'ClassController@delete')->name('class_delete')->middleware('permission:delete_class');
        Route::get('/edit/{id}', 'ClassController@edit')->name('class_edit')->middleware('permission:edit_class');
        Route::post('/update/{id}', 'ClassController@update')->name('class_update');
        Route::get('/students/{id}/{school_id?}', 'ClassController@listStudent')->name('list_student');
        Route::get('/student/create/{id}', 'ClassController@createStudent')->name('class_create.student');
        Route::post('/student/create', 'ClassController@storeStudent')->name('class_store.student');
        Route::get('/student/edit/{id}', 'ClassController@editStudent')->name('class_edit.student');
        Route::post('/student/update/{id}', 'ClassController@updateStudent')->name('class_update.student');
        Route::get('/student/delete/{id}', 'ClassController@deleteStudent')->name('class_delete.student');
        Route::get('/enable-all', 'ClassController@enableClass');
        Route::get('/enable-user', 'ClassController@enableUser');
        Route::get('/export', 'ExportFileController@exportClass')->name('class.class.export')->middleware('permission:export_class');
        Route::post('/import', 'ImportFileController@importFile')->name('class.import')->middleware('permission:import_class');
        Route::get('/update-status/{id}', 'ClassController@updateStatusClass');

    });
    Route::group([
        'prefix' => 'student',
        'namespace' => 'Admins',
    ], function () {
        // student
        Route::get('/', 'StudentController@index')->name('student.index')->middleware('permission:view_student');
        Route::get('/create', 'StudentController@create')->name('student.create')->middleware('permission:create_student');
        Route::post('student-store', 'StudentController@store')->name('student.store');
        Route::get('/edit/{id}', 'StudentController@edit')->name('student.edit')->middleware('permission:edit_student');
        Route::post('/update/{id}', 'StudentController@update')->name('student.update');
        Route::get('/destroy/{id}', 'StudentController@destroy')->name('student.destroy')->middleware('permission:delete_student');
        //ajax
        Route::get('/search-class', 'StudentController@searchClassAjax');
        Route::get('/update-status', 'StudentController@updateStatusAjax');
        Route::get('/enable-student', 'StudentController@enableStudent');
        Route::post('/import', 'ImportFileController@importFile')->name('student.import')->middleware('permission:import_student');
        Route::get('/export', 'ExportFileController@exportUser')->middleware('permission:export_student');


    });
    Route::group([
        'prefix' => 'teacher',
        'namespace' => 'Admins',
    ], function () {

        //teacher
        Route::get('/', 'TeacherController@index')->name('teacher.index')->middleware('permission:view_teacher');
        Route::get('/create', 'TeacherController@create')->name('teacher.create')->middleware('permission:create_teacher');
        Route::post('/store', 'TeacherController@store')->name('teacher.store');
        Route::get('/edit/{id}', 'TeacherController@edit')->name('teacher.edit')->middleware('permission:edit_teacher');
        Route::post('/update/{id}', 'TeacherController@update')->name('teacher.update');
        Route::get('/destroy/{id}', 'TeacherController@destroy')->name('teacher.destroy')->middleware('permission:delete_teacher');
        Route::get('/list-teacher/{id}', 'TeacherController@listTeacher');
        Route::get('/enable-teacher', 'TeacherController@enableTeacher');
        Route::post('/import', 'ImportFileController@importFile')->name('teacher.import')->middleware('permission:import_teacher');
        Route::get('/export', 'ExportFileController@exportUser')->middleware('permission:export_teacher');

    });
    Route::group([
        'prefix' => 'map',
        'namespace' => 'Admins',
    ], function () {
        Route::get('', 'MapController@index')->name('map_index');
        Route::get('/create', 'MapController@create')->name('map_create');
        Route::post('/create', 'MapController@store');
        Route::get('/delete/{id}', 'MapController@delete')->name('map_delete');
        Route::get('/edit/{id}', 'MapController@edit')->name('map_edit');
        Route::post('/update/{id}', 'MapController@update')->name('map_update');
    });
    Route::group([
        'prefix' => 'refer',
        'namespace' => 'Admins',
    ], function () {

        //refer
        Route::get('/', 'ReferController@index')->name('refer.index');
        Route::get('/create', 'ReferController@create')->name('refer.create');
        Route::post('/store', 'ReferController@store')->name('refer.store');
        Route::get('/edit/{id}', 'ReferController@edit')->name('refer.edit');
        Route::post('/update/{id}', 'ReferController@update')->name('refer.update');
        Route::get('/destroy/{id}', 'ReferController@destroy')->name('refer.destroy');
    });
    Route::group([
        'prefix' => 'grades',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'GradeController@index')->name('grade_index')->middleware('permission:view_grade');
        Route::get('/create', 'GradeController@create')->name('grade_create')->middleware('permission:create_grade');
        Route::post('/create', 'GradeController@store');
        Route::get('/delete/{id}', 'GradeController@delete')->name('grade_delete')->middleware('permission:delete_grade');
        Route::get('/edit/{id}', 'GradeController@edit')->name('grade_edit')->middleware('permission:edit_grade');
        Route::post('/update/{id}', 'GradeController@update')->name('grade_update');
        Route::get('/list-grade/{id}', 'GradeController@listGradeOfSchool');
    });
    Route::group([
        'prefix' => 'map/category',
        'namespace' => 'Admins',
    ], function () {
        Route::get('', 'MapCategoryController@index')->name('map_category_index');
        Route::get('/create', 'MapCategoryController@create')->name('map_category_create');
        Route::post('/create', 'MapCategoryController@store');
        Route::get('/delete/{id}', 'MapCategoryController@delete')->name('map_category_delete');
        Route::get('/edit/{id}', 'MapCategoryController@edit')->name('map_category_edit');
        Route::post('/update/{id}', 'MapCategoryController@update')->name('map_category_update');
    });

    Route::group([
        'prefix' => 'news',
        'namespace' => 'Admins',
    ], function () {
        Route::get('','NewsController@index')->name('news_index');
        Route::get('/create','NewsController@create')->name('news_create');
        Route::post('/create','NewsController@store');
        Route::get('/delete/{id}','NewsController@delete')->name('news_delete');
        Route::get('/edit/{id}','NewsController@edit')->name('news_edit');
        Route::post('/update/{id}','NewsController@update')->name('news_update');
        Route::get('/update-status', 'NewsController@updateStatusAjax');
        Route::get('/enable-all', 'NewsController@enableNews');
    });

    Route::group([
        'prefix' => 'news/category',
        'namespace' => 'Admins',
    ], function () {
        Route::get('', 'NewsCategoryController@index')->name('news_category_index');
        Route::get('/create', 'NewsCategoryController@create')->name('news_category_create');
        Route::post('/create', 'NewsCategoryController@store');
        Route::get('/delete/{id}', 'NewsCategoryController@delete')->name('news_category_delete');
        Route::get('/edit/{id}', 'NewsCategoryController@edit')->name('news_category_edit');
        Route::post('/update/{id}', 'NewsCategoryController@update')->name('news_category_update');
    });

    Route::group([
        'prefix' => 'supports',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/', 'SupportsController@index')->name('supports.index');
        Route::get('/create', 'SupportsController@create')->name('supports.create');
        Route::post('/store', 'SupportsController@store')->name('supports.store');
        Route::get('/edit/{id}', 'SupportsController@edit')->name('supports.edit');
        Route::post('/update/{id}', 'SupportsController@update')->name('supports.update');
        Route::get('/destroy/{id}', 'SupportsController@destroy')->name('supports.destroy');
    });
    Route::group([
        'prefix' => 'transcript',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/', 'TranscriptController@index')->name('transcript.index')->middleware('permission:view_mark');
        Route::get('search-user','TranscriptController@searchUser')->name('transcript.user');
        Route::get('/delete-user/{id}','TranscriptController@deleteUser')->name('transcript.user.delete')->middleware('permission:delete_mark');
        Route::get('/mark/{user_id}/{course_user_id}', 'TranscriptController@mark')->name('transcript.mark')->middleware('permission:create_mark');
        Route::post('/mark/store/{id}', 'TranscriptController@markStore')->name('transcript.mark.store');
        Route::get('/search-class', 'TranscriptController@searchClassAjax');
        Route::get('/search-course', 'TranscriptController@searchCourseAjax');
    });
    Route::group([
        'prefix' => 'school/course',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/', 'SchoolCourseController@index')->name('school_course.index')->middleware('permission:view_study_program');
        Route::get('/create/{id}', 'SchoolCourseController@create')->name('school_course.create')->middleware('permission:create_study_program');
        Route::post('/store/{id}', 'SchoolCourseController@store')->name('school_course.store');
        Route::get('/edit/{id}', 'SchoolCourseController@edit')->name('school_course.edit')->middleware('permission:edit_school_course');
        Route::post('/update/{id}', 'SchoolCourseController@update')->name('school_course.update');
        Route::get('/destroy/{school_id}/{id}', 'SchoolCourseController@destroy')->name('school_course.destroy')->middleware('permission:delete_school_course');

        Route::get('/class-create/{id}', 'SchoolCourseController@createClass')->name('school_course.createClass');
        Route::post('/class-store/{id}', 'SchoolCourseController@storeClass')->name('school_course.storeClass');
        Route::get('/class-destroy/{id}', 'SchoolCourseController@destroyClass')->name('school_course.destroyClass');
        Route::get('/export/course-school', 'ExportFileController@exportCourseSchool')->name('school_course.school_course.export')->middleware('permission:export_school_course');
    });
    Route::group([
        'prefix' => 'Web/service',
        'namespace' => 'Admins',
    ], function () {

        Route::get('/edit', 'ServiceController@edit')->name('service.edit');
        Route::post('/update/{id}', 'ServiceController@update')->name('service.update');
      
    });

    Route::group([
        'prefix' => 'policy',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'PolicyController@index')->name('policy.index');
        Route::post('/update/{id}', 'PolicyController@update')->name('policy.update');
    });

    Route::group([
        'prefix' => 'user',
        'namespace' => 'Admins',
    ], function () {
        // user
        Route::get('/', 'UserController@index')->name('user.index')->middleware('permission:view_user');
        Route::get('/create', 'UserController@create')->name('user.create')->middleware('permission:create_user');
        Route::post('user-store', 'UserController@store')->name('user.store');
        Route::get('/edit/{id}', 'UserController@edit')->name('user.edit')->middleware('permission:edit_user');
        Route::post('/update/{id}', 'UserController@update')->name('user.update');
        Route::get('/destroy/{id}', 'UserController@destroy')->name('user.destroy')->middleware('permission:delete_user');
        Route::post('/import', 'ImportFileController@importFile')->name('user.import')->middleware('permission:import_user');
        Route::get('/export', 'ExportFileController@exportUser')->middleware('permission:export_user');
        Route::get('/reset-password/{id}', 'UserController@resetPassword')->name('user.reset_password');
        Route::get('/enable-all', 'UserController@enableUser');
    });
    
    Route::group([
        'prefix' => 'profile',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/{id}', 'ProfileUserController@indexTeacher')->name('profile_user.index');
        Route::get('/change-password/{id}', 'ProfileUserController@changePassword')->name('profile_user.change_password');
        Route::post('/change-password/{id}', 'ProfileUserController@storePassword')->name('profile_user.store_password');
        Route::post('/change-avatar/{id}', 'ProfileUserController@storeAvatar')->name('profile_user.store_avatar');
    
    });
    Route::group([
        'prefix' => 'course_theory',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'CourseTheoryController@index')->name('course_theory.index')->middleware('permission:view_course_theory');
        Route::get('/create', 'CourseTheoryController@create')->name('course_theory.create')->middleware('permission:create_course_theory');
        Route::post('/create', 'CourseTheoryController@store')->name('course_theory.store');
        Route::get('/edit/{id}', 'CourseTheoryController@edit')->name('course_theory.edit')->middleware('permission:edit_course_theory');
        Route::post('/update/{id}', 'CourseTheoryController@update')->name('course_theory.update');
        Route::get('/destroy/{id}', 'CourseTheoryController@destroy')->name('course_theory.destroy')->middleware('permission:delete_course_theory');
        Route::get('/update-status', 'CourseTheoryController@addStatus');
        Route::get('/list-course/{id}', 'CourseTheoryController@listCourse');
        Route::get('/enable-all', 'CourseTheoryController@enableCourseTheory');

    });
    Route::group([
        'prefix' => 'notification',
        'namespace' => 'Admins',
    ], function () {
        Route::get('/', 'NotificationController@index')->name('notification.index')->middleware('permission:send_nottification');
        Route::get('/show-user', 'NotificationController@showUser');
        Route::post('/push-notification-general', 'NotificationController@pushNotificationGeneral')->name('push.notification.general');
    });

});
