<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1'], function () {
    Route::post('/user/signin', 'UserController@signIn')->name('api.user.signin');
    Route::post('/user/signup', 'UserController@signUp')->name('api.user.signup');
    Route::post('/user/forgetpasss', 'UserController@forgetPass');
    Route::post('/verify', 'UserController@actionVerifyPhone');
    Route::post('/change-pass/verify', 'UserController@changePassVerifyPhone');

    Route::group(['prefix' => 'list'], function () {
        Route::post('/school', 'SchoolController@index');
        Route::post('/class', 'ClassController@index');
    });

    Route::group(['prefix' => 'support'], function () {
        Route::post('/', 'SupportController@index');
        Route::get('/{id}', 'SupportController@show');
    });
    Route::group(['prefix' => 'chat'], function () {
        Route::post('/create', 'ChatController@store');
       // Route::get('/{id}', 'SupportController@show');
    });

});
Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1','middleware' =>'api.auth'], function () {

    Route::post('/images/upload', 'ImagesController@uploadImage');
    Route::group(['prefix' => 'user'], function () {
        Route::get('/select-type', 'UserController@listUsertype');
        Route::get('/me', 'UserController@me');
        Route::post('/changepass', 'UserController@changePass');
        Route::post('/update', 'UserController@update');
        Route::post('/device-token', 'UserController@addDeviceToken');
        Route::post('/log-out', 'UserController@logout');
        Route::put('/update/{id}', 'UserController@updateUser');
        Route::post('/', 'UserController@index');
        Route::get('/{id}', 'UserController@show');
        Route::get('/reset-pass/{id}', 'UserController@resetPass');
    });
    Route::group(['prefix' => 'course'], function () {
        Route::get('/category', 'CategoryCourseController@index');
        Route::post('/list', 'CourseController@index');
        Route::post('/list-offer', 'CourseController@listOffer');
        Route::post('/list-new', 'CourseController@listNew');
        Route::get('/{id}', 'CourseController@show');
        Route::post('/list-all', 'CourseController@listAll');
        Route::get('/join/{id}', 'CourseController@join');
        Route::get('/favourite/{id}', 'CourseController@favourite');
        Route::post('/history', 'CourseController@history');
        Route::post('/list-favourite', 'CourseController@listFavourite');
        Route::post('/reply/{id}', 'CourseController@reply');
        Route::post('/result', 'CourseController@result');
        Route::post('/list-admin', 'CourseController@listCourseAdmin');
        Route::put('/update-status/{id}','CourseController@updateStatusCourse');
        Route::get('/mark/{id}', 'CourseController@showMark');
        Route::post('/mark-store/{id}', 'CourseController@mark');
    });
    Route::group(['prefix' => 'refer'], function () {
        Route::post('/list', 'ReferController@index');
        Route::get('/{id}', 'ReferController@show');
    });
    Route::group(['prefix' => 'news'], function () {
        Route::post('/list', 'NewsController@index');
        Route::post('/allpage', 'NewsController@allPage');
        Route::get('/{id}', 'NewsController@show');
    });
    Route::group(['prefix' => 'map'], function () {
        Route::get('/category', 'CategoryMapController@index');
        Route::post('/list', 'MapController@index');
        Route::get('/{id}', 'MapController@show');
        Route::post('/update', 'CategoryMapController@update');
        Route::post('/update-location','MapController@addTheLastPosition');
    });
    Route::group(['prefix' => 'home'], function () {
        Route::get('/', 'HomeController@getHome');
    });
    Route::group(['prefix' => 'conversion'], function () {
        Route::post('/list', 'ConversationController@index');
        Route::post('/list-message/{id}', 'ConversationController@list');
        Route::post('/create-new', 'ConversationController@createConversation');
        Route::post('/create', 'ConversationController@store');
        Route::put('/update/{id}', 'ConversationController@update');
        Route::delete('delete/{id}', 'ConversationController@delete');
        Route::delete('delete-conversion/{id}', 'ConversationController@deleteConversion');
    });
    Route::group(['prefix' => 'new'], function () {
        Route::get('/category', 'CategoryNewController@index');
    });
    
    Route::group(['prefix' => 'notifications'], function(){
        Route::get('/', 'NotificationController@notifcations');
        Route::post('/list', 'NotificationController@index');
        Route::get('/unread', 'NotificationController@unread');
        Route::post('/read-all', 'NotificationController@readAll');
        Route::get('/read-detail/{id}', 'NotificationController@readDetail');
        Route::post('/update-status', 'NotificationController@userUpdateGetNotification');
    });
    Route::group(['prefix' => 'course-theory'], function () {
        Route::post('/list', 'CourseTheoryController@index');
        Route::post('/list-offer', 'CourseTheoryController@listOffer');
        Route::post('/list-new', 'CourseTheoryController@listNew');
        Route::get('/{id}', 'CourseTheoryController@show');
        Route::post('/list-all', 'CourseTheoryController@listAll');
    });
    Route::group(['prefix' => 'student'], function () {
        Route::post('/', 'StudentController@index');
        Route::post('/update-status', 'StudentController@updateStatusStudent');
        Route::get('/{id}', 'StudentController@show');
        Route::put('/{id}', 'StudentController@updateStatusStudentFollowClass');
    });
    Route::group(['prefix' => 'transcript'], function () {
        Route::post('/', 'TranscriptController@index');
    });
    Route::group(['prefix' => 'statistic'], function () {
        Route::post('/', 'StatisticController@index');
    });
    Route::group(['prefix' => 'roles'], function () {
        Route::get('/list', 'RoleController@listSelect');
      });
      Route::group(['prefix' => 'school/course'], function () {
        Route::post('/', 'SchoolCourseController@index');
    });
    Route::group(['prefix' => 'permission'], function () {
        Route::get('/', 'PermissionController@index');
        Route::get('/list/{id}', 'PermissionController@list');
        Route::post('/update/{id}', 'PermissionController@update');
        Route::post('/check-all/{id}', 'PermissionController@checkAll');
    });
});
